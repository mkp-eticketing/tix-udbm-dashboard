import apiHelper from "../../utils/pariwisata/axios-cash-id-billing";

const getDataCash = (data) => apiHelper.POST_AUTH("/idbilling_bjtg/public/reporting/v1/cash/list", data);
const getDataCardCash = (data) => apiHelper.POST_AUTH("/idbilling_bjtg/public/reporting/v1/cash/card", data);

export { getDataCash, getDataCardCash };
