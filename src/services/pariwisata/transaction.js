import apiHelper from "../../utils/pariwisata/axios";

const getTrxByProductPayment = (data) => apiHelper.POST_AUTH("/reporting-public/list/trx-by-product-paymentmethod", data)
const getTrxByProductUser = (data) => apiHelper.POST_AUTH("/reporting-public/list/trx-by-product-paymentuser", data)
const getTrxByProduct = (data) => apiHelper.POST_AUTH("/reporting-public/list/trx-by-product", data)
const getPaymentList = (data) => apiHelper.GET_AUTH("/payment-category-core/list", data)
const getProductList = (data) => apiHelper.POST_AUTH('/product-core/list/index', data)
const getTrxList = (data) => apiHelper.POST_AUTH('/reporting-public/list/trx-udb', data)
const getTrxDetail = (data) => apiHelper.POST_AUTH('/reporting-public/list/detail-trx-udb', data)
const getTrxByUser = (data) => apiHelper.POST_AUTH('/reporting-public/list/trx-by-user', data)
const updateInvoiceVendorRef = (data) => apiHelper.POST_AUTH("/bookingv2/update-inv-vendor-ref", data)
export {
  getTrxByProductPayment,
  getTrxByProductUser,
  getTrxByProduct,
  getPaymentList,
  getProductList,
  getTrxDetail,
  getTrxList,
  getTrxByUser,
  updateInvoiceVendorRef
}