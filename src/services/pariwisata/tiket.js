import apiHelper from "../../utils/pariwisata/axios";

const getTiketSummary = (data) => apiHelper.POST_AUTH("/reporting-public/ticketcore/summary", data);
const getTiketSummaryByProductName = (data) => apiHelper.POST_AUTH("/reporting-public/ticketcore/summary-by-productname", data);
const getTiketDetail = (data) => apiHelper.POST_AUTH("/reporting-public/ticketcore/detail-ticket", data);
const getTerminalData = (data) => apiHelper.POST_AUTH("/terminal-public/list-by-index", data);
const getTiketSummaryV2 = (data) => apiHelper.POST("/reporting-public/ticketcore/summary-v2", data);
const getTiketSummaryByProductNameV2 = (data) => apiHelper.POST("/reporting-public/ticketcore/summary-by-productname-v2", data);
const getTiketDetailV2 = (data) => apiHelper.POST("/reporting-public/ticketcore/detail-ticket-v2", data);
const getTerminalDataV2 = (data) => apiHelper.POST_AUTH("/terminal-public/list-by-index-v2", data);

export { getTiketSummary, getTiketSummaryByProductName, getTiketDetail, getTerminalData, getTiketSummaryV2, getTiketSummaryByProductNameV2, getTiketDetailV2, getTerminalDataV2 };
