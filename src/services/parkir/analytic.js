import apiHelper from "../../utils/parkir/axios";
const getChartJenisKendaraan = (data) => apiHelper.POST_TRX_WITH_AUTH("/ops/chart-jenis-kendaraan", data)
const getChartTrafikKendaraan = (data) => apiHelper.POST_TRX_WITH_AUTH("/ops/chart-traffic", data)
const getChartTrafficIntellegent = (data) => apiHelper.POST_TRX_WITH_AUTH("/ops/chart-intelligence-trx-forOps", data)

export {
    getChartJenisKendaraan,
    getChartTrafficIntellegent,
    getChartTrafikKendaraan
}
