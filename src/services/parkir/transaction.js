import apiHelper from "../../utils/parkir/axios";
const getTransactionShiftPeriod = (data) => apiHelper.POST_TRX("/trx-without-auth/list", data)
const getExcelTransactionShiftPeriod = (data) => apiHelper.POST_TRX("/trx-without-auth/export-shift-period", data, {}, "blob")
const getSummaryTransactionShiftPeriod = (data) => apiHelper.POST_TRX("/trx-without-auth/summary", data)
const getTransactionDetailById = (id) => apiHelper.GET_TRX("/trx-without-auth/detail/" + id)
const getSummaryTransaction = (data) => apiHelper.POST_TRX_WITH_AUTH("/ops/summary-trx", data)

export {
    getTransactionShiftPeriod,
    getExcelTransactionShiftPeriod,
    getSummaryTransactionShiftPeriod,
    getTransactionDetailById,
    getSummaryTransaction
}