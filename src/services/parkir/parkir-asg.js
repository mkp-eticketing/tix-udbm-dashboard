import apiHelper from "../../utils/parkir/axios";
const getChartJumlahKendaraan = (data) => apiHelper.POST_TRX_ASG("/ops/summary-dashboard-ops", data)
const getTableDurasiKendaraan = (data) => apiHelper.POST_TRX_ASG("/ops/list-data-trx-duration", data)
const getTableJumlahKendaraanDanPendapatan = (data) => apiHelper.POST_TRX_ASG("/ops/get-summary-parking-opsv2", data)

const getExcelExportDuration = (data) => apiHelper.POST_TRX_ASG("/ops/export-duration", data, {}, "blob")
const getExcelExportMonitoring = (data) => apiHelper.POST_TRX_ASG("/ops/export-report-monitoring", data, {}, "blob")
const getExcelExportSummary = (data) => apiHelper.POST_TRX_ASG("/ops/export-summary", data, {}, "blob")
const getDetailMonitor = (data) => apiHelper.GET_TRX_ASG(`/ops/detail/${data.id}`)

// CREATE MEMBER APPROVAL
const getTableCreateMember = (data) => apiHelper.POST_PG_ASG("/approval-member/list", data)
const createMember = (data) => apiHelper.POST_PG_ASG("/approval-member/add", data)
const updateMember = (data) => apiHelper.POST_PG_ASG("/approval-member/status-update", data)
const inquiryMember = (data) => apiHelper.POST_PG_ASG("/approval-member/inquiry", data)
const confirmMember = (data) => apiHelper.POST_PG_ASG("/approval-member/confirm", data)
const getExcelExportMember = (data) => apiHelper.POST_PG_ASG("/approval-member/export-excel", data, {}, "blob")
const getTableReportMember = (data) => apiHelper.POST_TRX_ASG("/ops/report-membership", data)

const getExcelExportReport = (data) => apiHelper.POST_TRX_ASG("/ops/report-membership-excel", data, {}, "blob")


export {
    getChartJumlahKendaraan,
    getTableDurasiKendaraan,
    getTableJumlahKendaraanDanPendapatan,
    getExcelExportDuration,
    getExcelExportSummary,
    getDetailMonitor,
    getTableCreateMember,
    createMember,
    updateMember,
    inquiryMember,
    confirmMember,
    getExcelExportMember,
    getTableReportMember,
    getExcelExportReport,
    getExcelExportMonitoring
}