import apiHelper from "../../utils/b2b/axios";

const getMerchantList = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/merchant/list"
})
const getMerchantListRecord = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/merchant/list/record-data"
})
const addMerchant = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {
        "Content-Type": "multipart/form-data",
    },
    path: "/api/v1/public/merchant/add"
})
const updateMerchant = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {
        "Content-Type": "multipart/form-data",
    },
    path: "/api/v1/public/merchant/update"
})
const getMerchantByID = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/merchant/findbyid"
})
const deleteMerchantByID = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/merchant/delete"
})
export {
    getMerchantList,
    getMerchantListRecord,
    addMerchant,
    updateMerchant,
    getMerchantByID,
    deleteMerchantByID
}