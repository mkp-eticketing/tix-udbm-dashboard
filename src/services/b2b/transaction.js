import apiHelper from "../../utils/b2b/axios";

const getTransactionList = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/transaction/ticket/list"
})
const getTransactionListRecord = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/transaction/ticket/list/record-data"
})
const getTransactionSummary = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/transaction/ticket/summary"
})
const getTransactionReport = (data) => apiHelper.fetcher({
    method: "POST",
    data: data,
    headers: {},
    path: "/api/v1/public/transaction/ticket/report"
})
export {
    getTransactionList,
    getTransactionListRecord,
    getTransactionSummary,
    getTransactionReport
}