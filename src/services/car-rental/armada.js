import apiHelper from "../../utils/car-rental/axios";

const getBrand = (data) => apiHelper.POST(`/brands/get`, data)
const getCarProvider = (query, data) => apiHelper.POST(`/car-providers/get?size=${query.size}&page=${query.page}`, data)
const getCarProviderById = (data) => apiHelper.GET(`/car-providers/${data}`)
const getCarTypeProvider = (data) => apiHelper.POST(`/car-type-providers/get`, data)
const editCarProvider = (id, data) => apiHelper.PUT(`/car-providers/${id}`, data)
const addCarProvider = (data) => apiHelper.POST(`/car-providers`, data)
const deleteCarProvider = (id) => apiHelper.DELETE(`/car-providers/${id}`)
const getCarProviderByMerchant = (data) => apiHelper.POST(`/car-providers/get`, data)

export {
  getBrand,
  getCarProvider,
  getCarTypeProvider,
  editCarProvider,
  getCarProviderById,
  addCarProvider,
  deleteCarProvider,
  getCarProviderByMerchant
}