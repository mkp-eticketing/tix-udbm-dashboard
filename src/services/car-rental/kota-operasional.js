import apiHelper from "../../utils/car-rental/axios";
import apiHelper2 from "../../utils/car-rental/axios-location";

const getPlaceCorporate = (query, data) => apiHelper.POST(`/corporate-province-cities/get?size=${query.size}&page=${query.page}`, data)
const getProvinceCorporate = (data) => apiHelper.POST(`/corporate-province-cities/get`, data)
const addPlaceCorporate = (data) => apiHelper.POST(`/corporate-province-cities`, data)
const deletePlaceCorporate = (id) => apiHelper.DELETE(`/corporate-province-cities/${id}`)

const getCountry = () => apiHelper2.GET(`/public/province-city/country`)
const getCity = (data) => apiHelper2.POST(`/public/province-city/city/index`, data)

export {
  getProvinceCorporate,
  getPlaceCorporate,
  addPlaceCorporate,
  deletePlaceCorporate,
  getCountry,
  getCity
}