import apiHelper from "../../utils/car-rental/axios";

const getDrivers = (query, data) => apiHelper.POST(`/drivers/get?size=${query.size}&page=${query.page}`, data)
const getDriversByMerchant = (data) => apiHelper.POST(`/drivers/get`, data)
const getDriversById = (data) => apiHelper.GET(`/drivers/${data}`)
const addDrivers = (data) => apiHelper.POST(`/drivers`, data)
const editDrivers = (id, data) => apiHelper.PUT(`/drivers/${id}`, data)
const deleteDrivers = (id) => apiHelper.DELETE(`/drivers/${id}`)

export {
  getDrivers,
  getDriversById,
  addDrivers,
  editDrivers,
  deleteDrivers,
  getDriversByMerchant
}