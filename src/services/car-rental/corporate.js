import apiHelper from "../../utils/car-rental/axios";

const getCorporate = (data) => apiHelper.POST("/corporates/get", data)

const devCorporate = [
  {
      "id": 1,
      "createdAt": "2024-05-28T17:00:08.590+07:00",
      "updatedAt": "2024-05-28T17:00:08.590+07:00",
      "corporateCode": "CO1",
      "corporateName": "MKP",
      "email": null,
      "phoneNumber": null,
      "address": null
  },
  {
      "id": 10,
      "createdAt": "2024-06-03T14:11:43.097+07:00",
      "updatedAt": "2024-06-03T14:11:43.097+07:00",
      "corporateCode": "CO3",
      "corporateName": "Super Travel",
      "ouCode": "CC1111",
      "email": "super-travel@gmail.com",
      "phoneNumber": "0855555",
      "address": "Surabaya"
  },
  {
      "id": 2,
      "createdAt": "2024-05-28T17:00:08.856+07:00",
      "updatedAt": "2024-06-03T14:12:30.161+07:00",
      "corporateCode": "CO2",
      "corporateName": "Top Travel",
      "ouCode": "BB1111",
      "email": "top-travel@gmail.com",
      "phoneNumber": "0855555",
      "address": "Semarang",
      "corporateParentId": 1
  }
]

export {
  getCorporate,
  devCorporate
}