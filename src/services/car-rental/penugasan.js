import apiHelper from "../../utils/car-rental/axios";

const getDriverSchedule = (query) => apiHelper.GET(`/driver-schedules/${query.driver}/${query.tanggal}`)
const addDriverSchedule = (data) => apiHelper.POST(`/driver-schedules`, data)
const editDriverSchedule = (id, data) => apiHelper.PUT(`/driver-schedules/${id}`, data)
const deleteDriverSchedule = (id) => apiHelper.DELETE(`/driver-schedules/${id}`)

export {
  getDriverSchedule,
  addDriverSchedule,
  editDriverSchedule,
  deleteDriverSchedule
}