import apiHelper from "../../utils/car-rental/axios";

const getVehiclePrice = (query) => apiHelper.GET(`/car-type-pricings//${query.vehicle}/${query.tanggal}`)
const addVehiclePrice = (data) => apiHelper.POST(`/car-type-pricings`, data)
const editVehiclePrice = (id, data) => apiHelper.PUT(`/car-type-pricings/${id}`, data)
const deleteVehiclePrice = (id) => apiHelper.DELETE(`/car-type-pricings/${id}`)

export {
  getVehiclePrice,
  addVehiclePrice,
  editVehiclePrice,
  deleteVehiclePrice
}