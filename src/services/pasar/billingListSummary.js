import apiHelper from "../../utils/pasar/axios";

const getBillingList = (data) =>
  apiHelper.POST("/public/reporting/billing", data);
const getBillingTypeList = (data) =>
  apiHelper.POST("/public/filter/billing-type", data);

// billing summary api endpoint
const getBillingSummaryList = (data) =>
  apiHelper.POST("/public/reporting/billing-summary", data);
const getBillingSummaryMetadata = (data) =>
  apiHelper.POST("/public/record-data/billing-summary", data);
const getBillingSummaryCard = (data) =>
  apiHelper.POST("/public/card/billing-summary", data);

export {
  getBillingList,
  getBillingTypeList,
  getBillingSummaryList,
  getBillingSummaryMetadata,
  getBillingSummaryCard,
};
