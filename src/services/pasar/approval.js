import apiHelper from "../../utils/pasar/axios";

// approval page
const getApprovalList = (data) =>
  apiHelper.POST("/public/reporting/approval-remove", data);
const getApprovalMetadata = (data) =>
  apiHelper.POST("/public/record-data/approval-remove", data);
const editApproval = (data) =>
  apiHelper.POST("/public/edit/approval-remove", data);

// approval detail
const getApprovalDetail = (data) =>
  apiHelper.POST("/public/reporting/approval-detail", data);
const getApprovalDetailMetadata = (data) =>
  apiHelper.POST("/public/record-data/approval-detail", data);
const getSummaryApproval = (data) => 
  apiHelper.POST("/public/card/approval-detail", data)

export {
  getApprovalList,
  getApprovalMetadata,
  editApproval,
  getApprovalDetail,
  getApprovalDetailMetadata,
  getSummaryApproval
};
