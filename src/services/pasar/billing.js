import apiHelper from "../../utils/pasar/axios";

const getBillingRemoveList = (data) =>
  apiHelper.POST("/public/reporting/billing-remove", data);
const getBillingRemoveMetadata = (data) =>
  apiHelper.POST("/public/record-data/billing-remove", data);
const getInvoiceList = (data) =>
  apiHelper.POST("/public/filter/billing-profile", data);
const getReasonPermit = (data) =>
  apiHelper.POST("/public/filter/reason-permit", data);
const addBulkSession = (data) =>
  apiHelper.POST("/public/add/billing-session", data);
const addApprovalRemove = (data) =>
  apiHelper.POST("/public/add/approval-remove", data);

export {
  getBillingRemoveList,
  getBillingRemoveMetadata,
  getInvoiceList,
  getReasonPermit,
  addBulkSession,
  addApprovalRemove,
};
