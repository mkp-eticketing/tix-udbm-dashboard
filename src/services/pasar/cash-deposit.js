import apiHelper from "../../utils/pasar/axios";

// cash deposit page
const getCashDepositList = (data) =>
  apiHelper.POST("/public/reporting/bjtg-cash-deposit", data);
const getCashDepositMetadata = (data) =>
  apiHelper.POST("/public/record-data/bjtg-cash-deposit", data);
const getSummaryCashDeposit = (data) =>
  apiHelper.POST("/public/card/bjtg-cash-deposit", data);

// cash deposit detail
const getCashDepositDetail = (data) =>
  apiHelper.POST("/public/reporting/bjtg-cash-deposit-detail", data);
const getCashDepositDetailMetadata = (data) =>
  apiHelper.POST("/public/record-data/bjtg-cash-deposit-detail", data);
const getSummaryCashDepositDetail = (data) =>
  apiHelper.POST("/public/card/bjtg-cash-deposit-detail", data);

export {
  getCashDepositList,
  getCashDepositMetadata,
  getSummaryCashDeposit,
  getCashDepositDetail,
  getCashDepositDetailMetadata,
  getSummaryCashDepositDetail,
};
