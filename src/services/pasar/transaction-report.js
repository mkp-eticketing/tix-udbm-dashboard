import apiHelper from "../../utils/pasar/axios";

const getTransactionList = (data) =>
  apiHelper.POST("/public/reporting/transaction", data);
const getTransactionMetadata = (data) =>
  apiHelper.POST("/public/record-data/transaction", data);
const getDistrictList = (data) =>
  apiHelper.POST("/public/filter/district", data);
const getUserList = (data) => apiHelper.POST("/public/filter/users", data);
const getSummaryTransaction = (data) =>
  apiHelper.POST("/public/card/transaction", data);

export {
  getTransactionList,
  getTransactionMetadata,
  getDistrictList,
  getUserList,
  getSummaryTransaction,
};
