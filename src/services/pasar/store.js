import apiHelper from "../../utils/pasar/axios";

const getStoreDetailList = (data) => apiHelper.POST("/public/reporting/store", data)
const getStoreDetailMetadata = (data) => apiHelper.POST("/public/record-data/store", data)

export {
    getStoreDetailList,
    getStoreDetailMetadata,
}