import apiHelper from "../../utils/pasar/axios";

const getUserAccountList = (data) =>
  apiHelper.POST("/public/reporting/account", data);
const getUserAccountMetadata = (data) =>
  apiHelper.POST("/public/record-data/account", data);

export { getUserAccountList, getUserAccountMetadata };
