import { Stack, Typography } from "@mui/material";
import React, { useState } from "react";

const CustomTruncate = ({ value, length }) => {
    const [seeMore, setSeeMore] = useState(false);

    if (value.length > length) {
        return (
            <Stack>
                <span>{seeMore ? value : `${value.slice(0, length)}...`}</span>
                <Typography
                    sx={{ cursor: "pointer", color: "#3875CA" }}
                    onClick={() => setSeeMore(!seeMore)}
                >
                    {seeMore ? "See Less..." : "See More..."}
                </Typography>
            </Stack>
        );
    }

    return <span>{value}</span>;
};

export { CustomTruncate };
