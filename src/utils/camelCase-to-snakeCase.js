/**
 * Converts a camelCase string to snake_case.
 * @param {string} str - The camelCase string to convert.
 * @return {string} - The converted snake_case string.
 */
export function camelToSnake(str) {
    return str.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
  }
  