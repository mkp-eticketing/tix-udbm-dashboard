import axios from 'axios';
import { Base64 } from "js-base64";

const modeDevelop = false;
const modeDevelopASG = false;

const instance = axios.create({
    baseURL: modeDevelop ? "https://sandbox.mkpmobile.com/api/parking/master" : "https://api.mkpmobile.com/parking/master/master",
});
const trx_instance = axios.create({
    baseURL: modeDevelop ? "https://sandbox.mkpmobile.com/api/parking/trx" : "https://api.mkpmobile.com/parking/trx/trx",
});

const endpoint_asg = modeDevelopASG ? "https://sandbox.mkpmobile.com/api/parking/trx" : "https://api.mkpmobile.com/parking/trx/trx";

const dashboard_trx_asg_instance = axios.create({
    baseURL: `${endpoint_asg}`
});

const dashboard_pg_asg_instance = axios.create({
    baseURL: modeDevelopASG ? "https://sandbox.mkpmobile.com/api/parking/pg" : "https://api.mkpmobile.com/parking/mpos",
});

const username = "mkpmobile";
const password = "MKPmobile123456@";
instance.defaults.headers.common['Content-Type'] = "application/json";


const POST = async (path = "", data, headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.post(path, data, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const POST_TRX = async (path = "", data, headers = {}, responseType = "json") => {
    let promise = new Promise((resolve, reject) => {
        trx_instance.post(path, data, {
            headers: {
                ...headers
            },
            responseType: responseType
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const POST_WITH_AUTH = async (path = "", data, headers = {}) => {
    let authString = username + ":" + password
    let promise = new Promise((resolve, reject) => {
        instance.post(path, data, {
            headers: {
                ...headers,
                'Authorization': "Basic " + Base64.btoa(authString)
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const POST_TRX_WITH_AUTH = async (path = "", data, headers = {}) => {
    let authString = username + ":" + password
    let promise = new Promise((resolve, reject) => {
        trx_instance.post(path, data, {
            headers: {
                ...headers,
                'Authorization': "Basic " + Base64.btoa(authString)
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const GET = async (path = "", data, headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.get(path, data, {
            headers: {
                ...headers,
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const GET_WITH_AUTH = async (path = "",  headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        let authString = username + ":" + password
        instance.get(path, {
            headers: {
                ...headers,
                'Authorization': "Basic " + Base64.btoa(authString)
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const GET_TRX = async (path = "", data, headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        trx_instance.get(path, data, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const GET_TRX_ASG = async (path = "", headers = {}) => {
    let authString = username + ":" + password
    let promise = new Promise((resolve, reject) => {
        dashboard_trx_asg_instance.get(path, {
            headers: {
                ...headers,
                'Authorization': "Basic " + Base64.btoa(authString)
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const POST_TRX_ASG = async (path = "", data, headers = {}, responseType = "json") => {
    let authString = username + ":" + password
    let promise = new Promise((resolve, reject) => {
        dashboard_trx_asg_instance.post(path, data, {
            headers: {
                ...headers,
                'Authorization': "Basic " + Base64.btoa(authString)
            },
            responseType: responseType
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const POST_PG_ASG = async (path = "", data, headers = {}, responseType = "json") => {
    let authString = username + ":" + password
    let promise = new Promise((resolve, reject) => {
        dashboard_pg_asg_instance.post(path, data, {
            headers: {
                ...headers,
                'Authorization': "Basic " + Base64.btoa(authString)
            },
            responseType: responseType
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

export default {
    POST,
    POST_TRX,
    GET,
    GET_TRX,
    GET_WITH_AUTH,
    POST_TRX_WITH_AUTH,
    POST_WITH_AUTH,
    GET_TRX_ASG,
    POST_TRX_ASG,
    POST_PG_ASG,
};