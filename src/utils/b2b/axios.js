import axios from 'axios';
import { Base64 } from "js-base64";

// const instance = axios.create({
//     baseURL: "https://sandbox.mkpmobile.com/api/b2binvmaster",
// });

const instance = axios.create({
    baseURL: "https://apipayment.mkpmobile.com/b2binvmaster",
});

instance.defaults.headers.common['Content-Type'] = "application/json";

const username = "mkpmobile";
const password = "mkpmobile123";

const fetcher = async (
    {
        method = "GET",
        path = "",
        data,
        headers = {}
    }) => {
    let config = {}
    let authString = username + ":" + password
    config = {
        method: method,
        url: path,
        headers: {
            ...headers,
            'Authorization': "Basic " + Base64.btoa(authString)
        }
    }
    if (method === "GET") {
        config.params = data
    } else if (method === "POST") {
        config.data = data
    }
    let promise = new Promise((resolve, reject) => {
        instance({
            ...config,
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

export default {
    fetcher
};