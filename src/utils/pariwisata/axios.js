import axios from 'axios';
import { Base64 } from "js-base64";

const isDev = false

const instance = axios.create({
    baseURL: isDev ? 'https://sandbox.mkpmobile.com/api/onlineticketing' : "https://apipayment.mkpmobile.com/onlineticketing",
});

instance.defaults.headers.common['Content-Type'] = "application/json";

const POST = async (path = "", data, headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.post(path, data, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const GET = async (path = "", data, headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.get(path, data, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const username = 'mkpmobile'
const password = 'mkpmobile123'

const POST_AUTH = async (path = "", data, headers = {}) => {
  let authString = username + ":" + password
  let promise = new Promise((resolve, reject) => {
      instance.post(path, data, {
        headers: {
            ...headers,
            'Authorization': "Basic " + Base64.btoa(authString)
        }
      }).then((res) => {
          if (res.status === 200 || res.status === 201) {
              resolve(res.data)
          }
      }).catch(async (error) => {
          reject(error);
      })
  })
  return promise;
}

const GET_AUTH = async (path = "", data, headers = {}) => {
  let authString = username + ":" + password
  let promise = new Promise((resolve, reject) => {
      instance.get(path, data, {
        headers: {
            ...headers,
            'Authorization': "Basic " + Base64.btoa(authString)
        }
      }).then((res) => {
          if (res.status === 200 || res.status === 201) {
              resolve(res.data)
          }
      }).catch(async (error) => {
          reject(error);
      })
  })
  return promise;
}

export default {
    POST,
    POST_AUTH,
    GET,
    GET_AUTH
};