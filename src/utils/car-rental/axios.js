import axios from 'axios';
import { Base64 } from "js-base64";

const isDev = true

const instance = axios.create({
    baseURL: isDev ? 'https://sandbox.mkpmobile.com/api/carrental' : "",
});

instance.defaults.headers.common['Content-Type'] = "application/json";

const POST = async (path = "", data, headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.post(path, data, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const PUT = async (path = "", data, headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.put(path, data, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const GET = async (path = "", headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.get(path, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const DELETE = async (path = "", headers = {}) => {
    let promise = new Promise((resolve, reject) => {
        instance.delete(path, {
            headers: {
                ...headers
            }
        }).then((res) => {
            if (res.status === 200 || res.status === 201) {
                resolve(res.data)
            }
        }).catch(async (error) => {
            reject(error);
        })
    })
    return promise;
}

const username = 'mkpmobile'
const password = 'mkpmobile123'
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb3Jwb3JhdGVfaWQiOjEsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwiZXhwIjoxNzE1OTMwNDI2LCJyb2xlX2lkIjoxLCJ1c2VyX2lkIjoyfQ.MiPsducR42uzN_OScXpPoWPB8SxI2u-ZHsBucc4Ix18'

const POST_AUTH = async (path = "", headers = {}) => {
  let authString = username + ":" + password
  let promise = new Promise((resolve, reject) => {
      instance.post(path, {
        headers: {
            ...headers,
            'Authorization': "Bearer " + token
        }
      }).then((res) => {
          if (res.status === 200 || res.status === 201) {
              resolve(res.data)
          }
      }).catch(async (error) => {
          reject(error);
      })
  })
  return promise;
}

const GET_AUTH = async (path = "", data, headers = {}) => {
  let authString = username + ":" + password
  let promise = new Promise((resolve, reject) => {
      instance.get(path, data, {
        headers: {
            ...headers,
            'Authorization': "Basic " + Base64.btoa(authString)
        }
      }).then((res) => {
          if (res.status === 200 || res.status === 201) {
              resolve(res.data)
          }
      }).catch(async (error) => {
          reject(error);
      })
  })
  return promise;
}

export default {
    POST,
    POST_AUTH,
    GET,
    GET_AUTH,
    PUT,
    DELETE
};