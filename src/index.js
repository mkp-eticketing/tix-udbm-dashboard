import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import LaporanWajibRetribusi from "./pages/parkir/laporan-transaksi";
import StoreList from "./pages/pasar/store-list";
import UserPasar from "./pages/pasar/user-pasar";
import LaporanAnalitikParkir from "./pages/parkir/laporan-analitik";
import LaporanTiket from "./pages/b2b/master-transaction";
import LaporanTransaksiProduct from "./pages/b2b//master-merchant";
import { merchant_data } from "./data/merchant";
import DashboardASG from "./pages/parkir/parkir-asg/dashboard";
import VehicleDurationASG from "./pages/parkir/parkir-asg/laporan-durasi-produk";
import MasterCardMember from "./pages/parkir/master-card-member/card-member";
import TotalVehicleAndIncome from "./pages/parkir/parkir-asg/laporan-summary-produk";
// import MasterMember from './pages/parkir/master-member';
import CarRental from "./pages/car-rental/penjadwalan-tarif";
// import ApprovalPage from "./pages/pasar/approvals/approval-page";
// import LaporanWajibRetribusi from './pages/parkir/laporan-transaksi';
// import StoreList from './pages/pasar/store-list';
// import UserPasar from "./pages/pasar/user-pasar";
import LaporanBilling from "./pages/pasar/laporan-billing";
import Approval from "./pages/pasar/approvals";
import LaporanMembership from "./pages/parkir/parkir-asg/membership/laporan-membership";
import LaporanDurasiProduk from "./pages/parkir/parkir-asg/laporan-durasi-produk";
import LaporanSummaryPendapatanProduk from "./pages/parkir/parkir-asg/laporan-summary-produk";
import LaporanMonitoringTransaksi from "./pages/parkir/parkir-asg/laporan-monitoring-transaksi";
import PengajuanMembership from "./pages/parkir/parkir-asg/membership/laporan-pengajuan";
import PeninjauanMembership from "./pages/parkir/parkir-asg/membership/laporan-peninjauan";
import PersetujuanMembership from "./pages/parkir/parkir-asg/membership/laporan-persetujuan";
import RiwayatMembership from "./pages/parkir/parkir-asg/membership/laporan-riwayat";
import LaporanShiftPeriod from "./pages/parkir/laporan-shift-period";
import LaporanPariwisata from "./pages/pariwisata/laporan-transaksi";

let dev = [
  {
    ouCode: "PAU-PDG.001",
    ouName: "PEDARINGAN",
    id: 300,
    tenantId: 10,
    ouParentId: 292,
    ouTypeId: 11,
    createUsername: "system",
    createAt: "20231008094558",
    updateUsername: "system",
    updateAt: "20231008094558",
    active: "Y",
    activeAt: "20231008094558",
    nonActiveAt: "",
  },
  {
    ouCode: "TEMSTING",
    ouName: "TESMSTING",
    id: 300,
    tenantId: 10,
    ouParentId: 292,
    ouTypeId: 11,
    createUsername: "system",
    createAt: "20231008094558",
    updateUsername: "system",
    updateAt: "20231008094558",
    active: "Y",
    activeAt: "20231008094558",
    nonActiveAt: "",
  },
];

let prodDemo = [
  {
    id: 129,
    tenantId: 10,
    ouCode: "000.MKPDEMO.001",
    ouName: "demo 1",
    ouParentId: 128,
    ouTypeId: 11,
    createUsername: "admops",
    createAt: "20230204173551",
    updateUsername: "admops",
    updateAt: "20230204173551",
    active: "Y",
    activeAt: "20230204173551",
    nonActiveAt: "",
  },
  {
    id: 129,
    tenantId: 10,
    ouCode: "MKP-DEMO.002",
    ouName: "demo 2",
    ouParentId: 128,
    ouTypeId: 11,
    createUsername: "admops",
    createAt: "20230204173551",
    updateUsername: "admops",
    updateAt: "20230204173551",
    active: "Y",
    activeAt: "20230204173551",
    nonActiveAt: "",
  },
];

let prod = [
  {
    id: 129,
    tenantId: 10,
    ouCode: "TRX3.PK.002-1",
    ouName: "KAWASAN INDUSTRI PEDARINGAN",
    ouParentId: 128,
    ouTypeId: 11,
    createUsername: "admops",
    createAt: "20230204173551",
    updateUsername: "admops",
    updateAt: "20230204173551",
    active: "Y",
    activeAt: "20230204173551",
    nonActiveAt: "",
  },
  {
    id: 64,
    tenantId: 10,
    ouCode: "TRX29.PK.004-1",
    ouName: "KIM GRESIK",
    ouParentId: 63,
    ouTypeId: 11,
    createUsername: "admops",
    createAt: "20230201183636",
    updateUsername: "admops",
    updateAt: "20230201183636",
    active: "Y",
    activeAt: "20230201183636",
    nonActiveAt: "",
  },
  // Mas hans Punya
  {
    id: 55,
    ouCode: "TRX67.PK.010-1",
    ouName: "RSUD Buleleng",
    address:
      "Jl. Ngurah Rai No.30, SIngaraja, Kec. Buleleng, Kabupaten Buleleng, Bali 81113",
    city: "KAB. BULELENG",
    fax: "-",
    ouParentId: 56,
    ouParentCode: "000000000000056",
    ouParentName: "RSUD BULELENG",
    ouType: "BRANCH",
    merchantKey:
      "CsIRj3SFGRDflP18ubEVA8f2UZANjXx0qOsdCBSHfrYFYhRr_Hwfpyvj3zHwyQZjVzB8PewpKML5NhoVwC-AuevdFUmEhJMaDzMqC32aqHnxfu1KbRaF6wqa05cTzpgmYYBd912I008r_M4y5b4SsYlYo_wrynnpOWmjuFXUXg9kD_lbLnZWTcn5BQEop1F1zTegHucb6QEJvuYsMII0BmMyuejIIzZLLe-Q2WXhgIRqnKsZovFEJ6jji-T5PjPYqIQFWlTvmjrxTAmcVuiMHdd15jghqgyhfXJJauYth9YKhvfeU4v8E7E=",
    outletCode: "",
    amountPpn: 0,
    typeIncPpn: "",
    createdAt: "2022-05-20T13:25:32Z",
    createdBy: "widana",
    updatedAt: "2023-02-06T14:49:33Z",
    updatedBy: "widanamkp",
  },
];

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    {/* <LaporanPariwisata merchantData={[{ ouName: "300377", ouCode: "300377" }, { ouName: "300352", ouCode: "300352" }, { ouName: "300353", ouCode: "300353" }]} /> */}
    {/* <LaporanAnalitikParkir username="arya" ouName='Grafis Summary Jenis Kendaraan' /> */}
    {/* <GrafikKendaraan /> */}
    {/* <MasterMember/> */}
    {/* <LaporanWajibRetribusi /> */}
    {/* <GrafikKendaraan /> */}
    {/* <Approval /> */}
    {/* <LaporanBilling
      merchantData={[
        { ouName: "TEST", ouCode: "184.PS.001" },
        { ouName: "TEST2", ouCode: "184.PS.002" },
        { ouName: "TEST3", ouCode: "184.PS.003" },
        { ouName: "TEST4", ouCode: "188.PS.006" },
      ]}
    /> */}
    {/* <StoreList /> */}

    {/* <DashboardASG merchantData={sip}/>   */}
    {/* <LaporanDurasiProduk merchantData={prod} /> DONE */}
    {/* <LaporanSummaryPendapatanProduk merchantData={dev}/> DONE */}
    {/* <LaporanMonitoringTransaksi merchantData={dev} /> DONE */}

    <PengajuanMembership
      merchantData={prod}
      username={"luthfiananda"}
      roleTask={
        "viewMembershipSubmission,updateStatusMembershipSubmission,createMembershipSubmission"
      }
    />
    {/* <PeninjauanMembership merchantData={DEMOMKP} username={'hanserhanto'} roleTask={"viewMembershipReview,updateStatusMembershipReview"} /> */}
    {/* <PersetujuanMembership merchantData={DEMOMKP} username={'labibsanjaya'} roleTask={"viewMembershipApproval,updateStatusMembershipApproval"}/> */}
    {/* <RiwayatMembership merchantData={prod} username={'widana'} />  DONE */}

    {/* <LaporanMembership merchantData={prodDemo} /> DONE NEED */}

    {/* <LaporanShiftPeriod merchantData={prod}/> */}
    {/* <LaporanTiket merchantData={merchant_data}/> */}
    {/* <LaporanTransaksiProduct merchantData={merchant_data}/> */}
    {/* <KotaOp merchantData={devCorporate}/> */}
    {/* <Armada merchantData={devCorporate}/> */}
    {/* <Supir merchantData={devCorporate}/> */}
    {/* <Schedule merchantData={devCorporate}/> */}
    {/* <Tarif merchantData={devCorporate}/> */}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
