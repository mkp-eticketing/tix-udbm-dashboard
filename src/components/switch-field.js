import React from "react"
import { Switch, Box, Typography, FormControlLabel } from "@mui/material";
import { styled } from '@mui/material/styles';
import CustomSwitch from "./custom-switch";

const IOSSwitch = styled((props) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
    width: 50,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 25,
        height: 22,
        marginRight: -30
    },
    '& .MuiSwitch-track': {
        borderRadius: 30 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));

const SwitchField = ({
    label,
    setValue,
    required = false,
    selectedValue,
    errorMessage = "Merchant wajib diisi",
    isError = false,
    ...other
}) => {
    const handleChange = (event) => {
        setValue(event.target.checked);
    };

    return (
        <Box sx={{ display: "flex", flexDirection: "column", gap: 1, width: "100%" }}>
            <Box sx={{ display: "flex", flexDirection: "column", gap: 1, width: "100%"}}>
                <Typography color={"rgb(71 85 105)"} fontSize="0.875rem" fontWeight={"600"} height={25}>{label} <span style={{ color: 'red' }}>{`${required ? "*" : ""}`}</span></Typography>
                <FormControlLabel
                    control={<IOSSwitch sx={{marginLeft: 1}} />}
                    checked={selectedValue}
                    onChange={handleChange}
                />
            </Box>
            <Typography fontSize={12} color="red">{isError && errorMessage}</Typography>
        </Box>
    )
}
export default SwitchField;