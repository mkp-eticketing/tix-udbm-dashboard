import React, { useState, useMemo } from "react";
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Checkbox, Paper, Box } from "@mui/material";
import { visuallyHidden } from "@mui/utils";
import TableSortLabel from "@mui/material/TableSortLabel";
import { styled } from "@mui/system";

const StyledTableContainer = styled(TableContainer)`
    box-shadow: none;
`;

const StyledTable = styled(Table)`
    min-width: 650px;
    border: 0px solid rgba(0, 0, 0, 0.12);
    border-radius: 4px;
`;

const StyledTableHeadRow = styled(TableRow)`
    background-color: #ffffff;
`;

const StyledTableCell = styled(TableCell)`
    padding: 8px 24px;
    font-weight: bold;
    font-size: 14px;
    color: rgba(0, 0, 0, 0.87);
`;

const StyledTableRow = styled(TableRow)`
    &:hover {
        background-color: #f3f3f3;
    }
`;

const StyledTableCellData = styled(TableCell)`
    padding: 6px 24px;
    font-size: 14px;
    color: rgba(0, 0, 0, 0.87);
`;

const StyledTableCellHeader = styled(TableCell)`
    padding: 6px 24px;
    font-size: 14px;
    width: 500px;
`;

const StyledTableBody = styled(TableBody)`
    padding: 6px 24px;
    height: 50px;
`;

const baseRenderCell = (item, header) => {
    return <span>{item[header.value]}</span>;
};

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

const useComparator = (order, orderBy) => {
    return useMemo(() => {
        return order === "desc" ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : b[orderBy] > a[orderBy] ? 1 : 0) : (a, b) => (a[orderBy] < b[orderBy] ? -1 : a[orderBy] > b[orderBy] ? 1 : 0);
    }, [order, orderBy]);
};

const CustomTableSort = ({ headers, items, bodyHeight = "30px", renderCell = baseRenderCell, checkBox = false, onItemCheck, onAllCheck, groupHead = false, group, keyName, order, setOrder, orderBy, setOrderBy }) => {
    const [checkedItems, setCheckedItems] = useState([]);

    const comparator = useComparator(order, orderBy);

    const createSortHandler = (property) => (event) => {
        handleRequestSort(event, property);
    };

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === "asc";
        setOrder(isAsc ? "desc" : "asc");
        setOrderBy(property);
    };

    const handleItemCheck = (item) => {
        const updatedCheckedItems = checkedItems.includes(item) ? checkedItems.filter((checkedItem) => checkedItem !== item) : [...checkedItems, item];
        setCheckedItems(updatedCheckedItems);
        if (onItemCheck) {
            onItemCheck(updatedCheckedItems);
        }
    };

    const handleAllCheck = () => {
        const updatedCheckedItems = checkedItems.length === items.length ? [] : [...items];
        setCheckedItems(updatedCheckedItems);
        if (onAllCheck) {
            onAllCheck(updatedCheckedItems);
        }
    };

    const visibleRows = useMemo(() => {
        return [...items].sort(comparator);
    }, [order, orderBy, items]);

    return (
        <StyledTableContainer component={Paper}>
            <StyledTable>
                {groupHead ? (
                    <TableHead>
                        <StyledTableHeadRow>
                            {group.map((group) => (
                                <StyledTableCellHeader
                                    sx={{
                                        fontWeight: "bold",
                                    }}
                                    align="center"
                                    colSpan={group.colSpan}
                                    style={{
                                        borderRight: group.rightBorder ? "1px solid #ccc" : "",
                                        borderLeft: group.leftBorder ? "1px solid #ccc" : "",
                                    }}
                                    key={group.title}
                                >
                                    <span className="-ml-1">{group.title.toUpperCase()}</span>
                                </StyledTableCellHeader>
                            ))}
                        </StyledTableHeadRow>
                    </TableHead>
                ) : null}
                <TableHead>
                    <StyledTableHeadRow>
                        {checkBox && (
                            <StyledTableCellHeader>
                                <Checkbox checked={checkedItems.length === items.length && items.length !== 0} onClick={handleAllCheck} />
                            </StyledTableCellHeader>
                        )}
                        {headers.map(
                            (header) =>
                                !header.hidden && (
                                    <StyledTableCellHeader
                                        sx={{
                                            minWidth: header.width,
                                            fontWeight: "bold",
                                            height: bodyHeight,
                                        }}
                                        align={header.align}
                                        key={header.value}
                                        style={{
                                            borderRight: header.rightBorder ? "1px solid #ccc" : "",
                                        }}
                                    >
                                        <TableSortLabel disabled={typeof header.sort == "boolean" ? !header.sort : false} active={orderBy === header.value} direction={orderBy === header.value ? order : "asc"} onClick={createSortHandler(header.value)}>
                                            <span className="-ml-1">{header.title.toUpperCase()}</span>
                                            {orderBy === header.value && (
                                                <Box component="span" sx={visuallyHidden}>
                                                    {order === "desc" ? "sorted descending" : "sorted ascending"}
                                                </Box>
                                            )}
                                        </TableSortLabel>
                                    </StyledTableCellHeader>
                                )
                        )}
                    </StyledTableHeadRow>
                </TableHead>
                <TableBody>
                    {visibleRows.map((item, index) => (
                        <StyledTableRow style={{ height: bodyHeight }} key={keyName ? item[keyName] : item.id}>
                            {checkBox && (
                                <StyledTableCellData>
                                    <Checkbox checked={checkedItems.includes(item)} onClick={() => handleItemCheck(item)} />
                                </StyledTableCellData>
                            )}
                            {headers.map(
                                (header) =>
                                    !header.hidden && (
                                        <StyledTableCellData
                                            align={header.align}
                                            key={header.value}
                                            style={{
                                                borderRight: header.rightBorder ? "1px solid #ccc" : "",
                                            }}
                                        >
                                            {renderCell(item, header, index)}
                                        </StyledTableCellData>
                                    )
                            )}
                        </StyledTableRow>
                    ))}
                </TableBody>
            </StyledTable>
        </StyledTableContainer>
    );
};

export default CustomTableSort;
