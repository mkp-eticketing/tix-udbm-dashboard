import React from "react"
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Box} from '@mui/material'

export default function FormDialog({
  openModal,
  setOpenModal,
  title,
  contentText,
  children,
  cancelLabel,
  submitLabel,
  submitClick = () => {},
  cancelClick = () => {},
  fullWidth = true,
  maxWidth = 'sm',
  backgroundColorTitle = '#3875CA',
  ...others}
) {
  return (
    <Dialog
      open={openModal}
      onClose={() => {
        setOpenModal(false)
        cancelClick()
      }}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      {...others}
    >
      <DialogTitle
        sx={{
          backgroundColor: backgroundColorTitle
        }}
      >
        {title}
      </DialogTitle>
      <DialogContent>
        <DialogContentText
          sx={{
            marginTop: '16px',
                        marginBottom: '16px'
          }}
        >{contentText}</DialogContentText>
        {children}
      </DialogContent>
      <DialogActions>
        <Button onClick={() => {
          setOpenModal(false)
          cancelClick()
        }} sx={{ color: 'red' }}>{cancelLabel}</Button>
        <Button onClick={() => submitClick()}>{submitLabel}</Button>
      </DialogActions>
    </Dialog>
  );
}
