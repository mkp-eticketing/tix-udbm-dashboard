import * as XLSX from "xlsx";
import moment from "moment";
import CustomButton from "./custom-button";
import { getStoreDetailList } from "../services/pasar/store";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { useState } from "react";
import React from "react";

const StoreExportButton = ({
  ouCodeSelected,
  sheetName = "list-store",
  color,
}) => {
  const [exportLoading, setExportLoading] = useState(false);

  const exportToExcel = async () => {
    setExportLoading(true);
    let data = {
      outletCode: ouCodeSelected,
    };
    getStoreDetailList(data)
      .then((res) => {
        if (res.result) {
          // List of properties to keep
          const propertyMap = {
            storeCode: "Store Code",
            storeLength: "Store Length",
            storeWidth: "Store Width",
            storeArea: "Store Area",
            storeStatus: "Status",
            storeSerialNumber: "Serial Number",
            districtName: "District Name",
            corporateName: "Merchant Name",
            createdAt: "Registered Date",
          };

          //   modify the data to only keep the desired column
          const modifiedData = res.result.map((obj) => {
            const newObj = {};
            for (const [originalProp, customProp] of Object.entries(
              propertyMap
            )) {
              if (obj.hasOwnProperty(originalProp)) {
                newObj[customProp] = obj[originalProp];
              }
            }
            return newObj;
          });

          // change the created at column from modifiedData into the desired format
          const modifiedDateFormat = modifiedData.map((data) => ({
            ...data,
            "Registered Date": moment(data["Registered Date"]).format(
              "Do MMMM YYYY, h:mm:ss"
            ),
          }));

          // convert to excel file
          const workbook = XLSX.utils.book_new();
          const worksheet = XLSX.utils.json_to_sheet(modifiedDateFormat);
          XLSX.utils.book_append_sheet(workbook, worksheet, sheetName);
          XLSX.writeFile(workbook, `${sheetName}.xlsx`);
        } else {
        }
        setExportLoading(false);
      })
      .catch((e) => {
        setExportLoading(false);
        // notify(e.message, "error");
      });
  };

  return (
    <CustomButton
      onClick={exportToExcel}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={exportLoading}
      color={color}
    >
      Download
    </CustomButton>
  );
};

export default StoreExportButton;
