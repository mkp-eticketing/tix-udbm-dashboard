import { Close } from "@mui/icons-material";
import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";
import React from "react";

const ConfirmModal = ({
    title = "",
    description = "",
    open = false,
    handleClose = () => { },
    handleConfirm = () => { }
}) => {
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            maxWidth="sm"
            fullWidth
        >
            <DialogTitle id="alert-dialog-title" sx={{ position: "relative" }}>
                {title}
                <Box
                    onClick={() => handleClose()}
                    sx={{
                        position: "absolute",
                        top: 2,
                        right: 10,
                        cursor: "pointer"
                    }}>
                    <Close sx={{ color: "gray" }} />
                </Box>
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {description}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>No</Button>
                <Button onClick={handleConfirm} autoFocus>
                    Yes
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default ConfirmModal;