import React, { useMemo, useRef } from "react"
import { Box, Typography } from "@mui/material";
import JoditEditor from 'jodit-react';

const EditorField = ({
    label,
    required = false,
    errorMessage = "Merchant wajib diisi",
    isError = false,
    setContent = () => { },
    content
}) => {
    const editor = useRef(null);
    const config = {
        readonly: false, // all options from https://xdsoft.net/jodit/docs/,
        placeholder: 'Start typings...',
        height: 500,
        showPoweredBy: false
    }
    return (
        <Box sx={{ display: "flex", flexDirection: "column", gap: 1, width: "100%" }}>
            <Typography color={"rgb(71 85 105)"} fontSize="0.875rem" fontWeight={"600"} height={25}>{label} <span style={{ color: 'red' }}>{`${required ? "*" : ""}`}</span></Typography>
            <JoditEditor
                ref={editor}
                value={content}
                config={config}
                tabIndex={1} // tabIndex of textarea
                onBlur={newContent => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
                onChange={newContent => { }}
            />
            <Typography fontSize={12} color="red">{isError && errorMessage}</Typography>
        </Box>
    )
}

export default EditorField;