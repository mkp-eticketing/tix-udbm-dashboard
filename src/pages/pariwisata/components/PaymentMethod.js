import React from "react";
import { Button, Avatar } from "@mui/material";
import { styled } from "@mui/system";
import moment from "moment";
import NONE from "../PaymentMethod/NONE.png";
import CASH from "../PaymentMethod/CASH.png";
import BRIZZI from "../PaymentMethod/BRIZZI.png";
import FLAZZ from "../PaymentMethod/FLAZZ.png";
import TAPCASH from "../PaymentMethod/TAPCASH.png";
import EMONEY from "../PaymentMethod/E-MONEY.png";
import DEBIT from "../PaymentMethod/DEBIT.png";
import QRIS from "../PaymentMethod/QRIS.png";
import JAKCARD from "../PaymentMethod/JAKCARD.png";
import LIFESTYLE from "../PaymentMethod/LIFESTYLE.png";
import CREDITCARD from "../PaymentMethod/CREDITCARD.png";
import MTI from "../PaymentMethod/MTI.png";
import DISBURSEMENT from "../PaymentMethod/DISBURSEMENT.png";

const PaymentMethod = ({
  type,
  text = true,
  showLabel = true,
  avatarSize = 40,
}) => {
  return (
    <div className={`flex items-center `}>
      <Avatar
        sx={{
          bgcolor: "#e8e8e8",
          border: "0.5px solid",
          borderColor: "#E4E4E4",
          marginRight: showLabel ? 0 : 1,
          width: avatarSize,
          height: avatarSize,
        }}
        // srcSet="../../images/PaymentMethod/NONE.png"
        src={
          type === "CASH"
            ? CASH
            : type === "BRIZZI"
            ? BRIZZI
            : type === "FLAZZ"
            ? FLAZZ
            : type === "TAPCASH"
            ? TAPCASH
            : type === "E-MONEY"
            ? EMONEY
            : type === "DEBIT"
            ? DEBIT
            : type === "QRIS"
            ? QRIS
            : type === "JAKCARD"
            ? JAKCARD
            : type === "BCA LIFESTYLE"
            ? LIFESTYLE
            : type === "MTI"
            ? MTI
            : type === "CREDIT CARD"
            ? CREDITCARD
            : type === "DISBURSEMENT"
            ? DISBURSEMENT
            : NONE
        }
      />
      {showLabel ? (
        <p hidden={!text} className="ml-2 font-mediumxx">
          {type}
        </p>
      ) : (
        ""
      )}
    </div>
  );
};

export default PaymentMethod;
