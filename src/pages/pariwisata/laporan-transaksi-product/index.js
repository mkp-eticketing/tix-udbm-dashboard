import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { getTrxByProductPayment, getTrxByProduct, getPaymentList, getProductList, getTrxByProductUser } from "../../../services/pariwisata/transaction";
import SelectField from "../../../components/select-field";
import SelectField2 from "../../../components/select-field-2";
import CustomTable from "../../../components/custom-table";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import ExportButton from "./components/export-to-excel-transaction";  
import DatePickerField from "../../../components/datepicker-field";
import CustomTab from '../components/custom-tab'
import { formatCurrency } from "../components/custom-format-currency";
import { thousandSeparator } from "../../../utils/thousand-separator";
import moment from "moment";

const LaporanTransaksiByProduct = ({
  label = "Transaction By Product Report",
  titleInfo = "To Display & Download Specific Transactions, Use the Filters Above.",
  subTitleInfo = ["Max Range of Date is 1 Month (31 Days)", "Select the Merchant to Use the Product Filter"],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
  avatarPayment = () => {},
}) => {
  const [disableDownload, setDisableDownload] = useState(true);
  const [dataTable1, setDataTable1] = useState([]);
  const [dataTable2, setDataTable2] = useState([]);
  const [dataTable3, setDataTable3] = useState([]);

  const [merchant, setMerchant] = useState(null);
  const [tanggalAwal, setTanggalAwal] = useState(moment().subtract(7, "days"));
  const [tanggalAkhir, setTanggalAkhir] = useState(moment());
  const [product, setProduct] = useState(null);
  const [payment, setPayment] = useState(null);

  const [merchantOption, setMerchantOption] = useState([]);
  const [productOption, setProductOption] = useState([]);
  const [paymentOption, setPaymentOption] = useState([]);

  //untuk menyimpan data filter yang sudah di search
  const [ouCodeSelected, setOuCodeSelected] = useState("");
  const [tanggalAwalSelected, setTanggalAwalSelected] = useState("");
  const [tanggalAkhirSelected, setTanggalAkhirSelected] = useState("");
  const [productCodeSelected, setProductCodeSelected] = useState("");
  const [paymentCodeSelected, setPaymentCodeSelected] = useState(false);

  const [errormerchant, setErrormerchant] = useState(false);
  const [errorTanggalAwal, setErrorTanggalAwal] = useState(false);
  const [errorTanggalAkhir, setErrorTanggalAkhir] = useState(false);

  const [productInput, setProductInput] = useState('');

  const header = [
    {
      title: "No",
      value: "no",
      align: "left",
      width: "30px",
    },
    {
      title: "Merchant",
      value: "corporate",
      align: "left",
      width: "250px",
    },
    {
      title: "Product Name",
      value: "productName",
      align: "left",
      width: "250px",
    },
    {
      title: "Payment Method",
      value: "paymentCategory",
      align: "left",
      width: "200px",
    },
    {
      title: "Price",
      value: "price",
      align: "left",
      width: "150px",
    },
    {
      title: "Quantity",
      value: "quantity",
      align: "left",
      width: "100px",
    },
    {
      title: "Total Amount",
      value: "totalAmount",
      align: "left",
      width: "200px",
    },
  ];

  const renderCell = (item, header, index) => {
    if (header.value === "no") {
      return <span>{index >= 0 ? index + 1 : ""}</span>;
    } else if (header.value === "corporate") {
      return <span>{item.corporate ? item.corporate : "-"}</span>;
    } else if (header.value === "price") {
      return <span>{item.price ? formatCurrency(item.price) : "-"}</span>;
    } else if (header.value === "productName") {
      return <span>{item.productName ? item.productName : "-"}</span>;
    } else if (header.value === "quantity") {
      return <span>{item.quantity ? thousandSeparator(item.quantity) : "-"}</span>;
    } else if (header.value === "totalAmount") {
      return <span>{item.totalAmount ? formatCurrency(item.totalAmount) : "-"}</span>;
    } else if (header.value === "paymentCategory") {
      let modifiedPayment
      if(item.paymentCategory === 'TUNAI'){
        modifiedPayment = "CASH"
      } else if(item.paymentCategory === 'CC'){
        modifiedPayment = "CREDIT CARD"
      } else {
        modifiedPayment = item.paymentCategory
      }
      return (
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            gap: 1
          }}
        >
          {avatarPayment(modifiedPayment)}
        </Box>
      );
    } 
  };

  const header2 = [
    {
      title: "No",
      value: "no",
      align: "left",
      width: "30px",
    },
    {
      title: "Merchant",
      value: "corporate",
      align: "left",
      width: "250px",
    },
    {
      title: "Product Name",
      value: "productName",
      align: "left",
      width: "250px",
    },
    {
      title: "Price",
      value: "price",
      align: "left",
      width: "100px",
    },
    {
      title: "Quantity",
      value: "quantity",
      align: "left",
      width: "100px",
    },
    {
      title: "Total Amount",
      value: "totalAmount",
      align: "left",
      width: "200px",
    },
  ];

  const renderCell2 = (item, header, index) => {
    if (header.value === "no") {
      return <span>{index >= 0 ? index + 1 : ""}</span>;
    } else if (header.value === "corporate") {
      return <span>{item.corporate ? item.corporate : "-"}</span>;
    } else if (header.value === "price") {
      return <span>{item.price ? formatCurrency(item.price) : "-"}</span>;
    } else if (header.value === "productName") {
      return <span>{item.productName ? item.productName : "-"}</span>;
    } else if (header.value === "quantity") {
      return <span>{item.quantity ? thousandSeparator(item.quantity) : "-"}</span>;
    } else if (header.value === "totalAmount") {
      return <span>{item.totalAmount ? formatCurrency(item.totalAmount) : "-"}</span>;
    }
  };

  const header3 = [
    {
      title: "No",
      value: "no",
      align: "left",
      width: "30px",
    },
    {
      title: "Merchant",
      value: "corporate",
      align: "left",
      width: "250px",
    },
    {
      title: "Product Name",
      value: "productName",
      align: "left",
      width: "250px",
    },
    {
      title: "Payment Method",
      value: "paymentCategory",
      align: "left",
      width: "200px",
    },
    {
      title: "Username",
      value: "username",
      align: "left",
      width: "200px",
    },
    {
      title: "Name",
      value: "name",
      align: "left",
      width: "200px",
    },
    {
      title: "Device ID",
      value: "deviceId",
      align: "left",
      width: "200px",
    },
    {
      title: "Price",
      value: "price",
      align: "left",
      width: "150px",
    },
    {
      title: "Quantity",
      value: "quantity",
      align: "left",
      width: "100px",
    },
    {
      title: "Total Amount",
      value: "totalAmount",
      align: "left",
      width: "200px",
    },
  ];

  const renderCell3 = (item, header, index) => {
    if (header.value === "no") {
      return <span>{index >= 0 ? index + 1 : ""}</span>;
    } else if (header.value === "corporate") {
      return <span>{item.corporate ? item.corporate : "-"}</span>;
    } else if (header.value === "price") {
      return <span>{item.price ? formatCurrency(item.price) : "-"}</span>;
    } else if (header.value === "productName") {
      return <span>{item.productName ? item.productName : "-"}</span>;
    } else if (header.value === "quantity") {
      return <span>{item.quantity ? thousandSeparator(item.quantity) : "-"}</span>;
    } else if (header.value === "username") {
      return <span>{item.username ? item.username : "-"}</span>;
    } else if (header.value === "name") {
      return <span>{item.name ? item.name : "-"}</span>;
    } else if (header.value === "deviceId") {
      return <span>{item.deviceId ? item.deviceId : "-"}</span>;
    } else if (header.value === "totalAmount") {
      return <span>{item.totalAmount ? formatCurrency(item.totalAmount) : "-"}</span>;
    } else if (header.value === "paymentCategory") {
      let modifiedPayment
      if(item.paymentCategory === 'TUNAI'){
        modifiedPayment = "CASH"
      } else if(item.paymentCategory === 'CC'){
        modifiedPayment = "CREDIT CARD"
      } else {
        modifiedPayment = item.paymentCategory
      }
      return (
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            gap: 1
          }}
        >
          {avatarPayment(modifiedPayment)}
        </Box>
      );
    } 
  };

  // untuk rename option select field merchant
  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    // console.log('merchantArr',merchantArr.length)
    handleGetPaymentData();
    setMerchantOption(merchantArr);
  }, [merchantData]);

  useEffect(() => {
    if (merchant && merchant.value !== ouCodeSelected) {
      setProductOption([]);
      setProduct(null);
      setProductInput('')
      handleGetProductData(merchant.value);
    } else if (merchant === null) {
      setProductInput('')
      setProductOption([]);
      setProduct(null);
    }
  }, [merchant]);

  const handleGetProductData = async (ouCodeValue) => {
    getProductList({ outletCode: [ouCodeValue]})
      .then((res) => {
        if (res.success) {
          let productArr = [];
          res.result.map((item) => {
            productArr.push({
              label: item.productName,
              value: item.productCode,
            });
          });
          setProductOption(productArr);
          // console.log(res.message || "Success Get product Data");
          notify(res.message || "Success Get Product Data", "success");
        }
      })
      .catch((error) => {
        setProductOption([]);
        if (error.response) {
          // console.log(error.response.data.message);
          notify(error.response.data.message, "error");
        } else {
          // console.log(error.message || error);
          notify(error.message || error, "error");
        }
      });
  };

  const handleGetPaymentData = async () => {
    getPaymentList()
      .then((res) => {
        if (res.success) {
          let paymentArr = [];
          res.result.map((item) => {
            paymentArr.push({
              label: item.paymentCategory,
              value: item.paymentCategoryID,
            });
          });
          setPaymentOption(paymentArr);
          // console.log(res.message || "Success Get payment Data");
          notify(res.message || "Success Get Payment Method Data", "success");
        }
      })
      .catch((error) => {
        setPaymentOption([]);
        if (error.response) {
          // console.log(error.response.data.message);
          notify(error.response.data.message, "error");
        } else {
          // console.log(error.message || error);
          notify(error.message || error, "error");
        }
      });
  };

  //get laporan transaksi tiket
  const handleGetTransactionData = async ({ouCodeValue, dateFrom, dateTo, productCode, paymentCategoryID}) => {
    if (ouCodeValue && dateFrom && dateTo) {
      const dateDiff = dateTo.diff(dateFrom, "days") + 1;
      if (dateTo.isBefore(dateFrom, 'day')) {
        // console.log("Select The Correct End Date")
        notify("Select The Correct End Date", "warning");
        setTanggalAkhir(null);
      } else if (dateDiff > 31) {
        // console.log("Maximum Range of 31 Days")
        notify("Maximum Range of 31 Days", "warning");
      } else {
        setLoading(true);
        setOuCodeSelected(ouCodeValue);
        setProductCodeSelected(productCode);
        setTanggalAwalSelected(dateFrom);
        setTanggalAkhirSelected(dateTo);
        setPaymentCodeSelected(paymentCategoryID)
        setDisableDownload(true);

        let data1 = {
          outletCode: [ouCodeValue],
          transactionDateStart: dateFrom.format("YYYY-MM-DD"),
          transactionDateTo: dateTo.format("YYYY-MM-DD"),
          productCode,
        };
        let data2 = {
          outletCode: [ouCodeValue],
          transactionDateStart: dateFrom.format("YYYY-MM-DD"),
          transactionDateTo: dateTo.format("YYYY-MM-DD"),
          productCode,
          paymentCategoryID : Number(paymentCategoryID)
        };
  
        // console.log(data1)
        // console.log(data2)

        await getTrxByProductPayment(data2)
          .then((res) => {
            if (res.success) {
              // console.log('Ticket Detail Success', res.result[0]);
              setDataTable1(res.result);
              notify(res.message || "Success Get Data List By Product & Payement Method ", "success");
              setDisableDownload(false);
            }
          })
          .catch((error) => {
            setDisableDownload(true);
            setDataTable1([]);
            if (error.response) {
              // console.log(error.response.data.message);
              notify(error.response.data.message, "error");
            } else {
              // console.log(error.message || error);
              notify(error.message || error, "error");
            }
          });

        await getTrxByProductUser(data2)
          .then((res) => {
            if (res.success) {
              // console.log('Ticket Detail Success', res.result[0]);
              setDataTable3(res.result);
              notify(res.message || "Success Get Data List By Product & User", "success");
              setDisableDownload(false);
            }
          })
          .catch((error) => {
            setDisableDownload(true);
            setDataTable3([]);
            if (error.response) {
              // console.log(error.response.data.message);
              notify(error.response.data.message, "error");
            } else {
              // console.log(error.message || error);
              notify(error.message || error, "error");
            }
          });

        await getTrxByProduct(data1)
          .then((res) => {
            if (res.success) {
              // console.log('Ticket Detail Success', res.result[0]);
              setDataTable2(res.result);
              notify(res.message || "Success Get Data List By Product", "success");
              setDisableDownload(false);
            }
          })
          .catch((error) => {
            setDisableDownload(true);
            setDataTable2([]);
            if (error.response) {
              // console.log(error.response.data.message);
              notify(error.response.data.message, "error");
            } else {
              // console.log(error.message || error);
              notify(error.message || error, "error");
            }
          });
        setLoading(false);
      }
    } else {
      if (!ouCodeValue) {
        setErrormerchant(true);
      }
      if (!dateFrom) {
        setErrorTanggalAwal(true);
      }
      if (!dateTo) {
        setErrorTanggalAkhir(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  };

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>
            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: ["repeat(1, 1fr)","repeat(1, 1fr)","repeat(3, 1fr)"],
                  gap: 2,
                }}
              >
                <SelectField
                  label={"Merchant"}
                  placeholder="Select Merchant"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={merchantOption}
                  selectedValue={merchant}
                  setValue={(newValue) => {
                    setMerchant(newValue);
                    setErrormerchant(false);
                  }}
                  required={true}
                  isError={errormerchant}
                  errorMessage={"Merchant is required"}
                />

                <DatePickerField
                  label={"Date Range"}
                  placeholder="DD MMM YYYY"
                  sx={{ width: "100%", fontSize: "16px" }}
                  value={tanggalAwal}
                  onChange={(newValue) => {
                    setTanggalAwal(newValue);
                    setErrorTanggalAwal(false);
                  }}
                  required={true}
                  format={"DD MMM YYYY"}
                  isError={errorTanggalAwal}
                  errorMessage={"Starting Date is required"}
                />

                <DatePickerField
                  label={<span />}
                  placeholder="DD MMM YYYY"
                  sx={{ width: "100%", fontSize: "16px", marginTop: ["-2rem","-2rem", "0"] }}
                  value={tanggalAkhir}
                  onChange={(newValue) => {
                    setTanggalAkhir(newValue);
                    setErrorTanggalAkhir(false);
                  }}
                  format={"DD MMM YYYY"}
                  isError={errorTanggalAkhir}
                  errorMessage={"End Date required"}
                />

                <SelectField2 
                  label={"Product"} 
                  placeholder={productOption.length > 0 ? "All Product" : "Please Select Merchant"} 
                  sx={{ width: "100%", fontSize: "16px" }} 
                  data={productOption} 
                  selectedValue={product} 
                  setValue={setProduct} 
                  inputValue={productInput}
                  setInputValue={setProductInput}
                />

                <SelectField 
                  label={"Payment Method"} 
                  placeholder={paymentOption.length > 0 ? "All Payment Method" : "Please Select Merchant"} 
                  sx={{ width: "100%", fontSize: "16px" }} 
                  data={paymentOption} 
                  selectedValue={payment} 
                  setValue={setPayment} 
                />

              </Box>
            </Stack>
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "flex-start"],
                gap: 3,
                justifyContent: "space-between",
                mt: "2rem",
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              <div
                style={{
                  display: "flex",
                  gap: 10,
                }}
              >
                <ExportButton
                  disabled={disableDownload}
                  setDisableDownload={setDisableDownload}
                  ouCodeValue={ouCodeSelected}
                  dateFrom={tanggalAwalSelected}
                  dateTo={tanggalAkhirSelected}
                  productCode={productCodeSelected}
                  paymentCategoryID={paymentCodeSelected}
                  notify={notify}
                  color="success"
                />
                <CustomButton
                  onClick={() => {
                    handleGetTransactionData({
                      ouCodeValue: merchant ? merchant.value : "",
                      dateFrom: tanggalAwal,
                      dateTo: tanggalAkhir,
                      productCode: product ? product.value : "",
                      paymentCategoryID: payment ? payment.value : "",
                    });
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttonFilter}
                >
                  Filter
                </CustomButton>
              </div>
            </Stack>
            <Box sx={{ width: "100%", display: 'flex', flexDirection: 'column', gap: '2rem' }} mt={"2rem"}>
              <CustomTab
                dataTabList={[
                  {
                    label : 'Transaction By Product',
                    table : <CustomTable headers={header2} items={dataTable2} renderCell={renderCell2} />
                  },
                  {
                    label : 'Transaction By Product & Payment Method',
                    table : <CustomTable headers={header} items={dataTable1} renderCell={renderCell} />
                  },
                  {
                    label : 'Transaction By Product & User',
                    table : <CustomTable headers={header3} items={dataTable3} renderCell={renderCell3} />
                  }
                ]}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default LaporanTransaksiByProduct;
