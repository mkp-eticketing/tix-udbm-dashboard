import ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { getTrxByProductPayment, getTrxByProduct, getTrxByProductUser} from "../../../../services/pariwisata/transaction";
import React from "react";
import { formatCurrency } from "../../components/custom-format-currency";
import { thousandSeparator } from '../../../../utils/thousand-separator';

const ExportToExcel = ({ disabled, setDisableDownload, ouCodeValue, dateFrom, dateTo, productCode, paymentCategoryID, notify }) => {
  const handleFetch = async () => {
    setDisableDownload(true);
    let data1 = {
      outletCode: [ouCodeValue],
      transactionDateStart: dateFrom.format("YYYY-MM-DD"),
      transactionDateTo: dateTo.format("YYYY-MM-DD"),
      productCode,
    };
    let data2 = {
      outletCode: [ouCodeValue],
      transactionDateStart: dateFrom.format("YYYY-MM-DD"),
      transactionDateTo: dateTo.format("YYYY-MM-DD"),
      productCode,
      paymentCategoryID : Number(paymentCategoryID)
    };

    try {
      const trxByProduct = await getTrxByProduct(data1);
      let modifiedData1
      if (trxByProduct && trxByProduct.result) {
        // console.log('Ticket Summary Success');
        modifiedData1 = trxByProduct.result.map((data) => ({
          ...data,
          "price" : formatCurrency(data['price']),
          "quantity": thousandSeparator(data['quantity']),
          "totalAmount" : formatCurrency(data['totalAmount']),
        }))
        notify(trxByProduct.message || "Success Get Data By Product", "success");
      } else {
        // console.log('No Data Summary Found');
        notify("No Data By Product Found", "warning");
      };

      const trxByProductPayment = await getTrxByProductPayment(data2);
      let modifiedData2
      if (trxByProductPayment && trxByProductPayment.result) {
        // console.log('Ticket Summary By Name Success');
        modifiedData2 = trxByProductPayment.result.map((data) => ({
          ...data,
          "price" : formatCurrency(data['price']),
          "quantity": thousandSeparator(data['quantity']),
          "totalAmount" : formatCurrency(data['totalAmount']),
        }))
        notify(trxByProductPayment.message || "Success Get Data By Product & Payment Method", "success");
      } else {
        // console.log('No Data Summary By Name Found');
        notify("No Data By Product & Payment Method Found", "warning");
      };

      const trxByProductUser = await getTrxByProductUser(data2)
      let modifiedData3
      if (trxByProductUser && trxByProductUser.result) {
        // console.log('Ticket Summary By Name Success');
        modifiedData3 = trxByProductUser.result.map((data) => ({
          ...data,
          "price" : formatCurrency(data['price']),
          "quantity": thousandSeparator(data['quantity']),
          "totalAmount" : formatCurrency(data['totalAmount']),
        }))
        notify(trxByProductUser.message || "Success Get Data By Product & Payment User", "success");
      } else {
        // console.log('No Data Summary By Name Found');
        notify("No Data By Product & Payment User", "warning");
      };

      if(modifiedData1 && modifiedData2 && modifiedData3){
        exportToExcel( modifiedData1, modifiedData2, modifiedData3)
      }
    } catch (error) {
      setDisableDownload(false)
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    }
  };

  const exportToExcel = (data1, data2, data3) => {
    const workbook = new ExcelJS.Workbook();
    
    const ws1 = workbook.addWorksheet("Transaction By Product Name");
    const ws2 = workbook.addWorksheet("Transaction By Product Name & Payment Method");
    const ws3 = workbook.addWorksheet("Transaction By Product Name & User");

    ws1.columns = [
        { header: 'Corporate', key: 'corporate', width: 35 },
        { header: 'Product Code', key: 'productCode', width: 25 },
        { header: 'Product Name', key: 'productName', width: 35 },
        { header: 'Price', key: 'price', width: 25 },
        { header: 'Quantity', key: 'quantity', width: 25 },
        { header: 'Total Amount', key: 'totalAmount', width: 25 },
      ];
    ws1.addRows(data1);
    
    ws2.columns = [
      { header: 'Corporate', key: 'corporate', width: 35 },
      { header: 'Payment Category', key: 'paymentCategory', width: 25 },
      { header: 'Product Code', key: 'productCode', width: 25 },
      { header: 'Product Name', key: 'productName', width: 35 },
      { header: 'Price', key: 'price', width: 25 },
      { header: 'Quantity', key: 'quantity', width: 25 },
      { header: 'Total Amount', key: 'totalAmount', width: 25 },
    ];
    ws2.addRows(data2);

    ws3.columns = [
      { header: 'Corporate', key: 'corporate', width: 35 },
      { header: 'Payment Category', key: 'paymentCategory', width: 25 },
      { header: 'Product Code', key: 'productCode', width: 25 },
      { header: 'Product Name', key: 'productName', width: 35 },
      { header: 'Username', key: 'username', width: 35 },
      { header: 'Name', key: 'name', width: 35 },
      { header: 'Device ID', key: 'deviceId', width: 35 },
      { header: 'Price', key: 'price', width: 25 },
      { header: 'Quantity', key: 'quantity', width: 25 },
      { header: 'Total Amount', key: 'totalAmount', width: 25 },
    ];
    ws3.addRows(data3);
    
    [ws1, ws2, ws3].forEach(worksheet => {
      worksheet.eachRow({ includeEmpty: false }, function(row, rowNumber) {
          row.eachCell(cell => {
              cell.border = {
                  top: {style:'thin'},
                  left: {style:'thin'},
                  bottom: {style:'thin'},
                  right: {style:'thin'}
              };
              cell.alignment = { horizontal: 'left' }
          });
          if (rowNumber === 1) {
              row.eachCell(cell => {
                  cell.font = { bold: true };
                  cell.alignment = { horizontal: 'center' };
                  cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FF3875CA' }
                  };
              });
          }
      });
    });

    workbook.xlsx.writeBuffer()
    .then(buffer => {
        saveAs(new Blob([buffer]), `Ticket-Transaction-By-Product (${dateFrom.format("YYYY_MM_DD")} - ${dateTo.format("YYYY_MM_DD")}).xlsx`);
        setDisableDownload(false); 
    })
  };

  return (
    <CustomButton
      onClick={handleFetch}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={disabled}
      color='success'
    >
      Download
    </CustomButton>
  );
};

export default ExportToExcel;
