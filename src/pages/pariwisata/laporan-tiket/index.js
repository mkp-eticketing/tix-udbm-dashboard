import React, { useEffect, useState } from "react";
import { Avatar, Box, Card, CardContent, Stack, Typography } from "@mui/material";
import SelectField from "../../../components/select-field";
import SelectField2 from "../../../components/select-field-2";
import SwitchField from "../../../components/switch-field";
import SearchIcon from "@mui/icons-material/Search";
import { getTiketSummaryV2, getTerminalDataV2, getTiketDetailV2, getTiketSummaryByProductNameV2 } from "../../../services/pariwisata/tiket";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import ExportButton from "./components/export-to-excel-ticket";
import moment from "moment";
import CardTicketSummary from "./components/card-ticket-summary";
import DatePickerField from "../../../components/datepicker-field";
import ConfirmationNumberIcon from "@mui/icons-material/ConfirmationNumber";
import OutputIcon from "@mui/icons-material/Output";
import Button from "@mui/material/Button";
import SearchField from "./components/search-field";
import CustomTab from "../components/custom-tab";
import DialogQR from "./components/dialog-qr";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { grey } from "@mui/material/colors";
import AccessTimeFilledRoundedIcon from "@mui/icons-material/AccessTimeFilledRounded";
import { formatCurrency } from "../components/custom-format-currency";
import { CustomTruncate } from "../../../utils/custom-truncate";

const LaporanTiket = ({
    label = "Ticket Report",
    titleInfo = "To Display & Download Specific Transactions, Use the Filters Above.",
    subTitleInfo = [
        "Max Range of Date is 1 Month (31 Days)",
        "Select the Merchant to Use the Terminal Filter",
        "Use the Required Filter to Search the Detail Report",
        "Total Ticket is Total Ticket Sold From Success Transactions",
        "Validated Ticket In is Total Validated Ticket From Gate In",
        "Validated Ticket Out is Total Validated Ticket From Gate Out",
    ],
    merchantData = [],
    setLoading = () => {},
    notify = () => {},
    buttonFilter = "Apply",
}) => {
    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState(0);
    const [disableDownload, setDisableDownload] = useState(true);
    const [data, setData] = useState([]);
    const [dataSummary, setDataSummary] = useState([]);

    const [merchant, setMerchant] = useState([]);
    const [tanggalAwal, setTanggalAwal] = useState(moment().subtract(7, "days"));
    const [tanggalAkhir, setTanggalAkhir] = useState(moment());
    const [terminal, setTerminal] = useState(null);
    const [statusMasuk, setStatusMasuk] = useState(null);
    const [statusKeluar, setStatusKeluar] = useState(null);
    const [useTrx, setUseTrx] = useState(false);
    const [search, setSearch] = useState("");
    const [isSearch, setIsSearch] = useState(false);
    const [totalSearch, setTotalSearch] = useState(0);

    const [summaryLoading, setSummaryLoading] = useState(false);
    const [searchLoading, setSearchLoading] = useState(false);

    const [merchantOption, setMerchantOption] = useState([]);
    const [terminalOption, setTerminalOption] = useState([]);

    const [terminalInput, setTerminalInput] = useState("");

    const [openDialogQr, setOpenDialogQr] = useState(false);
    const [selectedQr, setSelectedQr] = useState("");

    const statusMasukOption = [
        {
            label: "Used",
            value: "true",
        },
        {
            label: "Not Used",
            value: "false",
        },
    ];
    const statusKeluarOption = [
        {
            label: "Out",
            value: "true",
        },
        {
            label: "Not Out",
            value: "false",
        },
    ];

    //untuk menyimpan data filter yang sudah di search
    const [ouCodeSelected, setOuCodeSelected] = useState("");
    const [tanggalAwalSelected, setTanggalAwalSelected] = useState("");
    const [tanggalAkhirSelected, setTanggalAkhirSelected] = useState("");
    const [terminalCodeSelected, setTerminalCodeSelected] = useState("");
    const [statusMasukSelected, setStatusMasukSelected] = useState(false);
    const [statusKeluarSelected, setStatusKeluarSelected] = useState(false);
    const [useTrxSelected, setUseTrxSelected] = useState(false);

    const [totalTiket, setTotalTiket] = useState(0);
    const [tiketMasuk, setTiketMasuk] = useState(0);
    const [tiketKeluar, setTiketKeluar] = useState(0);
    const [selfkiosTickets, setSelfkiosTickets] = useState(0);
    const [onlineTickets, setOnlineTickets] = useState(0);
    const [onsiteTickets, setOnsiteTickets] = useState(0);

    const [errormerchant, setErrormerchant] = useState(false);
    const [errorTanggalAwal, setErrorTanggalAwal] = useState(false);
    const [errorTanggalAkhir, setErrorTanggalAkhir] = useState(false);

    const header = [
        {
            title: "No",
            value: "no",
            align: "left",
            width: "30px",
        },
        {
            title: "QRCode",
            value: "qrCode",
            align: "left",
            width: "400px",
        },
        {
            title: "Corporate Name",
            value: "corporateName",
            align: "left",
            width: "250px",
        },
        {
            title: "Created Date",
            value: "createdAt",
            align: "left",
            width: "200px",
        },
        {
            title: "Transaction Number",
            value: "noHeader",
            align: "left",
            width: "250px",
        },
        {
            title: "Product Name",
            value: "productName",
            align: "left",
            width: "250px",
        },
        {
            title: "Device ID",
            value: "deviceID",
            align: "left",
            width: "250px",
        },
        {
            title: "Price",
            value: "price",
            align: "left",
            width: "250px",
        },
        {
            title: "Platform",
            value: "platform",
            align: "left",
            width: "250px",
        },
        {
            title: "Customer Type",
            value: "customerType",
            align: "left",
            width: "250px",
        },
        {
            title: "Expired Date",
            value: "tanggalExpired",
            align: "left",
            width: "200px",
        },
        {
            title: "In Status",
            value: "inStatus",
            align: "left",
            width: "200px",
        },
        {
            title: "In Date",
            value: "tanggalPakai",
            align: "left",
            width: "200px",
        },
        {
            title: "Out Status",
            value: "outStatus",
            align: "left",
            width: "200px",
        },
        {
            title: "Out Date",
            value: "tanggalKeluar",
            align: "left",
            width: "200px",
        },
        {
            title: "Terminal Code",
            value: "kodeTerminal",
            align: "left",
            width: "200px",
        },
        {
            title: "Terminal Name",
            value: "namaTerminal",
            align: "left",
            width: "300px",
        },
        {
            title: "Visitor Name",
            value: "visitorName",
            align: "left",
            width: "250px",
        },
        {
            title: "Visitor ID Type",
            value: "visitorIDType",
            align: "left",
            width: "250px",
        },
        {
            title: "Visitor ID",
            value: "visitorID",
            align: "left",
            width: "250px",
        },
        {
            title: "Visitor Phone",
            value: "visitorPhone",
            align: "left",
            width: "250px",
        },
        {
            title: "Visitor Email",
            value: "visitorEmail",
            align: "left",
            width: "250px",
        },
        {
            title: "Visitor Gender",
            value: "visitorGender",
            align: "left",
            width: "250px",
        },
        {
            title: "Visitor Region",
            value: "visitorRegion",
            align: "left",
            width: "250px",
        },
        {
            title: "Remark",
            value: "remark",
            align: "left",
            width: "250px",
        },
        // "visitorName": "",
        //     "visitorIDType": "",
        //     "visitorID": "",
        //     "visitorPhone": "",
        //     "visitorEmail": "",
        //     "visitorGender": "",
        //     "visitorRegion": "",
        //     "remark": "",
    ];

    const renderCell = (item, header, index) => {
        if (header.value === "no") {
            return <span>{index >= 0 ? index + offset + 1 : ""}</span>;
        } else if (header.value === "tanggalBerlaku") {
            return;
        } else if (header.value === "createdAt") {
            return <span>{item.createdAt ? moment(item.createdAt).format("DD MMM YYYY HH:mm:ss") : "-"}</span>;
        } else if (header.value === "noHeader") {
            return <span>{item.noHeader ? item.noHeader : "-"}</span>;
        } else if (header.value === "qrCode") {
            return (
                <Box sx={{ display: "flex", gap: 1, width: "100%", alignItems: "center" }}>
                    <Box sx={{ display: "flex", flexDirection: "column", width: "100%" }}>
                        <Box
                            onClick={() => handleOpenDialogQR(item.qrCode)}
                            sx={{
                                fontWeight: "bold",
                                borderBottom: 1,
                                color: "#0d47a1",
                                cursor: "pointer",
                                ":hover": {
                                    color: "#2196f3",
                                },
                            }}
                        >
                            {item.qrCode ? item.qrCode : "-"}
                        </Box>
                        <Box>{item.tanggalBerlaku ? moment(item.tanggalBerlaku).format("DD MMM YYYY") : "-"}</Box>
                    </Box>
                    <Avatar
                        sx={{
                            width: 28,
                            height: 28,
                            backgroundColor: item.inStatus ? "success.main" : grey["500"],
                            color: "white",
                        }}
                    >
                        {item.inStatus ? <CheckCircleIcon fontSize="small" /> : <AccessTimeFilledRoundedIcon fontSize="small" />}
                    </Avatar>
                </Box>
            );
        } else if (header.value === "productName") {
            return <span>{item.productName ? item.productName : "-"}</span>;
        } else if (header.value === "tanggalExpired") {
            return <span>{item.tanggalExpired ? moment(item.tanggalExpired).format("DD MMM YYYY HH:mm:ss") : "-"}</span>;
        } else if (header.value === "inStatus") {
            return (
                <span className="flex gap-2 items-center">
                    <Avatar
                        sx={{
                            width: 28,
                            height: 28,
                            backgroundColor: item.inStatus ? "success.main" : "error.main",
                            color: "white",
                        }}
                    >
                        {item.inStatus ? <CheckCircleIcon fontSize="small" /> : <CancelIcon fontSize="small" />}
                    </Avatar>
                    {item.inStatus ? "Used" : "Not Used"}
                </span>
            );
        } else if (header.value === "tanggalPakai") {
            return <span>{item.tanggalPakai ? moment(item.tanggalPakai).format("DD MMM YYYY HH:mm:ss") : "-"}</span>;
        } else if (header.value === "outStatus") {
            return (
                <span className="flex gap-2 items-center">
                    <Avatar
                        sx={{
                            width: 28,
                            height: 28,
                            backgroundColor: item.outStatus ? "success.main" : "error.main",
                            color: "white",
                        }}
                    >
                        {item.outStatus ? <CheckCircleIcon fontSize="small" /> : <CancelIcon fontSize="small" />}
                    </Avatar>
                    {item.outStatus ? "Out" : "Not Out"}
                </span>
            );
        } else if (header.value === "tanggalKeluar") {
            return <span>{item.tanggalKeluar ? moment(item.tanggalKeluar).format("DD MMM YYYY HH:mm:ss") : "-"}</span>;
        } else if (header.value === "kodeTerminal") {
            return <span>{item.kodeTerminal ? item.kodeTerminal : "-"}</span>;
        } else if (header.value === "namaTerminal") {
            return <span>{item.namaTerminal ? item.namaTerminal : "-"}</span>;
        } else if (header.value === "deviceID") {
            return <span>{item.deviceID ? item.deviceID : "-"}</span>;
        } else if (header.value === "price") {
            return <span>{formatCurrency(item.price)}</span>;
        } else if (header.value === "customerType") {
            return <span>{item.customerType ? item.customerType : "-"}</span>;
        } else if (header.value === "platform") {
            return <span>{item.platform ? item.platform : "-"}</span>;
        } else if (header.value === "visitorName") {
            return <span>{item.visitorName ? item.visitorName : "-"}</span>;
        } else if (header.value === "visitorIDType") {
            return <span>{item.visitorIDType ? item.visitorIDType : "-"}</span>;
        } else if (header.value === "visitorID") {
            return <span>{item.visitorID ? item.visitorID : "-"}</span>;
        } else if (header.value === "visitorPhone") {
            return <span>{item.visitorPhone ? item.visitorPhone : "-"}</span>;
        } else if (header.value === "visitorEmail") {
            return <span>{item.visitorEmail ? item.visitorEmail : "-"}</span>;
        } else if (header.value === "visitorGender") {
            return <span>{item.visitorGender ? item.visitorGender : "-"}</span>;
        } else if (header.value === "visitorRegion") {
            return <span>{item.visitorRegion ? item.visitorRegion : "-"}</span>;
        } else if (header.value === "corporateName") {
            return <span>{item.corporateName ? item.corporateName : "-"}</span>;
        } else if (header.value === "remark") {
            return item.remark ? <CustomTruncate value={item.remark} length={70} /> : <span>-</span> ;
        }
    };

    const header2 = [
        {
            title: "Product Name",
            value: "productName",
            align: "left",
            width: "300px",
        },
        {
            title: "Total Ticket",
            value: "totalTickets",
            align: "center",
            width: "50px",
        },
        {
            title: "Ticket In",
            value: "inTickets",
            align: "center",
            width: "50px",
        },
        {
            title: "Ticket Out",
            value: "outTickets",
            align: "center",
            width: "50px",
        },
        {
            title: "Self Kios Tickets",
            value: "selfKiosTickets",
            align: "center",
            width: "100px",
        },
        {
            title: "Online Tickets",
            value: "onlineTickets",
            align: "center",
            width: "100px",
        },
        {
            title: "Onsite Tickets",
            value: "onsiteTickets",
            align: "center",
            width: "100px",
        },
    ];

    const renderCell2 = (item, header, index) => {
        if (header.value === "productName") {
            return <span>{item.productName ? item.productName : "-"}</span>;
        } else if (header.value === "inTickets") {
            return <span>{item.inTickets ? item.inTickets : "0"}</span>;
        } else if (header.value === "totalTickets") {
            return <span>{item.totalTickets ? item.totalTickets : "0"}</span>;
        } else if (header.value === "outTickets") {
            return <span>{item.outTickets ? item.outTickets : "0"}</span>;
        } else if (header.value === "selfKiosTickets") {
            return <span>{item.selfKiosTickets ? item.selfKiosTickets : "0"}</span>;
        } else if (header.value === "onlineTickets") {
            return <span>{item.onlineTickets ? item.onlineTickets : "0"}</span>;
        } else if (header.value === "onsiteTickets") {
            return <span>{item.onsiteTickets ? item.onsiteTickets : "0"}</span>;
        }
    };

    const handleOpenDialogQR = (value) => {
        setOpenDialogQr(true);
        setSelectedQr(value);
    };

    // untuk rename option select field merchant
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                label: item.ouName,
                value: item.ouCode,
            });
        });
        // console.log('merchantArr',merchantArr.length)
        setMerchantOption(merchantArr);
    }, [merchantData]);

    useEffect(() => {
        if (merchant && Array.isArray(merchant) && merchant.length > 0) {
            setTerminalOption([]);
            setTerminal(null);
            setTerminalInput("");
            const arrayOuCode = merchant.map((data) => data.value);
            handleGetTerminalData(arrayOuCode);
        } else if (merchant.length === 0) {
            setTerminalInput("");
            setTerminalOption([]);
            setTerminal(null);
        }
    }, [merchant]);

    const handleGetTerminalData = async (ouCodeValue) => {
        getTerminalDataV2({ outletCode: ouCodeValue })
            .then((res) => {
                if (res.success) {
                    let terminalArr = [];
                    res.result.map((item) => {
                        terminalArr.push({
                            label: item.uraian,
                            value: item.kodeTerminal,
                        });
                    });
                    setTerminalOption(terminalArr);
                    // console.log(res.message || "Success Get Terminal Data");
                    notify(res.message || "Success Get Terminal Data", "success");
                }
            })
            .catch((error) => {
                setTerminalOption([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });
    };

    const checkFilter = async ({ limitDt, offsetDt, ouCodeValue, arrivalDateFrom, arrivalDateTo, terminalCode, inStatus, outStatus, isUsingTrxDate }) => {
        if (ouCodeValue.length > 0 && arrivalDateFrom && arrivalDateTo) {
            const dateDiff = arrivalDateTo.diff(arrivalDateFrom, "days") + 1;
            if (arrivalDateTo.isBefore(arrivalDateFrom, "day")) {
                // console.log("Choose The Correct End Date")
                notify("Choose The Correct End Date", "warning");
                setTanggalAkhir(null);
            } else if (dateDiff > 31) {
                // console.log("Maximum Range of 31 Days")
                notify("Maximum Range of 31 Days", "warning");
            } else {
                setLoading(true);
                setDisableDownload(true);
                const arrayOuCode = ouCodeValue.map((data) => data.value);
                await handleGetTiketData({
                    limitDt: limitDt,
                    offsetDt: offsetDt,
                    ouCodeValue: arrayOuCode,
                    arrivalDateFrom,
                    arrivalDateTo,
                    terminalCode,
                    inStatus,
                    outStatus,
                    isUsingTrxDate,
                });
                await handleGetSummaryData({
                    limitDt: limitDt,
                    offsetDt: offsetDt,
                    ouCodeValue: arrayOuCode,
                    arrivalDateFrom,
                    arrivalDateTo,
                    terminalCode,
                    inStatus,
                    outStatus,
                    isUsingTrxDate,
                });
                await handleSummaryByProduct({
                    ouCodeValue: arrayOuCode,
                    arrivalDateFrom,
                    arrivalDateTo,
                    terminalCode,
                    inStatus,
                    outStatus: statusKeluar ? statusKeluar.value : "",
                    isUsingTrxDate,
                });
                setDisableDownload(false);
                setLoading(false);
            }
        } else {
            if (!ouCodeValue) {
                setErrormerchant(true);
            }
            if (!arrivalDateFrom) {
                setErrorTanggalAwal(true);
            }
            if (!arrivalDateTo) {
                setErrorTanggalAkhir(true);
            }
            notify("Fill The Required Filter", "warning");
        }
    };

    //get laporan tiket
    const handleGetTiketData = async ({ limitDt, offsetDt, ouCodeValue, arrivalDateFrom, arrivalDateTo, terminalCode, inStatus, outStatus, isUsingTrxDate }) => {
        let data2 = {
            outletCode: ouCodeValue,
            arrivalDateFrom: arrivalDateFrom.format("YYYY-MM-DD"),
            arrivalDateTo: arrivalDateTo.format("YYYY-MM-DD"),
            limit: limitDt,
            offset: offsetDt,
            terminalCode,
            inStatus,
            outStatus,
            isUsingTrxDate,
        };

        setIsSearch(false);
        setSearch("");
        setOuCodeSelected(ouCodeValue);
        setTerminalCodeSelected(terminalCode);
        setTanggalAwalSelected(arrivalDateFrom);
        setTanggalAkhirSelected(arrivalDateTo);
        setStatusMasukSelected(inStatus);
        setStatusKeluarSelected(outStatus);
        setUseTrxSelected(isUsingTrxDate);
        setData([]);

        await getTiketDetailV2(data2)
            .then((res) => {
                if (res.success) {
                    // console.log('Ticket Detail Success', res.result[0]);
                    setData(res.result);
                    notify(res.message || "Success Get Data List", "success");
                }
            })
            .catch((error) => {
                setData([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });
    };

    const handleGetSummaryData = async ({ ouCodeValue, arrivalDateFrom, arrivalDateTo, terminalCode, inStatus, outStatus, isUsingTrxDate }) => {
        let data1 = {
            outletCode: ouCodeValue,
            arrivalDateFrom: arrivalDateFrom.format("YYYY-MM-DD"),
            arrivalDateTo: arrivalDateTo.format("YYYY-MM-DD"),
            terminalCode,
            inStatus,
            outStatus,
            isUsingTrxDate,
        };
        setSummaryLoading(true);
        await getTiketSummaryV2(data1)
            .then((res) => {
                if (res.success) {
                    setTotalTiket(res.result[0].totalTickets);
                    setTiketMasuk(res.result[0].inTickets);
                    setTiketKeluar(res.result[0].outTickets);
                    setSelfkiosTickets(res.result[0].selfKiosTickets);
                    setOnlineTickets(res.result[0].onlineTickets);
                    setOnsiteTickets(res.result[0].onsiteTickets);
                    // console.log('Ticket Summary Success', res.result[0]);
                    notify(res.message || "Success Get Data Summary", "success");
                } else {
                    notify("No Data Summary Found", "warning");
                }
                setSummaryLoading(false);
            })
            .catch((error) => {
                setSummaryLoading(false);
                setTotalTiket(0);
                setTiketMasuk(0);
                setTiketKeluar(0);
                setSelfkiosTickets(0);
                setOnlineTickets(0);
                setOnsiteTickets(0);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error);
                    notify(error.message || error, "error");
                }
            });
    };

    const handleSummaryByProduct = async ({ ouCodeValue, arrivalDateFrom, arrivalDateTo, terminalCode, inStatus, outStatus, isUsingTrxDate }) => {
        let data1 = {
            outletCode: ouCodeValue,
            arrivalDateFrom: arrivalDateFrom.format("YYYY-MM-DD"),
            arrivalDateTo: arrivalDateTo.format("YYYY-MM-DD"),
            terminalCode,
            inStatus,
            outStatus,
            isUsingTrxDate,
        };
        setDataSummary([]);
        await getTiketSummaryByProductNameV2(data1)
            .then((res) => {
                if (res.success && res.result.length > 0) {
                    setDataSummary(res.result);
                    // console.log('Ticket Summary Success', res.result[0]);
                    notify(res.message || "Success Get Data Summary By Product Name", "success");
                } else {
                    notify("No Data Summary Found", "warning");
                }
            })
            .catch((error) => {
                setDataSummary([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error);
                    notify(error.message || error, "error");
                }
            });
    };

    const handleGetSearchData = async ({ limitDt, offsetDt, ouCodeValue, arrivalDateFrom, arrivalDateTo, terminalCode, inStatus, outStatus, isUsingTrxDate, keyword }) => {
        setSearchLoading(true);
        setDisableDownload(true);
        setOuCodeSelected(ouCodeValue);
        setTerminalCodeSelected(terminalCode);
        setTanggalAwalSelected(arrivalDateFrom);
        setTanggalAkhirSelected(arrivalDateTo);
        setStatusMasukSelected(inStatus);
        setStatusKeluarSelected(outStatus);
        setUseTrxSelected(isUsingTrxDate);
        setData([]);

        let body1 = {
            outletCode: ouCodeValue,
            arrivalDateFrom: arrivalDateFrom.format("YYYY-MM-DD"),
            arrivalDateTo: arrivalDateTo.format("YYYY-MM-DD"),
            limit: limitDt,
            offset: offsetDt,
            terminalCode,
            inStatus,
            outStatus,
            isUsingTrxDate,
            keyword,
        };
        let body2 = {
            outletCode: ouCodeValue,
            arrivalDateFrom: arrivalDateFrom.format("YYYY-MM-DD"),
            arrivalDateTo: arrivalDateTo.format("YYYY-MM-DD"),
            terminalCode,
            inStatus,
            outStatus,
            isUsingTrxDate,
            keyword,
        };

        await getTiketDetailV2(body1)
            .then((res) => {
                if (res.success) {
                    setData(res.result);
                    notify(res.message || "Success Get Data List", "success");
                }
            })
            .catch((error) => {
                setData([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });

        await getTiketSummaryV2(body2)
            .then((res) => {
                if (res.success) {
                    // console.log(res.result[0].totalTickets)
                    setTotalSearch(res.result[0].totalTickets);
                } else {
                    notify("No Data Summary Found", "warning");
                }
            })
            .catch((error) => {
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error);
                    notify(error.message || error, "error");
                }
            });
        setSearchLoading(false);
    };

    const handleSearch = async (value) => {
        setLimit(25);
        setOffset(0);
        if (value.length > 0) {
            !isSearch && setIsSearch(true);
            setLoading(true);
            await handleGetSearchData({
                limitDt: 25,
                offsetDt: 0,
                ouCodeValue: ouCodeSelected,
                arrivalDateFrom: tanggalAwalSelected,
                arrivalDateTo: tanggalAkhirSelected,
                terminalCode: terminalCodeSelected,
                inStatus: statusMasukSelected,
                outStatus: statusKeluarSelected,
                isUsingTrxDate: useTrxSelected,
                keyword: value,
            });
            setLoading(false);
        } else {
            setLoading(true);
            isSearch && setIsSearch(false);
            isSearch &&
                (await handleGetTiketData({
                    limitDt: 25,
                    offsetDt: 0,
                    ouCodeValue: ouCodeSelected,
                    arrivalDateFrom: tanggalAwalSelected,
                    arrivalDateTo: tanggalAkhirSelected,
                    terminalCode: terminalCodeSelected,
                    inStatus: statusMasukSelected,
                    outStatus: statusKeluarSelected,
                    isUsingTrxDate: useTrxSelected,
                }));
            setLoading(false);
        }
    };

    const pageChange = async (value) => {
        const ofset = value * limit; //index awal
        setOffset(ofset);
        // console.log(isSearch)
        if (isSearch) {
            setLoading(true);
            await handleGetSearchData({
                limitDt: limit,
                offsetDt: ofset,
                ouCodeValue: ouCodeSelected,
                arrivalDateFrom: tanggalAwalSelected,
                arrivalDateTo: tanggalAkhirSelected,
                terminalCode: terminalCodeSelected,
                inStatus: statusMasukSelected,
                outStatus: statusKeluarSelected,
                isUsingTrxDate: useTrxSelected,
                keyword: search,
            });
            setLoading(false);
        } else {
            setLoading(true);
            await handleGetTiketData({
                limitDt: limit,
                offsetDt: ofset,
                ouCodeValue: ouCodeSelected,
                arrivalDateFrom: tanggalAwalSelected,
                arrivalDateTo: tanggalAkhirSelected,
                terminalCode: terminalCodeSelected,
                inStatus: statusMasukSelected,
                outStatus: statusKeluarSelected,
                isUsingTrxDate: useTrxSelected,
            });
            setLoading(false);
        }
    };

    const rowsChange = async (e) => {
        setOffset(0);
        setLimit(e.props.value);
        if (isSearch) {
            setLoading(true);
            await handleGetSearchData({
                limitDt: e.props.value,
                offsetDt: 0,
                ouCodeValue: ouCodeSelected,
                arrivalDateFrom: tanggalAwalSelected,
                arrivalDateTo: tanggalAkhirSelected,
                terminalCode: terminalCodeSelected,
                inStatus: statusMasukSelected,
                outStatus: statusKeluarSelected,
                isUsingTrxDate: useTrxSelected,
                keyword: search,
            });
            setLoading(false);
        } else {
            setLoading(true);
            await handleGetTiketData({
                limitDt: e.props.value,
                offsetDt: 0,
                ouCodeValue: ouCodeSelected,
                arrivalDateFrom: tanggalAwalSelected,
                arrivalDateTo: tanggalAkhirSelected,
                terminalCode: terminalCodeSelected,
                inStatus: statusMasukSelected,
                outStatus: statusKeluarSelected,
                isUsingTrxDate: useTrxSelected,
            });
            setLoading(false);
        }
    };

    return (
        <React.Fragment>
            <Stack direction={"column"} p={"2rem"}>
                <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                    <CardContent sx={{ p: "2rem" }}>
                        <Box display="flex" flexDirection="column">
                            <Typography variant="h4" fontWeight="600">
                                {label}
                            </Typography>
                            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                                <Box
                                    sx={{
                                        display: "grid",
                                        gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(4, 1fr)"],
                                        gap: 2,
                                    }}
                                >
                                    <SelectField
                                        multiple
                                        label={"Merchant"}
                                        placeholder="Select Merchant"
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        data={merchantOption}
                                        selectedValue={merchant}
                                        setValue={(newValue) => {
                                            setMerchant(newValue);
                                            setErrormerchant(false);
                                        }}
                                        required={true}
                                        isError={errormerchant}
                                        errorMessage={"Merchant is required"}
                                        limitTags={1}
                                    />

                                    <SelectField2
                                        label={"Terminal"}
                                        placeholder={terminalOption.length > 0 ? "All Terminal" : "Please Select Merchant"}
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        data={terminalOption}
                                        selectedValue={terminal}
                                        setValue={setTerminal}
                                        inputValue={terminalInput}
                                        setInputValue={setTerminalInput}
                                    />

                                    <DatePickerField
                                        label={"Date Range"}
                                        placeholder="DD MMM YYYY"
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        value={tanggalAwal}
                                        onChange={(newValue) => {
                                            setTanggalAwal(newValue);
                                            setErrorTanggalAwal(false);
                                        }}
                                        required={true}
                                        format={"DD MMM YYYY"}
                                        isError={errorTanggalAwal}
                                        errorMessage={"Starting Date is required"}
                                    />

                                    <DatePickerField
                                        label={<span />}
                                        placeholder="DD MMM YYYY"
                                        sx={{ width: "100%", fontSize: "16px", marginTop: ["-1.25rem", "0"] }}
                                        value={tanggalAkhir}
                                        onChange={(newValue) => {
                                            setTanggalAkhir(newValue);
                                            setErrorTanggalAkhir(false);
                                        }}
                                        format={"DD MMM YYYY"}
                                        isError={errorTanggalAkhir}
                                        errorMessage={"End Date required"}
                                    />

                                    <SelectField label={"In Status"} placeholder="All Status" sx={{ width: "100%", fontSize: "16px" }} data={statusMasukOption} selectedValue={statusMasuk} setValue={setStatusMasuk} />
                                    <SelectField label={"Out Status"} placeholder="All Status" sx={{ width: "100%", fontSize: "16px" }} data={statusKeluarOption} selectedValue={statusKeluar} setValue={setStatusKeluar} />
                                    <SwitchField label={"By Transaction Date ?"} selectedValue={useTrx} setValue={setUseTrx} />
                                </Box>
                            </Stack>
                            <Stack
                                sx={{
                                    width: "100%",
                                    display: "flex",
                                    flexDirection: ["column", "row"],
                                    alignItems: ["end", "flex-start"],
                                    gap: 3,
                                    justifyContent: "space-between",
                                    mt: "2rem",
                                }}
                            >
                                <FilterMessageNote
                                    sx={{
                                        width: ["100%", "50%"],
                                    }}
                                    title={titleInfo}
                                    subtitle={subTitleInfo}
                                />
                                <div
                                    style={{
                                        display: "flex",
                                        gap: 10,
                                    }}
                                >
                                    <ExportButton
                                        disabled={disableDownload}
                                        setDisableDownload={setDisableDownload}
                                        ouCodeValue={ouCodeSelected}
                                        cid={ouCodeSelected}
                                        arrivalDateFrom={tanggalAwalSelected}
                                        arrivalDateTo={tanggalAkhirSelected}
                                        terminalCode={terminalCodeSelected}
                                        inStatus={statusMasukSelected}
                                        outStatus={statusKeluarSelected}
                                        isUsingTrxDate={useTrxSelected}
                                        limit={totalTiket}
                                        notify={notify}
                                        color="success"
                                    />
                                    <CustomButton
                                        onClick={() => {
                                            setLimit(25);
                                            setOffset(0);
                                            checkFilter({
                                                limitDt: 25,
                                                offsetDt: 0,
                                                ouCodeValue: merchant.length > 0 ? merchant : [],
                                                arrivalDateFrom: tanggalAwal,
                                                arrivalDateTo: tanggalAkhir,
                                                terminalCode: terminal ? terminal.value : "",
                                                inStatus: statusMasuk ? statusMasuk.value : "",
                                                outStatus: statusKeluar ? statusKeluar.value : "",
                                                isUsingTrxDate: useTrx,
                                            });
                                        }}
                                        startIcon={<SearchIcon size="14px" />}
                                        name={buttonFilter}
                                    />
                                </div>
                            </Stack>
                            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                                <Typography variant="h6" fontWeight="600">
                                    Ticket Summary
                                </Typography>
                                <Stack
                                    sx={{
                                        display: "grid",
                                        gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(3, 1fr)"],
                                        gap: 2,
                                    }}
                                >
                                    <CardTicketSummary title="Total Ticket" amount={totalTiket ? totalTiket : 0} isCurrency={false} isLoading={summaryLoading} src={<ConfirmationNumberIcon sx={{ fontSize: "30px", color: "#3875CA" }} />} />

                                    <CardTicketSummary title="Validated Ticket In" amount={tiketMasuk ? tiketMasuk : 0} isCurrency={false} isLoading={summaryLoading} src={<OutputIcon sx={{ fontSize: "30px", color: "#3875CA" }} />} />

                                    <CardTicketSummary
                                        title="Validated Ticket Out"
                                        amount={tiketKeluar ? tiketKeluar : 0}
                                        isCurrency={false}
                                        isLoading={summaryLoading}
                                        src={<OutputIcon sx={{ fontSize: "30px", color: "#3875CA", transform: "rotate(180deg)" }} />}
                                    />
                                    <CardTicketSummary
                                        title="Self Kios Ticket"
                                        amount={selfkiosTickets ? selfkiosTickets : 0}
                                        isCurrency={false}
                                        isLoading={summaryLoading}
                                        src={<ConfirmationNumberIcon sx={{ fontSize: "30px", color: "#3875CA" }} />}
                                    />
                                    <CardTicketSummary
                                        title="Online Tickets"
                                        amount={onlineTickets ? onlineTickets : 0}
                                        isCurrency={false}
                                        isLoading={summaryLoading}
                                        src={<ConfirmationNumberIcon sx={{ fontSize: "30px", color: "#3875CA" }} />}
                                    />
                                    <CardTicketSummary
                                        title="Onsite Tickets"
                                        amount={onsiteTickets ? onsiteTickets : 0}
                                        isCurrency={false}
                                        isLoading={summaryLoading}
                                        src={<ConfirmationNumberIcon sx={{ fontSize: "30px", color: "#3875CA" }} />}
                                    />
                                </Stack>
                            </Stack>
                            <Box sx={{ width: "100%" }} mt={"2rem"}>
                                <CustomTab
                                    dataTabList={[
                                        {
                                            label: "Detail Report",
                                            table: (
                                                <Stack>
                                                    <Box display="flex" flexDirection={["row"]} width={"100%"} gap={1} justifyContent={"end"} alignItems={"center"}>
                                                        <SearchField
                                                            value={search}
                                                            setValue={setSearch}
                                                            sx={{ width: ["100%", "100%", "40%"], height: "100%" }}
                                                            variant={"outlined"}
                                                            showIcon={search.length > 0}
                                                            disabled={ouCodeSelected && tanggalAwalSelected && tanggalAkhirSelected && !searchLoading ? false : true}
                                                        />
                                                        <Button
                                                            sx={{ height: "100%", width: "fit-content", padding: 1 }}
                                                            onClick={() => handleSearch(search)}
                                                            disabled={ouCodeSelected && tanggalAwalSelected && tanggalAkhirSelected && !searchLoading ? false : true}
                                                            variant="contained"
                                                        >
                                                            <SearchIcon size="14px" />
                                                        </Button>
                                                    </Box>
                                                    <CustomPagination
                                                        limit={limit}
                                                        countLoading={false}
                                                        offset={offset}
                                                        count={isSearch ? totalSearch : totalTiket}
                                                        pageChange={(event, v) => pageChange(v)}
                                                        rowsChange={async (event, e) => rowsChange(e)}
                                                    />
                                                    <CustomTable headers={header} items={data} renderCell={renderCell} />
                                                    <CustomPagination
                                                        limit={limit}
                                                        countLoading={false}
                                                        offset={offset}
                                                        count={isSearch ? totalSearch : totalTiket}
                                                        pageChange={(event, v) => pageChange(v)}
                                                        rowsChange={async (event, e) => rowsChange(e)}
                                                    />
                                                </Stack>
                                            ),
                                        },
                                        {
                                            label: "Summary By Product Name",
                                            table: <CustomTable headers={header2} items={dataSummary} renderCell={renderCell2} />,
                                        },
                                    ]}
                                />
                            </Box>
                        </Box>
                    </CardContent>
                </Card>
            </Stack>

            <DialogQR open={openDialogQr} setOpen={setOpenDialogQr} qrcode={selectedQr} setQrCode={setSelectedQr} />
        </React.Fragment>
    );
};

export default LaporanTiket;
