import * as React from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import QRCode from "react-qr-code";
import { Box, DialogActions, DialogContent } from '@mui/material';
import CustomButton from '../../../../components/custom-button';
import CancelIcon from "@mui/icons-material/Cancel";

export default function DialogQR({qrcode = '', open = false, setOpen = () => {}, setQrCode = () => {}}) {
  const handleClose = () => {
    setQrCode('');
    setOpen(false);
  };

  return (
    <Dialog onClose={handleClose} open={open} fullWidth={true} maxWidth={'sm'}>
      <DialogTitle sx={{justifyContent: 'center', display: 'flex'}}>{qrcode}</DialogTitle>
      <DialogContent>
        <Box sx={{padding: '24px', display: 'flex', justifyContent: 'center'}} >
          <QRCode
            style={{ height: "auto", width: "60%"}}
            value={qrcode}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <CustomButton
          onClick={() => handleClose()}
          startIcon={<CancelIcon size="14px" />}
          name={"Close"}
          sx={{
              backgroundColor: "error.main",
              ":hover": {
                  backgroundColor: "error.light"
              }
          }}
        />
      </DialogActions>
    </Dialog>
  );
}