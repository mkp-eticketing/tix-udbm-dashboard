import React, { useState } from "react"
import { Box, InputAdornment, TextField, Typography } from "@mui/material";
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import EventIcon from '@mui/icons-material/Event';
import 'moment/locale/id';

const DatePickerField = ({
    label,
    placeholder = 'DD MMM YYYY',
    errorMessage = "Tanggal wajib diisi",
    isError = false,
    valueStartDate,
    valueEndDate,
    onChangeStartDate,
    onChangeEndDate,
    format = 'DD MMM YYYY',
    minStartDate,
    maxEndDate,
    ...other
}) => {
    const [openStart, setOpenStart] = useState(false)
    const [openEnd, setOpenEnd] = useState(false)
    return (
        <Box sx={{ display: "flex", flexDirection: "column", gap: 1, width: "100%", gridColumn: 'span 2' }}>
            {
                label && <Typography color={"rgb(71 85 105)"} fontSize="0.875rem" fontWeight={"600"} height={25}>{label} <span style={{ color: 'red' }}>{`${other.required ? "*" : ""}`}</span>
                </Typography>
            }
            <Box sx={{ display: "flex", flexDirection: "row", gap: 1, width: "100%", alignItems: 'center' }}>
              <LocalizationProvider dateAdapter={AdapterMoment} adapterLocale="de">
                  <DatePicker
                      slots={{
                          openPickerIcon: () => {
                              return null;
                          }
                      }}
                      slotProps={{
                          textField: {
                              variant: "standard",
                              onClick: () => setOpenStart(true),
                              placeholder: placeholder,
                              InputProps: {
                                  startAdornment: (
                                      <InputAdornment position="start">
                                          <EventIcon />
                                      </InputAdornment>
                                  )
                              }
                          },
                      }}
                      open={openStart}
                      onClose={() => setOpenStart(false)}
                      value={valueStartDate}
                      onChange={onChangeStartDate}
                      format={format}
                      minDate={minStartDate}
                      {...other}
                  />
              </LocalizationProvider>
              <Typography fontSize="1.3rem">-</Typography>
              <LocalizationProvider dateAdapter={AdapterMoment} adapterLocale="de">
                  <DatePicker
                      slots={{
                          openPickerIcon: () => {
                              return null;
                          }
                      }}
                      slotProps={{
                          textField: {
                              variant: "standard",
                              onClick: () => setOpenEnd(true),
                              placeholder: placeholder,
                              InputProps: {
                                  startAdornment: (
                                      <InputAdornment position="start">
                                          <EventIcon />
                                      </InputAdornment>
                                  )
                              }
                          },
                      }}
                      open={openEnd}
                      onClose={() => setOpenEnd(false)}
                      value={valueEndDate}
                      onChange={onChangeEndDate}
                      format={format}
                      maxDate={maxEndDate}
                      {...other}
                  />
              </LocalizationProvider>
            </Box>
            <Typography fontSize={12} color="red">{isError && errorMessage}</Typography>
        </Box>
    )
}

export default DatePickerField;