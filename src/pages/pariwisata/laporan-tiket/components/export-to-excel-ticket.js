import ExcelJS from "exceljs";
import { saveAs } from "file-saver";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { getTiketSummaryV2, getTiketSummaryByProductNameV2, getTiketDetailV2 } from "../../../../services/pariwisata/tiket";
import React from "react";
import { formatCurrency } from "../../components/custom-format-currency";

const ExportToExcel = ({ disabled, setDisableDownload, ouCodeValue, arrivalDateFrom, arrivalDateTo, terminalCode, inStatus, outStatus, notify, isUsingTrxDate, limit }) => {
    const handleFetch = async () => {
        setDisableDownload(true);
        let data1 = {
            outletCode: ouCodeValue,
            arrivalDateFrom: arrivalDateFrom.format("YYYY-MM-DD"),
            arrivalDateTo: arrivalDateTo.format("YYYY-MM-DD"),
            terminalCode,
            inStatus,
            outStatus,
            isUsingTrxDate,
        };
        let data2 = {
            outletCode: ouCodeValue,
            arrivalDateFrom: arrivalDateFrom.format("YYYY-MM-DD"),
            arrivalDateTo: arrivalDateTo.format("YYYY-MM-DD"),
            terminalCode,
            limit,
            offset: 0,
            inStatus,
            outStatus,
            isUsingTrxDate,
        };

        try {
            const summaryResultByName = await getTiketSummaryByProductNameV2(data1);
            if (summaryResultByName && summaryResultByName.result) {
                // console.log('Ticket Summary By Name Success');
                notify(summaryResultByName.message || "Success Get Data Summary By Name", "success");
            } else {
                // console.log('No Data Summary By Name Found');
                notify("No Data Summary By Name Found", "warning");
            }

            const summaryResult = await getTiketSummaryV2(data1);
            if (summaryResult && summaryResult.result) {
                // console.log('Ticket Summary Success');
                notify(summaryResult.message || "Success Get Data Summary", "success");
            } else {
                // console.log('No Data Summary Found');
                notify("No Data Summary Found", "warning");
            }

            let modifiedDetailResult;
            const detailResult = await getTiketDetailV2(data2);
            if (detailResult && detailResult.result) {
                // console.log('Ticket Detail Success');
                notify(detailResult.message || "Success Get Data List", "success");
                modifiedDetailResult = detailResult.result.map((data) => ({
                    ID: data["id"],
                    "Transaction Number": data["noHeader"],
                    "QR Code": data["qrCode"],
                    "Product Name": data["productName"],
                    "Device ID": data["deviceID"],
                    Price: formatCurrency(data["price"]),
                    "Effective Date": data["tanggalBerlaku"],
                    "Expired Date": data["tanggalExpired"],
                    Platform: data["platform"],
                    "Customer Type": data["customerType"],
                    "In Status": data["inStatus"] ? "Used" : "Not Used",
                    "In Date": data["tanggalPakai"],
                    "Out Status": data["outStatus"] ? "Out" : "Not Out",
                    "Out Date": data["tanggalKeluar"],
                    "Terminal Code": data["kodeTerminal"],
                    "Terminal Name": data["namaTerminal"],
                    "Visitor Name": data["visitorName"],
                    "Visitor ID Type": data["visitorIDType"],
                    "Visitor ID": data["visitorID"],
                    "Visitor Phone": data["visitorPhone"],
                    "Visitor Email": data["visitorEmail"],
                    "Visitor Gender": data["visitorGender"],
                    "Visitor Region": data["visitorRegion"],
                    Remark: data["remark"],
                    "Corporate Name" : data["corporateName"]
                }));
            } else {
                // console.log('No Data List Found');
                notify("No Data List Found", "warning");
            }
            // console.log(modifiedDetailResult)
            if (modifiedDetailResult && summaryResult && summaryResultByName) {
                exportToExcel(modifiedDetailResult, summaryResult.result, summaryResultByName.result);
            }
        } catch (error) {
            setDisableDownload(false);
            if (error.response) {
                // console.log(error.response.data.message);
                notify(error.response.data.message, "error");
            } else {
                // console.log(error.message || error);
                notify(error.message || error, "error");
            }
        }
    };

    const exportToExcel = (data1, data2, data3) => {
        const workbook = new ExcelJS.Workbook();

        const ws1 = workbook.addWorksheet("List Detail Ticket");
        const ws2 = workbook.addWorksheet("Summary Ticket");
        const ws3 = workbook.addWorksheet("Summary Ticket By Product Name");

        ws1.columns = [
            { header: "ID", key: "ID", width: 10 },
            { header: "Corporate Name", key: "Corporate Name", width: 35 },
            { header: "Transaction Number", key: "Transaction Number", width: 35 },
            { header: "QR Code", key: "QR Code", width: 35 },
            { header: "Product Name", key: "Product Name", width: 35 },
            { header: "Device ID", key: "Device ID", width: 35 },
            { header: "Price", key: "Price", width: 35 },
            { header: "Platform", key: "Platform", width: 35 },
            { header: "Customer Type", key: "Customer Type", width: 35 },
            { header: "Effective Date", key: "Effective Date", width: 20 },
            { header: "Expired Date", key: "Expired Date", width: 20 },
            { header: "In Status", key: "In Status", width: 20 },
            { header: "In Date", key: "In Date", width: 20 },
            { header: "Out Status", key: "Out Status", width: 20 },
            { header: "Out Date", key: "Out Date", width: 20 },
            { header: "Terminal Code", key: "Terminal Code", width: 20 },
            { header: "Terminal Name", key: "Terminal Name", width: 45 },
            { header: "Visitor ID Type", key: "Visitor ID Type", width: 35 },
            { header: "Visitor ID", key: "Visitor ID", width: 35 },
            { header: "Visitor Phone", key: "Visitor Phone", width: 35 },
            { header: "Visitor Email", key: "Visitor Email", width: 35 },
            { header: "Visitor Gender", key: "Visitor Gender", width: 35 },
            { header: "Visitor Region", key: "Visitor Region", width: 35 },
            { header: "Remark", key: "Remark", width: 35 },
        ];
        ws1.addRows(data1);

        ws2.columns = [
            { header: "Total Tickets", key: "totalTickets", width: 25 },
            { header: "In Tickets", key: "inTickets", width: 25 },
            { header: "Out Tickets", key: "outTickets", width: 25 },
            { header: "Self Kios Tickets", key: "selfKiosTickets", width: 25 },
            { header: "Online Tickets", key: "onlineTickets", width: 25 },
            { header: "Onsite Tickets", key: "onsiteTickets", width: 25 },
        ];
        ws2.addRows(data2);

        ws3.columns = [
            { header: "Product Name", key: "productName", width: 25 },
            { header: "Total Tickets", key: "totalTickets", width: 25 },
            { header: "In Tickets", key: "inTickets", width: 25 },
            { header: "Out Tickets", key: "outTickets", width: 25 },
            { header: "Self Kios Tickets", key: "selfKiosTickets", width: 25 },
            { header: "Online Tickets", key: "onlineTickets", width: 25 },
            { header: "Onsite Tickets", key: "onsiteTickets", width: 25 },
        ];
        ws3.addRows(data3);

        [ws1, ws2, ws3].forEach((worksheet) => {
            worksheet.eachRow({ includeEmpty: false }, function (row, rowNumber) {
                row.eachCell((cell) => {
                    cell.border = {
                        top: { style: "thin" },
                        left: { style: "thin" },
                        bottom: { style: "thin" },
                        right: { style: "thin" },
                    };
                    cell.alignment = { horizontal: "left" };
                });
                if (rowNumber === 1) {
                    row.eachCell((cell) => {
                        cell.font = { bold: true };
                        cell.alignment = { horizontal: "center" };
                        cell.fill = {
                            type: "pattern",
                            pattern: "solid",
                            fgColor: { argb: "FF3875CA" },
                        };
                    });
                }
            });
        });

        workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer]), `Ticket-Summary (${arrivalDateFrom.format("YYYY_MM_DD")} - ${arrivalDateTo.format("YYYY_MM_DD")}).xlsx`);
            notify("Success Export Data", "success");
            setDisableDownload(false);
        });
    };

    return (
        <CustomButton onClick={handleFetch} startIcon={<FileDownloadIcon size="20px" />} name="Export&nbsp;" loading={disabled} color="success">
            Download
        </CustomButton>
    );
};

export default ExportToExcel;
