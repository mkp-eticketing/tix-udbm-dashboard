import ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { getTrxList, getTrxByUser} from "../../../../services/pariwisata/transaction";
import React from "react";
import { formatCurrency } from "../../components/custom-format-currency";
import { thousandSeparator } from '../../../../utils/thousand-separator';
import { offset } from 'highcharts';

const ExportToExcel = ({ disabled, setDisableDownload, ouCodeValue, dateFrom, dateTo, paymentCategory, notify }) => {
  const handleFetch = async () => {
    setDisableDownload(true);
    let data1 = {
      outletCode: ouCodeValue,
      transactionDateStart: dateFrom.startOf('day').format("YYYY-MM-DD HH:mm:ss"),
      transactionDateTo: dateTo.endOf('day').format("YYYY-MM-DD HH:mm:ss"),
      paymentStatus : ["SUCCESS"],
      paymentCategory : paymentCategory ? [paymentCategory] : [],
      limit: 0,
      offset: 0
    };
    let data2 = {
      outletCode: ouCodeValue,
      transactionDateStart: dateFrom.startOf('day').format("YYYY-MM-DD HH:mm:ss"),
      transactionDateTo: dateTo.endOf('day').format("YYYY-MM-DD HH:mm:ss"),
      paymentStatus : ["SUCCESS"],
      paymentCategory : paymentCategory ? [paymentCategory] : []
    };
    // console.log(data1)

    try {
      const trxList = await getTrxList(data1);
      let modifiedData1
      if (trxList && trxList.result) {
        // console.log('Ticket Summary Success');
        modifiedData1 = trxList.result.map((data) => ({
          ...data,
          "amount" : formatCurrency(data['amount']),
          "serviceFee" : formatCurrency(data['serviceFee']),
          "paymentFee" : formatCurrency(data['paymentFee']),
          "lastBalance" : formatCurrency(data['lastBalance']),
          "currentBalance" : formatCurrency(data['currentBalance']),
        }))
        notify(trxList.message || "Success Get Data By Product", "success");
      } else {
        // console.log('No Data Summary Found');
        notify("No Data Summary Found", "warning");
      };

      const trxByUser = await getTrxByUser(data2);
      let modifiedData2
      if (trxByUser && trxByUser.result) {
        // console.log('Ticket Summary By Name Success');
        modifiedData2 = trxByUser.result.map((data) => ({
          ...data,
          "quantity": thousandSeparator(data['quantity']),
          "totalAmount" : formatCurrency(data['totalAmount']),
        }))
        notify(trxByUser.message || "Success Get Data By Product & Payment Method", "success");
      } else {
        // console.log('No Data Summary By Name Found');
        notify("No Data Summary By Name Found", "warning");
      };

      if(modifiedData1 && modifiedData2){
        exportToExcel( modifiedData1, modifiedData2)
      }
    } catch (error) {
      setDisableDownload(false)
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    }
  };

  const exportToExcel = (data1, data2) => {
    const workbook = new ExcelJS.Workbook();
    
    const ws1 = workbook.addWorksheet("Transaction List");
    const ws2 = workbook.addWorksheet("Transaction By User");

    ws1.columns = [
      { header: "Core Ticketing Ref", key: "coreTicketingRef", width: 45 },
      { header: "MKP Transaction Number", key: "vendorRequestRef", width: 25 },
      { header: "Partner Reservation Number", key: "vendorResponseRef", width: 25 },
      { header: "Partner Invoice Number", key: "vendorInvoiceRef", width: 25 },
      { header: "Merchant", key: "corporateName", width: 30 },
      { header: "Transaction Date", key: "transactionDate", width: 25 },
      { header: "Payment Status", key: "paymentStatus", width: 25 },
      { header: "Payment Method", key: "paymentCategory", width: 25 },
      { header: "Payment Ref", key: "paymentRef", width: 25 },
      { header: "Amount", key: "amount", width: 25 },
      { header: "Discount", key: "discount", width: 25 },
      { header: "Promo Code", key: "promoCode", width: 25 },
      { header: "Username", key: "username", width: 25 },
      { header: "Device ID", key: "deviceid", width: 25 },
      { header: "Card Pan", key: "cardPan", width: 25 },
      { header: "Card Type", key: "cardType", width: 25 },
      { header: "Last Balance", key: "lastBalance", width: 25 },
      { header: "Current Balance", key: "currentBalance", width: 25 },
    ];
  
    ws1.addRows(data1);
    
    ws2.columns = [
      { header: 'Corporate', key: 'corporate', width: 35 },
      { header: 'Device ID', key: 'deviceId', width: 35 },
      { header: 'Username', key: 'username', width: 25 },
      { header: 'Quantity', key: 'quantity', width: 25 },
      { header: 'Total Amount', key: 'totalAmount', width: 25 },
  ];
    ws2.addRows(data2);
    
    [ws1, ws2].forEach(worksheet => {
      worksheet.eachRow({ includeEmpty: false }, function(row, rowNumber) {
          row.eachCell(cell => {
              cell.border = {
                  top: {style:'thin'},
                  left: {style:'thin'},
                  bottom: {style:'thin'},
                  right: {style:'thin'}
              };
              cell.alignment = { horizontal: 'left' }
          });
          if (rowNumber === 1) {
              row.eachCell(cell => {
                  cell.font = { bold: true };
                  cell.alignment = { horizontal: 'center' };
                  cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FF3875CA' }
                  };
              });
          }
      });
    });

    workbook.xlsx.writeBuffer()
    .then(buffer => {
        saveAs(new Blob([buffer]), `Ticket-Transaction (${dateFrom.format("YYYY_MM_DD")} - ${dateTo.format("YYYY_MM_DD")}).xlsx`);
        setDisableDownload(false); 
    })
  };

  return (
    <CustomButton
      onClick={handleFetch}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={disabled}
      color='success'
    >
      Download
    </CustomButton>
  );
};

export default ExportToExcel;
