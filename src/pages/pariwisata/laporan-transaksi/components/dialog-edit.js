import React, { useState, useEffect } from "react";

import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import { Box, DialogActions, DialogContent, Divider, Stack, Typography } from "@mui/material";
import CustomButton from "../../../../components/custom-button";
import CancelIcon from "@mui/icons-material/Cancel";
import SaveIcon from "@mui/icons-material/Save";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { formatCurrency } from "../../components/custom-format-currency";
import { thousandSeparator } from "../../../../utils/thousand-separator";
import TextField from "../../../../components/text-field";
import { updateInvoiceVendorRef } from "../../../../services/pariwisata/transaction";

export default function DialogEdit({ open = false, setOpen = () => {}, data = "", setData = () => {}, notify = () => {}, callback = () => {} }) {
    const handleClose = () => {
        setOpen(false);
        setData(null);
        setInvoiceRef("");
        setRequestRef("");
        setResponseRef("");
        setErrorInvoice(false);
        setErrorRequest(false);
        setErrorResponse(false);
    };

    const [invoiceRef, setInvoiceRef] = useState("");
    const [requestRef, setRequestRef] = useState("");
    const [responseRef, setResponseRef] = useState("");
    const [errorInvoice, setErrorInvoice] = useState(false);
    const [errorResponse, setErrorResponse] = useState(false);
    const [errorRequest, setErrorRequest] = useState(false);

    useEffect(() => {
        if (data) {
            setRequestRef(data.vendorRequestRef || data.coreTicketingRef);
            setResponseRef(data.vendorResponseRef);
            setInvoiceRef(data.vendorInvoiceRef);
        }
    }, [data]);

    const handleSubmit = () => {
        if (invoiceRef && requestRef && responseRef) {
            const mData = {
                bookingRef: data.coreTicketingRef,
                requestVendorRef: requestRef,
                responseVendorRef: responseRef,
                responseVendorInvoiceRef: invoiceRef,
            };
            updateInvoiceVendorRef(mData)
                .then((res) => {
                    if (res.success) {
                        handleClose();
                        notify(res.message || "Success Update Ref", "success");
                        callback();
                    }
                })
                .catch((error) => {
                    if (error.response) {
                        notify(error.response.data.message, "error");
                    } else {
                        notify(error.message || error, "error");
                    }
                });
        } else {
            if (!invoiceRef) {
                setErrorInvoice(true);
            }
            if (!requestRef) {
                setErrorRequest(true);
            }
            if (!responseRef) {
                setErrorResponse(true);
            }
        }
    };

    if (data) {
        return (
            <Dialog onClose={handleClose} open={open} fullWidth={true} maxWidth={"sm"}>
                <DialogContent>
                    <Stack sx={{ gap: 2 }}>
                        <Typography fontWeight="600" sx={{ fontSize: "1rem" }}>
                            Core Ticketing Ref : {data.coreTicketingRef ? data.coreTicketingRef : "-"}
                        </Typography>
                        <Divider />
                        <TextField
                            required
                            placeholder="Input Here"
                            label={"MKP Transaction Number"}
                            value={requestRef}
                            onChange={(e) => {
                                setErrorRequest(false);
                                setRequestRef(e.target.value);
                            }}
                            isError={errorRequest}
                            errorMessage={"MKP Transaction Number is required"}
                        />
                        <TextField
                            required
                            placeholder="Input Here"
                            label={"Partner Reservation Number"}
                            value={responseRef}
                            onChange={(e) => {
                                setErrorResponse(false);
                                setResponseRef(e.target.value);
                            }}
                            isError={errorResponse}
                            errorMessage={"Partner Reservation Number is required"}
                        />
                        <TextField
                            required
                            placeholder="Input Here"
                            label={"Partner Invoice Number"}
                            value={invoiceRef}
                            onChange={(e) => {
                                setErrorInvoice(false);
                                setInvoiceRef(e.target.value);
                            }}
                            isError={errorInvoice}
                            errorMessage={"Partner Invoice Number is required"}
                        />
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <CustomButton
                        onClick={() => handleClose()}
                        startIcon={<CancelIcon size="14px" />}
                        name={"Close"}
                        sx={{
                            backgroundColor: "error.main",
                            ":hover": {
                                backgroundColor: "error.light",
                            },
                        }}
                    />
                    <CustomButton onClick={() => handleSubmit()} startIcon={<SaveIcon size="14px" />} name={"Submit"} />
                </DialogActions>
            </Dialog>
        );
    }
}
