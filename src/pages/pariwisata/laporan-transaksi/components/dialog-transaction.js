import * as React from "react";
import Dialog from "@mui/material/Dialog";
import { Button, DialogActions, DialogContent, Stack, Typography } from "@mui/material";
import CustomButton from "../../../../components/custom-button";
import CancelIcon from "@mui/icons-material/Cancel";
import EditIcon from "@mui/icons-material/Edit";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { formatCurrency } from "../../components/custom-format-currency";
import { thousandSeparator } from "../../../../utils/thousand-separator";

export default function DialogTransaction({ open = false, setOpen = () => {}, data = [], setData = () => {}, buttonEditAction = () => {} }) {
    const handleClose = () => {
        setOpen(false);
        setData([]);
    };

    return (
        <Dialog onClose={handleClose} open={open} fullWidth={true} maxWidth={"sm"}>
            <DialogContent>
                <Stack sx={{ gap: 1 }}>
                    <Typography fontWeight="500" sx={{ fontSize: "1rem" }}>
                        Corporate Name : {data.length > 0 && data[0].corporateName}
                    </Typography>
                    <Typography fontWeight="500" sx={{ fontSize: "1rem" }}>
                        Core Ticketing Ref : {data.length > 0 && data[0].coreTicketingRef}
                    </Typography>
                    <Typography fontWeight="500" sx={{ fontSize: "1rem" }}>
                        Product List :{" "}
                    </Typography>
                    {data.length > 0 &&
                        data.map((obj, index) => {
                            return (
                                <Card key={index}>
                                    <CardContent>
                                        <Typography fontWeight="500" sx={{ fontSize: "1rem" }}>
                                            Product Name : {obj.productName}
                                        </Typography>
                                        <Typography fontWeight="500" sx={{ fontSize: "1rem" }}>
                                            Quantity : {thousandSeparator(obj.qty)}
                                        </Typography>
                                        <Typography fontWeight="500" sx={{ fontSize: "1rem" }}>
                                            Amount : {formatCurrency(obj.amount)}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            );
                        })}
                </Stack>
            </DialogContent>
            <DialogActions>
                <CustomButton
                    onClick={() => {
                        handleClose()
                        buttonEditAction()
                    }}
                    startIcon={<EditIcon size="14px" />}
                    name={"Edit Ref"}
                />
                <CustomButton
                    onClick={() => handleClose()}
                    startIcon={<CancelIcon size="14px" />}
                    name={"Close"}
                    sx={{
                        backgroundColor: "error.main",
                        ":hover": {
                            backgroundColor: "error.light",
                        },
                    }}
                />
            </DialogActions>
        </Dialog>
    );
}
