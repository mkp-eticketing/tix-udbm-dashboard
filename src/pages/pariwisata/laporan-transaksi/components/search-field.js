import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import CancelIcon from '@mui/icons-material/Cancel';

const SearchField = ({
  showIcon = false, 
  setValue = () => {},
  handleSearch = () => {},
  sx = {},
  disabled = false,
  value = ''
}) => {
  return (
    <FormControl sx={{ ...sx }} variant="outlined" size='small'>
        <InputLabel htmlFor="search">Search</InputLabel>
        <OutlinedInput
          id="search"
          type={'text'}
          value={value}
          endAdornment={ showIcon && 
            <InputAdornment position="end">
              <IconButton
                onClick={() => setValue('')}
                edge="end"
              >
                <CancelIcon sx={{color: '#000000', opacity: '20%'}} />
              </IconButton>
            </InputAdornment>
          }
          onChange={(e) => setValue(e.target.value)}
          onKeyDown={(e) =>
            e.key === "Enter" && handleSearch()
          }
          disabled={disabled}
          label="Search"
        />
      </FormControl>
  );
}
 
export default SearchField;