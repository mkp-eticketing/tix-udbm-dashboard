import React, { useEffect, useState } from "react";
import { Box, Button, Card, CardContent, Stack, Typography } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import EditIcon from "@mui/icons-material/Edit";
import { getPaymentList, getTrxList, getTrxByUser, getTrxDetail } from "../../../services/pariwisata/transaction";
import SelectField from "../../../components/select-field";
import SelectField2 from "../../../components/select-field-2";
import CustomTable from "../../../components/custom-table";
import CustomTableSort from "../../../components/custom-table-sort";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import ExportButton from "./components/export-to-excel-transaction";
import DatePickerField from "../../../components/datepicker-field";
import CustomTab from "../components/custom-tab";
import { formatCurrency } from "../components/custom-format-currency";
import { thousandSeparator } from "../../../utils/thousand-separator";
import moment from "moment";
import CustomPagination from "../../../components/custom-pagination";
import DialogTransaction from "./components/dialog-transaction";
import DialogEdit from "./components/dialog-edit";
import SearchField from "./components/search-field";
import CardSummary from "../components/card-summary";
import ReceiptRoundedIcon from '@mui/icons-material/ReceiptRounded';

const LaporanTransaksi = ({
    label = "Transaction Report",
    titleInfo = "To Display & Download Specific Transactions, Use the Filters Above.",
    subTitleInfo = ["Max Range of Date is 1 Month (31 Days)"],
    merchantData = [],
    setLoading = () => {},
    notify = () => {},
    buttonFilter = "Search",
    avatarPayment = () => {},
}) => {
    const [disableDownload, setDisableDownload] = useState(true);
    const [dataTable1, setDataTable1] = useState([]);
    const [allDataTable1, setAllDataTable1] = useState([]);
    const [dataTable2, setDataTable2] = useState([]);

    const [merchant, setMerchant] = useState([]);
    const [tanggalAwal, setTanggalAwal] = useState(moment().subtract(7, "days"));
    const [tanggalAkhir, setTanggalAkhir] = useState(moment());
    const [payment, setPayment] = useState(null);

    const [merchantOption, setMerchantOption] = useState([]);
    const [paymentOption, setPaymentOption] = useState([]);

    //untuk menyimpan data filter yang sudah di search
    const [ouCodeSelected, setOuCodeSelected] = useState("");
    const [tanggalAwalSelected, setTanggalAwalSelected] = useState("");
    const [tanggalAkhirSelected, setTanggalAkhirSelected] = useState("");
    const [paymentSelected, setPaymentSelected] = useState(false);

    const [errormerchant, setErrormerchant] = useState(false);
    const [errorTanggalAwal, setErrorTanggalAwal] = useState(false);
    const [errorTanggalAkhir, setErrorTanggalAkhir] = useState(false);

    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState();
    const [totalTransaksi, setTotalTransaksi] = useState(0);
    const [dialogDetail, setDialogDetail] = useState(false);
    const [dataDetail, setDataDetail] = useState([]);

    const [dialogEdit, setDialogEdit] = useState(false);
    const [dataEdit, setDataEdit] = useState();

    const [search, setSearch] = useState("");
    const [isSearch, setIsSearch] = useState(false);
    const [totalSearch, setTotalSearch] = useState(0);
    const [searchLoading, setSearchLoading] = useState(false);

    const [order, setOrder] = useState("asc");
    const [orderBy, setOrderBy] = useState("");

    const header = [
        {
            title: "No",
            value: "no",
            align: "left",
            width: "30px",
            sort: true
        },
        {
            title: "Core Ticketing Ref",
            value: "coreTicketingRef",
            align: "left",
            width: "300px",
            sort: true
        },
        // {
        //     title: "MKP Transaction Number",
        //     value: "vendorRequestRef",
        //     align: "left",
        //     width: "300px",
        // },
        {
            title: "Partner Reservation Number",
            value: "vendorResponseRef",
            align: "left",
            width: "300px",
            sort: true
        },
        {
            title: "Partner Invoice Number",
            value: "vendorInvoiceRef",
            align: "left",
            width: "300px",
            sort: true
        },
        {
            title: "Merchant",
            value: "corporateName",
            align: "left",
            width: "250px",
            sort: true
        },
        {
            title: "Transaction Date",
            value: "transactionDate",
            align: "left",
            width: "250px",
            sort: true
        },
        {
            title: "Payment Status",
            value: "paymentStatus",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Payment Method",
            value: "paymentCategory",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Payment Ref",
            value: "paymentRef",
            align: "left",
            width: "250px",
            sort: true
        },
        {
            title: "Amount",
            value: "amount",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Discount",
            value: "discount",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Promo Code",
            value: "promoCode",
            align: "left",
            width: "200px",
            sort: true
        },
        // {
        //   title: "MDR",
        //   value: "mdr",
        //   align: "left",
        //   width: "200px",
        // },
        // {
        //   title: "Service Fee",
        //   value: "serviceFee",
        //   align: "left",
        //   width: "200px",
        // },
        // {
        //   title: "Payment Fee",
        //   value: "paymentFee",
        //   align: "left",
        //   width: "200px",
        // },
        {
            title: "Username",
            value: "username",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Device ID",
            value: "deviceid",
            align: "left",
            width: "200px",
            sort: true
        },
        // {
        //   title: "MID",
        //   value: "mid",
        //   align: "left",
        //   width: "200px",
        // },
        // {
        //   title: "TID",
        //   value: "tid",
        //   align: "left",
        //   width: "200px",
        // },
        {
            title: "Card Pan",
            value: "cardPan",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Card Type",
            value: "cardType",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Last Balance",
            value: "lastBalance",
            align: "left",
            width: "200px",
            sort: true
        },
        {
            title: "Current Balance",
            value: "currentBalance",
            align: "left",
            width: "200px",
            sort: true
        },
    ];

    const renderCell = (item, header, index) => {
        if (header.value === "no") {
            return <span>{index >= 0 ? index + offset + 1 : ""}</span>;
        } else if (header.value === "corporateName") {
            return <span>{item.corporateName ? item.corporateName : "-"}</span>;
        } else if (header.value === "transactionDate") {
            return <span>{item.transactionDate ? item.transactionDate : "-"}</span>;
        } else if (header.value === "paymentRef") {
            return <span>{item.paymentRef ? item.paymentRef : "-"}</span>;
        } else if (header.value === "paymentStatus") {
            return <span>{item.paymentStatus ? item.paymentStatus : "-"}</span>;
        } else if (header.value === "amount") {
            return <span>{item.amount ? formatCurrency(item.amount) : formatCurrency(0)}</span>;
        } else if (header.value === "discount") {
            return <span>{item.discount ? item.discount : 0}</span>;
        } else if (header.value === "promoCode") {
            return <span>{item.promoCode ? item.promoCode : "-"}</span>;
        } else if (header.value === "mdr") {
            return <span>{item.mdr ? item.mdr : 0}</span>;
        } else if (header.value === "serviceFee") {
            return <span>{item.serviceFee ? formatCurrency(item.serviceFee) : formatCurrency(0)}</span>;
        } else if (header.value === "paymentFee") {
            return <span>{item.paymentFee ? formatCurrency(item.paymentFee) : formatCurrency(0)}</span>;
        } else if (header.value === "username") {
            return <span>{item.username ? item.username : "-"}</span>;
        } else if (header.value === "deviceid") {
            return <span>{item.deviceid ? item.deviceid : "-"}</span>;
        } else if (header.value === "mid") {
            return <span>{item.mid ? item.mid : "-"}</span>;
        } else if (header.value === "tid") {
            return <span>{item.tid ? item.tid : "-"}</span>;
        } else if (header.value === "cardPan") {
            return <span>{item.cardPan ? item.cardPan : "-"}</span>;
        } else if (header.value === "cardType") {
            return <span>{item.cardType ? item.cardType : "-"}</span>;
        } else if (header.value === "lastBalance") {
            return <span>{item.lastBalance ? formatCurrency(item.lastBalance) : formatCurrency(0)}</span>;
        } else if (header.value === "currentBalance") {
            return <span>{item.currentBalance ? formatCurrency(item.currentBalance) : formatCurrency(0)}</span>;
        } else if (header.value === "vendorResponseRef") {
            return <span>{item.vendorResponseRef ? item.vendorResponseRef : "-"}</span>;
        } else if (header.value === "vendorRequestRef") {
            return <span>{item.vendorRequestRef ? item.vendorRequestRef : "-"}</span>;
        } else if (header.value === "vendorInvoiceRef") {
            return <span>{item.vendorInvoiceRef ? item.vendorInvoiceRef : "-"}</span>;
        } else if (header.value === "paymentCategory") {
            let modifiedPayment;
            if (item.paymentCategory === "TUNAI") {
                modifiedPayment = "CASH";
            } else if (item.paymentCategory === "CC") {
                modifiedPayment = "CREDIT CARD";
            } else {
                modifiedPayment = item.paymentCategory;
            }
            return (
                <Box
                    sx={{
                        display: "flex",
                        alignItems: "center",
                        gap: 1,
                    }}
                >
                    {avatarPayment(modifiedPayment)}
                </Box>
            );
        } else if (header.value === "coreTicketingRef") {
            return (
                <Box sx={{ display: "flex", gap: 1, width: "100%", alignItems: "center" }}>
                    <Box
                        onClick={() => {
                            handleGetTransactionDetail([item.coreTicketingRef], [item.cid]);
                            setDataEdit(item);
                        }}
                        sx={{
                            fontWeight: "bold",
                            color: "#0d47a1",
                            cursor: "pointer",
                            textDecoration: "underline",
                            textUnderlineOffset: "4px",
                            ":hover": {
                                color: "#2196f3",
                            },
                        }}
                    >
                        {item.coreTicketingRef ? item.coreTicketingRef : "-"}
                    </Box>
                </Box>
            );
        }
    };

    const header2 = [
        {
            title: "No",
            value: "no",
            align: "left",
            width: "30px",
        },
        {
            title: "Merchant",
            value: "corporate",
            align: "left",
            width: "250px",
        },
        {
            title: "Username",
            value: "username",
            align: "left",
            width: "250px",
        },
        {
            title: "Device Id",
            value: "deviceId",
            align: "left",
            width: "100px",
        },
        {
            title: "Quantity",
            value: "quantity",
            align: "left",
            width: "100px",
        },
        {
            title: "Total Amount",
            value: "totalAmount",
            align: "left",
            width: "200px",
        },
    ];

    const renderCell2 = (item, header, index) => {
        if (header.value === "no") {
            return <span>{index >= 0 ? index + 1 : ""}</span>;
        } else if (header.value === "corporate") {
            return <span>{item.corporate ? item.corporate : "-"}</span>;
        } else if (header.value === "deviceId") {
            return <span>{item.deviceId ? item.deviceId : "-"}</span>;
        } else if (header.value === "username") {
            return <span>{item.username ? item.username : "-"}</span>;
        } else if (header.value === "quantity") {
            return <span>{item.quantity ? thousandSeparator(item.quantity) : "-"}</span>;
        } else if (header.value === "totalAmount") {
            return <span>{item.totalAmount ? formatCurrency(item.totalAmount) : "-"}</span>;
        }
    };

    // untuk rename option select field merchant
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                label: item.ouName,
                value: item.ouCode,
            });
        });
        // console.log('merchantArr',merchantArr.length)
        handleGetPaymentMethod();
        setMerchantOption(merchantArr);
    }, [merchantData]);

    const handleGetPaymentMethod = async () => {
        getPaymentList()
            .then((res) => {
                if (res.success) {
                    let paymentArr = [];
                    res.result.map((item) => {
                        paymentArr.push({
                            label: item.paymentCategory,
                            value: item.paymentCategory,
                        });
                    });
                    setPaymentOption(paymentArr);
                    // console.log(res.message || "Success Get payment Data");
                    notify(res.message || "Success Get Payment Method Data", "success");
                }
            })
            .catch((error) => {
                setPaymentOption([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });
    };

    //get laporan transaksi tiket
    const handleGetAllData = async ({ limit, offset, ouCodeValue, dateFrom, dateTo, paymentCategory }, pageRow = false) => {
        if (ouCodeValue.length > 0 && dateFrom && dateTo) {
            const dateDiff = dateTo.diff(dateFrom, "days") + 1;
            if (dateTo.isBefore(dateFrom, "day")) {
                // console.log("Select The Correct End Date")
                notify("Select The Correct End Date", "warning");
                setTanggalAkhir(null);
            } else if (dateDiff > 31) {
                // console.log("Maximum Range of 31 Days")
                notify("Maximum Range of 31 Days", "warning");
            } else {
                const arrayOuCode = pageRow ? ouCodeValue : ouCodeValue.map((data) => data.value);
                setLoading(true);
                setOuCodeSelected(arrayOuCode);
                setTanggalAwalSelected(dateFrom);
                setTanggalAkhirSelected(dateTo);
                setPaymentSelected(paymentCategory);
                setDisableDownload(true);

                let data1 = {
                    outletCode: arrayOuCode,
                    transactionDateStart: dateFrom.startOf("day").format("YYYY-MM-DD HH:mm:ss"),
                    transactionDateTo: dateTo.endOf("day").format("YYYY-MM-DD HH:mm:ss"),
                    paymentStatus: ["SUCCESS"],
                    paymentCategory: paymentCategory ? [paymentCategory] : [],
                    limit : 0,
                    offset : 0,
                };
                let data2 = {
                    outletCode: arrayOuCode,
                    transactionDateStart: dateFrom.startOf("day").format("YYYY-MM-DD HH:mm:ss"),
                    transactionDateTo: dateTo.endOf("day").format("YYYY-MM-DD HH:mm:ss"),
                    paymentStatus: ["SUCCESS"],
                    paymentCategory: paymentCategory ? [paymentCategory] : [],
                };
                // console.log(data1)
                // console.log(data2)

                //tabel 1
                await handleGetTrxList(data1);

                //tabel 2
                await handleGetTrxByUser(data2)

                //untuk total data transaksi
                // if (ouCodeValue !== ouCodeSelected || dateFrom !== tanggalAwalSelected || dateTo !== tanggalAkhirSelected || paymentCategory !== paymentSelected) {
                //     !pageRow &&
                //         (await getTrxList(data3)
                //             .then((res) => {
                //                 if (res.success) {
                //                     // console.log('Ticket Detail Success', res.result[0]);
                                    
                //                     notify(res.message || "Success Get Data List", "success");
                //                 }
                //             })
                //             .catch((error) => {
                //                 if (error.response) {
                //                     // console.log(error.response.data.message);
                //                     notify(error.response.data.message, "error");
                //                 } else {
                //                     // console.log(error.message || error);
                //                     notify(error.message || error, "error");
                //                 }
                //             }));
                // }
                setLoading(false);
            }
        } else {
            if (!ouCodeValue) {
                setErrormerchant(true);
            }
            if (!dateFrom) {
                setErrorTanggalAwal(true);
            }
            if (!dateTo) {
                setErrorTanggalAkhir(true);
            }
            notify("Fill The Required Filter", "warning");
        }
    };

    const handleGetTransactionDetail = async (coreTicketingRef, cid) => {
        if (coreTicketingRef.length > 0 && cid.length > 0) {
            const data = {
                coreTicketingRef,
                cid,
            };
            await getTrxDetail(data)
                .then((res) => {
                    if (res.success) {
                        // console.log('Ticket Detail Success', res.result[0]);
                        setDataDetail(res.result);
                        setDialogDetail(true);
                        notify(res.message || "Success Get Detail", "success");
                    }
                })
                .catch((error) => {
                    if (error.response) {
                        // console.log(error.response.data.message);
                        notify(error.response.data.message, "error");
                    } else {
                        // console.log(error.message || error);
                        notify(error.message || error, "error");
                    }
                });
        }
    };

    const handleGetSearchData = async ({ limit, offset, ouCodeValue, dateFrom, dateTo, paymentCategory, keyword }, pageRow = false) => {
        setSearchLoading(true);
        setDisableDownload(true);
        setDataTable1([]);

        let data1 = {
            outletCode: ouCodeValue,
            transactionDateStart: dateFrom.startOf("day").format("YYYY-MM-DD HH:mm:ss"),
            transactionDateTo: dateTo.endOf("day").format("YYYY-MM-DD HH:mm:ss"),
            paymentStatus: ["SUCCESS"],
            paymentCategory: paymentCategory ? [paymentCategory] : [],
            limit: 0,
            offset : 0,
            keyword,
        };
        await handleGetTrxList(data1);
        setSearchLoading(false);
        //untuk total data search
        // !pageRow &&
        //     (await getTrxList(data2)
        //         .then((res) => {
        //             if (res.success) {
        //                 setTotalSearch(res.result.length);
        //             }
        //         })
        //         .catch((error) => {
        //             setDataTable1([]);
        //             if (error.response) {
        //                 // console.log(error.response.data.message);
        //                 notify(error.response.data.message, "error");
        //             } else {
        //                 // console.log(error.message || error);
        //                 notify(error.message || error, "error");
        //             }
        //         }));

        
    };

    const handleGetTrxList = async (data1) => {
        await getTrxList(data1)
            .then((res) => {
                if (res.success) {
                    // console.log('Ticket Detail Success', res.result[0]);
                    setDataTable1(res.result);
                    setTotalTransaksi(res.result.length);
                    notify(res.message || "Success Get Data List", "success");
                    setDisableDownload(false);
                }
            })
            .catch((error) => {
                setDisableDownload(true);
                setDataTable1([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });
    };

    const handleGetTrxByUser = async (data) => {
        await getTrxByUser(data)
            .then((res) => {
                if (res.success) {
                    // console.log('Ticket Detail Success', res.result[0]);
                    setDataTable2(res.result);
                    notify(res.message || "Success Get Data List By User", "success");
                    setDisableDownload(false);
                }
            })
            .catch((error) => {
                setDisableDownload(true);
                setDataTable2([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });
    };

    const handleSearch = async (value) => {
        setLimit(25);
        setOffset(0);
        if (value.length > 0) {
            !isSearch && setIsSearch(true);
            setLoading(true);
            await handleGetSearchData({
                limit: 25,
                offset: 0,
                ouCodeValue: ouCodeSelected,
                dateFrom: tanggalAwalSelected,
                dateTo: tanggalAkhirSelected,
                paymentCategory: paymentSelected,
                keyword: value,
            });
            setLoading(false);
        } else {
            setLoading(true);
            isSearch && setIsSearch(false);
            isSearch &&
                (await handleGetAllData(
                    {
                        limit: 25,
                        offset: 0,
                        ouCodeValue: ouCodeSelected,
                        dateFrom: tanggalAwalSelected,
                        dateTo: tanggalAkhirSelected,
                        paymentCategory: paymentSelected,
                    },
                    true
                ));
            setLoading(false);
        }
    };

    // const pageChange = async (value) => {
    //     const ofset = value * limit; //index awal
    //     setOffset(ofset);
    //     setLoading(true);
    //     if (isSearch) {
    //         setLoading(true);
    //         await handleGetSearchData(
    //             {
    //                 limit: limit,
    //                 offset: ofset,
    //                 ouCodeValue: ouCodeSelected,
    //                 dateFrom: tanggalAwalSelected,
    //                 dateTo: tanggalAkhirSelected,
    //                 paymentCategory: paymentSelected,
    //                 keyword: search,
    //             },
    //             true
    //         );
    //         setLoading(false);
    //     } else {
    //         setLoading(true);
    //         await handleGetAllData(
    //             {
    //                 limit: limit,
    //                 offset: ofset,
    //                 ouCodeValue: ouCodeSelected,
    //                 dateFrom: tanggalAwalSelected,
    //                 dateTo: tanggalAkhirSelected,
    //                 paymentCategory: paymentSelected,
    //             },
    //             true
    //         );
    //         setLoading(false);
    //     }
    // };

    // const rowsChange = async (e) => {
    //     setOffset(0);
    //     setLimit(e.props.value);
    //     setLoading(true);
    //     if (isSearch) {
    //         setLoading(true);
    //         await handleGetSearchData(
    //             {
    //                 limit: e.props.value,
    //                 offset: 0,
    //                 ouCodeValue: ouCodeSelected,
    //                 dateFrom: tanggalAwalSelected,
    //                 dateTo: tanggalAkhirSelected,
    //                 paymentCategory: paymentSelected,
    //                 keyword: search,
    //             },
    //             true
    //         );
    //         setLoading(false);
    //     } else {
    //         setLoading(true);
    //         await handleGetAllData(
    //             {
    //                 limit: e.props.value,
    //                 offset: 0,
    //                 ouCodeValue: ouCodeSelected,
    //                 dateFrom: tanggalAwalSelected,
    //                 dateTo: tanggalAkhirSelected,
    //                 paymentCategory: paymentSelected,
    //             },
    //             true
    //         );
    //         setLoading(false);
    //     }
    // };

    const handleCallbackDialogEdit = () => {
        let data1 = {
            outletCode: ouCodeSelected,
            transactionDateStart: tanggalAwalSelected.startOf("day").format("YYYY-MM-DD HH:mm:ss"),
            transactionDateTo: tanggalAkhirSelected.endOf("day").format("YYYY-MM-DD HH:mm:ss"),
            paymentStatus: ["SUCCESS"],
            paymentCategory: paymentSelected ? [paymentSelected] : [],
            limit,
            offset,
        };
        getTrxList(data1)
            .then((res) => {
                if (res.success) {
                    // console.log('Ticket Detail Success', res.result[0]);
                    setDataTable1(res.result);
                    notify(res.message || "Success Get Data List", "success");
                }
            })
            .catch((error) => {
                setDataTable1([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });
    };

    return (
        <React.Fragment>
            <Stack direction={"column"} p={"2rem"}>
                <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                    <CardContent sx={{ p: "2rem" }}>
                        <Box display="flex" flexDirection="column">
                            <Typography variant="h4" fontWeight="600">
                                {label}
                            </Typography>
                            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                                <Box
                                    sx={{
                                        display: "grid",
                                        gridTemplateColumns: ["repeat(1, 1fr)", "repeat(1, 1fr)", "repeat(3, 1fr)"],
                                        gap: 2,
                                    }}
                                >
                                    <SelectField
                                        multiple
                                        label={"Merchant"}
                                        placeholder="Select Merchant"
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        data={merchantOption}
                                        selectedValue={merchant}
                                        setValue={(newValue) => {
                                            setMerchant(newValue);
                                            setErrormerchant(false);
                                        }}
                                        required={true}
                                        isError={errormerchant}
                                        errorMessage={"Merchant is required"}
                                        limitTags={1}
                                    />

                                    <DatePickerField
                                        label={"Date Range"}
                                        placeholder="DD MMM YYYY"
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        value={tanggalAwal}
                                        onChange={(newValue) => {
                                            setTanggalAwal(newValue);
                                            setErrorTanggalAwal(false);
                                        }}
                                        required={true}
                                        format={"DD MMM YYYY"}
                                        isError={errorTanggalAwal}
                                        errorMessage={"Starting Date is required"}
                                    />

                                    <DatePickerField
                                        label={<span />}
                                        placeholder="DD MMM YYYY"
                                        sx={{ width: "100%", fontSize: "16px", marginTop: ["-2rem", "-2rem", "0"] }}
                                        value={tanggalAkhir}
                                        onChange={(newValue) => {
                                            setTanggalAkhir(newValue);
                                            setErrorTanggalAkhir(false);
                                        }}
                                        format={"DD MMM YYYY"}
                                        isError={errorTanggalAkhir}
                                        errorMessage={"End Date required"}
                                    />

                                    <SelectField
                                        label={"Payment Method"}
                                        placeholder={paymentOption.length > 0 ? "All Payment Method" : "Please Select Merchant"}
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        data={paymentOption}
                                        selectedValue={payment}
                                        setValue={(newValue) => {
                                            setPayment(newValue);
                                        }}
                                    />
                                </Box>
                            </Stack>
                            <Stack
                                sx={{
                                    width: "100%",
                                    display: "flex",
                                    flexDirection: ["column", "row"],
                                    alignItems: ["end", "flex-start"],
                                    gap: 3,
                                    justifyContent: "space-between",
                                    mt: "2rem",
                                }}
                            >
                                <FilterMessageNote
                                    sx={{
                                        width: ["100%", "50%"],
                                    }}
                                    title={titleInfo}
                                    subtitle={subTitleInfo}
                                />
                                <div
                                    style={{
                                        display: "flex",
                                        gap: 10,
                                    }}
                                >
                                    <ExportButton
                                        disabled={disableDownload}
                                        setDisableDownload={setDisableDownload}
                                        ouCodeValue={ouCodeSelected}
                                        dateFrom={tanggalAwalSelected}
                                        dateTo={tanggalAkhirSelected}
                                        paymentCategory={paymentSelected}
                                        notify={notify}
                                        color="success"
                                    />
                                    <CustomButton
                                        onClick={() => {
                                            setLimit(25);
                                            setOffset(0);
                                            handleGetAllData({
                                                limit: 25,
                                                offset: 0,
                                                ouCodeValue: merchant.length > 0 ? merchant : [],
                                                dateFrom: tanggalAwal,
                                                dateTo: tanggalAkhir,
                                                paymentCategory: payment ? payment.value : "",
                                            });
                                            setOrder("asc")
                                            setOrderBy("")
                                        }}
                                        startIcon={<SearchIcon size="14px" />}
                                        name={buttonFilter}
                                    >
                                        Filter
                                    </CustomButton>
                                </div>
                            </Stack>

                            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                                <Typography variant="h6" fontWeight="600">
                                    Transaction Summary
                                </Typography>
                                <Stack
                                    sx={{
                                        display: "grid",
                                        gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(3, 1fr)"],
                                        gap: 2,
                                    }}
                                >
                                    <CardSummary title="Total Transaction" amount={totalTransaksi ? totalTransaksi : 0} isCurrency={false} src={<ReceiptRoundedIcon sx={{ fontSize: "30px", color: "#3875CA" }} />} />
                                </Stack>
                            </Stack>

                            <Box sx={{ width: "100%", display: "flex", flexDirection: "column", gap: "2rem" }} mt={"2rem"}>
                                <CustomTab
                                    dataTabList={[
                                        {
                                            label: "Transaction List",
                                            table: (
                                                <Stack>
                                                    <Box display="flex" flexDirection={["row"]} width={"100%"} gap={1} justifyContent={"end"} alignItems={"center"}>
                                                        <SearchField
                                                            value={search}
                                                            setValue={setSearch}
                                                            sx={{ width: ["100%", "100%", "40%"], height: "100%" }}
                                                            variant={"outlined"}
                                                            showIcon={search.length > 0}
                                                            disabled={ouCodeSelected && tanggalAwalSelected && tanggalAkhirSelected && !searchLoading ? false : true}
                                                        />
                                                        <Button
                                                            sx={{ height: "100%", width: "fit-content", padding: 1 }}
                                                            onClick={() => handleSearch(search)}
                                                            disabled={ouCodeSelected && tanggalAwalSelected && tanggalAkhirSelected && !searchLoading ? false : true}
                                                            variant="contained"
                                                        >
                                                            <SearchIcon size="14px" />
                                                        </Button>
                                                    </Box>
                                                    {/* <CustomPagination
                                                        limit={limit}
                                                        countLoading={false}
                                                        offset={offset}
                                                        count={isSearch ? totalSearch : totalTransaksi}
                                                        pageChange={(event, v) => pageChange(v)}
                                                        rowsChange={async (event, e) => rowsChange(e)}
                                                    /> */}
                                                    <CustomTableSort headers={header} items={dataTable1} renderCell={renderCell} order={order} orderBy={orderBy} setOrder={setOrder} setOrderBy={setOrderBy} />
                                                    {/* <CustomPagination
                                                        limit={limit}
                                                        countLoading={false}
                                                        offset={offset}
                                                        count={isSearch ? totalSearch : totalTransaksi}
                                                        pageChange={(event, v) => pageChange(v)}
                                                        rowsChange={async (event, e) => rowsChange(e)}
                                                    /> */}
                                                </Stack>
                                            ),
                                        },
                                        {
                                            label: "Transaction By User",
                                            table: <CustomTable headers={header2} items={dataTable2} renderCell={renderCell2} />,
                                        },
                                    ]}
                                />
                            </Box>
                        </Box>
                    </CardContent>
                </Card>
            </Stack>
            <DialogTransaction open={dialogDetail} setOpen={setDialogDetail} data={dataDetail} setData={setDataDetail} buttonEditAction={() => setDialogEdit(true)} />
            <DialogEdit open={dialogEdit} setOpen={setDialogEdit} data={dataEdit} setData={setDataEdit} notify={notify} callback={handleCallbackDialogEdit} />
        </React.Fragment>
    );
};

export default LaporanTransaksi;
