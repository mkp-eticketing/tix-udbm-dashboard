import React, { useEffect, useState } from "react";
import { Box, Button, Card, CardContent, Stack, Typography } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import SelectField from "../../../components/select-field";
import CustomTableSort from "../../../components/custom-table-sort";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import ExportButton from "./components/export-to-excel-cash-id-billing";
import DatePickerField from "../../../components/datepicker-field";
import moment from "moment";
import CustomPagination from "../../../components/custom-pagination";
import CardSummary from "../components/card-summary";
import ReceiptRoundedIcon from "@mui/icons-material/ReceiptRounded";
import { getDataCash, getDataCardCash } from "../../../services/pariwisata/cash-id-billing";
import { formatCurrency } from "../components/custom-format-currency";
import { thousandSeparator } from "../../../utils/thousand-separator";
import PaidIcon from '@mui/icons-material/Paid';

const CashIdBilling = ({
    label = "Cash ID Billing",
    titleInfo = "To Display & Download Specific Transactions, Use the Filters Above.",
    subTitleInfo = ["Max Range of Date is 1 Month (31 Days)"],
    merchantData = [],
    setLoading = () => {},
    notify = () => {},
    buttonFilter = "Search",
    avatarPayment = () => {},
}) => {
    const [disableDownload, setDisableDownload] = useState(true);
    const [dataTable1, setDataTable1] = useState([]);

    const [merchant, setMerchant] = useState([]);
    const [tanggalAwal, setTanggalAwal] = useState(moment().subtract(7, "days"));
    const [tanggalAkhir, setTanggalAkhir] = useState(moment());

    const [merchantOption, setMerchantOption] = useState([]);

    //untuk menyimpan data filter yang sudah di search
    const [ouCodeSelected, setOuCodeSelected] = useState("");
    const [tanggalAwalSelected, setTanggalAwalSelected] = useState("");
    const [tanggalAkhirSelected, setTanggalAkhirSelected] = useState("");
    const [paymentSelected, setPaymentSelected] = useState(false);

    const [errormerchant, setErrormerchant] = useState(false);
    const [errorTanggalAwal, setErrorTanggalAwal] = useState(false);
    const [errorTanggalAkhir, setErrorTanggalAkhir] = useState(false);

    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState();
    const [totalTransaksi, setTotalTransaksi] = useState(0);
    const [totalAmount, setTotalAmount] = useState(0);

    const [order, setOrder] = useState("asc");
    const [orderBy, setOrderBy] = useState("");

    const header = [
        {
            title: "No",
            value: "no",
            align: "left",
            width: "30px",
            sort: true,
        },
        {
            title: "ID Billing",
            value: "idBilling",
            align: "left",
            width: "200px",
            sort: true,
        },
        {
            title: "Nomor Transaksi",
            value: "docNo",
            align: "left",
            width: "300px",
            sort: true,
        },
        {
            title: "Status",
            value: "status",
            align: "left",
            width: "200px",
            sort: true,
        },
        {
            title: "Jenis Transaksi",
            value: "mkpProductName",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Merchant",
            value: "mkpMerchantName",
            align: "left",
            width: "300px",
            sort: true,
        },
        {
            title: "Kode Merchant",
            value: "mkpMerchantCode",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nomor Referensi MKP",
            value: "mkpTrxNo",
            align: "left",
            width: "300px",
            sort: true,
        },
        // {
        //     title: "Nomor Referensi Pembayaran",
        //     value: "mkpPayrefNo",
        //     align: "left",
        //     width: "300px",
        //     sort: true,
        // },
        {
            title: "Metode Pembayaran",
            value: "mkpPaymentType",
            align: "left",
            width: "300px",
            sort: false,
        },
        {
            title: "Waktu Transaksi",
            value: "mkpInquiryDatetime",
            align: "left",
            width: "300px",
            sort: true,
        },
        // {
        //     title: "Waktu Bayar",
        //     value: "mkpPaymentDatetime",
        //     align: "left",
        //     width: "300px",
        //     sort: true,
        // },
        // {
        //     title: "Keterangan",
        //     value: "mkpRemarks",
        //     align: "left",
        //     width: "300px",
        //     sort: true,
        // },
        {
            title: "Nominal Transaksi",
            value: "paymentAmount",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode OPD",
            value: "bjtgKodeOpd",
            align: "left",
            width: "200px",
            sort: true,
        },
        {
            title: "Nama OPD",
            value: "bjtgNamaOpd",
            align: "left",
            width: "300px",
            sort: true,
        },
        {
            title: "Tanggal Penetapan",
            value: "bjtgTglPenetapan",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Tanggal Jatuh Tempo",
            value: "bjtgTglJatuhTempo",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Tanggal Posting",
            value: "bjtgTglPosting",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "NOP Wajib Retribusi",
            value: "bjtgNopWr",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Wajib Retribusi",
            value: "bjtgNamaWr",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Alamat Wajib Retribusi",
            value: "bjtgAlamatWr",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode Produk 1",
            value: "kodeBlj1",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Produk 1",
            value: "keteranganBlj1",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nominal Produk 1",
            value: "amtBlj1",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode Produk 2",
            value: "kodeBlj2",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Produk 2",
            value: "keteranganBlj2",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nominal Produk 2",
            value: "amtBlj2",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode Produk 3",
            value: "kodeBlj3",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Produk 3",
            value: "keteranganBlj3",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nominal Produk 3",
            value: "amtBlj3",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode Produk 4",
            value: "kodeBlj4",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Produk 4",
            value: "keteranganBlj4",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nominal Produk 4",
            value: "amtBlj4",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode Produk 5",
            value: "kodeBlj5",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Produk 5",
            value: "keteranganBlj5",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nominal Produk 5",
            value: "amtBlj5",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode Produk 6",
            value: "kodeBlj6",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Produk 6",
            value: "keteranganBlj6",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nominal Produk 6",
            value: "amtBlj6",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Kode Produk 7",
            value: "kodeBlj7",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nama Produk 7",
            value: "keteranganBlj7",
            align: "left",
            width: "250px",
            sort: true,
        },
        {
            title: "Nominal Produk 7",
            value: "amtBlj7",
            align: "left",
            width: "250px",
            sort: true,
        },
    ];

    const renderCell = (item, header, index) => {
        if (header.value === "no") {
            return <span>{index >= 0 ? index + offset + 1 : ""}</span>;
        } else if (header.value === "idBilling") {
            return <span>{item.idBilling ? item.idBilling : "-"}</span>;
        } else if (header.value === "docNo") {
            return <span>{item.docNo ? item.docNo : "-"}</span>;
        } else if (header.value === "status") {
            return <span>{item.status ? item.status : "-"}</span>;
        } else if (header.value === "mkpProductName") {
            return <span>{item.mkpProductName ? item.mkpProductName : "-"}</span>;
        } else if (header.value === "mkpMerchantName") {
            return <span>{item.mkpMerchantName ? item.mkpMerchantName : "-"}</span>;
        } else if (header.value === "mkpMerchantCode") {
            return <span>{item.mkpMerchantCode ? item.mkpMerchantCode : "-"}</span>;
        } else if (header.value === "mkpTrxNo") {
            return <span>{item.mkpTrxNo ? item.mkpTrxNo : "-"}</span>;
        } else if (header.value === "mkpPayrefNo") {
            return <span>{item.mkpPayrefNo ? item.mkpPayrefNo : "-"}</span>;
        } else if (header.value === "mkpInquiryDatetime") {
            return <span>{item.mkpInquiryDatetime ? item.mkpInquiryDatetime : "-"}</span>;
        } else if (header.value === "mkpPaymentDatetime") {
            return <span>{item.mkpPaymentDatetime ? item.mkpPaymentDatetime : "-"}</span>;
        } else if (header.value === "mkpRemarks") {
            return <span>{item.mkpRemarks ? item.mkpRemarks : "-"}</span>;
        } else if (header.value === "paymentAmount") {
            return <span>{item.paymentAmount ? formatCurrency(item.paymentAmount) : 0}</span>;
        } else if (header.value === "bjtgKodeOpd") {
            return <span>{item.bjtgKodeOpd ? item.bjtgKodeOpd : "-"}</span>;
        } else if (header.value === "bjtgNamaOpd") {
            return <span>{item.bjtgNamaOpd ? item.bjtgNamaOpd : "-"}</span>;
        } else if (header.value === "bjtgTglPenetapan") {
            return <span>{item.bjtgTglPenetapan ? item.bjtgTglPenetapan : "-"}</span>;
        } else if (header.value === "bjtgTglJatuhTempo") {
            return <span>{item.bjtgTglJatuhTempo ? item.bjtgTglJatuhTempo : "-"}</span>;
        } else if (header.value === "bjtgTglPosting") {
            return <span>{item.bjtgTglPosting ? item.bjtgTglPosting : "-"}</span>;
        } else if (header.value === "bjtgNopWr") {
            return <span>{item.bjtgNopWr ? item.bjtgNopWr : "-"}</span>;
        } else if (header.value === "bjtgNamaWr") {
            return <span>{item.bjtgNamaWr ? item.bjtgNamaWr : "-"}</span>;
        } else if (header.value === "bjtgAlamatWr") {
            return <span>{item.bjtgAlamatWr ? item.bjtgAlamatWr : "-"}</span>;
        } else if (header.value === "kodeBlj1") {
            return <span>{item.kodeBlj1 ? item.kodeBlj1 : "-"}</span>;
        } else if (header.value === "keteranganBlj1") {
            return <span>{item.keteranganBlj1 ? item.keteranganBlj1 : "-"}</span>;
        } else if (header.value === "amtBlj1") {
            return <span>{item.amtBlj1 ? formatCurrency(item.amtBlj1) : "-"}</span>;
        } else if (header.value === "kodeBlj2") {
            return <span>{item.kodeBlj2 ? item.kodeBlj2 : "-"}</span>;
        } else if (header.value === "keteranganBlj2") {
            return <span>{item.keteranganBlj2 ? item.keteranganBlj2 : "-"}</span>;
        } else if (header.value === "amtBlj2") {
            return <span>{item.amtBlj2 ? formatCurrency(item.amtBlj2) : "-"}</span>;
        } else if (header.value === "kodeBlj3") {
            return <span>{item.kodeBlj3 ? item.kodeBlj3 : "-"}</span>;
        } else if (header.value === "keteranganBlj3") {
            return <span>{item.keteranganBlj3 ? item.keteranganBlj3 : "-"}</span>;
        } else if (header.value === "amtBlj3") {
            return <span>{item.amtBlj3 ? formatCurrency(item.amtBlj3) : "-"}</span>;
        } else if (header.value === "kodeBlj4") {
            return <span>{item.kodeBlj4 ? item.kodeBlj4 : "-"}</span>;
        } else if (header.value === "keteranganBlj4") {
            return <span>{item.keteranganBlj4 ? item.keteranganBlj4 : "-"}</span>;
        } else if (header.value === "amtBlj4") {
            return <span>{item.amtBlj4 ? formatCurrency(item.amtBlj4) : "-"}</span>;
        } else if (header.value === "kodeBlj5") {
            return <span>{item.kodeBlj5 ? item.kodeBlj5 : "-"}</span>;
        } else if (header.value === "keteranganBlj5") {
            return <span>{item.keteranganBlj5 ? item.keteranganBlj5 : "-"}</span>;
        } else if (header.value === "amtBlj5") {
            return <span>{item.amtBlj5 ? formatCurrency(item.amtBlj5) : "-"}</span>;
        } else if (header.value === "kodeBlj6") {
            return <span>{item.kodeBlj6 ? item.kodeBlj6 : "-"}</span>;
        } else if (header.value === "keteranganBlj6") {
            return <span>{item.keteranganBlj6 ? item.keteranganBlj6 : "-"}</span>;
        } else if (header.value === "amtBlj6") {
            return <span>{item.amtBlj6 ? formatCurrency(item.amtBlj6) : "-"}</span>;
        } else if (header.value === "kodeBlj7") {
            return <span>{item.kodeBlj7 ? item.kodeBlj7 : "-"}</span>;
        } else if (header.value === "keteranganBlj7") {
            return <span>{item.keteranganBlj7 ? item.keteranganBlj7 : "-"}</span>;
        } else if (header.value === "amtBlj7") {
            return <span>{item.amtBlj7 ? formatCurrency(item.amtBlj7) : "-"}</span>;
        } else if (header.value === "mkpPaymentType") {
            let modifiedPayment;
            if (item.mkpPaymentType === "TUNAI") {
                modifiedPayment = "CASH";
            } else if (item.mkpPaymentType === "CC") {
                modifiedPayment = "CREDIT CARD";
            } else {
                modifiedPayment = item.mkpPaymentType;
            }
            return (
                <Box
                    sx={{
                        display: "flex",
                        alignItems: "center",
                        gap: 1,
                    }}
                >
                    {avatarPayment(modifiedPayment)}
                </Box>
            );
        }
    };

    // untuk rename option select field merchant
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                label: item.ouName,
                value: item.ouCode,
            });
        });
        // console.log('merchantArr',merchantArr.length)
        setMerchantOption(merchantArr);
    }, [merchantData]);

    //get laporan transaksi tiket
    const handleGetAllData = async ({ limit, offset, ouCodeValue, dateFrom, dateTo }) => {
        if (ouCodeValue.length > 0 && dateFrom && dateTo) {
            const dateDiff = dateTo.diff(dateFrom, "days") + 1;
            if (dateTo.isBefore(dateFrom, "day")) {
                // console.log("Select The Correct End Date")
                notify("Select The Correct End Date", "warning");
                setTanggalAkhir(null);
            } else if (dateDiff > 31) {
                // console.log("Maximum Range of 31 Days")
                notify("Maximum Range of 31 Days", "warning");
            } else {
                setLoading(true);
                setOuCodeSelected(ouCodeValue);
                setTanggalAwalSelected(dateFrom);
                setTanggalAkhirSelected(dateTo);
                setDisableDownload(true);

                let data1 = {
                    outletCode: ouCodeValue,
                    dateStart: dateFrom.format("YYYY-MM-DD"),
                    dateEnd: dateTo.format("YYYY-MM-DD"),
                    limit,
                    offset,
                };

                let data2 = {
                    outletCode: ouCodeValue,
                    dateStart: dateFrom.format("YYYY-MM-DD"),
                    dateEnd: dateTo.format("YYYY-MM-DD"),
                };

                //tabel 1
                await getDataCash(data1)
                    .then((res) => {
                        if (res.success && Array.isArray(res.result) && res.result.length > 0) {
                            setDataTable1(res.result);
                            notify(res.message || "Success Get Data List", "success");
                            setDisableDownload(false);
                        }else{
                            setDataTable1([]);
                            notify(res.message || "Data List Not Found", "error");

                        }
                    })
                    .catch((error) => {
                        setDisableDownload(true);
                        setDataTable1([]);
                        if (error.response) {
                            // console.log(error.response.data.message);
                            notify(error.response.data.message, "error");
                        } else {
                            // console.log(error.message || error);
                            notify(error.message || error, "error");
                        }
                    });

                //summary
                await getDataCardCash(data2)
                    .then((res) => {
                        if (res.success && res.result.totalAmount && res.result.totalTrx) {
                            setTotalAmount(res.result.totalAmount)
                            setTotalTransaksi(res.result.totalTrx)
                            setDisableDownload(false);
                            notify(res.message || "Success Get Data Summary", "success");
                        }else{
                            notify(res.message || "Data Summary Not Found", "error");
                            setDisableDownload(true);
                            setTotalTransaksi(0)
                            setTotalAmount(0)
                        }
                    })
                    .catch((error) => {
                        setDisableDownload(true);
                        setTotalAmount(0);
                        setTotalTransaksi(0);
                        if (error.response) {
                            console.log(error.response.data.message);
                            notify(error.response.data.message, "error");
                        } else {
                            console.log(error.message || error);
                            notify(error.message || error, "error");
                        }
                    });

                setLoading(false);
            }
        } else {
            if (!ouCodeValue) {
                setErrormerchant(true);
            }
            if (!dateFrom) {
                setErrorTanggalAwal(true);
            }
            if (!dateTo) {
                setErrorTanggalAkhir(true);
            }
            notify("Fill The Required Filter", "warning");
        }
    };

    const pageChange = async (value) => {
        const ofset = value * limit; //index awal
        setOffset(ofset);
        setLoading(true);
        await handleGetAllData({
            limit: limit,
            offset: ofset,
            ouCodeValue: ouCodeSelected,
            dateFrom: tanggalAwalSelected,
            dateTo: tanggalAkhirSelected,
            paymentCategory: paymentSelected,
        });
        setLoading(false);
    };

    const rowsChange = async (e) => {
        setOffset(0);
        setLimit(e.props.value);
        setLoading(true);
        await handleGetAllData({
            limit: e.props.value,
            offset: 0,
            ouCodeValue: ouCodeSelected,
            dateFrom: tanggalAwalSelected,
            dateTo: tanggalAkhirSelected,
            paymentCategory: paymentSelected,
        });
        setLoading(false);
    };

    return (
        <React.Fragment>
            <Stack direction={"column"} p={"2rem"}>
                <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                    <CardContent sx={{ p: "2rem" }}>
                        <Box display="flex" flexDirection="column">
                            <Typography variant="h4" fontWeight="600">
                                {label}
                            </Typography>
                            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                                <Box
                                    sx={{
                                        display: "grid",
                                        gridTemplateColumns: ["repeat(1, 1fr)", "repeat(1, 1fr)", "repeat(3, 1fr)"],
                                        gap: 2,
                                    }}
                                >
                                    <SelectField
                                        label={"Merchant"}
                                        placeholder="Select Merchant"
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        data={merchantOption}
                                        selectedValue={merchant}
                                        setValue={(newValue) => {
                                            setMerchant(newValue);
                                            setErrormerchant(false);
                                        }}
                                        required={true}
                                        isError={errormerchant}
                                        errorMessage={"Merchant is required"}
                                        limitTags={1}
                                    />

                                    <DatePickerField
                                        label={"Date Range"}
                                        placeholder="DD MMM YYYY"
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        value={tanggalAwal}
                                        onChange={(newValue) => {
                                            setTanggalAwal(newValue);
                                            setErrorTanggalAwal(false);
                                        }}
                                        required={true}
                                        format={"DD MMM YYYY"}
                                        isError={errorTanggalAwal}
                                        errorMessage={"Starting Date is required"}
                                    />

                                    <DatePickerField
                                        label={<span />}
                                        placeholder="DD MMM YYYY"
                                        sx={{ width: "100%", fontSize: "16px", marginTop: ["-2rem", "-2rem", "0"] }}
                                        value={tanggalAkhir}
                                        onChange={(newValue) => {
                                            setTanggalAkhir(newValue);
                                            setErrorTanggalAkhir(false);
                                        }}
                                        format={"DD MMM YYYY"}
                                        isError={errorTanggalAkhir}
                                        errorMessage={"End Date required"}
                                    />
                                </Box>
                            </Stack>
                            <Stack
                                sx={{
                                    width: "100%",
                                    display: "flex",
                                    flexDirection: ["column", "row"],
                                    alignItems: ["end", "flex-start"],
                                    gap: 3,
                                    justifyContent: "space-between",
                                    mt: "2rem",
                                }}
                            >
                                <FilterMessageNote
                                    sx={{
                                        width: ["100%", "50%"],
                                    }}
                                    title={titleInfo}
                                    subtitle={subTitleInfo}
                                />
                                <div
                                    style={{
                                        display: "flex",
                                        gap: 10,
                                    }}
                                >
                                    <ExportButton
                                        disabled={disableDownload}
                                        setDisableDownload={setDisableDownload}
                                        ouCodeValue={ouCodeSelected}
                                        dateFrom={tanggalAwalSelected}
                                        dateTo={tanggalAkhirSelected}
                                        notify={notify}
                                        color="success"
                                    />
                                    <CustomButton
                                        onClick={() => {
                                            setLimit(25);
                                            setOffset(0);
                                            handleGetAllData({
                                                limit: 25,
                                                offset: 0,
                                                ouCodeValue: merchant.value ? merchant.value : "",
                                                dateFrom: tanggalAwal,
                                                dateTo: tanggalAkhir,
                                            });
                                            setOrder("asc");
                                            setOrderBy("");
                                        }}
                                        startIcon={<SearchIcon size="14px" />}
                                        name={buttonFilter}
                                    />
                                </div>
                            </Stack>

                            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                                <Typography variant="h6" fontWeight="600">
                                    Cash ID Billing Summary
                                </Typography>
                                <Stack
                                    sx={{
                                        display: "grid",
                                        gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(3, 1fr)"],
                                        gap: 2,
                                    }}
                                >
                                    <CardSummary title="Total Transaction" amount={totalTransaksi ? thousandSeparator(totalTransaksi) : 0} isCurrency={false} src={<ReceiptRoundedIcon sx={{ fontSize: "30px", color: "#3875CA" }} />} />
                                    <CardSummary title="Total Amount" amount={totalAmount ? formatCurrency(totalAmount) : 0} isCurrency={false} src={<PaidIcon sx={{ fontSize: "30px", color: "#3875CA" }} />} />
                                </Stack>
                            </Stack>

                            <Box sx={{ width: "100%", display: "flex", flexDirection: "column", gap: "2rem" }} mt={"2rem"}>
                                {/* <CustomTab
                                    dataTabList={[
                                        {
                                            label: "Transaction List",
                                            table: (
                                                
                                            ),
                                        },
                                    ]}
                                /> */}

                                <Stack>
                                    <CustomPagination limit={limit} countLoading={false} offset={offset} count={totalTransaksi} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
                                    <CustomTableSort headers={header} items={dataTable1} renderCell={renderCell} order={order} orderBy={orderBy} setOrder={setOrder} setOrderBy={setOrderBy} />
                                    <CustomPagination limit={limit} countLoading={false} offset={offset} count={totalTransaksi} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
                                </Stack>
                            </Box>
                        </Box>
                    </CardContent>
                </Card>
            </Stack>
        </React.Fragment>
    );
};

export default CashIdBilling;
