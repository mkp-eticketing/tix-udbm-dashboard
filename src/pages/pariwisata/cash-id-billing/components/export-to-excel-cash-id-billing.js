import ExcelJS from "exceljs";
import { saveAs } from "file-saver";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { getTrxList, getTrxByUser } from "../../../../services/pariwisata/transaction";
import React from "react";
import { formatCurrency } from "../../components/custom-format-currency";
import { thousandSeparator } from "../../../../utils/thousand-separator";
import { getDataCash, getDataCardCash } from "../../../../services/pariwisata/cash-id-billing";

const ExportToExcel = ({ disabled, setDisableDownload, ouCodeValue, dateFrom, dateTo, notify }) => {
    const handleFetch = async () => {
        setDisableDownload(true);

        let data1 = {
            outletCode: ouCodeValue,
            dateStart: dateFrom.format("YYYY-MM-DD"),
            dateEnd: dateTo.format("YYYY-MM-DD"),
            limit: 0,
            offset: 0,
        };

        let data2 = {
            outletCode: ouCodeValue,
            dateStart: dateFrom.format("YYYY-MM-DD"),
            dateEnd: dateTo.format("YYYY-MM-DD"),
        };

        try {
            const cashList = await getDataCash(data1);
            let modifiedData1;
            if (cashList && cashList.result) {
                // console.log('Ticket Summary Success');
                modifiedData1 = cashList.result.map((data) => ({
                    "ID Billing": data["idBilling"],
                    "Nomor Transaksi": data["docNo"],
                    Status: data["status"],
                    "Jenis Transaksi": data["mkpProductName"],
                    "Nama Merchant": data["mkpMerchantName"],
                    "Kode Merchant": data["mkpMerchantCode"],
                    "Nomor Referensi MKP": data["mkpTrxNo"],
                    // "Nomor Referensi Pembayaran": data["mkpPayrefNo"],
                    "Metode Pembayaran": data["mkpPaymentType"],
                    "Waktu Transaksi": data["mkpInquiryDatetime"],
                    // "Waktu Bayar": data["mkpPaymentDatetime"],
                    // Keterangan: data["mkpRemarks"],
                    "Nominal Transaksi": data["paymentAmount"] ? formatCurrency(data["paymentAmount"]) : 0,
                    "Kode OPD": data["bjtgKodeOpd"],
                    "Nama OPD": data["bjtgNamaOpd"],
                    "Tanggal Penetapan": data["bjtgTglPenetapan"],
                    "Tanggal Jatuh Tempo": data["bjtgTglJatuhTempo"],
                    "Tanggal Posting": data["bjtgTglPosting"],
                    "NOP Wajib Retribusi": data["bjtgNopWr"],
                    "Nama Wajib Retribusi": data["bjtgNamaWr"],
                    "Alamat Wajib Retribusi": data["bjtgAlamatWr"],
                    "Kode Produk 1": data["kodeBlj1"],
                    "Nama Produk 1": data["keteranganBlj1"],
                    "Nominal Produk 1": data["amtBlj1"] ? formatCurrency(data["amtBlj1"]) : 0,
                    "Kode Produk 2": data["kodeBlj2"],
                    "Nama Produk 2": data["keteranganBlj2"],
                    "Nominal Produk 2": data["amtBlj2"] ? formatCurrency(data["amtBlj2"]) : 0,
                    "Kode Produk 3": data["kodeBlj3"],
                    "Nama Produk 3": data["keteranganBlj3"],
                    "Nominal Produk 3": data["amtBlj3"] ? formatCurrency(data["amtBlj3"]) : 0,
                    "Kode Produk 4": data["kodeBlj4"],
                    "Nama Produk 4": data["keteranganBlj4"],
                    "Nominal Produk 4": data["amtBlj4"] ? formatCurrency(data["amtBlj4"]) : 0,
                    "Kode Produk 5": data["kodeBlj5"],
                    "Nama Produk 5": data["keteranganBlj5"],
                    "Nominal Produk 5": data["amtBlj5"] ? formatCurrency(data["amtBlj5"]) : 0,
                    "Kode Produk 6": data["kodeBlj6"],
                    "Nama Produk 6": data["keteranganBlj6"],
                    "Nominal Produk 6": data["amtBlj6"] ? formatCurrency(data["amtBlj6"]) : 0,
                    "Kode Produk 7": data["kodeBlj7"],
                    "Nama Produk 7": data["keteranganBlj7"],
                    "Nominal Produk 7": data["amtBlj7"] ? formatCurrency(data["amtBlj7"]) : 0,
                }));
                notify(cashList.message || "Success Get Data Cash", "success");
            } else {
                notify("No Data Cash Found", "warning");
            }

            const summary = await getDataCardCash(data2);
            let modifiedData2;
            if (summary && summary.result) {
                modifiedData2 = [{
                    "Total Transaction": thousandSeparator(summary.result["totalTrx"]),
                    "Total Amount": formatCurrency(summary.result["totalAmount"]),
                }];
                notify(summary.message || "Success Get Data Summary", "success");
            } else {
                notify("No Data Summary By Name Found", "warning");
            }

            if (modifiedData1 && modifiedData2) {
                exportToExcel(modifiedData1, modifiedData2);
            }
        } catch (error) {
            setDisableDownload(false);
            if (error.response) {
                // console.log(error.response.data.message);
                notify(error.response.data.message, "error");
            } else {
                // console.log(error.message || error);
                notify(error.message || error, "error");
            }
        }
    };

    const exportToExcel = (data1, data2) => {
        const workbook = new ExcelJS.Workbook();

        const ws1 = workbook.addWorksheet("Cash Transaction List");
        const ws2 = workbook.addWorksheet("Cash Transaction Summary");

        ws1.columns = [
            { header: "ID Billing", key: "ID Billing", width: 25 },
            { header: "Nomor Transaksi", key: "Nomor Transaksi", width: 25 },
            { header: "Status", key: "Status", width: 25 },
            { header: "Jenis Transaksi", key: "Jenis Transaksi", width: 25 },
            { header: "Nama Merchant", key: "Nama Merchant", width: 25 },
            { header: "Kode Merchant", key: "Kode Merchant", width: 25 },
            { header: "Nomor Referensi MKP", key: "Nomor Referensi MKP", width: 25 },
            // { header: "Nomor Referensi Pembayaran", key: "Nomor Referensi Pembayaran", width: 25 },
            { header: "Metode Pembayaran", key: "Metode Pembayaran", width: 25 },
            { header: "Waktu Transaksi", key: "Waktu Transaksi", width: 25 },
            // { header: "Waktu Bayar", key: "Waktu Bayar", width: 25 },
            // { header: "Keterangan", key: "Keterangan", width: 25 },
            { header: "Nominal Transaksi", key: "Nominal Transaksi", width: 25 },
            { header: "Kode OPD", key: "Kode OPD", width: 25 },
            { header: "Nama OPD", key: "Nama OPD", width: 25 },
            { header: "Tanggal Penetapan", key: "Tanggal Penetapan", width: 25 },
            { header: "Tanggal Jatuh Tempo", key: "Tanggal Jatuh Tempo", width: 25 },
            { header: "Tanggal Posting", key: "Tanggal Posting", width: 25 },
            { header: "NOP Wajib Retribusi", key: "NOP Wajib Retribusi", width: 25 },
            { header: "Nama Wajib Retribusi", key: "Nama Wajib Retribusi", width: 25 },
            { header: "Alamat Wajib Retribusi", key: "Alamat Wajib Retribusi", width: 25 },
            { header: "Kode Produk 1", key: "Kode Produk 1", width: 25 },
            { header: "Nama Produk 1", key: "Nama Produk 1", width: 25 },
            { header: "Nominal Produk 1", key: "Nominal Produk 1", width: 25 },
            { header: "Kode Produk 2", key: "Kode Produk 2", width: 25 },
            { header: "Nama Produk 2", key: "Nama Produk 2", width: 25 },
            { header: "Nominal Produk 2", key: "Nominal Produk 2", width: 25 },
            { header: "Kode Produk 3", key: "Kode Produk 3", width: 25 },
            { header: "Nama Produk 3", key: "Nama Produk 3", width: 25 },
            { header: "Nominal Produk 3", key: "Nominal Produk 3", width: 25 },
            { header: "Kode Produk 4", key: "Kode Produk 4", width: 25 },
            { header: "Nama Produk 4", key: "Nama Produk 4", width: 25 },
            { header: "Nominal Produk 4", key: "Nominal Produk 4", width: 25 },
            { header: "Kode Produk 5", key: "Kode Produk 5", width: 25 },
            { header: "Nama Produk 5", key: "Nama Produk 5", width: 25 },
            { header: "Nominal Produk 5", key: "Nominal Produk 5", width: 25 },
            { header: "Kode Produk 6", key: "Kode Produk 6", width: 25 },
            { header: "Nama Produk 6", key: "Nama Produk 6", width: 25 },
            { header: "Nominal Produk 6", key: "Nominal Produk 6", width: 25 },
            { header: "Kode Produk 7", key: "Kode Produk 7", width: 25 },
            { header: "Nama Produk 7", key: "Nama Produk 7", width: 25 },
            { header: "Nominal Produk 7", key: "Nominal Produk 7", width: 25 },
        ];

        ws1.addRows(data1);

        ws2.columns = [
            { header: "Total Transaction", key: "Total Transaction", width: 25 },
            { header: "Total Amount", key: "Total Amount", width: 25 },
        ];
        ws2.addRows(data2);

        [ws1, ws2].forEach((worksheet) => {
            worksheet.eachRow({ includeEmpty: false }, function (row, rowNumber) {
                row.eachCell((cell) => {
                    cell.border = {
                        top: { style: "thin" },
                        left: { style: "thin" },
                        bottom: { style: "thin" },
                        right: { style: "thin" },
                    };
                    cell.alignment = { horizontal: "left" };
                });
                if (rowNumber === 1) {
                    row.eachCell((cell) => {
                        cell.font = { bold: true };
                        cell.alignment = { horizontal: "center" };
                        cell.fill = {
                            type: "pattern",
                            pattern: "solid",
                            fgColor: { argb: "FF3875CA" },
                        };
                    });
                }
            });
        });

        workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer]), `Cash-ID-Billing-Transaction (${dateFrom.format("YYYY_MM_DD")} - ${dateTo.format("YYYY_MM_DD")}).xlsx`);
            setDisableDownload(false);
        });
    };

    return (
        <CustomButton onClick={handleFetch} startIcon={<FileDownloadIcon size="20px" />} name="Export&nbsp;" loading={disabled} color="success">
            Download
        </CustomButton>
    );
};

export default ExportToExcel;
