import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CloseIcon from '@mui/icons-material/Close';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import SearchIcon from '@mui/icons-material/Search';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Icon, IconButton, Input, MenuItem, Select, Stack, Typography } from '@mui/material';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import { merchant_data } from '../../../../../data/merchant';
import { confirmMember, getTableCreateMember, inquiryMember, updateMember } from '../../../../../services/parkir/parkir-asg';
import { formatCurrency } from '../../../../../utils/format-currency';
import CustomLOATable from '../components/custom-table-loa';
import DatepickerPopover from '../components/datepicker-popover';
import FilterPopover from '../components/filter-popover';

const PersetujuanMembership = (
    {
        merchantData = merchant_data,
        notify = () => { },
        username = "",
        setLoading = () => { },
        roleTask = "",
        label = "Membership Approval"
    }
) => {
    //REFRESH DATA (add label and value in merchantOption for rendering in select field)
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])

    const [userRoleTask, setUserRoleTask] = useState([])

    // AscDesc 
    const [isAscending, setIsAscending] = useState({
        value: "createdAt",
        order: "DESC"
    })

    // Filter
    const [search, setSearch] = useState("")
    const [debouncedSearch, setDebouncedSearch] = useState("")

    // Table
    const [dataTable, setDataTable] = useState({
        data: [],
        total: 0,
    })
    const [checkedValue, setCheckedValue] = useState([])
    const [offset, setOffset] = useState(1)
    const [limit, setLimit] = useState(25)

    // Popover (Filter Button)
    const statusTypeList = ["Reviewed", "Approved", "Rejected"]

    const [selectedOuCode, setSelectedOuCode] = useState("")
    const [selectedMemberType, setSelectedMemberType] = useState(["Freepass", "Member", "Special Member", "Multi Product"])
    const [selectedStatusType, setSelectedStatusType] = useState(["Reviewed", "Approved", "Rejected"])
    const [anchorElFilter, setAnchorElFilter] = useState(null);

    const handleClickPopperFilter = (event) => {
        setAnchorElFilter(event.currentTarget);
    };

    // Popover (Date Button)
    const [dateValue, setDateValue] = useState({
        startDate: moment().subtract(7, 'days'),
        endDate: moment()
    })
    const [anchorElDate, setAnchorElDate] = useState(null);

    const handleClickPopperDate = (event) => {
        setAnchorElDate(event.currentTarget);
    };

    const handleClosePopover = () => {
        setAnchorElFilter(null);
        setAnchorElDate(null)
    }

    const headerTable = [
        {
            title: CheckBoxOutlineBlankIcon,
            width: "30px",
        },
        {
            title: "No.Member",
            value: "docNoAprroval",
            width: "40px",
        },
        {
            title: "Name",
            value: "firstName",
        },
        {
            title: "Email",
            value: "email",
        },
        {
            title: "No. HP",
            value: "phoneNumber",
        },
        {
            title: "Member Type",
            value: "typePartner",
        },
        {
            title: "Duration Period",
            value: "durationPeriod",
        },
        {
            title: "Vehicle Type",
            value: "productName",
        },
        {
            title: "Date Created",
            value: "createdAt",
        },
        {
            title: "Start Date",
            value: "dateFrom",
        },
        {
            title: "End Date",
            value: "dateTo",
        },
        {
            title: "Expired Date",
            value: "expiredDate",
        },
        {
            title: "Amount",
            value: "amount",
        },
        {
            title: "Status",
            value: "statusApprovalCode", //statusApproval
        },
        {
            title: "Action",
            value: "",
        }
    ]

    const handleGetData = ({ withNotification = true, outletCode, offsetData = offset, ascDesc = isAscending.order, columnOrder = isAscending.value, draw = 0, callback = () => { } }) => {
        setLoading(true)
        let data = {
            "outletCode": outletCode,
            "keyword": debouncedSearch,
            "draw": draw,
            "ascDesc": ascDesc,
            "columnOrderName": columnOrder,
            "limit": limit,
            "offset": (offsetData - 1) * limit,
            "status": selectedStatusType.toString().toUpperCase(), // WAITING, SUBMITTED
            "typeMember": selectedMemberType.toString().toUpperCase(), //FREEPASS
            "dateStart": `${moment(dateValue.startDate).format("YYYY-MM-DD")} 00:00:00`,
            "dateEnd": `${moment(dateValue.endDate).format("YYYY-MM-DD")} 23:59:59`
        }
        getTableCreateMember(data).then((res) => {
            if (res.result !== null) {
                let fetchedData = res.result.dataApprovalMember
                let fetchedTotal = res.result.countDataMember
                setDataTable((prev) => ({ ...prev, data: fetchedData }))
                if (draw === 1) {
                    setDataTable((prev) => ({ ...prev, total: fetchedTotal }))
                }
                if (withNotification) {
                    notify("Success Get Data List", "success");
                }
            }
        }).finally(() => {
            setLoading(false)
            callback()
        }).catch((e) => {
            setLoading(false)
            setDataTable({ data: [], total: 0 })
            if (withNotification) {
                if (e.response) {
                    notify(e.response.data.message, "warning");
                } else {
                    notify(e.message, "warning");
                }
            }
        })
    }

    const handleApproval = ({ id = "", callback = () => { } }) => {
        setLoading(true)
        let data = {
            "_id": id,
            "paymentMethod": "TUNAI",
            "paymentCode": "10001",
            "paymentMethodAliasName": "TUNAI",
            "inquiryDatetime": `${moment().format("YYYY-MM-DD HH:mm:ss")}`,
        }

        inquiryMember(data).then((res) => {
        }).finally(() => {
            setTimeout(() => {
                let dataConfirm = {
                    "_id": id,
                    "username": username
                }
                confirmMember(dataConfirm).then((res) => {
                    if (res.result) {
                        notify("Success Data Has Been Updated", "success");
                    } 
                }).finally(() => {
                    setLoading(false)
                    callback()
                    handleGetData({ withNotification: false, outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
                }).catch((e) => {
                    setLoading(false)
                    if (e.response) {
                        notify(e.response.data.message, "warning")
                    } else {
                        notify(e.message, "warning");
                    }
                })
            }, 500)
        }).catch((e) => {
            setLoading(false)
            if (e.response) {
                notify(e.response.data.message, "warning")
            } else {
                notify(e.message, "warning");
            }
        })
    }

    const handleReject = ({ id = "", callback = () => { } }) => {
        setLoading(true)
        let data = {
            "statusApproval": "REJECTED",
            "username": username,
            "date": `${moment().format("YYYY-MM-DD HH:mm:ss")}`,
            "idList": [id]
        }

        updateMember(data).then((res) => {
            if (res.result) {
                notify(`Success ${res.result.countDataUpdated} Data Has Been Updated`)
            }
        }).finally(() => {
            setLoading(false)
            callback()
            handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order })
        }).catch((e) => {
            setLoading(false)
            if (e.response) {
                notify(e.response.data.message, "warning")
            } else {
                notify(e.message, "warning");
            }
        })
    }

    // CHECK BUTTON LANJUTKAN
    const checkBtnLanjutkan = useMemo(() => {
        if (checkedValue.length > 0) {
            return false
        } else {
            return true
        }
    }, [checkedValue])

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setUserRoleTask(roleTask.split(","))
        setMerchantOption(merchantArr);
    }, [merchantData, roleTask]);

    useEffect(() => {
        if (merchantOption.length > 0 && userRoleTask.includes("viewMembershipApproval")) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetData({ outletCode: ouCodeArr, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
    }

    // HANDLE FILTER POPOVER & DATEPICKER
    useEffect(() => {
        if (allOutletCodeList.length > 0) {
            handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: 1, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
            setOffset(1)
        }
    }, [dateValue, selectedOuCode, selectedMemberType, selectedStatusType, debouncedSearch, limit])

    // HANDLE FILTER SEARCH
    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setDebouncedSearch(search);
        }, 500)
        return () => clearTimeout(timeoutId);
    }, [search, 500])

    // =========== QUEUE LOGIC (CONTINUE PROCESS) ============
    const [openDialogQueue, setOpenDialogQueue] = useState(false);
    const [selectedDataQueue, setSelectedDataQueue] = useState([]);
    // FUNCTION BUTTON LANJUTKAN
    const handleBtnMultipleContinueProcess = () => {
        // set selectedData dengan membandingkan array checkedValue [ (isinya ID only) index ke 0, dengan array dataTable.data (isinya semua data)
        setSelectedDataQueue(dataTable.data.find((item) => item.id === checkedValue[0]))
        setOpenDialogQueue(true)
    };

    const handleCloseDialogQueue = () => {
        setOpenDialogQueue(false)
    }

    const updateSelectedDataQueue = () => {
        if (checkedValue.length > 1) { // update value selectedDataQueue dengan index ke 1 pada checked value
            setSelectedDataQueue(dataTable.data.find((item) => item.id === checkedValue[1]))
        }
        setCheckedValue((prev) => {
            prev.length > 1 ? setOpenDialogQueue(true) : setOpenDialogQueue(false) // mencabut index ke 0 pada checked value (FIFO logic)
            return prev.filter((item) => {
                return item !== checkedValue[0]
            })
        })
    }
    if (!userRoleTask.includes("viewMembershipApproval")) {
        return null
    } else {
        return (
            <Stack mx={"3rem"} mt={"1.5rem"} direction={"column"} gap={1} fontFamily={'sans-serif'}>
                {/* Header */}
                <Typography variant="h1" sx={{ fontWeight: "bold", fontSize: "2.5rem", color: "#2B3499" }}>{label}</Typography>
                {/* Pengajuan & Search */}
                <Stack direction={['column-reverse', 'row']} gap={1} mt={2} justifyContent={"space-between"} alignItems={"flex-start"}>
                    <Stack direction={"row"} gap={1} alignItems={"center"}>
                        <Select
                            value={limit}
                            onChange={(e) => setLimit(e.target.value)}
                            sx={{ color: 'black', p: 0, height: "40px", width: "75px", borderRadius: "10px" }}
                        >
                            <MenuItem value={25}>25</MenuItem>
                            <MenuItem value={50}>50</MenuItem>
                            <MenuItem value={100}>100</MenuItem>
                        </Select>
                        <Typography>Entries per Page</Typography>
                    </Stack>
                    <Stack direction={["column", "row"]} gap={1} sx={{ fontSize: "14px" }} width={["100%", "auto"]}>
                        {userRoleTask.includes("updateStatusMembershipApproval") ?
                            <Button disabled={checkBtnLanjutkan} sx={{ height: [46, "auto"], ":disabled": { bgcolor: "#C2C2C2", color: "#FFF7F7" }, ":hover": { bgcolor: "#1d257a" }, bgcolor: "#2B3499", color: "#FFF7F7", borderRadius: "10px", padding: "5px 20px", textTransform: "capitalize" }}
                                onClick={() => handleBtnMultipleContinueProcess()}
                            >Continue Process</Button> : null
                        }
                        <Stack direction={"row"} gap={1} justifyContent={["space-between", "flex-end"]} >
                            {/* Datepicker Button */}
                            <Button onClick={(e) => handleClickPopperDate(e)} sx={{ width: ["100%", "auto"], ":disabled": { bgcolor: "#C2C2C2" }, ":hover": { bgcolor: "#2B3499", "& svg": { color: "#FFFFFF" } }, "& svg": { color: anchorElDate ? "#FFFFFF" : "#2B3499" }, bgcolor: anchorElDate ? "#2B3499" : "transparent", border: "1px solid #2B3499", color: "#FFF7F7", borderRadius: "10px", minWidth: "auto" }}><Icon sx={{ fontSize: "1.5rem" }} component={CalendarMonthIcon} /></Button>
                            {/* Filter Button */}
                            <Button onClick={(e) => handleClickPopperFilter(e)} sx={{ width: ["100%", "auto"], ":disabled": { bgcolor: "#C2C2C2" }, ":hover": { bgcolor: "#2B3499", "& svg": { color: "#FFFFFF" } }, "& svg": { color: anchorElFilter ? "#FFFFFF" : "#2B3499" }, bgcolor: anchorElFilter ? "#2B3499" : "transparent", border: "1px solid #2B3499", color: "#FFF7F7", borderRadius: "10px", minWidth: "auto" }}><Icon sx={{ fontSize: "1.5rem" }} component={FilterAltIcon} /></Button>
                        </Stack>

                        <Box sx={{ border: "1px solid #C2C2C2", borderRadius: "10px", padding: "5px 10px", display: 'flex', alignItems: 'center', gap: 1, color: "#2B3499" }}>
                            <Icon sx={{ fontSize: "1.5rem" }} component={SearchIcon} />
                            <Input color='#2B3499' disableUnderline type='search' value={search} onChange={(e) => setSearch(e.target.value)} placeholder='Search' />
                        </Box>
                    </Stack>
                    <FilterPopover anchorEl={anchorElFilter} setAnchorEl={setAnchorElFilter} handleClosePopover={handleClosePopover} statusTypeList={statusTypeList} merchantOption={merchantOption} selectedOuCode={selectedOuCode} setSelectedOuCode={setSelectedOuCode} selectedMemberType={selectedMemberType} setSelectedMemberType={setSelectedMemberType} selectedStatusType={selectedStatusType} setSelectedStatusType={setSelectedStatusType} />
                    <DatepickerPopover anchorEl={anchorElDate} setAnchorEl={setAnchorElDate} handleClosePopover={handleClosePopover} dateValue={dateValue} setDateValue={setDateValue} />
                </Stack>

                <CustomLOATable dataTable={dataTable.data} totalData={dataTable.total} headerTable={headerTable} checkedValue={checkedValue} setCheckedValue={setCheckedValue} isAscending={isAscending} setIsAscending={setIsAscending} limit={limit} offset={offset} setOffset={setOffset} approvalProhibition={["APPROVED", "REJECTED"]}
                    handleGetDataTable={({ pages, asc, setIsAsc }) => handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: pages, limitData: limit, ascDesc: asc ? asc.order : "ASC", columnOrder: asc ? asc.value : isAscending.value, callback: () => setIsAsc() })}
                    handleBtnContinueProcess={({ selectedId, closeDialog }) => handleApproval({ id: selectedId, callback: () => closeDialog() })}
                    handleBtnRejectProcess={({ selectedId, closeDialog }) => handleReject({ id: selectedId, callback: () => closeDialog() })}
                    rejectButton={userRoleTask.includes("updateStatusMembershipApproval")} approveButton={userRoleTask.includes("updateStatusMembershipApproval")} checkbox={userRoleTask.includes("updateStatusMembershipApproval")} />

                {/* DIALOG FOR CONTINUE PROCESS (QUEUE) */}
                <Dialog
                    open={openDialogQueue}
                    onClose={() => handleCloseDialogQueue()}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle paddingX={3} id="alert-dialog-title" color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>
                        Submission Detail
                    </DialogTitle>
                    <IconButton
                        aria-label="close"
                        onClick={() => handleCloseDialogQueue()}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: "black",
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <DialogContent sx={{ paddingX: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Card Number</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.cardNumber ? selectedDataQueue.cardNumber : "-"}</Typography>
                                </Stack>
                            </Grid>
                            {/* <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Vehicle Number</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.vehicleNumber ? selectedDataQueue.vehicleNumber : "-"}</Typography>
                                </Stack>
                            </Grid> */}
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Member Number</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.docNoAprroval}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Full Name</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.firstName} {selectedDataQueue.lastName}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Email</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.email}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Phone Number</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.phoneNumber}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Start Date</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.dateFrom}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>End Date</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.dateTo}</Typography>
                                </Stack>
                            </Grid>
                            {selectedDataQueue.expiredDate !== "" &&
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Expired Date</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.expiredDate}</Typography>
                                    </Stack>
                                </Grid>
                            }
                            {selectedDataQueue.productName !== "" &&
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Product Type</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.productName}</Typography>
                                    </Stack>
                                </Grid>
                            }
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Member Type</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.typePartner}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Group Type</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.roleType}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack gap={"4px"}>
                                    <Typography sx={{ color: "#757575" }}>Remark</Typography>
                                    <Typography sx={{ color: "#1C1B1F" }}>{selectedDataQueue.remark ? selectedDataQueue.remark : "-"}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12}>
                                <Box sx={{ borderTop: "1px dashed #ccc", mt: 2 }}></Box>
                                <Typography sx={{ fontSize: "18px", color: "#1C1B1F", fontWeight: "550", mt: 4 }}>Approved List</Typography>
                            </Grid>
                            {selectedDataQueue.approvalList ? selectedDataQueue.approvalList.map((item, index) => {
                                return (
                                    <Grid item xs={6} pt={0} key={index}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575", textTransform: "capitalize" }}>{item.statusApproval.toLowerCase() === "waiting" ? "created" : item.statusApproval.toLowerCase()} by</Typography>
                                            <Typography sx={{ color: "#1C1B1F", fontWeight: "500" }}>{item.username}</Typography>
                                            <Typography sx={{ color: "#4d4c4c" }}>{item.approvalTime}</Typography>
                                        </Stack>
                                    </Grid>
                                )
                            }) : null}
                            <Grid item xs={12} textAlign={"center"} mt={2}>
                                <Stack>
                                    <Typography sx={{ fontSize: "20px", color: "#1C1B1F" }}>Amount</Typography>
                                    <Typography sx={{ fontSize: "36px", color: "#2B3499", fontWeight: "bold" }}>{formatCurrency(selectedDataQueue.amount)}</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions sx={{ display: "flex", justifyContent: "center", px: 3 }}>
                        <Button onClick={() => {
                            handleReject({ id: selectedDataQueue.id, callback: () => updateSelectedDataQueue() })
                        }} autoFocus sx={{ width: "50%", bgcolor: "transparent", py: "12px", mb: "5px", color: "#C41F1F", textTransform: "capitalize", border: "1px solid #C41F1F", ":hover": { bgcolor: "#C41F1F", color: "#FFF7F7" } }}>
                            Reject
                        </Button>
                        {/* BUAT PROMISE AGAR BERURUTAN JALANNYA */}
                        <Button onClick={() => {
                            handleApproval({ id: selectedDataQueue.id, callback: () => updateSelectedDataQueue() })
                        }} autoFocus sx={{ width: "50%", bgcolor: "#2B3499", py: "12px", mb: "5px", color: "#FFF7F7", textTransform: "capitalize", ":hover": { bgcolor: "#1d257a" } }}>
                            Approve
                        </Button>
                    </DialogActions>
                </Dialog>
            </Stack >
        )
    }
}


export default PersetujuanMembership