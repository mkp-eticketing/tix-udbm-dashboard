import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CloseIcon from '@mui/icons-material/Close';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import SearchIcon from '@mui/icons-material/Search';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Icon, IconButton, Input, MenuItem, Select, Stack, Typography } from '@mui/material';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import { merchant_data } from '../../../../../data/merchant';
import { getTableCreateMember, updateMember } from '../../../../../services/parkir/parkir-asg';
import CustomLOATable from '../components/custom-table-loa';
import DatepickerPopover from '../components/datepicker-popover';
import FilterPopover from '../components/filter-popover';

const PeninjauanMembership = (
    {
        merchantData = merchant_data,
        notify = () => { },
        username = "",
        setLoading = () => { },
        roleTask = "",
        label = "Membership Review"
    }
) => {
    //REFRESH DATA (add label and value in merchantOption for rendering in select field)
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])

    const [userRoleTask, setUserRoleTask] = useState([])

    // AscDesc 
    const [isAscending, setIsAscending] = useState({
        value: "createdAt",
        order: "DESC"
    })

    // Dialog Multiple Continue
    const [openDialogMultipleContinue, setOpenDialogMultipleContinue] = useState(false)

    // Filter
    const [search, setSearch] = useState("")
    const [debouncedSearch, setDebouncedSearch] = useState("")

    // Table
    const [dataTable, setDataTable] = useState({
        data: [],
        total: 0,
    })
    const [checkedValue, setCheckedValue] = useState([])
    const [offset, setOffset] = useState(1)
    const [limit, setLimit] = useState(25)

    // Popover (Filter Button)
    const statusTypeList = ["Submitted", "Reviewed"]

    const [selectedOuCode, setSelectedOuCode] = useState("")
    const [selectedMemberType, setSelectedMemberType] = useState(["Freepass", "Member", "Special Member", "Multi Product"])
    const [selectedStatusType, setSelectedStatusType] = useState(["Submitted", "Reviewed"])
    const [anchorElFilter, setAnchorElFilter] = useState(null);

    const handleClickPopperFilter = (event) => {
        setAnchorElFilter(event.currentTarget);
    };

    // Popover (Date Button)
    const [dateValue, setDateValue] = useState({
        startDate: moment().subtract(7, 'days'),
        endDate: moment()
    })
    const [anchorElDate, setAnchorElDate] = useState(null);

    const handleClickPopperDate = (event) => {
        setAnchorElDate(event.currentTarget);
    };

    const handleClosePopover = () => {
        setAnchorElFilter(null);
        setAnchorElDate(null)
    }

    const headerTable = [
        {
            title: CheckBoxOutlineBlankIcon,
            width: "30px",
        },
        {
            title: "No.Member",
            value: "docNoAprroval",
            width: "40px",
        },
        {
            title: "Name",
            value: "firstName",
        },
        {
            title: "Email",
            value: "email",
        },
        {
            title: "No. HP",
            value: "phoneNumber",
        },
        {
            title: "Member Type",
            value: "typePartner",
        },
        {
            title: "Duration Period",
            value: "durationPeriod",
        },
        {
            title: "Vehicle Type",
            value: "productName",
        },
        {
            title: "Date Created",
            value: "createdAt",
        },
        {
            title: "Start Date",
            value: "dateFrom",
        },
        {
            title: "End Date",
            value: "dateTo",
        },
        {
            title: "Expired Date",
            value: "expiredDate",
        },
        {
            title: "Amount",
            value: "amount",
        },
        {
            title: "Status",
            value: "statusApprovalCode", //statusApproval
        },
        {
            title: "Action",
            value: "",
        }
    ]

    const handleGetData = ({ withNotification = true, outletCode, offsetData = offset, ascDesc = isAscending.order, columnOrder = isAscending.value, draw = 0, callback = () => { } }) => {
        setLoading(true)
        let data = {
            "outletCode": outletCode,
            "keyword": debouncedSearch,
            "draw": draw,
            "ascDesc": ascDesc,
            "columnOrderName": columnOrder,
            "limit": limit,
            "offset": (offsetData - 1) * limit,
            "status": selectedStatusType.toString().toUpperCase(), // WAITING, SUBMITTED
            "typeMember": selectedMemberType.toString().toUpperCase(), //FREEPASS
            "dateStart": `${moment(dateValue.startDate).format("YYYY-MM-DD")} 00:00:00`,
            "dateEnd": `${moment(dateValue.endDate).format("YYYY-MM-DD")} 23:59:59`
        }

        getTableCreateMember(data).then((res) => {
            if (res.result !== null) {
                let fetchedData = res.result.dataApprovalMember
                let fetchedTotal = res.result.countDataMember
                setDataTable((prev) => ({ ...prev, data: fetchedData }))
                if (draw === 1) {
                    setDataTable((prev) => ({ ...prev, total: fetchedTotal }))
                }
                if (withNotification) {
                    notify("Success Get Data List", "success");
                }
            }
        }).finally(() => {
            setLoading(false)
            callback()
        }).catch((e) => {
            setLoading(false)
            setDataTable({ data: [], total: 0 })
            if (withNotification) {
                if (e.response) {
                    notify(e.response.data.message, "warning");
                } else {
                    notify(e.message, "warning");
                }
            }
        })
    }

    const handleApproval = ({ id = "", callback = () => { } }) => {
        setLoading(true)
        let data = {
            "statusApproval": "REVIEWED",
            "username": username,
            "date": `${moment().format("YYYY-MM-DD HH:mm:ss")}`,
            "idList": Array.isArray(id) ? id : [id]
        }
        updateMember(data).then((res) => {
            if (res.result) {
                notify(`Success, ${res.result.countDataUpdated} Data Has Been Updated`, "success");
            } 
        }).finally(() => {
            setLoading(false)
            callback()
            handleGetData({ withNotification: false, outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
            setCheckedValue([])
        }).catch((e) => {
            setLoading(false)
            if (e.response) {
                notify(e.response.data.message, "warning");
            } else {
                notify(e.message, "warning");
            }
        })
    }

    // CHECK BUTTON LANJUTKAN
    const checkBtnLanjutkan = useMemo(() => {
        if (checkedValue.length > 0) {
            return false
        } else {
            return true
        }
    }, [checkedValue])

    // FUNCTION BUTTON LANJUTKAN
    const handleBtnMultipleContinueProcess = () => {
        handleApproval({
            id: checkedValue
            , callback: () => {
                setOpenDialogMultipleContinue(false)
                // handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
            }
        })
    };

    const handleOpenDialogMultipleContinueProcess = () => {
        setOpenDialogMultipleContinue(true)
    }

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setUserRoleTask(roleTask.split(","))
        setMerchantOption(merchantArr);
    }, [merchantData, roleTask]);

    useEffect(() => {
        if (merchantOption.length > 0 && userRoleTask.includes("viewMembershipReview")) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetData({ outletCode: ouCodeArr, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
    }

    // HANDLE FILTER POPOVER & DATEPICKER
    useEffect(() => {
        if (allOutletCodeList.length > 0) {
            handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: 1, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
            setOffset(1)
        }
    }, [dateValue, selectedOuCode, selectedMemberType, selectedStatusType, debouncedSearch, limit])

    // HANDLE FILTER SEARCH
    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setDebouncedSearch(search);
        }, 500)
        return () => clearTimeout(timeoutId);
    }, [search, 500])

    if (!userRoleTask.includes("viewMembershipReview")) {
        return null
    }
    else {
        return (
            <Stack mx={"3rem"} mt={"1.5rem"} direction={"column"} gap={1} fontFamily={'sans-serif'}>
                {/* Header */}
                <Typography variant="h1" sx={{ fontWeight: "bold", fontSize: "2.5rem", color: "#2B3499" }}>{label}</Typography>
                {/* Pengajuan & Search */}
                <Stack direction={['column-reverse', 'row']} gap={1} mt={2} justifyContent={"space-between"} alignItems={"flex-start"}>
                    <Stack direction={"row"} gap={1} alignItems={"center"}>
                        <Select
                            value={limit}
                            onChange={(e) => setLimit(e.target.value)}
                            sx={{ color: 'black', p: 0, height: "40px", width: "75px", borderRadius: "10px" }}
                        >
                            <MenuItem value={25}>25</MenuItem>
                            <MenuItem value={50}>50</MenuItem>
                            <MenuItem value={100}>100</MenuItem>
                        </Select>
                        <Typography>Entries per Page</Typography>
                    </Stack>
                    <Stack direction={["column", "row"]} gap={1} sx={{ fontSize: "14px" }} width={["100%", "auto"]}>
                        {userRoleTask.includes("updateStatusMembershipReview") ?
                            <Button disabled={checkBtnLanjutkan} sx={{ height:[ 46, "auto"], ":disabled": { bgcolor: "#C2C2C2", color: "#FFF7F7" }, ":hover": { bgcolor: "#1d257a" }, bgcolor: "#2B3499", color: "#FFF7F7", borderRadius: "10px", padding: "5px 20px", textTransform: "capitalize" }}
                                onClick={() => handleOpenDialogMultipleContinueProcess()}
                            >Continue Process</Button> : null
                        }
                        <Stack direction={"row"} gap={1} justifyContent={["space-between", "flex-end"]}>
                            {/* Datepicker Button */}
                            <Button onClick={(e) => handleClickPopperDate(e)} sx={{ width: ["100%", "auto"], ":disabled": { bgcolor: "#C2C2C2" }, ":hover": { bgcolor: "#2B3499", "& svg": { color: "#FFFFFF" } }, "& svg": { color: anchorElDate ? "#FFFFFF" : "#2B3499" }, bgcolor: anchorElDate ? "#2B3499" : "transparent", border: "1px solid #2B3499", color: "#FFF7F7", borderRadius: "10px", minWidth: "auto" }}><Icon sx={{ fontSize: "1.5rem" }} component={CalendarMonthIcon} /></Button>
                            {/* Filter Button */}
                            <Button onClick={(e) => handleClickPopperFilter(e)} sx={{ width: ["100%", "auto"], ":disabled": { bgcolor: "#C2C2C2" }, ":hover": { bgcolor: "#2B3499", "& svg": { color: "#FFFFFF" } }, "& svg": { color: anchorElFilter ? "#FFFFFF" : "#2B3499" }, bgcolor: anchorElFilter ? "#2B3499" : "transparent", border: "1px solid #2B3499", color: "#FFF7F7", borderRadius: "10px", minWidth: "auto" }}><Icon sx={{ fontSize: "1.5rem" }} component={FilterAltIcon} /></Button>
                        </Stack>

                        <Box sx={{ border: "1px solid #C2C2C2", borderRadius: "10px", padding: "5px 10px", display: 'flex', alignItems: 'center', gap: 1, color: "#2B3499" }}>
                            <Icon sx={{ fontSize: "1.5rem" }} component={SearchIcon} />
                            <Input color='#2B3499' disableUnderline type='search' value={search} onChange={(e) => setSearch(e.target.value)} placeholder='Search' />
                        </Box>
                    </Stack>
                    <FilterPopover anchorEl={anchorElFilter} setAnchorEl={setAnchorElFilter} handleClosePopover={handleClosePopover} statusTypeList={statusTypeList} merchantOption={merchantOption} selectedOuCode={selectedOuCode} setSelectedOuCode={setSelectedOuCode} selectedMemberType={selectedMemberType} setSelectedMemberType={setSelectedMemberType} selectedStatusType={selectedStatusType} setSelectedStatusType={setSelectedStatusType} />
                    <DatepickerPopover anchorEl={anchorElDate} setAnchorEl={setAnchorElDate} handleClosePopover={handleClosePopover} dateValue={dateValue} setDateValue={setDateValue} />
                </Stack>

                <CustomLOATable dataTable={dataTable.data} totalData={dataTable.total} headerTable={headerTable} checkedValue={checkedValue} setCheckedValue={setCheckedValue} isAscending={isAscending} setIsAscending={setIsAscending} limit={limit} offset={offset} setOffset={setOffset} approvalProhibition='REVIEWED'
                    handleGetDataTable={({ pages, asc, setIsAsc }) => handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: pages, limitData: limit, ascDesc: asc ? asc.order : "ASC", columnOrder: asc ? asc.value : isAscending.value, callback: () => setIsAsc() })}
                    handleBtnContinueProcess={({ selectedId, closeDialog }) => handleApproval({ id: selectedId, callback: () => closeDialog() })}
                    approveButton={userRoleTask.includes("updateStatusMembershipReview")} checkbox={userRoleTask.includes("updateStatusMembershipReview")}
                />

                {openDialogMultipleContinue ?
                    <Dialog
                        open={openDialogMultipleContinue}
                        onClose={() => setOpenDialogMultipleContinue(false)}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle sx={{ borderBottom: "1px solid #ccc" }} id="alert-dialog-title" color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>
                            Are You Sure
                        </DialogTitle>
                        <IconButton
                            aria-label="close"
                            onClick={() => setOpenDialogMultipleContinue(false)}
                            sx={{
                                position: 'absolute',
                                right: 8,
                                top: 8,
                                color: "black",
                            }}
                        >
                            <CloseIcon />
                        </IconButton>
                        <DialogContent>
                            <Typography>Are you sure want to continue the process for {checkedValue.length} data</Typography>
                        </DialogContent>
                        <DialogActions sx={{ display: "flex", justifyContent: "center", px: 3 }}>
                            <Button onClick={() => {
                                setOpenDialogMultipleContinue(false)
                            }} autoFocus sx={{ width: "50%", borderRadius: "8px", bgcolor: "transparent", py: "12px", mb: "5px", color: "#C41F1F", textTransform: "capitalize", border: "1px solid #C41F1F", ":hover": { bgcolor: "#C41F1F", color: "#FFF7F7" } }}>
                                No
                            </Button>
                            <Button onClick={() => {
                                handleBtnMultipleContinueProcess()
                            }} autoFocus sx={{ width: "50%", borderRadius: "8px", bgcolor: "#2B3499", py: "12px", mb: "5px", color: "#FFF7F7", textTransform: "capitalize", ":hover": { bgcolor: "#1d257a" } }}>
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                    : null}
            </Stack>
        )
    }
}


export default PeninjauanMembership