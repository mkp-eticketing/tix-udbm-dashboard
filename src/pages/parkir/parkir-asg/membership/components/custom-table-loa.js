import { Box, Breadcrumbs, Button, Card, Checkbox, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Fade, FormControlLabel, FormGroup, Grid, Icon, IconButton, Input, Popper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from '@mui/material'
import React, { useEffect, useMemo, useState } from 'react'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import IndeterminateCheckBoxIcon from '@mui/icons-material/IndeterminateCheckBox';
import CloseIcon from '@mui/icons-material/Close';
import { formatCurrency } from "../../../../../utils/format-currency";


const CustomLOATable = ({
    headerTable = [],
    dataTable = [],
    checkedValue = [],
    setCheckedValue = () => { },
    offset = 1,
    setOffset = () => { },
    totalData = 0,
    isAscending,
    setIsAscending = () => { },
    handleGetDataTable = () => { },
    handleBtnContinueProcess = () => { },
    handleBtnRejectProcess = () => { },
    limit = 10,
    rejectButton = false,
    approveButton = true,
    approvalProhibition = "", // jika diisi maka akan melakukan limit approval mana yang akan di 'disabled'
    checkbox = true
}) => {
    const approvalProhibitionArr = typeof approvalProhibition === "string" ? [approvalProhibition] : approvalProhibition

    // Dialog
    const [openDialog, setOpenDialog] = useState(false)
    const [selectedData, setSelectedData] = useState()

    // Handle open the dialog
    const handleClickOpen = (data) => {
        setSelectedData(data)
        console.log(data)
        setOpenDialog(true);
    };

    const handleClickClose = () => {
        setSelectedData()
        setOpenDialog(false)
    }

    // CHECKBOX FUNCTIONs
    const handleCheckBoxClickedALL = () => {
        if (checkedValue.length === 0) {
            if (dataTable) {
                dataTable.map((item) => {
                    if (checkedValue.includes(item.id)) { // Kalau sudah dicentang maka jangan lakukan apa apa
                        return
                    } else { // Kalau belum dicentang
                        if (approvalProhibitionArr.indexOf(item.statusApproval) !== -1) { // jika statusnya sesuai dengan prohibition maka jangan masukkan 
                            return
                        } else {
                            setCheckedValue(prevState => [...prevState, item.id]) // masukkan ke checkedValue
                        }
                    }
                })
            }
        } else {
            setCheckedValue([])
        }
    }
    const handleCheckBoxClicked = (id) => {
        setCheckedValue(prevState => {
            if (prevState.includes(id)) {
                return prevState.filter(item => item !== id)
            } else {
                return [...prevState, id]
            }
        })
    }

    // Pagination
    const [pagination, setPagination] = useState(1) // Page Display
    const [inputGoTo, setInputGoto] = useState('')

    useEffect(() => {
        // Pagination Logic
        let paginationCounter = Math.ceil(totalData / limit)
        if (paginationCounter === 0) {
            setPagination(1)
        } else {
            setPagination(paginationCounter)
        }
    }, [dataTable])

    // Pagination FUNCTIONS
    const debounce = (func, delay) => {
        let timeoutId;
        return function (...args) {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(() => {
                func.apply(this, args);
            }, delay);
        };
    };

    const handleGoToPagination = (e) => {
        let inputValue = e.target.value
        let isValidInput = /^\d*$/.test(inputValue);
        if (isValidInput && inputValue <= pagination) {
            if (parseInt(inputValue)) {
                setOffset(parseInt(inputValue))
                handleGetDataTable({ pages: parseInt(inputValue) })
            }
        }
        if (inputValue === '') {
            setOffset(1)
            handleGetDataTable({ pages: 1 })
        }
    }

    const debouncedHandleGoToPagination = debounce(handleGoToPagination, 500);

    const handlePagination = (operator) => {
        let newOffset = 0
        if (operator === 'add') {
            handleGetDataTable({ pages: offset + 1 })

            setOffset((prevValue) => {
                newOffset = prevValue + 1
                // console.log('pagination val', newOffset);
                return newOffset
            })
        } else if (operator === 'sub') {
            handleGetDataTable({ pages: offset - 1 })

            setOffset((prevValue) => {
                newOffset = prevValue - 1
                // console.log('pagination val', newOffset);
                return newOffset
            })
        }
    }

    const handleAscDesc = (headerValue) => {
        if (isAscending.value !== headerValue || isAscending.order === "DESC") { // jika value berbeda (dari Phonenumber, ke status) maka default akan menjadi asc || jika status desc maka akan menjadi ascending
            handleGetDataTable({
                asc: { value: headerValue, order: "ASC" }, setIsAsc: () => {
                    setIsAscending({
                        value: headerValue,
                        order: "ASC"
                    })
                }
            })
        } else {
            console.log('descending', headerValue);
            handleGetDataTable({
                asc: { value: headerValue, order: "DESC" }, setIsAsc: () => {
                    setIsAscending({
                        value: headerValue,
                        order: "DESC"
                    })
                }
            })
        }
    }

    return (
        <Stack>
            <Card sx={{ borderRadius: "0.625rem", mt: "0.5rem" }}>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                {headerTable ? headerTable.map((item, index) => {
                                    return <TableCell onClick={index > 0 ? () => handleAscDesc(item.value) : null} sx={{
                                        display: index === 0 ? checkbox ? "table-cell" : "none" : "table-cell",
                                        background: '#EDEDED',
                                        color: '#2B3499',
                                        textTransform: 'uppercase',
                                        fontWeight: 'bold',
                                        whiteSpace: "nowrap",
                                        width: "auto",
                                        position: index < 1 && "sticky",
                                        left: index < 1 && `${index * 40}px`,
                                        zIndex: index < 1 && 10,
                                        cursor: index < 1 ? "default" : "pointer",
                                    }} key={item.title}>
                                        <Stack direction={"row"} gap={1} alignItems={"center"} textAlign={'center'}>
                                            {index < 1 ? <Icon sx={{ fontSize: "1.5rem", ":hover": { cursor: "pointer" } }} component={checkedValue.length === 0 ? CheckBoxOutlineBlankIcon : CheckBoxIcon} onClick={handleCheckBoxClickedALL} /> : item.title}
                                            {/* render arrow and asc desc */}
                                            {index < 1 || index > (headerTable.length - 2) ? null : isAscending.value === item.value ? isAscending.order === "ASC" ? <ArrowUpwardIcon sx={{ width: "16px" }} /> : <ArrowDownwardIcon sx={{ width: "16px" }} /> : <Stack direction={"row"}> <ArrowUpwardIcon sx={{ width: "16px", mr: "-4px" }} /> <ArrowDownwardIcon sx={{ width: "16px" }} /> </Stack>}
                                        </Stack>
                                    </TableCell>
                                }) : null}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {dataTable ? dataTable.map((item, index) =>
                                <TableRow key={index} sx={{
                                    ":nth-of-type(even)": {
                                        background: '#F1F2FB'
                                    },
                                    ":nth-of-type(odd)": {
                                        background: 'white'
                                    },
                                    td: {
                                        color: '#2B3499',
                                        whiteSpace: "nowrap"
                                    },
                                }}>

                                    <TableCell sx={{ position: "sticky", left: `0px`, zIndex: 99, bgcolor: "inherit", display: checkbox ? "table-cell" : "none" }}>
                                        <Icon sx={{
                                            textAlign: "center", fontSize: "1.5rem",
                                            ":hover": { cursor: approvalProhibitionArr.indexOf(item.statusApproval) !== -1 ? null : "pointer" }, color: approvalProhibitionArr.indexOf(item.statusApproval) !== -1 && "#3c3c3c"
                                        }}
                                            onClick={approvalProhibitionArr.indexOf(item.statusApproval) !== -1 ? null : () => handleCheckBoxClicked(item.id)}
                                            component={approvalProhibitionArr.indexOf(item.statusApproval) !== -1 ? IndeterminateCheckBoxIcon : checkedValue.includes(item.id) ? CheckBoxIcon : CheckBoxOutlineBlankIcon} />
                                    </TableCell>
                                    <TableCell>{item.docNoAprroval}</TableCell>
                                    <TableCell>{item.firstName}</TableCell>
                                    <TableCell>{item.email}</TableCell>
                                    <TableCell>{item.phoneNumber}</TableCell>
                                    <TableCell>{item.typePartner}</TableCell>
                                    <TableCell>{item.duration}</TableCell>
                                    <TableCell>{item.productName}</TableCell>
                                    <TableCell>{item.createdAt}</TableCell>
                                    <TableCell>{item.dateFrom}</TableCell>
                                    <TableCell>{item.dateTo}</TableCell>
                                    <TableCell>{item.expiredDate ? item.expiredDate : "-"}</TableCell>

                                    <TableCell>{formatCurrency(item.amount)}</TableCell>
                                    {/* "#CCFEFF" waiting */}
                                    <TableCell sx={{ textAlign: "center" }}><Typography sx={{
                                        fontSize: "12px", borderRadius: "10px", textTransform: "capitalize", px: "12px", py: "10px", fontWeight: "550", letterSpacing: "0.5px",
                                        bgcolor: item.statusApproval === "WAITING" ? "#FCDAC6" : item.statusApproval === "APPROVED" ? "#D3F2D0" : item.statusApproval === "REJECTED" ? "#F9E9E9" : "#CCFEFF",
                                        color: item.statusApproval === "WAITING" ? "#F68340" : item.statusApproval === "APPROVED" ? "#269E19" : item.statusApproval === "REJECTED" ? "#C41F1F" : "#008082",
                                    }} >{item.statusApproval.toLowerCase()}</Typography></TableCell>
                                    <TableCell sx={{ textAlign: "center" }}><Button sx={{ ":hover": { cursor: "pointer", bgcolor: "#1d257a" }, fontSize: "0.75rem", bgcolor: "#2B3499", color: "#FFF7F7", borderRadius: "10px", textTransform: "capitalize" }} onClick={() => handleClickOpen(item)}>Detail</Button></TableCell>
                                </TableRow>
                            ) : null}

                        </TableBody>
                    </Table>
                </TableContainer>

                {/* Dialog */}
                {openDialog ?
                    <Dialog
                        open={openDialog}
                        onClose={() => handleClickClose()}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >

                        <DialogTitle sx={{ borderBottom: "1px solid #ccc" }} id="alert-dialog-title" color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>
                            <Stack sx={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <Typography sx={{ color: "#1C1B1F", paddingX: 2, fontSize: "20px", fontWeight: "bold" }}>Submission Detail</Typography>
                                <IconButton
                                    aria-label="close"
                                    onClick={() => handleClickClose()}
                                    sx={{ color: "#1C1B1F", fontWeight: "bold" }}
                                >
                                    <CloseIcon />
                                </IconButton>
                            </Stack>
                        </DialogTitle>

                        <DialogContent sx={{ paddingX: 5, py: 3.5 }}>
                            <Grid container spacing={2} pt={3}>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Card Number</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.cardNumber ? selectedData.cardNumber : "-"}</Typography>
                                    </Stack>
                                </Grid>
                                {/* <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Vehicle Number</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.vehicleNumber ? selectedData.vehicleNumber : "-"}</Typography>
                                    </Stack>
                                </Grid> */}
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Member Number</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.docNoAprroval}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Full Name</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.firstName} {selectedData.lastName}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Email</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.email}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Phone Number</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.phoneNumber}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Start Date</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.dateFrom}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>End Date</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.dateTo}</Typography>
                                    </Stack>
                                </Grid>
                                {selectedData.expiredDate !== "" &&
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Expired Date</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.expiredDate}</Typography>
                                        </Stack>
                                    </Grid>
                                }
                                {selectedData.productName !== "" &&
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Product Type</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.productName}</Typography>
                                        </Stack>
                                    </Grid>
                                }
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Member Type</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.typePartner}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Group Type</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.roleType}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Stack gap={"4px"}>
                                        <Typography sx={{ color: "#757575" }}>Remark</Typography>
                                        <Typography sx={{ color: "#1C1B1F" }}>{selectedData.remark ? selectedData.remark : "-"}</Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12}>
                                    <Box sx={{ borderTop: "1px dashed #ccc", mt: 2 }}></Box>
                                    <Typography sx={{ fontSize: "18px", color: "#1C1B1F", fontWeight: "550", mt: 4 }}>Approved List</Typography>
                                </Grid>
                                {selectedData.approvalList.map((item, index) => {
                                    console.log(item);
                                    return (
                                        <Grid item xs={12} md={6} pt={0} key={index}>
                                            <Stack gap={"4px"}>
                                                <Typography sx={{ color: "#757575", textTransform: "capitalize" }}>{item.statusApproval.toLowerCase() === "waiting" ? "created" : item.statusApproval.toLowerCase()} by</Typography>
                                                <Typography sx={{ color: "#1C1B1F", fontWeight: "500" }}>{item.username}</Typography>
                                                <Typography sx={{ color: "#4d4c4c" }}>{item.approvalTime}</Typography>
                                            </Stack>
                                        </Grid>
                                    )
                                })}
                                <Grid item xs={12} textAlign={"center"} mt={2}>
                                    <Stack>
                                        <Typography sx={{ fontSize: "20px", color: "#1C1B1F" }}>Amount</Typography>
                                        <Typography sx={{ fontSize: "36px", color: "#2B3499", fontWeight: "bold" }}>{formatCurrency(selectedData.amount)}</Typography>
                                    </Stack>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions sx={{ display: "flex", justifyContent: "center", px: 3 }}>
                            <Button onClick={() => {
                                handleBtnRejectProcess({ selectedId: selectedData.id, closeDialog: () => handleClickClose() })
                            }} autoFocus sx={{ display: (!rejectButton || approvalProhibitionArr.indexOf(selectedData.statusApproval)) !== -1 && "none", width: "50%", bgcolor: "transparent", py: "12px", mb: "5px", color: "#C41F1F", textTransform: "capitalize", border: "1px solid #C41F1F", borderRadius: "8px", ":hover": { bgcolor: "#C41F1F", color: "#FFF7F7" } }}>
                                Reject
                            </Button>
                            {/* BUAT PROMISE AGAR BERURUTAN JALANNYA */}
                            <Button onClick={() => {
                                handleBtnContinueProcess({ selectedId: selectedData.id, closeDialog: () => handleClickClose() })
                            }} autoFocus sx={{ display: (!approveButton || approvalProhibitionArr.indexOf(selectedData.statusApproval)) !== -1 && "none", width: rejectButton ? "50%" : "100%", bgcolor: "#2B3499", py: "12px", mb: "5px", color: "#FFF7F7", textTransform: "capitalize", borderRadius: "8px", ":hover": { bgcolor: "#1d257a" } }}>
                                {rejectButton ? "Approve" : "Continue Process"}
                            </Button>
                        </DialogActions>
                    </Dialog>
                    : null}
            </Card >

            <Stack mt={"1.25rem"} pb={3} >
                <Card sx={{ borderRadius: "0.625rem", padding: '1.25rem' }}>
                    <Stack direction={'row'} alignItems={'center'} gap={1} flexWrap="wrap" justifyContent={'space-between'} >
                        <Stack direction={'row'} alignItems={'center'} gap={1} flexWrap="wrap" >
                            <Stack direction={'row'} alignItems={'center'} gap={1} flexWrap="wrap"
                                sx={{
                                    button: {
                                        padding: 0,
                                        margin: 0,
                                        minWidth: '40px',
                                        minHeight: '40px',
                                        fontWeight: 'bold',
                                        ":hover": {
                                            color: '#535AAD',
                                            border: '1px solid #535AAD'
                                        }
                                    },
                                    overflowX: 'auto'
                                }}>
                                {pagination > 1 &&
                                    <Button
                                        sx={{
                                            border: '1px solid #D9D9D9',
                                        }}
                                        disabled={offset === 1 ? true : false}
                                        onClick={() => handlePagination('sub')}>
                                        &lt;
                                    </Button>}

                                {Array.from({ length: pagination }, (_, index) => {
                                    // Untuk mengecek apakah kita di 1-6 / 7-12 / 13-1
                                    if (index + 1 <= (Math.ceil(offset / 6) * 6) && index + 1 >= ((Math.ceil(offset / 6) - 1) * 6) + 1) {
                                        return (<Button key={index + 1} sx={{
                                            color: offset === index + 1 ? '#535AAD' : '#505050',
                                            border: offset === index + 1 ? '1px solid #535AAD' : '1px solid #D9D9D9'
                                        }} onClick={() => {
                                            setOffset(index + 1)
                                            handleGetDataTable({ pages: index + 1 })
                                            // handleGetData(selectedSettlementFrom, selectedSettlementTo, selectedMerchantOption, selectedKeyword, index + 1, selectedOvernight, selectedIsAscending, selectedColumnOrder)
                                        }}>{`${index + 1}`}</Button>
                                        )

                                    } if (index === Math.ceil(offset / 6) * 6) {
                                        return (
                                            <Stack direction={'row'} alignItems={'center'} gap={1} key={index + 1}><Typography sx={{
                                                fontWeight: 'bold',

                                            }}>· · ·</Typography>
                                                <Button key={index + 1} sx={{
                                                    color: offset === index + 1 ? '#535AAD' : '#505050',
                                                    border: offset === index + 1 ? '1px solid #535AAD' : '1px solid #D9D9D9'
                                                }} onClick={() => {
                                                    setOffset(pagination)
                                                    handleGetDataTable({ pages: pagination })

                                                    // handleGetData(selectedSettlementFrom, selectedSettlementTo, selectedMerchantOption, selectedKeyword, pagination, selectedOvernight, selectedIsAscending, selectedColumnOrder)
                                                }}>{pagination}
                                                </Button>
                                            </Stack>)
                                    } return
                                })}
                                {pagination > 1 &&
                                    <Button
                                        sx={{
                                            border: '1px solid #D9D9D9',
                                        }}
                                        disabled={offset === pagination ? true : false}
                                        onClick={() => handlePagination('add')}
                                    >&gt;</Button>
                                }
                            </Stack>
                            {pagination > 1 &&
                                <Stack direction={'row'} alignItems={'center'}>
                                    <Typography>
                                        Go to
                                    </Typography>

                                    <TextField autoComplete="off" size="small" type="text"
                                        value={inputGoTo}
                                        onChange={(event) => {
                                            setInputGoto(() => (/^\d*$/.test(event.target.value) && event.target.value <= pagination && event.target.value > 0) ? event.target.value : '') // Display
                                            debouncedHandleGoToPagination(event) // Render Function
                                        }}
                                        inputProps={{
                                            pattern: '[0-9]*', // Specify regex pattern for HTML5 validation
                                            inputMode: 'numeric', // Set input mode to numeric for better mobile support
                                            style: {
                                                textAlign: 'center'
                                            }
                                        }} sx={{
                                            minWidth: '40px',
                                            width: '80px',
                                            padding: '10px', // Adjust padding to center the text vertically and horizontally
                                            fontSize: '14px', // Adjust font size as needed
                                            boxSizing: 'border-box', // Include padding and border in the elem
                                        }} />
                                </Stack>
                            }
                        </Stack>
                        <Stack justifyItems={'center'}>
                            <Typography align="center" color={'#747474'} fontWeight={'bold'}>
                                Showing {totalData} Results
                            </Typography>
                        </Stack>
                    </Stack>
                </Card>
            </Stack >
        </Stack>
    )
}

export default CustomLOATable