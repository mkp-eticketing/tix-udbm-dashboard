import { Autocomplete, Box, Button, Checkbox, FormControlLabel, FormGroup, Icon, Popover, Stack, TextField, Typography } from '@mui/material';
import React, { useEffect } from 'react';
import SelectField from '../../../../../components/select-field';
import SearchIcon from '@mui/icons-material/Search';


const FilterPopover = ({
    anchorEl = null,
    handleClosePopover = () => { },

    statusTypeList = [], // Status yang akan tampil
    selectedStatusType = [], // Status yang dipilih
    setSelectedStatusType = () => { }, // Fungsi untuk set status yang dipilih
    selectedMemberType = [], // Member Type yang dipilih
    setSelectedMemberType = () => { }, // Fungsi untuk set member type yang dipilih
    // ouCode
    merchantOption = [],
    selectedOuCode = "", // Merchant yang dipilih
    setSelectedOuCode = () => { },
    ...other
}) => {
    const open = Boolean(anchorEl);
    const id = open ? 'simple-popper' : undefined;

    const memberTypeList = ["Freepass", "Member", "Special Member", "Multi Product"]
    return (
        <Popover id={id} open={open} anchorEl={anchorEl} onClose={handleClosePopover} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
        }} transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
        }} sx={{ zIndex: 999, }}>
            <Box sx={{ width: ["auto", "400px", "600px"], p: 2, mt: 1, bgcolor: 'background.paper', borderRadius: '10px', boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', }} >
                <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"}>
                    <Typography color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>Filter By</Typography>
                    <Button sx={{ color: "#2B3499", textTransform: "capitalize", fontWeight: 400 }} onClick={(e) => {
                        setSelectedMemberType(memberTypeList)
                        setSelectedOuCode("")
                        setSelectedStatusType(statusTypeList)
                    }}>Reset</Button>
                </Stack>

                {/* SELECTED CHECKBOX */}
                <Stack direction={"row"} alignItems={"center"} gap={1} mt={1} flexWrap={["wrap"]}>
                    {selectedMemberType ? selectedMemberType.map((item, index) => {
                        return (
                            <Typography key={index} sx={{ backgroundColor: "#F1F2FB", color: "#2B3499", py: "6px", px: "12px", borderRadius: "15px" }}>{item}</Typography>
                        )
                    }) : null}
                    {selectedStatusType ? selectedStatusType.map((item, index) => {
                        return (
                            <Typography key={index} sx={{ backgroundColor: "#F1F2FB", color: "#2B3499", py: "6px", px: "12px", borderRadius: "15px" }}>{item}</Typography>
                        )
                    }) : null}
                </Stack>

                <Typography color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"} mt={2}>Member Type</Typography>
                {/* CHECKBOX */}
                <Stack direction={"row"} alignItems={"center"} gap={1}>
                    <FormGroup sx={{ display: 'flex', flexDirection: ["column", "column", "row"], alignItems: ["left", "left", "center"] }}>
                        {memberTypeList ? memberTypeList.map((item, index) => {
                            return (
                                <FormControlLabel key={index} control={<Checkbox defaultChecked={true} checked={selectedMemberType.some((filtered) => filtered === item)} color='success' value={item}
                                    onChange={(e) => setSelectedMemberType((prev) => {
                                        if (!e.target.checked) {
                                            if (prev.length > 1) {
                                                return prev.filter((filtered) => filtered !== item)
                                            } else {
                                                return prev
                                            }
                                        } else {
                                            return [...prev, item]
                                        }
                                    }
                                    )} />} label={`${item}`} />
                            )
                        }) : null}
                    </FormGroup>
                </Stack>

                <Typography color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"} mt={2}>Status</Typography>
                {/* CHECKBOX */}
                <Stack direction={"row"} alignItems={"center"} gap={1}>
                    <FormGroup sx={{ display: 'flex', flexDirection: ["column", "column", "row"], alignItems: ["left", "left", "center"] }}>
                        {statusTypeList ? statusTypeList.map((item, index) => {
                            return (
                                <FormControlLabel key={index} control={<Checkbox defaultChecked={true} checked={selectedStatusType.some((filtered) => filtered === item)} color='success' value={item}
                                    onChange={(e) => {
                                        setSelectedStatusType((prev) => {
                                            // console.log(e.target.checked);
                                            if (!e.target.checked) {
                                                if (prev.length > 1) {
                                                    return prev.filter((filtered) => filtered !== item)
                                                } else {
                                                    return prev
                                                }
                                            } else {
                                                return [...prev, item]
                                            }
                                        })
                                    }} />} label={`${item}`} />
                            )
                        }) : null}
                    </FormGroup>
                </Stack>

                <Typography color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"} mt={2}>Merchant</Typography>
                <Stack direction={"row"} alignItems={"center"} gap={1} pt={1}  >
                    <Icon sx={{ color: "#2B3499", fontSize: "1.5rem" }} component={SearchIcon} />
                    <Autocomplete
                        sx={{ width: "100%" }}
                        disablePortal
                        id="combo-box-demo"
                        options={merchantOption}
                        value={selectedOuCode}
                        onChange={(_, values) => {
                            setSelectedOuCode(values)
                        }}
                        renderInput={(params) => <TextField variant="standard" {...params} placeholder={'All Merchant'} />}
                        freeSolo={true}
                        forcePopupIcon
                    />
                </Stack>
            </Box>
        </Popover>
    )
}

export default FilterPopover