import { Box, Button, Popover, Stack, Typography } from '@mui/material';
import moment from 'moment';
import React, { useState } from 'react';
import DatePickerField from '../../../../../components/datepicker-field';

const DatepickerPopover = ({
    anchorEl = null,
    dateValue = null,
    setDateValue = () => { },
    setLoading = () => { },
    handleClosePopover = () => { },
}) => {
    const open = Boolean(anchorEl);
    const id = open ? 'simple-popper' : undefined;

    const [startDate, setStartDate] = useState(dateValue.startDate);
    const [endDate, setEndDate] = useState(dateValue.endDate);

    return (
        <Popover id={id} open={open} anchorEl={anchorEl} onClose={handleClosePopover} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
        }} transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
        }} sx={{ zIndex: 999, }}>
            <Box sx={{ width: ["auto", "400px", "600px"], p: 2, mt: 1, bgcolor: 'background.paper', borderRadius: '10px', boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', }}>
                <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"}>
                    <Typography color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>Filter By Date</Typography>
                    <Button sx={{ color: "#2B3499", textTransform: "capitalize", fontWeight: 400 }} onClick={(e) => {
                        setDateValue({ startDate: moment().subtract(7, 'day'), endDate: moment() })
                        setStartDate(moment().subtract(7, 'day'))
                        setEndDate(moment())
                    }}
                        >Reset</Button>
                </Stack>

                <Stack direction={"row"} alignItems={"center"} gap={1} mt={3}>
                    <DatePickerField
                        label={"Start Date"}
                        value={startDate}
                        maxDate={moment()}
                        onChange={(val) => {
                            setStartDate(() => val)
                            setEndDate(() => val)
                            // setDateValue((prev) => ({ startDate: val, endDate: val }))
                        }} />
                    <DatePickerField
                        label={"End Date"}
                        value={endDate}
                        minDate={startDate}
                        maxDate={moment(startDate).add(14, 'days') > moment() ? moment() : moment(startDate).add(14, 'days')}
                        onChange={(val) => {
                            setEndDate(() => val)
                            // setDateValue((prev) => ({ ...prev, endDate: val }))
                        }} />
                </Stack>

                <Button sx={{ mt: 2, color: "#FFF7F7", bgcolor: "#2B3499", width: "100%", textTransform: "capitalize", fontWeight: 400, ":hover": { bgcolor: "#1d257a" } }}
                    onClick={() => {
                        setDateValue(() => ({ startDate: startDate, endDate: endDate }))
                        handleClosePopover()
                    }}>Apply</Button>
            </Box>
        </Popover>
    )
}

export default DatepickerPopover