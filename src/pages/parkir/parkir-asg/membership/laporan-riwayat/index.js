import CloseIcon from '@mui/icons-material/Close';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import { Box, Button, Card, CardContent, Dialog, DialogContent, DialogTitle, Grid, IconButton, Stack, Typography } from '@mui/material';
import { saveAs } from 'file-saver';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import CustomPagination from '../../../../../components/custom-pagination';
import CustomTable from '../../../../../components/custom-table';
import DatePickerField from '../../../../../components/datepicker-field';
import FilterMessageNote from '../../../../../components/filter-message-note';
import SelectField from '../../../../../components/select-field';
import InputField from '../../../../../components/text-field';
import { merchant_data } from '../../../../../data/merchant';
import { getExcelExportMember, getTableCreateMember } from '../../../../../services/parkir/parkir-asg';
import { formatCurrency } from '../../../../../utils/format-currency';
import SearchIcon from '@mui/icons-material/Search';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import CustomButton from '../../../../../components/custom-button';

const RiwayatMembership = (
    {
        merchantData = merchant_data,
        label = "Membership Approval History",
        notify = () => { },
        setLoading = () => { },
        subTitleInfo = ["Max Range of Date is 2 Weeks (14 Days)"],
        titleInfo = "To Display Specific Datas & Durations, Use the Filters Above.",
    }
) => {
    const [tableData, setTableData] = useState([])
    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState(0);
    const [count, setCount] = useState(-99);
    const [countLoading, setCountLoading] = useState(false);
    const [disableNext, setDisableNext] = useState(false);

    const [openDialog, setOpenDialog] = useState(false)
    const [selectedData, setSelectedData] = useState()

    // Merchant Options
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [filterForm, setFilterForm] = useState({
        outletCode: null,
        search: "",
        status: "",
        memberType: "",
        startDate: moment().subtract(7, 'days'),
        endDate: moment()
    })

    // Selected Filter
    const [selectedOuCode, setSelectedOuCode] = useState([])
    const [selectedFilterForm, setSelectedFilterForm] = useState({
        outletCode: null,
        keyword: null,
        status: null,
        memberType: null,
        startDate: moment().subtract(7, 'days'),
        endDate: moment()
    })

    const header = [
        {
            title: "#",
            value: 'id',
            align: "center",
            width: "40px",
        },
        {
            title: "Action",
            value: "action",
            align: "center",
            width: "40px",
        },
        {
            title: "No.Member",
            value: "docNoAprroval",
            align: "center",
            width: "250px",
        },
        {
            title: "Name",
            value: "firstName",
            align: "center",
            width: "250px",
        },
        {
            title: "Email",
            value: "email",
            align: "center",
            width: "250px",
        },
        {
            title: "No. HP",
            value: "phoneNumber",
            align: "center",
            width: "250px",
        },
        {
            title: "Member Type",
            value: "typePartner",
            align: "center",
            width: "250px",
        },
        {
            title: "Duration Period",
            value: "durationPeriod",
            align: "center",
            width: "250px",
        },
        {
            title: "Vehicle Type",
            value: "productName",
            align: "center",
            width: "250px",
        },
        {
            title: "Date Created",
            value: "createdAt",
            align: "center",
            width: "250px",
        },
        {
            title: "Start Date",
            value: "dateFrom",
            align: "center",
            width: "250px",
        },
        {
            title: "End Date",
            value: "dateTo",
            align: "center",
            width: "250px",
        },
        {
            title: "Expired Date",
            value: "expiredDate",
            align: "center",
            width: "250px",
        },
        {
            title: "Amount",
            value: "amount",
            align: "center",
            width: "250px",
        },
        {
            title: "Status",
            value: "statusApproval", //statusApproval
            align: "center",
            width: "250px",
        },
    ]

    const handleGetData = ({
        outletCode,
        filter,
        offset,
        length,
        draw = 0,
        callback = () => { }
    }) => {
        const dateFrom = moment(filter.settlementFrom)
        const dateTo = moment(filter.settlementTo)
        if (dateTo.diff(dateFrom, 'days') <= 14) {
            let data = {
                "outletCode": outletCode,
                "keyword": filter.search ? filter.search : "",
                "draw": draw,
                "ascDesc": `asc`,
                "columnOrderName": `createdAt`,
                "limit": length,
                "offset": offset,
                "status": filter.status ? filter.status.toUpperCase() : "", // WAITING, SUBMITTED
                "typeMember": filter.memberType ? filter.memberType.toUpperCase() : "", //FREEPASS
                "dateStart": `${moment(filter.startDate).format("YYYY-MM-DD")} 00:00:00`,
                "dateEnd": `${moment(filter.endDate).format("YYYY-MM-DD")} 23:59:59`
            }

            setCountLoading(true)
            setDisableNext(false)
            setLoading(true)

            setSelectedFilterForm(filter)
            setSelectedOuCode(outletCode)

            getTableCreateMember(data).then((res) => {
                if (res.result !== null && res.result.dataApprovalMember !== null) {
                    setTableData(res.result.dataApprovalMember)
                    if (draw === 1) {
                        setCount(res.result.countDataMember)
                    }
                    notify("Success Get Data List", "success");
                } else if (res.result === null || res.result.dataApprovalMember === null) {
                    setTableData([])
                    setCount(0)
                    notify("No Data Found", "warning");
                }
            }).finally(() => {
                callback()
                setLoading(false)
                setCountLoading(false)
            }).catch((e) => {
                setLoading(false)
                setCountLoading(false)
                notify(e.message, "warning");
            })
        } else {
            notify("Max Range of Date is 14 Days", "warning");
        }
    }

    const handleGetExcel = ({
        outletCode,
        filter,
        callback = () => { }
    }) => {
        let data = {
            "outletCode": outletCode,
            "keyword": filter.search ? filter.search : "",
            "draw": 1,
            "ascDesc": "asc",
            "columnOrderName": "createdAt",
            "offset": 0,
            "status": filter.status ? filter.status.toUpperCase() : "", // WAITING, SUBMITTED
            "typeMember": filter.memberType ? filter.memberType.toUpperCase() : "", //FREEPASS
            "dateStart": `${moment(filter.startDate).format("YYYY-MM-DD")} 00:00:00`,
            "dateEnd": `${moment(filter.endDate).format("YYYY-MM-DD")} 23:59:59`
        }

        setLoading(true)

        getExcelExportMember(data).then((res) => {
            if (res !== null) {
                saveAs(new Blob([res], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }), `History Membership (${moment(filter.startDate).format("YYYY_MM_DD")} - ${moment(filter.endDate).format("YYYY_MM_DD")}).xlsx`);
                notify("Data Downloaded", "success");
            } else {
                notify("No Data Found", "warning");
            }
        }).finally(() => {
            setLoading(false)
            callback()
        }).catch((e) => {
            setLoading(false)
            notify(e.message, "warning");
        })
    }

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetData({
            outletCode: ouCodeArr,
            filter: filterForm,
            length: limit,
            offset: offset,
            draw: 1
        })
    }

    const pageChange = async (value) => {
        var ofset = value * limit;
        setOffset(ofset);
        handleGetData({
            outletCode: selectedOuCode,
            filter: selectedFilterForm,
            length: limit,
            offset: ofset,
            draw: 0,
        });
    };

    const rowsChange = async (e) => {
        setOffset(0);
        setLimit(e.props.value);
        handleGetData({
            outletCode: selectedOuCode,
            filter: selectedFilterForm,
            length: e.props.value,
            offset: 0,
            draw: 0
        })
    };

    const renderCell = (item, header, index) => {
        if (header.value === "id") {
            return <span>{offset + index + 1}</span>;
        } else if (header.value === "action") {
            return (
                <Button onClick={() => {
                    setSelectedData(item)
                    setOpenDialog(true)
                }}>
                    <FormatListBulletedIcon sx={{ fontSize: "18px", }} />
                </Button >
            )
        }
        else if (header.value === "docNoAprroval") {
            return <span>{item.docNoAprroval}</span>;
        } else if (header.value === "firstName") {
            return <span>{item.firstName}</span>;
        } else if (header.value === "email") {
            return <span>{item.email}</span>;
        } else if (header.value === "phoneNumber") {
            return <span>{item.phoneNumber}</span>;
        } else if (header.value === "typePartner") {
            return <span>{item.typePartner}</span>;
        } else if (header.value === "durationPeriod") {
            return <span>{item.duration}</span>;
        } else if (header.value === "productName") {
            return <span>{item.productName}</span>;
        } else if (header.value === "createdAt") {
            return <span>{item.createdAt}</span>;
        } else if (header.value === "dateFrom") {
            return <span>{item.dateFrom}</span>;
        } else if (header.value === "dateTo") {
            return <span>{item.dateTo}</span>;
        } else if (header.value === "expiredDate") {
            return <span>{item.expiredDate}</span>;
        } else if (header.value === "amount") {
            return <span>{formatCurrency(item.amount)}</span>;
        } else if (header.value === "statusApproval") {
            return <span>{item.statusApproval}</span>;
        } else if (header.value === "action") {
            return <span>sip</span>
        }

        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                    </Box>

                    {/* Render Filter */}
                    <Stack gap={2} mt={"1.6rem"} direction={{ sm: "column" }} sx={{ background: '#FFFFFF', color: "#FEFEFE", borderRadius: "0.625rem" }} justifyContent={'space-between'} mb={2} >
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'}>
                                <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    data={merchantOption}
                                    selectedValue={filterForm.outletCode}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            console.log('value => ', val);
                                            return ({
                                                ...prev,
                                                outletCode: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                            <Stack width={'100%'}>
                                <SelectField
                                    label={"Status"}
                                    placeholder="All Status"
                                    data={["Waiting", "Submitted", "Reviewed", "Approved", "Rejected"]}
                                    selectedValue={filterForm.status}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            console.log(filterForm.status);
                                            return ({
                                                ...prev,
                                                status: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'} direction={'row'} gap={2}>

                                <SelectField
                                    label={"Member Type"}
                                    placeholder="All Member"
                                    data={["Freepass", "Special Member", "Member", "Multi Product"]}
                                    selectedValue={filterForm.memberType}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            return ({
                                                ...prev,
                                                memberType: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                <InputField
                                    label={"Keyword"}
                                    placeholder="Search"
                                    onChange={(e) => setFilterForm((prev) => ({ ...prev, search: e.target.value }))}
                                    value={filterForm.search}
                                />
                            </Stack>
                        </Stack>
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* From */}
                                <DatePickerField
                                    label={"Creation Date"}
                                    placeholder="DD MMMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment()}
                                    value={filterForm.startDate}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            startDate: moment(newValue),
                                            endDate: moment(newValue)
                                        }))
                                    }}
                                />

                            </Stack>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* To */}
                                <DatePickerField
                                    label={"Approval Date"}
                                    placeholder="DD MMMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(filterForm.startDate).add(14, 'days') > moment() ? moment() : moment(filterForm.startDate).add(14, 'days')}
                                    minDate={filterForm.startDate}
                                    value={filterForm.endDate}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            endDate: moment(newValue)
                                        }))
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack style={{ boxSizing: "border-box" }} sx={{
                            display: "flex",
                            flexDirection: ["column", "row"],
                            alignItems: ["end", "center"],
                            gap: 3,
                            justifyContent: "space-between",
                        }} width={'100%'} gap={2}>
                            <FilterMessageNote
                                style={{ boxSizing: "border-box" }}
                                sx={{
                                    width: ["100%", "100%"]
                                }}
                                title={titleInfo}
                                subtitle={subTitleInfo}
                            />
                            <Stack width={{ xs: '100%', lg: '100%' }} gap={2} direction={'row'} justifyContent={'end'}>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        handleGetExcel({
                                            outletCode: filterForm.outletCode ? [filterForm.outletCode.value] : allOutletCodeList,
                                            filter: filterForm
                                        })
                                    }}
                                    startIcon={<FileUploadIcon size="14px" />}
                                    name="Export"
                                    color={'success'}>
                                </CustomButton>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        setLimit(25)
                                        setOffset(0)
                                        handleGetData({
                                            filter: filterForm,
                                            length: 25,
                                            offset: 0,
                                            draw: 1,
                                            outletCode: filterForm.outletCode ? [filterForm.outletCode.value] : allOutletCodeList
                                        })
                                    }}
                                    startIcon={<SearchIcon size="14px" />}
                                    name="Search">
                                </CustomButton>
                            </Stack>
                        </Stack>
                    </Stack>

                    {/* Render Table */}
                    <Box sx={{ width: "100%" }}>
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                        <CustomTable
                            headers={header}
                            items={tableData}
                            renderCell={renderCell}
                        />
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                    </Box>

                    {/* Render Dialog */}
                    {openDialog ?
                        <Dialog
                            open={openDialog}
                            onClose={() => {
                                setSelectedData()
                                setOpenDialog(false)
                            }}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >

                            <DialogTitle sx={{ borderBottom: "1px solid #ccc" }} id="alert-dialog-title" color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>
                                <Stack sx={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                    <Typography sx={{ color: "#1C1B1F", paddingX: 2, fontSize: "20px", fontWeight: "bold" }}>Submission Detail</Typography>
                                    <IconButton
                                        aria-label="close"
                                        onClick={() => {
                                            setSelectedData()
                                            setOpenDialog(false)
                                        }}
                                        sx={{ color: "#1C1B1F", fontWeight: "bold" }}
                                    >
                                        <CloseIcon />
                                    </IconButton>
                                </Stack>
                            </DialogTitle>

                            <DialogContent sx={{ paddingX: 5, py: 3.5 }}>
                                <Grid container spacing={2} pt={3}>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Card Number</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.cardNumber ? selectedData.cardNumber : "-"}</Typography>
                                        </Stack>
                                    </Grid>
                                    {/* <Grid item xs={12} md={6}>
                            <Stack gap={"4px"}>
                                <Typography sx={{ color: "#757575" }}>Vehicle Number</Typography>
                                <Typography sx={{ color: "#1C1B1F" }}>{selectedData.vehicleNumber ? selectedData.vehicleNumber : "-"}</Typography>
                            </Stack>
                        </Grid> */}
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Member Number</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.docNoAprroval}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Full Name</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.firstName} {selectedData.lastName}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Email</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.email}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Phone Number</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.phoneNumber}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Start Date</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.dateFrom}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>End Date</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.dateTo}</Typography>
                                        </Stack>
                                    </Grid>
                                    {selectedData.expiredDate !== "" &&
                                        <Grid item xs={12} md={6}>
                                            <Stack gap={"4px"}>
                                                <Typography sx={{ color: "#757575" }}>Expired Date</Typography>
                                                <Typography sx={{ color: "#1C1B1F" }}>{selectedData.expiredDate}</Typography>
                                            </Stack>
                                        </Grid>
                                    }
                                    {selectedData.productName !== "" &&
                                        <Grid item xs={12} md={6}>
                                            <Stack gap={"4px"}>
                                                <Typography sx={{ color: "#757575" }}>Product Type</Typography>
                                                <Typography sx={{ color: "#1C1B1F" }}>{selectedData.productName}</Typography>
                                            </Stack>
                                        </Grid>
                                    }
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Member Type</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.typePartner}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Group Type</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.roleType}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Remark</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{selectedData.remark ? selectedData.remark : "-"}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box sx={{ borderTop: "1px dashed #ccc", mt: 2 }}></Box>
                                        <Typography sx={{ fontSize: "18px", color: "#1C1B1F", fontWeight: "550", mt: 4 }}>Approved List</Typography>
                                    </Grid>
                                    {selectedData.approvalList.map((item, index) => {
                                        return (
                                            <Grid item xs={12} md={6} pt={0} key={index}>
                                                <Stack gap={"4px"}>
                                                    <Typography sx={{ color: "#757575", textTransform: "capitalize" }}>{item.statusApproval.toLowerCase() === "waiting" ? "created" : item.statusApproval.toLowerCase()} by</Typography>
                                                    <Typography sx={{ color: "#1C1B1F", fontWeight: "500" }}>{item.username}</Typography>
                                                    <Typography sx={{ color: "#4d4c4c" }}>{item.approvalTime}</Typography>
                                                </Stack>
                                            </Grid>
                                        )
                                    })}
                                    <Grid item xs={12} textAlign={"center"} mt={2}>
                                        <Stack>
                                            <Typography sx={{ fontSize: "20px", color: "#1C1B1F" }}>Amount</Typography>
                                            <Typography sx={{ fontSize: "36px", color: "#2B3499", fontWeight: "bold" }}>{formatCurrency(selectedData.amount)}</Typography>
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </DialogContent>
                        </Dialog>
                        : null}

                </CardContent>
            </Card >
        </Stack>
    )
}

export default RiwayatMembership