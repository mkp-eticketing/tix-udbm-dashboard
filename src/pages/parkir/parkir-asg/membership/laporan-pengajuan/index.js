import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CloseIcon from '@mui/icons-material/Close';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import SearchIcon from '@mui/icons-material/Search';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Icon, IconButton, Input, MenuItem, Select, Stack, Typography } from '@mui/material';
import { useFormik } from 'formik';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import * as yup from "yup";
import DatePickerField from '../../../../../components/datepicker-field';
import SelectField from '../../../../../components/select-field';
import InputField from '../../../../../components/text-field';
import { merchant_data } from '../../../../../data/merchant';
import { getComboList, getDurationList, getMemberTypeList } from '../../../../../services/parkir/combo';
import { createMember, getTableCreateMember, updateMember } from '../../../../../services/parkir/parkir-asg';
import { getProductPolicyList } from '../../../../../services/parkir/product-policy';
import { formatCurrency } from '../../../../../utils/format-currency';
import CustomLOATable from '../components/custom-table-loa';
import DatepickerPopover from '../components/datepicker-popover';
import FilterPopover from '../components/filter-popover';
import { color } from 'highcharts';

const PengajuanMembership = (
    {
        merchantData = merchant_data,
        notify = () => { },
        username = "",
        setLoading = () => { },
        label = "Membership Submission",
        roleTask = "",
    }
) => {
    //REFRESH DATA (add label and value in merchantOption for rendering in select field)
    const [merchantOption, setMerchantOption] = useState([])
    const [ouCodeRegistration, setOuCodeRegistration] = useState("")
    const [allOutletCodeList, setAllOutletCodeList] = useState([])

    const [userRoleTask, setUserRoleTask] = useState([])

    // Pages STATE
    const [pagesState, setPagesState] = useState("laporan-pengajuan")

    // AscDesc 
    const [isAscending, setIsAscending] = useState({
        value: "createdAt",
        order: "DESC"
    })

    // Dialog Multiple Continue
    const [openDialogMultipleContinue, setOpenDialogMultipleContinue] = useState(false)

    // Filter
    const [search, setSearch] = useState("")
    const [debouncedSearch, setDebouncedSearch] = useState("")

    // Table
    const [dataTable, setDataTable] = useState({
        data: [],
        total: 0,
    })
    const [checkedValue, setCheckedValue] = useState([])
    const [offset, setOffset] = useState(1)
    const [limit, setLimit] = useState(25)

    // Popover (Filter Button)
    const statusTypeList = ["Waiting", "Submitted"]

    const [selectedOuCode, setSelectedOuCode] = useState("")
    const [selectedMemberType, setSelectedMemberType] = useState(["Freepass", "Member", "Special Member", "Multi Product"])
    const [selectedStatusType, setSelectedStatusType] = useState(["Waiting", "Submitted"])
    const [anchorElFilter, setAnchorElFilter] = useState(null);

    const handleClickPopperFilter = (event) => {
        setAnchorElFilter(event.currentTarget);
    };

    // Popover (Date Button)
    const [dateValue, setDateValue] = useState({
        startDate: moment().subtract(7, 'days'),
        endDate: moment()
    })
    const [anchorElDate, setAnchorElDate] = useState(null);

    const handleClickPopperDate = (event) => {
        setAnchorElDate(event.currentTarget);
    };

    const handleClosePopover = () => {
        setAnchorElFilter(null);
        setAnchorElDate(null)
    }

    const headerTable = [
        {
            title: CheckBoxOutlineBlankIcon,
            width: "30px",
        },
        {
            title: "No.Member",
            value: "docNoAprroval",
            width: "40px",
        },
        {
            title: "Name",
            value: "firstName",
        },
        {
            title: "Email",
            value: "email",
        },
        {
            title: "No. HP",
            value: "phoneNumber",
        },
        {
            title: "Member Type",
            value: "typePartner",
        },
        {
            title: "Duration Period",
            value: "durationPeriod",
        },
        {
            title: "Vehicle Type",
            value: "productName",
        },
        {
            title: "Date Created",
            value: "createdAt",
        },
        {
            title: "Start Date",
            value: "dateFrom",
        },
        {
            title: "End Date",
            value: "dateTo",
        },
        {
            title: "Expired Date",
            value: "expiredDate",
        },
        {
            title: "Amount",
            value: "amount",
        },
        {
            title: "Status",
            value: "statusApprovalCode", //statusApproval
        },
        {
            title: "Action",
            value: "",
        }
    ]

    const handleGetData = ({ withNotification = true, outletCode, offsetData = offset, ascDesc = isAscending.order, columnOrder = isAscending.value, draw = 0, callback = () => { } }) => {
        setLoading(true)
        let data = {
            "outletCode": outletCode,
            "keyword": debouncedSearch,
            "draw": draw,
            "ascDesc": ascDesc,
            "columnOrderName": columnOrder,
            "limit": limit,
            "offset": (offsetData - 1) * limit,
            "status": selectedStatusType.toString().toUpperCase(), // WAITING, SUBMITTED
            "typeMember": selectedMemberType.toString().toUpperCase(), //FREEPASS
            "dateStart": `${moment(dateValue.startDate).format("YYYY-MM-DD")} 00:00:00`,
            "dateEnd": `${moment(dateValue.endDate).format("YYYY-MM-DD")} 23:59:59`
        }

        getTableCreateMember(data).then((res) => {
            if (res.result !== null) {
                let fetchedData = res.result.dataApprovalMember
                let fetchedTotal = res.result.countDataMember
                setDataTable((prev) => ({ ...prev, data: fetchedData }))
                if (draw === 1) {
                    setDataTable((prev) => ({ ...prev, total: fetchedTotal }))
                }
                if (withNotification) {
                    notify("Success Get Data List", "success");
                }
            } 
        }).finally(() => {
            setLoading(false)
            callback()
        }).catch((e) => {
            setLoading(false)
            setDataTable({ data: [], total: 0 })
            if (withNotification) {
                if (e.response) {
                    notify(e.response.data.message, "warning");
                } else {
                    notify(e.message, "warning");
                }
            }
        })
    }

    const handleApproval = ({ id = "", callback = () => { } }) => {
        setLoading(true)
        let data = {
            "statusApproval": "SUBMITTED",
            "username": username,
            "date": `${moment().format("YYYY-MM-DD HH:mm:ss")}`,
            "idList": Array.isArray(id) ? id : [id]
        }

        updateMember(data).then((res) => {
            if (res.result) {
                notify(`Success ${res.result.countDataUpdated} Data Has Been Updated`, "success");
            } 
        }).finally(() => {
            setLoading(false)
            callback()
            handleGetData({ withNotification: false, outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
            setCheckedValue([])
        }).catch((e) => {
            setLoading(false)
            if (e.response) {
                notify(e.response.data.message, "warning");
            } else {
                notify(e.message, "warning");
            }
        })
    }

    // CHECK BUTTON LANJUTKAN
    const checkBtnLanjutkan = useMemo(() => {
        if (checkedValue.length > 0) {
            return false
        } else {
            return true
        }
    }, [checkedValue])

    // FUNCTION BUTTON LANJUTKAN
    const handleBtnMultipleContinueProcess = () => {
        handleApproval({
            id: checkedValue
            , callback: () => {
                setOpenDialogMultipleContinue(false)
            }
        })
    };

    const handleOpenDialogMultipleContinueProcess = () => {
        setOpenDialogMultipleContinue(true)
    }

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setUserRoleTask(roleTask.split(","))
        setMerchantOption(merchantArr);
    }, [merchantData, roleTask]);

    useEffect(() => {
        if (merchantOption.length > 0 && userRoleTask.includes("viewMembershipSubmission")) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetData({ outletCode: ouCodeArr, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
    }

    // HANDLE FILTER POPOVER & DATEPICKER
    useEffect(() => {
        if (allOutletCodeList.length > 0) {
            handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: 1, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
            setOffset(1)
        }
    }, [dateValue, selectedOuCode, selectedMemberType, selectedStatusType, debouncedSearch, limit])

    // HANDLE FILTER SEARCH
    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setDebouncedSearch(search);
        }, 500)
        return () => clearTimeout(timeoutId);
    }, [search, 500])

    // ======== CREATE MEMBER =========
    // ======== FORMIK =========
    const initialValues = {
        "ouId": "", //merchant
        "duration": "",
        "partnerCode": "",
        "product": "", // product (vehicle type)
        "firstName": "",
        "lastName": "",
        "phoneNumber": "",
        "roleType": "", // group type
        "email": "",
        "cardNumberUuid": "", //card number
        "vehicleNumber": "",
        "typePartner": "",//member type
        "registeredType": {
            "label": "Card Number",
            "value": "CARD_NUMBER"
        },
        "remark": "", //not required
        "startDate": "",
        "username": username, //ambil dari UDBM
    }

    const formik = useFormik({
        initialValues,
        onSubmit: (values) => {
            setOpenCreateDialog(true)
        },
        validationSchema: yup.object({
            ouId: yup.object().required("Outlet Code is required"),
            firstName: yup.string().required("First Name is required"),
            email: yup.string().email("Email is invalid").required("Email is required"),
            phoneNumber: yup.string().required("Phone Number is required"),
            roleType: yup.string().required("Type Partner is required"),
            typePartner: yup.object().required("Member Type is required"),
            product: yup.object().required("Vehicle Type is required"),
            duration: yup.object().required("Duration is required"),
            startDate: yup.string().required("Start Date is required"),
            registeredType: yup.object().required("Registered Type is required"),

            cardNumberUuid: yup.string().when("registeredType", {
                is: (registeredType) => registeredType && (registeredType.value === "CARD_NUMBER" || registeredType.value === "MIX"),
                then: () => yup.string().test('len', 'Card number must be exactly 14 characters', val => val.length === 14).required("Card Number is required"),
                otherwise: () => yup.string()
            }),
            vehicleNumber: yup.string().when("registeredType", {
                is: (registeredType) => registeredType && (registeredType.value === "VEHICLE_NUMBER" || registeredType.value === "MIX"),
                then: () => yup.string().required("Vehicle Number is required"),
                otherwise: () => yup.string()
            }),
        }),
        enableReinitialize: true
    })

    const [openCreateDialog, setOpenCreateDialog] = useState(false)
    const [roleList, setRoleList] = useState([])
    const [memberType, setMemberType] = useState([])
    const [productList, setProductList] = useState([])
    const [durationList, setDurationList] = useState([])

    useEffect(() => {
        if (pagesState === "create-membership") {
            getMemberTypeList().then(async (res) => {
                if (res.result) {
                    setMemberType(res.result.map((item) => {
                        return {
                            ...item,
                            label: item.comboName,
                            value: item.comboCode
                        }
                    }))
                }
            }).catch((e) => {
                setMemberType([])
                notify(JSON.stringify(e), "error");
            })
        }
    }, [pagesState])

    useEffect(() => {
        let roleListOption = ["GENERAL"]
        if (formik.values.ouId) {
            getComboList(formik.values.ouId.value).then(async (res) => {
                if (res.result) {
                    res.result[0].comboName.split("|").map((item) => {
                        if (item !== "GENERAL") {
                            roleListOption.push(item)
                        }
                    })
                    setRoleList(roleListOption)
                } 
            }).catch((e) => {
                setRoleList([])
                setRoleList(roleListOption)
                notify(e.message || "Network error!", "error");
            })
            getProductPolicyList(formik.values.ouId.value).then(async (res) => {
                if (res.result) {
                    setProductList(res.result.map((item) => {
                        return {
                            ...item,
                            label: item.productName,
                            value: item.productId
                        }
                    }))
                }
            }).catch((e) => {
                setProductList([])
                notify(e.message || "Network error!", "error");
            })
        } else {
            setRoleList(roleListOption)
        }
    }, [formik.values.ouId]);

    useEffect(() => {
        if (formik.values.ouId && formik.values.product) {
            if (formik.values.product.productId !== -99) {
                getDurationList(formik.values.product.value, formik.values.ouId.value).then(async (res) => {
                    if (res.result !== null) {
                        setDurationList(res.result.map((item) => {
                            return {
                                ...item,
                                label: item.productMembershipName,
                                value: item.id
                            }
                        }))
                    } else {
                        setDurationList([])
                    }
                }).catch((e) => {
                    setDurationList([])
                    notify(e.message || "Network error!", "warning");
                })
            }
        }
    }, [formik.values.ouId, formik.values.product]);

    useEffect(() => {
        if (formik.values.ouId && formik.values.typePartner) {
            if (formik.values.typePartner.value === "FREEPASS") {
                getDurationList(formik.values.product.productId, formik.values.ouId.value).then(async (res) => {
                    if (res.result) {
                        setDurationList(res.result.map((item) => {
                            return {
                                ...item,
                                label: item.productMembershipName,
                                value: item.id
                            }
                        }))
                    }
                }).catch((e) => {
                    setDurationList([])
                    notify(e.message || "Network error!", "warning");
                })
            }
        }
    }, [formik.values.typePartner, formik.values.ouId]);

    const handleBtnCreateMember = () => {
        setLoading(true)
        let { label, value, id, ...rest } = formik.values.duration
        let data = {
            "outletCode": formik.values.ouId.ouCode,

            "productMembershipId": id,
            ...rest,

            "productId": formik.values.product.productId,
            "productCode": formik.values.product.productCode,
            "productName": formik.values.product.productName,

            "partnerCode": formik.values.partnerCode,
            "firstName": formik.values.firstName,
            "lastName": formik.values.lastName,
            "phoneNumber": formik.values.phoneNumber,
            "roleType": formik.values.roleType,
            "email": formik.values.email,
            "cardNumberUuid": formik.values.cardNumberUuid,
            "vehicleNumber": formik.values.vehicleNumber,
            "typePartner": formik.values.typePartner.value,
            "registeredType": formik.values.registeredType.value,
            "remark": formik.values.remark,
            "startDate": formik.values.startDate,
            "username": username
        }
        createMember(data).then((res) => {
            if (res.success === true) {
                formik.resetForm()
                setPagesState("laporan-pengajuan")
                handleGetData({ withNotification: false, outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: offset, limitData: limit, columnOrder: isAscending.value, ascDesc: isAscending.order, draw: 1 })
            }
        }).finally(() => {
            setLoading(false)
            setOpenCreateDialog(false)
        }).catch((e) => {
            if (e.response) {
                notify(e.response.data.message, "warning");
            } else { 
                notify(e.message, "warning")
            }
        })
    }

    if (!userRoleTask.includes("viewMembershipSubmission")) {
        return null
    } else {
        if (pagesState === "laporan-pengajuan") {
            return (
                <Stack mx={"3rem"} mt={"1.5rem"} direction={"column"} gap={1} fontFamily={'sans-serif'}>
                    {/* Header */}
                    <Typography variant="h1" sx={{ fontWeight: "bold", fontSize: "2.5rem", color: "#2B3499" }}>{label}</Typography>
                    {/* Pengajuan & Search */}
                    <Stack direction={['column-reverse', 'row']} gap={1} mt={2} justifyContent={"space-between"} alignItems={"flex-start"}>
                        <Stack direction={"row"} gap={1} alignItems={"center"} justifyContent={"center"}>
                            <Select
                                value={limit}
                                onChange={(e) => setLimit(e.target.value)}
                                sx={{ color: 'black', p: 0, height: "40px", width: "75px", borderRadius: "10px" }}
                            >
                                <MenuItem value={25}>25</MenuItem>
                                <MenuItem value={50}>50</MenuItem>
                                <MenuItem value={100}>100</MenuItem>
                            </Select>
                            <Typography>Entries per Page</Typography>
                        </Stack>
                        <Stack direction={["column", "row"]} gap={1} sx={{ fontSize: "14px" }} width={["100%", "auto"]}>
                            {userRoleTask.includes('updateStatusMembershipSubmission') ?
                                <Button disabled={checkBtnLanjutkan} sx={{ height: [46, "auto"], ":disabled": { bgcolor: "#C2C2C2", color: "#FFF7F7" }, ":hover": { bgcolor: "#1d257a" }, bgcolor: "#2B3499", color: "#FFF7F7", borderRadius: "10px", padding: "5px 20px", textTransform: "capitalize" }}
                                    // onClick={() => handleBtnMultipleContinueProcess()}
                                    onClick={() => handleOpenDialogMultipleContinueProcess(true)}
                                >Continue Process</Button> : null}
                            {userRoleTask.includes('createMembershipSubmission') ?
                                <Button sx={{ height: [46, "auto"], ":disabled": { bgcolor: "#C2C2C2", color: "#FFF7F7" }, ":hover": { bgcolor: "#1d257a" }, bgcolor: "#2B3499", color: "#FFF7F7", borderRadius: "10px", padding: "5px 20px", textTransform: "capitalize" }} onClick={() => setPagesState("create-membership")}>Create Membership</Button>
                                : null}
                            <Stack gap={1} direction={"row"} justifyContent={["space-between", "flex-end"]}>
                                {/* Datepicker Button */}
                                <Button onClick={(e) => handleClickPopperDate(e)} sx={{ width: ["100%", "auto"], ":disabled": { bgcolor: "#C2C2C2" }, ":hover": { bgcolor: "#2B3499", "& svg": { color: "#FFFFFF" } }, "& svg": { color: anchorElDate ? "#FFFFFF" : "#2B3499" }, bgcolor: anchorElDate ? "#2B3499" : "transparent", border: "1px solid #2B3499", color: "#FFF7F7", borderRadius: "10px", minWidth: "auto" }}><Icon sx={{ fontSize: "1.5rem" }} component={CalendarMonthIcon} /></Button>
                                {/* Filter Button */}
                                <Button onClick={(e) => handleClickPopperFilter(e)} sx={{ width: ["100%", "auto"], ":disabled": { bgcolor: "#C2C2C2" }, ":hover": { bgcolor: "#2B3499", "& svg": { color: "#FFFFFF" } }, "& svg": { color: anchorElFilter ? "#FFFFFF" : "#2B3499" }, bgcolor: anchorElFilter ? "#2B3499" : "transparent", border: "1px solid #2B3499", color: "#FFF7F7", borderRadius: "10px", minWidth: "auto" }}><Icon sx={{ fontSize: "1.5rem" }} component={FilterAltIcon} /></Button>
                            </Stack>

                            <Box sx={{ border: "1px solid #C2C2C2", borderRadius: "10px", padding: "5px 10px", display: 'flex', alignItems: 'center', gap: 1, color: "#2B3499" }}>
                                <Icon sx={{ fontSize: "1.5rem" }} component={SearchIcon} />
                                <Input color='#2B3499' disableUnderline type='search' value={search} onChange={(e) => setSearch(e.target.value)} placeholder='Search' />
                            </Box>
                        </Stack>
                        <FilterPopover anchorEl={anchorElFilter} setAnchorEl={setAnchorElFilter} handleClosePopover={handleClosePopover} statusTypeList={statusTypeList} merchantOption={merchantOption} selectedOuCode={selectedOuCode} setSelectedOuCode={setSelectedOuCode} selectedMemberType={selectedMemberType} setSelectedMemberType={setSelectedMemberType} selectedStatusType={selectedStatusType} setSelectedStatusType={setSelectedStatusType} />
                        <DatepickerPopover anchorEl={anchorElDate} setAnchorEl={setAnchorElDate} handleClosePopover={handleClosePopover} dateValue={dateValue} setDateValue={setDateValue} />
                    </Stack>

                    <CustomLOATable dataTable={dataTable.data} totalData={dataTable.total} headerTable={headerTable} checkedValue={checkedValue} setCheckedValue={setCheckedValue} isAscending={isAscending} setIsAscending={setIsAscending} limit={limit} offset={offset} setOffset={setOffset} approvalProhibition='SUBMITTED'
                        handleGetDataTable={({ pages, asc, setIsAsc }) => handleGetData({ outletCode: selectedOuCode ? [selectedOuCode.value] : allOutletCodeList, offsetData: pages, limitData: limit, ascDesc: asc ? asc.order : "ASC", columnOrder: asc ? asc.value : isAscending.value, callback: () => setIsAsc() })}
                        handleBtnContinueProcess={({ selectedId, closeDialog }) => handleApproval({ id: selectedId, callback: () => closeDialog() })}
                        approveButton={userRoleTask.includes('updateStatusMembershipSubmission')} checkbox={userRoleTask.includes('updateStatusMembershipSubmission')} />

                    {openDialogMultipleContinue ?
                        <Dialog
                            open={openDialogMultipleContinue}
                            onClose={() => setOpenDialogMultipleContinue(false)}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogTitle sx={{ borderBottom: "1px solid #ccc" }} id="alert-dialog-title" color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>
                                Are You Sure
                            </DialogTitle>
                            <IconButton
                                aria-label="close"
                                onClick={() => setOpenDialogMultipleContinue(false)}
                                sx={{
                                    position: 'absolute',
                                    right: 8,
                                    top: 8,
                                    color: "black",
                                }}
                            >
                                <CloseIcon />
                            </IconButton>
                            <DialogContent>
                                <Typography>Are you sure want to continue the process for {checkedValue.length} data</Typography>
                            </DialogContent>
                            <DialogActions sx={{ display: "flex", justifyContent: "center", px: 3 }}>
                                <Button onClick={() => {
                                    setOpenDialogMultipleContinue(false)
                                }} autoFocus sx={{ width: "50%", borderRadius: "8px", bgcolor: "transparent", py: "12px", mb: "5px", color: "#C41F1F", textTransform: "capitalize", border: "1px solid #C41F1F", ":hover": { bgcolor: "#C41F1F", color: "#FFF7F7" } }}>
                                    No
                                </Button>
                                <Button onClick={() => {
                                    handleBtnMultipleContinueProcess()
                                }} autoFocus sx={{ width: "50%", borderRadius: "8px", bgcolor: "#2B3499", py: "12px", mb: "5px", color: "#FFF7F7", textTransform: "capitalize", ":hover": { bgcolor: "#1d257a" } }}>
                                    Yes
                                </Button>
                            </DialogActions>
                        </Dialog>
                        : null}
                </Stack>
            )
        } else if (pagesState === "create-membership") {
            return (
                <Stack mx={"3rem"} mt={"1.5rem"} direction={"column"} gap={1} fontFamily={'sans-serif'}>
                    {/* Header */}
                    <Typography variant="h1" sx={{ fontWeight: "bold", fontSize: "2.5rem", color: "#2B3499" }}>Create Membership</Typography>

                    {/* Pengajuan & Search */}
                    <Stack direction={'column'} gap={1} mt={2} justifyContent={"center"} alignItems={"start"} bgcolor={"#FFFFFF"} borderRadius={2} py={2} px={4} mb={3}>
                        <Grid container spacing={2} mt={1} sx={{ alignItems: "center" }} component={"form"} onSubmit={formik.handleSubmit}>
                            <Grid item xs={6}>
                                <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    required={true}
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={merchantOption}
                                    selectedValue={formik.values.ouId}
                                    setValue={(val) => {
                                        formik.setFieldValue("ouId", val)
                                        formik.setFieldValue("roleType", "")
                                        formik.setFieldValue("product", "")
                                        formik.setFieldValue("typePartner", "")
                                        formik.setFieldValue("duration", "")
                                    }}
                                    isError={formik.touched.ouId && formik.errors.ouId}
                                    errorMessage='Merchant is required'
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <InputField label={"Member Code"} placeholder={"PSR-PKR-0001, ..."}
                                    value={formik.values.partnerCode}
                                    onChange={(e) => formik.setFieldValue("partnerCode", e.target.value)} />
                            </Grid>
                            <Grid item xs={6}>
                                <InputField required={true} label={"First Name"}
                                    errorMessage={formik.errors.firstName}
                                    autoComplete="nope"
                                    value={formik.values.firstName}
                                    isError={formik.touched.firstName && formik.errors.firstName}
                                    onChange={(e) => formik.setFieldValue("firstName", e.target.value)} />
                            </Grid>
                            <Grid item xs={6}>
                                <InputField label={"Last Name"}
                                    value={formik.values.lastName}
                                    autoComplete="nope"
                                    onChange={(e) => formik.setFieldValue("lastName", e.target.value)} />
                            </Grid>
                            <Grid item xs={6}>
                                <InputField required={true} label={"Email"}
                                    errorMessage={formik.errors.email}
                                    isError={formik.touched.email && formik.errors.email}
                                    value={formik.values.email}
                                    onChange={(e) => formik.setFieldValue("email", e.target.value)} />
                            </Grid>
                            <Grid item xs={6}>
                                <InputField required={true} label={"Phone Number"}
                                    errorMessage={formik.errors.phoneNumber}
                                    isError={formik.touched.phoneNumber && formik.errors.phoneNumber}
                                    value={formik.values.phoneNumber}
                                    onChange={(e) => formik.setFieldValue("phoneNumber", e.target.value)} />
                            </Grid>
                            <Grid item xs={6}>
                                <SelectField
                                    label={"Group Type"}
                                    placeholder="Choose Group Type"
                                    required={true}
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={roleList}
                                    selectedValue={formik.values.roleType}
                                    setValue={(val) => formik.setFieldValue("roleType", val)}
                                    isError={formik.touched.roleType && formik.errors.roleType}
                                    errorMessage={formik.errors.roleType}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <SelectField
                                    label={"Member Type"}
                                    placeholder="Choose Member Type"
                                    required={true}
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={memberType}
                                    selectedValue={formik.values.typePartner}
                                    setValue={(val) => {
                                        formik.setFieldValue("typePartner", val)
                                        formik.setFieldValue("duration", "")
                                        if (val) {
                                            if (val.value === "FREEPASS") {
                                                // Set Vehicle Type
                                                formik.setFieldValue("product", {
                                                    "productId": -99,
                                                    "productName": "",
                                                    "productCode": "",
                                                })

                                                // Reset Registered Type
                                                // formik.setFieldValue("registeredType", "")
                                                // formik.setFieldValue("cardNumberUuid", "")
                                                // formik.setFieldValue("vehicleNumber", "")
                                            } else {
                                                formik.setFieldValue("product", "")
                                            }
                                        } else {
                                            formik.setFieldValue("product", "")
                                        }
                                    }}
                                    isError={formik.touched.typePartner && formik.errors.typePartner}
                                    errorMessage={formik.errors.typePartner}
                                />
                            </Grid>
                            {formik.values.typePartner && formik.values.typePartner.value === "FREEPASS" ? null
                                : <Grid item xs={6}>
                                    <SelectField
                                        label={"Vehicle Type"}
                                        placeholder="Choose Vehicle Type"
                                        required={true}
                                        sx={{ width: "100%", fontSize: "16px" }}
                                        data={productList} // productType
                                        selectedValue={formik.values.product}
                                        setValue={(val) => {
                                            formik.setFieldValue("product", val)
                                            formik.setFieldValue("duration", "")
                                        }}
                                        isError={formik.touched.product && formik.errors.product}
                                        errorMessage={formik.errors.product}
                                    />
                                </Grid>}
                            <Grid item xs={6}>
                                <SelectField
                                    label={"Duration"}
                                    placeholder="Choose Duration"
                                    required={true}
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={durationList}
                                    selectedValue={formik.values.duration}
                                    setValue={(val) => formik.setFieldValue("duration", val)}
                                    isError={formik.touched.duration && formik.errors.duration}
                                    errorMessage={formik.errors.duration}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <DatePickerField
                                    label={"Start Date"}
                                    minDate={moment()}
                                    placeholder={"DD MMMM YYYY"}
                                    onChange={(val) => formik.setFieldValue("startDate", moment(val).format("YYYY-MM-DD"))}
                                    isError={formik.touched.startDate && formik.errors.startDate}
                                    errorMessage={formik.errors.startDate}
                                    required={true} />
                            </Grid>
                            <Grid item xs={6}>
                                <InputField
                                    label={"Remark"}
                                    onChange={(e) => formik.setFieldValue("remark", e.target.value)}
                                    value={formik.values.remark} />
                            </Grid>
                              {/* <Grid item xs={formik.values.registeredType ? 2 : 6}> */}
                              <Grid item xs={6}>
                               <SelectField
                                    label={"Registered"} required={true}
                                    data={formik.values.typePartner && formik.values.typePartner.value === "FREEPASS" ? [{ label: "CARD NUMBER", value: "CARD_NUMBER" }] : [{ label: "MIX", value: "MIX" }, { label: "PLAT NOMOR", value: "VEHICLE_NUMBER" }, { label: "CARD NUMBER", value: "CARD_NUMBER" }]}
                                    isError={formik.touched.registeredType && formik.errors.registeredType}
                                    setValue={(val) => {
                                        formik.setFieldValue("registeredType", val)
                                        formik.setFieldValue("cardNumberUuid", "")
                                        formik.setFieldValue("vehicleNumber", "")
                                    }
                                    }
                                    selectedValue={formik.values.registeredType}
                                    errorMessage={formik.errors.registeredType}
                                /> 
                                {/* <Grid item xs={2}>
                                <InputField
                                    label={"Registered"}
                                    // required={true}
                                    placeholder={"1234-e87"}
                                    value={"MIX"}
                                    disabled
                                    sx={{
                                        "input": {
                                            "&::placeholder": {
                                                color: "black",
                                            }
                                        }
                                        
                                    }}
                                    errorMessage={formik.errors.cardNumberUuid}
                                    isError={formik.touched.cardNumberUuid && formik.errors.cardNumberUuid}
                                    onChange={(e) => formik.setFieldValue("cardNumberUuid", e.target.value)}
                                /> */}
                            </Grid>
                            {/* <Grid item xs={formik.values.registeredType.value === "CARD_NUMBER" ? 4 : 2}>
                                    <InputField
                                        label={"Card Number"}
                                        required={true}
                                        placeholder={"1234-e87"}
                                        errorMessage={formik.errors.cardNumberUuid}
                                        isError={formik.touched.cardNumberUuid && formik.errors.cardNumberUuid}
                                        onChange={(e) => formik.setFieldValue("cardNumberUuid", e.target.value)}
                                    />
                                </Grid> */}
                                {/* hardcode geming */}

                            {formik.values.registeredType && (formik.values.registeredType.value === "MIX" || formik.values.registeredType.value === "CARD_NUMBER") ?
                                // <Grid item xs={formik.values.registeredType.value === "CARD_NUMBER" ? 4 : 2}>
                                <Grid item xs={formik.values.registeredType.value === "CARD_NUMBER" ? 6 : 3}>
                                    <InputField
                                        label={"Card Number"}
                                        required={true}
                                        placeholder={"1234-e87"}
                                        errorMessage={formik.errors.cardNumberUuid}
                                        isError={formik.touched.cardNumberUuid && formik.errors.cardNumberUuid}
                                        onChange={(e) => formik.setFieldValue("cardNumberUuid", e.target.value)}
                                    />
                                </Grid>
                                : null}
                            {formik.values.registeredType && (formik.values.registeredType.value === "MIX" || formik.values.registeredType.value === "VEHICLE_NUMBER") ?
                                // <Grid item xs={formik.values.registeredType.value === "VEHICLE_NUMBER" ? 4 : 2}>
                                <Grid item xs={formik.values.registeredType.value === "VEHICLE_NUMBER" ? 6 : 3}>
                                    <InputField
                                        label={"License Plate"}
                                        required={true}
                                        placeholder={"H 9872 LM"}
                                        errorMessage={formik.errors.vehicleNumber}
                                        isError={formik.touched.vehicleNumber && formik.errors.vehicleNumber}
                                        onChange={(e) => formik.setFieldValue("vehicleNumber", e.target.value)}
                                    />
                                </Grid>
                                : null}

                            <Grid item xs={12}>
                                <Stack direction={"row"} justifyContent={"end"} gap={1}>
                                    <Button onClick={() => setPagesState("laporan-pengajuan")} sx={{ color: "#2B3499", border: "1px solid #2B3499", width: "100px", borderRadius: "5px" }}>Back</Button>
                                    <Button type='submit' sx={{ color: "#FFF7F7", backgroundColor: "#2B3499", width: "100px", borderRadius: "5px", ":hover": { backgroundColor: "#1d257a" } }}>Save</Button>
                                </Stack>
                            </Grid>
                        </Grid>

                        {/* Dialog */}
                        <Dialog
                            open={openCreateDialog}
                            onClose={() => setOpenCreateDialog(false)}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogTitle paddingX={3} id="alert-dialog-title" color={"#1C1B1F"} fontSize={"20px"} fontWeight={"bold"}>
                                Detail Pengajuan
                            </DialogTitle>
                            <IconButton
                                aria-label="close"
                                onClick={() => setOpenCreateDialog(false)}
                                sx={{
                                    position: 'absolute',
                                    right: 8,
                                    top: 8,
                                    color: "black",
                                }}
                            >
                                <CloseIcon />
                            </IconButton>
                            <DialogContent sx={{ paddingX: 3 }}>
                                <Grid container spacing={2}>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Merchant</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.ouId ? formik.values.ouId.label : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Member Code</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.partnerCode ? formik.values.partnerCode : "AUTO-GENERATED"}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>First Name</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.firstName ? formik.values.firstName : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Last Name</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.lastName ? formik.values.lastName : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Email</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.email ? formik.values.email : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Phone Number</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.phoneNumber ? formik.values.phoneNumber : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Group Type</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.roleType ? formik.values.roleType : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Member Type</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.typePartner ? formik.values.typePartner.label : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    {formik.values.product ?
                                        <Grid item xs={6}>
                                            <Stack gap={"4px"}>
                                                <Typography sx={{ color: "#757575" }}>Vehicle Type</Typography>
                                                <Typography sx={{ color: "#1C1B1F" }}>{formik.values.product ? formik.values.product.label : ""}</Typography>
                                            </Stack>
                                        </Grid> : null}
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Duration</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.duration ? formik.values.duration.label : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Start Date</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.startDate ? moment(formik.values.startDate).format("DD MMMM YYYY") : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Remark</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.remark ? formik.values.remark : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Registered</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.registeredType ? formik.values.registeredType.label : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6} sx={{ display: formik.values.vehicleNumber ? "block" : "none" }}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Vehicle Number</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.vehicleNumber ? formik.values.vehicleNumber : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={6} sx={{ display: formik.values.cardNumberUuid ? "block" : "none" }}>
                                        <Stack gap={"4px"}>
                                            <Typography sx={{ color: "#757575" }}>Card Number</Typography>
                                            <Typography sx={{ color: "#1C1B1F" }}>{formik.values.cardNumberUuid ? formik.values.cardNumberUuid : ""}</Typography>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} textAlign={"center"}>
                                        <Stack>
                                            <Typography sx={{ fontSize: "20px", color: "#1C1B1F" }}>Amount</Typography>
                                            <Typography sx={{ fontSize: "36px", color: "#2B3499", fontWeight: "bold" }}>{formatCurrency(formik.values.duration ? formik.values.duration.price : 0)}</Typography>
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </DialogContent>
                            <DialogActions sx={{ display: "flex", justifyContent: "center", px: 3, gap: 1 }}>
                                <Button onClick={() => setOpenCreateDialog(false)} autoFocus sx={{ width: "100%", bgcolor: "transparent", py: "12px", mb: "5px", color: "#2B3499", textTransform: "capitalize", border: "1px solid #2B3499", borderRadius: "10px" }}>
                                    Cancel
                                </Button>
                                <Button onClick={() => handleBtnCreateMember()} autoFocus sx={{ width: "100%", bgcolor: "#2B3499", py: "12px", mb: "5px", color: "#FFF7F7", textTransform: "capitalize", ":hover": { bgcolor: "#1d257a" }, borderRadius: "10px" }}>
                                    Create
                                </Button>
                            </DialogActions>
                        </Dialog>
                    </Stack>
                </Stack>

            )
        }
    }

}

export default PengajuanMembership