import FileUploadIcon from '@mui/icons-material/FileUpload'
import FilterAltIcon from '@mui/icons-material/FilterAlt'
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted'
import { Box, Button, Card, CardContent, Icon, Stack, Typography } from "@mui/material"
import { saveAs } from 'file-saver'
import moment from "moment/moment"
import React, { useEffect, useState } from "react"
import CustomPagination from '../../../../../components/custom-pagination'
import CustomTable from '../../../../../components/custom-table'
import DatePickerField from "../../../../../components/datepicker-field"
import FilterMessageNote from "../../../../../components/filter-message-note"
import SelectField from "../../../../../components/select-field"
import InputField from "../../../../../components/text-field"
import TimePickerField from "../../../../../components/timepicker-field"
import { getExcelExportReport, getTableReportMember } from "../../../../../services/parkir/parkir-asg"
import { dateFormat } from '../../../../../utils/dateformat'
import { formatCurrency } from '../../../../../utils/format-currency'
import DialogDetailMember from './components/dialog-detail'
import SearchIcon from '@mui/icons-material/Search';
import CustomButton from '../../../../../components/custom-button';

const LaporanMembership = ({
    label = 'Membership Payment Report',
    titleInfo = "To Display Specific Datas & Durations, Use the Filters Above.",
    subTitleInfo = ["Max Range of Date is 2 Weeks (14 Days)"],
    merchantData = [],
    setLoading = () => { },
    notify = () => { },
}) => {

    const [tableData, setTableData] = useState([])
    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState(0);
    const [count, setCount] = useState(-99);
    const [countLoading, setCountLoading] = useState(false);
    const [disableNext, setDisableNext] = useState(false);

    // Merchant Option
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [filterForm, setFilterForm] = useState({
        settlementFrom: moment(Date.now()).subtract(7, 'days').hours(0).minutes(0).seconds(0).milliseconds(0),
        settlementTo: moment(Date.now()).hours(23).minutes(59).seconds(59).milliseconds(99),
        outletCode: null,
        transactionType: "",
        periodType: "",
        search: ""
    })

    // Selected Filter Options
    const [selectedOuCode, setSelectedOuCode] = useState([])
    const [selectedFilterForm, setSelectedFilterForm] = useState({
        settlementFrom: null,
        settlementTo: null,
        outletCode: null,
        transactionType: null,
        periodType: null,
        search: null,
    })

    // Dialog
    const [openDetail, setOpenDetail] = useState(false)
    const [dataDialog, setDataDialog] = useState(null)

    // Header 
    const header = [
        {
            title: "#",
            value: "id",
            align: "center",
            width: "40px",
        },
        {
            title: "Action",
            value: "action",
            align: "center",
            width: "40px",
        },
        {
            title: "Doc No",
            value: "docNo",
            align: "center",
            width: "250px",
        },
        {
            title: "Date",
            value: "docDate",
            align: "center",
            width: "250px",
        },
        {
            title: "Business Unit Name",
            value: "ouName",
            align: "center",
            width: "250px",
        },
        {
            title: "Product Name",
            value: "productName",
            align: "center",
            width: "250px",
        },
        {
            title: "Transaction Type",
            value: "transactionType",
            align: "center",
            width: "250px",
        },
        {
            title: "Product Membership Name",
            value: "productMembershipName",
            align: "center",
            width: "300px",
        },
        {
            title: "Duration Period",
            value: "durationMembership",
            align: "center",
            width: "250px",
        },
        {
            title: "Grand Total",
            value: "grandTotal",
            align: "center",
            width: "250px",
        },
        {
            title: "Service Fee",
            value: "serviceFee",
            align: "center",
            width: "250px",
        },
        {
            title: "MDR",
            value: "mdr",
            align: "center",
            width: "250px",
        },
        {
            title: "Total Nett",
            value: "totalNett",
            align: "center",
            width: "250px",
        },
        {
            title: "Payment Method",
            value: "paymentMethod",
            align: "center",
            width: "250px",
        },
        {
            title: "Status",
            value: "statusDesc",
            align: "center",
            width: "250px",
        },
        {
            title: "Settlement Date Time",
            value: "settlementDatetime",
            align: "center",
            width: "250px",
        },
        {
            title: "Deduct Date Time",
            value: "deductDatetime",
            align: "center",
            width: "250px",
        },
        {
            title: "Partner Code",
            value: "partnerCode",
            align: "center",
            width: "250px",
        },
        {
            title: "Card Number",
            value: "cardNumberUuid",
            align: "center",
            width: "250px",
        },
        {
            title: "Vehicle Number",
            value: "vehicleNumber",
            align: "center",
            width: "250px",
        },
        {
            title: "Username",
            value: "username",
            align: "center",
            width: "250px",
        },
        {
            title: "Start Date",
            value: "startDate",
            align: "center",
            width: "250px",
        },
        {
            title: "End Date",
            value: "endDate",
            align: "center",
            width: "250px",
        }
    ]

    const renderCell = (item, header, index) => {
        if (header.value === "id") {
            return <span>{offset + index + 1}</span>;
        } else if (header.value === "action") {
            return (
                <Button onClick={() => {
                    setDataDialog(item)
                    setOpenDetail(true)
                }}>
                    <FormatListBulletedIcon sx={{ fontSize: "18px" }} />
                </Button >
            )
        } else if (header.value === "docNo") {
            return <span>{item.docNo}</span>;
        } else if (header.value === "docDate") {
            return <span>{item.docDate}</span>;
        } else if (header.value === "ouName") {
            return <span>{item.ouName}</span>;
        } else if (header.value === "productName") {
            return <span>{item.productName}</span>;
        } else if (header.value === "transactionType") {
            return <span>{item.transactionType}</span>;
        } else if (header.value === "productMembershipName") {
            return <span>{item.productMembershipName}</span>;
        } else if (header.value === "durationMembership") {
            return <span>{item.durationMembership}</span>;
        } else if (header.value === "grandTotal") {
            return <span>{formatCurrency(item.grandTotal)}</span>;
        } else if (header.value === "serviceFee") {
            return <span>{formatCurrency(item.serviceFee)}</span>;
        } else if (header.value === "mdr") {
            return <span>{formatCurrency(item.mdr)}</span>;
        } else if (header.value === "totalNett") {
            return <span>{formatCurrency(item.grandTotal - (item.serviceFee + item.mdr))}</span>;
        } else if (header.value === "paymentMethod") {
            return <span>{item.paymentMethod}</span>;
        } else if (header.value === "statusDesc") {
            return <span>{item.statusDesc}</span>;
        } else if (header.value === "settlementDatetime") {
            return <span>{item.settlementDatetime && dateFormat(item.settlementDatetime)}</span>;
        } else if (header.value === "deductDatetime") {
            return <span>{item.deductDatetime && dateFormat(item.deductDatetime)}</span>;
        } else if (header.value === "partnerCode") {
            return <span>{item.partnerCode}</span>;
        } else if (header.value === "cardNumberUuid") {
            return <span>{item.cardNumberUuid}</span>;
        } else if (header.value === "vehicleNumber") {
            return <span>{item.vehicleNumber}</span>;
        } else if (header.value === "username") {
            return <span>{item.username}</span>;
        } else if (header.value === "startDate") {
            return <span>{item.startDate && dateFormat(item.startDate)}</span>;
        } else if (header.value === "endDate") {
            return <span>{item.endDate && dateFormat(item.endDate)}</span>;
        }

        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }

    const handleGetData = ({
        outletCode,
        draw = 0,
        filter,
        length,
        offset,
        callback = () => { }
    }) => {
        const dateFrom = moment(filter.settlementFrom)
        const dateTo = moment(filter.settlementTo)
        if (dateTo.diff(dateFrom, 'days') <= 14) {
            const data = {
                "draw": draw,
                "settlementFrom": `${moment(filter.settlementFrom).format("YYYY-MM-DD HH:mm")}`,
                "settlementTo": `${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}`,
                "outletCode": outletCode,
                "keyword": filter.search ? filter.search : "",
                "paymentMethod": "",
                "ColumOrderName": `settlementDatetime`,
                "ascDesc": "ASC",
                "Start": offset,
                "Length": length,
                "trxType": filter.transactionType ? filter.transactionType.value : "",
                "periodType": filter.periodType ? filter.periodType.value : "",
            }

            setCountLoading(true)
            setDisableNext(false)
            setLoading(true)

            setSelectedFilterForm(filter)
            setSelectedOuCode(outletCode)

            getTableReportMember(data).then((res) => {
                if (res.result.resultData !== null) {
                    notify("Success Get Data List", "success");
                    let fetchedData = res.result.resultData
                    setTableData(fetchedData)
                    if (draw === 1) {
                        setCount(res.result.totalCount)
                    }
                } else {
                    setTableData([])
                    setCount(0)
                    notify("No Data Found", "warning");
                }
            }).then(() => {
                setCountLoading(false)
                setLoading(false)
                callback()
            }).catch((e) => {
                setCount(-99);
                setCountLoading(false)
                setLoading(false)
                notify(e, "error");
            })
        } else {
            notify("Max Range of Date is 14 Days", "warning");
        }
    }

    const handleGetExcel = ({
        outletCode,
        filter,
        callback = () => { }
    }) => {
        const dateFrom = moment(filter.settlementFrom)
        const dateTo = moment(filter.settlementTo)
        if (dateTo.diff(dateFrom, 'days') <= 14) {
            setLoading(true)
            const data = {
                "draw": 1,
                "settlementFrom": `${moment(filter.settlementFrom).format("YYYY-MM-DD HH:mm")}`,
                "settlementTo": `${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}`,
                "outletCode": outletCode,
                "keyword": filter.search ? filter.search : "",
                "paymentMethod": "",
                "trxType": filter.transactionType ? filter.transactionType.value : "",
                "periodType": filter.periodType ? filter.periodType.value : "",
            }

            getExcelExportReport(data).then((res) => {
                if (res !== null) {
                    saveAs(new Blob([res], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }), `Report Membership (${moment(filter.settlementFrom).format("YYYY_MM_DD")} - ${moment(filter.settlementTo).format("YYYY_MM_DD")}).xlsx`);
                    notify("Data Downloaded", "success");
                } else {
                    notify("No Data Found", "warning");
                }
            }).then(() => {
                setLoading(false)
                callback()
            }).catch((e) => {
                setLoading(false)
                notify(e, "error");
            })
        } else {
            notify("Max Range of Date is 14 Days", "warning");
        }
    }

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetData({
            outletCode: ouCodeArr,
            draw: 1,
            filter: filterForm,
            length: limit,
            offset: offset,
            length: limit
        })
    }

    const pageChange = async (value) => {
        var ofset = value * limit;
        setOffset(ofset);
        handleGetData({
            outletCode: selectedOuCode,
            offset: ofset,
            length: limit,
            draw: 0,
            filter: selectedFilterForm
        });
    };

    const rowsChange = async (e) => {
        setOffset(0);
        setLimit(e.props.value);
        handleGetData({
            outletCode: selectedOuCode,
            offset: 0,
            length: e.props.value,
            draw: 0,
            filter: selectedFilterForm
        })
    };

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                    </Box>

                    {/* Render Dialog */}
                    <DialogDetailMember open={openDetail} setOpen={setOpenDetail} data={dataDialog} />

                    {/* Render Filter */}
                    <Stack gap={2} mt={"1.6rem"} direction={{ sm: "column" }} sx={{ borderRadius: "0.625rem", bgcolor: "#FFFFFF", padding: "20px", color: "#111111" }} justifyContent={'space-between'} mb={2} >
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'}>
                                <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    data={merchantOption}
                                    selectedValue={filterForm.outletCode}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            return ({
                                                ...prev,
                                                outletCode: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                            <Stack width={'100%'}>
                                <InputField
                                    label={"Keyword"}
                                    placeholder="Search"
                                    onChange={(e) => setFilterForm((prev) => ({ ...prev, search: e.target.value }))}
                                    value={filterForm.search}
                                />
                            </Stack>
                        </Stack>
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* From */}
                                <DatePickerField
                                    label={"Start Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(Date.now())}
                                    value={filterForm.settlementFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: moment(newValue).hour(23).minute(59).second(59),
                                            settlementFrom: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.settlementFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementFrom: newValue
                                        }))
                                    }}
                                />

                            </Stack>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* To */}
                                <DatePickerField
                                    label={"End Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(filterForm.settlementFrom).add(14, 'days') > moment() ? moment() : moment(filterForm.settlementFrom).add(14, 'days')}
                                    minDate={filterForm.settlementFrom}
                                    value={filterForm.settlementTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.settlementTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: newValue,
                                        }))
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack direction={{ xs: "column", sm: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                <SelectField
                                    label={"Transaction Type"}
                                    placeholder="All Transaction Type"
                                    data={[{ value: "EXTEND", label: "Extend" }, { value: "REGISTRATION", label: "Registration" }]}
                                    selectedValue={filterForm.transactionType}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            return ({
                                                ...prev,
                                                transactionType: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                <SelectField
                                    label={"Duration Period"}
                                    placeholder="All Duration Period"
                                    data={[{ value: "DAY", label: "Day" }, { value: "MONTH", label: "Month" }, { value: "YEAR", label: "Year" }]}
                                    selectedValue={filterForm.periodType}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            return ({
                                                ...prev,
                                                periodType: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack style={{ boxSizing: "border-box" }} sx={{
                            display: "flex",
                            flexDirection: ["column", "row"],
                            alignItems: ["end", "center"],
                            gap: 3,
                            justifyContent: "space-between",
                        }} width={'100%'} gap={2}>
                            <FilterMessageNote
                                style={{ boxSizing: "border-box" }}
                                sx={{
                                    width: ["100%", "100%"]
                                }}
                                title={titleInfo}
                                subtitle={subTitleInfo}
                            />
                            <Stack width={{ xs: '100%', lg: '100%' }} gap={2} direction={'row'} justifyContent={['center','end']}>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        handleGetExcel({
                                            outletCode: filterForm.outletCode ? [filterForm.outletCode.value] : allOutletCodeList,
                                            filter: filterForm
                                        })
                                    }}
                                    startIcon={<FileUploadIcon size="14px" />}
                                    name="Export"
                                    color={'success'}>
                                </CustomButton>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        setLimit(25)
                                        setOffset(0)
                                        handleGetData({
                                            outletCode: filterForm.outletCode ? [filterForm.outletCode.value] : allOutletCodeList,
                                            draw: 1,
                                            offset: 0,
                                            filter: filterForm,
                                            length: 25,
                                        })
                                    }}
                                    startIcon={<SearchIcon size="14px" />}
                                    name="Search">
                                </CustomButton>
                            </Stack>
                        </Stack>
                    </Stack>

                    {/* Render Table */}
                    <Box sx={{ width: "100%" }}>
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                        <CustomTable
                            headers={header}
                            items={tableData}
                            renderCell={renderCell}
                        />
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                    </Box>
                </CardContent>
            </Card>
        </Stack>
    )
}

export default LaporanMembership