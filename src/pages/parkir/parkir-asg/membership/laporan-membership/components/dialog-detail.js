import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, Grid, Stack, Typography } from "@mui/material"
import React, { useState } from 'react'
import { formatCurrency } from "../../../../../../utils/format-currency";

const DialogDetailMember = ({ open, setOpen, data = null }) => {
    // Dialog 
    const handleClose = () => {
        setOpen(false);
    };

    const checkString = (string) => {
        if (string) {
            return string
        } else {
            return "-"
        }
    }

    if (data === null) {
        return (
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                fullWidth={true}
                maxWidth={"xl"}
            >
                <DialogTitle sx={{ m: 0, p: 2, textTransform: "uppercase", fontWeight: "bold" }} id="customized-dialog-title">
                    Report Detail
                </DialogTitle>
                <DialogContent dividers sx={{ color: "#9e9e9e" }}>
                    <Typography variant="h1" fontSize={"5rem"} textAlign={"center"}>Data Not Found</Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" color="error" sx={{ color: "#FEFEFE", backgroundColor: "#B71B15", width: "80px", mr: 2, }}>
                        Tutup
                    </Button>
                </DialogActions>
            </Dialog>
        )
    } else {
        return (
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                fullWidth={true}
                maxWidth={"xl"}
                sx={{ "& .MuiDialogContent-root": { borderTop: "none" } }}
            >
                <DialogTitle sx={{ m: 0, p: 2, textTransform: "uppercase", fontWeight: "bold", color: "#494949" }} id="customized-dialog-title">
                    Report Detail
                </DialogTitle>
                <DialogContent dividers sx={{ textTransform: "uppercase", paddingY: "20px", paddingX: "50px", color: "#494949" }}>
                    <Stack direction={"column"} gap={2}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Doc No</Typography>
                                    <Typography>{checkString(data.docNo)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Doc Date</Typography>
                                    <Typography>{checkString(data.docDate)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Ref Doc No</Typography>
                                    <Typography>{checkString(data.refDocNo)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Outlet Name</Typography>
                                    <Typography>{checkString(data.ouName)}</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                        <Divider orientation="horizontal" />  {/* Divider */}
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Product Code</Typography>
                                    <Typography>{checkString(data.productCode)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Product Name</Typography>
                                    <Typography>{checkString(data.productName)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Product Membership</Typography>
                                    <Typography>{checkString(data.productMembershipName)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Partner Code</Typography>
                                    <Typography>{checkString(data.partnerCode)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Card Number UUID</Typography>
                                    <Typography>{checkString(data.cardNumberUuid)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Vehicle Number</Typography>
                                    <Typography>{checkString(data.vehicleNumber)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Start Date</Typography>
                                    <Typography>{checkString(data.startDate)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>End Date</Typography>
                                    <Typography>{checkString(data.endDate)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Partner Type</Typography>
                                    <Typography>{checkString(data.partnerType)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Registered Type</Typography>
                                    <Typography>{checkString(data.registeredType).replace("_", " ")}</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                        <Divider orientation="horizontal" />  {/* Divider */}
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Payment Ref Doc No</Typography>
                                    <Typography>{checkString(data.paymentRefDocNo)}</Typography>
                                </Stack>
                            </Grid>

                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Card Pan</Typography>
                                    <Typography>{checkString(data.cardPan)}</Typography>
                                </Stack>
                            </Grid>

                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Shift Code</Typography>
                                    <Typography>{checkString(data.shiftCode)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Username</Typography>
                                    <Typography>{checkString(data.username)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>MID</Typography>
                                    <Typography>{checkString(data.mid)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>TID</Typography>
                                    <Typography>{checkString(data.tid)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Device ID</Typography>
                                    <Typography>{checkString(data.deviceId)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Transaction Type</Typography>
                                    <Typography>{checkString(data.transactionType)}</Typography>
                                </Stack>
                            </Grid>
                            {/* Trans */}

                        </Grid>
                        <Divider orientation="horizontal" />  {/* Divider */}
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Grand Total</Typography>
                                    <Typography>{formatCurrency(data.grandTotal)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Service Fee</Typography>
                                    <Typography>{formatCurrency(data.serviceFee)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>MDR</Typography>
                                    <Typography>{formatCurrency(data.mdr)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Total Nett</Typography>
                                    <Typography>{data.grandTotal - (data.mdr + data.serviceFee) >= 0 ? formatCurrency(data.grandTotal - (data.mdr + data.serviceFee)) : formatCurrency(0)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Payment Method</Typography>
                                    <Typography>{checkString(data.paymentMethod)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Status</Typography>
                                    <Typography>{checkString(data.statusDesc)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Settlement Date Time</Typography>
                                    <Typography>{checkString(data.settlementDatetime)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Deduct Date Time</Typography>
                                    <Typography>{checkString(data.deductDatetime)}</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" color="error" sx={{ color: "#FEFEFE", backgroundColor: "#B71B15", width: "80px", mr: 2, }}>
                        Tutup
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default DialogDetailMember