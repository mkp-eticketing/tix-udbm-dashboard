import { Box, Button, Card, CircularProgress, Grid, Stack, Typography } from "@mui/material";
import Highcharts from 'highcharts';
import HighchartsReact from "highcharts-react-official";
import moment from "moment";
import React, { useEffect, useState } from "react";
import DatePickerField from "../../../../components/datepicker-field";
import SelectField from "../../../../components/select-field";
import { getChartJumlahKendaraan } from "../../../../services/parkir/parkir-asg";
import { formatCurrency } from "../../../../utils/format-currency";

const DashboardASG = ({
    label = "Report of Daily and Monthly Performance",
    username = "username",
    merchantData = [],
    notify = () => { },
}
) => {
    const [jmlKendaraanHarian, setJmlKendaraanHarian] = useState(0)
    const [jmlKendaraanBulanan, setJmlKendaraanBulanan] = useState(0)
    const [jmlPendapatanHarian, setJmlPendapatanHarian] = useState(0)
    const [jmlPendapatanBulanan, setJmlPendapatanBulanan] = useState(0)

    const [statistikJmlKendaraanHarian, setStatistikJmlKendaraanHarian] = useState()
    const [statistikJmlKendaraanBulanan, setStatistikJmlKendaraanBulanan] = useState()
    const [statistikPendapatanHarian, setStatistikPendapatanHarian] = useState()
    const [statistikPendapatanBulanan, setStatistikPendapatanBulanan] = useState()

    const [chartStatus, setChartStatus] = useState('harian')

    const [fetchedData, setFetchedData] = useState()
    const [error, setError] = useState(null)
    const [isLoading, setIsLoading] = useState(true)

    const [chartData, setChartData] = useState([])
    const [chartCategory, setChartCategory] = useState([])

    const [triggerRecursive, setTriggerRecursive] = useState(Date.now())

    // Merchant Option
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState(null)
    const [filterForm, setFilterForm] = useState({
        inputDate: moment(Date.now()),
        outletCode: null,
        ouType: ""
    })

    // Hitung Persentase Increase/Decrease
    function calculateChange(oldValue, newValue) {
        const percentageChange = ((newValue - oldValue) / Math.abs(oldValue)) * 100;
        return percentageChange.toFixed(2)
    }

    const handleGetChartJumlahKendaraan = (inputDate, outletCode, callback = () => { }) => {
        let data = {
            "yearMonth": inputDate ? `${inputDate.format("YYYY-MM")}` : null,
            "year": inputDate ? `${inputDate.format("YYYY")}` : null,
            "yearList": inputDate ? `[${inputDate.format("YYYY")}]` : null,
            "outletCode": typeof (outletCode) === 'string' ? [outletCode] : outletCode,
            "ouType": ""
        }

        getChartJumlahKendaraan(data).then((res) => {
            if (res.result) {
                let data = res.result
                setFetchedData(data)
                // let [JmlKendaraanHarianCounter, jmlPendapatanHarianCounter, jmlKendaraanBulananCounter, jmlPendapatanBulananCounter] = [0, 0, 0, 0]

                if (data.dailyTrxList) { // Error Handling
                    // data.dailyTrxList.map((item) => {
                    //     JmlKendaraanHarianCounter += item.totalTrx
                    //     jmlPendapatanHarianCounter += item.totalAmount
                    // })

                    let dailyTrxListLength = data.dailyTrxList.length
                    if (data.dailyTrxList[dailyTrxListLength - 1].docDate === moment().format('YYYY-MM-DD')) {
                        setJmlKendaraanHarian(data.dailyTrxList[dailyTrxListLength - 1].totalTrx)
                        setJmlPendapatanHarian(data.dailyTrxList[dailyTrxListLength - 1].totalAmount)
                    } else {
                        setJmlKendaraanHarian(0)
                        setJmlPendapatanHarian(0)
                    }

                    // Cek apabila data terdapat lebih dari 1 untuk komparasi
                    if (dailyTrxListLength === 1) { // Jika data hanya 1, jangan tampilkan statistik
                        setStatistikJmlKendaraanHarian()
                        setStatistikPendapatanHarian()
                    } else if (dailyTrxListLength > 1) {
                        setStatistikJmlKendaraanHarian(calculateChange(data.dailyTrxList[dailyTrxListLength - 2].totalTrx, data.dailyTrxList[dailyTrxListLength - 1].totalTrx))
                        setStatistikPendapatanHarian(calculateChange(data.dailyTrxList[dailyTrxListLength - 2].totalAmount, data.dailyTrxList[dailyTrxListLength - 1].totalAmount))
                    }
                } if (data.dailyTrxList === null) { // Jika data daily tidak ada
                    setJmlKendaraanHarian(0)
                    setJmlPendapatanHarian(0)
                    setStatistikJmlKendaraanHarian()
                    setStatistikPendapatanHarian()
                }

                if (data.monthlyTrxList) { // Error Handling
                    // let stopper = false
                    // data.monthlyTrxList.map((item) => {
                    //     if (stopper === false) {
                    //         jmlKendaraanBulananCounter += item.totalTrx
                    //         jmlPendapatanBulananCounter += item.totalAmount
                    //     } if (item.yearMonth === `${filterForm.inputDate.clone().format("YYYY-MM")}`) {
                    //         stopper = true
                    //     }
                    // })

                    let monthlyTrxListLength = data.monthlyTrxList.length
                    if (data.monthlyTrxList[monthlyTrxListLength - 1].yearMonth === moment().format('YYYY-MM')) {
                        setJmlKendaraanBulanan(data.monthlyTrxList[monthlyTrxListLength - 1].totalTrx)
                        setJmlPendapatanBulanan(data.monthlyTrxList[monthlyTrxListLength - 1].totalAmount)
                    } else {
                        setJmlKendaraanBulanan(0)
                        setJmlPendapatanBulanan(0)
                    }

                    // Hitung Statistik
                    if (`${filterForm.inputDate.format("YYYY-MM")}` === filterForm.inputDate.format("YYYY-[01]")) {
                        setStatistikJmlKendaraanBulanan()
                        setStatistikPendapatanBulanan()
                    } else {
                        let currentMonth = data.monthlyTrxList.find((element) => element.yearMonth === `${filterForm.inputDate.clone().format("YYYY-MM")}`)
                        let previousMonth = data.monthlyTrxList.find((element) => element.yearMonth === `${filterForm.inputDate.clone().subtract(1, 'months').format("YYYY-MM")}`)
                        setStatistikJmlKendaraanBulanan(calculateChange(previousMonth.totalTrx, currentMonth.totalTrx))
                        setStatistikPendapatanBulanan(calculateChange(previousMonth.totalAmount, currentMonth.totalAmount))
                    }
                } if (data.monthlyTrxList === null) { // Jika data monthly tidak ada
                    setJmlKendaraanBulanan(0)
                    setJmlPendapatanBulanan(0)
                    setStatistikJmlKendaraanBulanan()
                    setStatistikPendapatanBulanan()
                }
                if (data.monthlyTrxList === null || data.dailyTrxList === null) {
                    notify("No Data Found", "warning");
                }
            } else {
                notify("No Data Found", "warning");
            }
        }).catch((e) => {
            notify(e.message, "error");
        }).finally(() => {
            setIsLoading(false)
            callback()
        })
    }

    // Insert the data into Chart 
    useEffect(() => {
        let data = []
        let category = []
        // setError('Data Tidak Ditemukan')
        if (fetchedData) {
            if (fetchedData.dailyTrxList !== null && chartStatus === 'harian' && isLoading === false) {
                fetchedData.dailyTrxList.map((item) => {
                    data.push(item.totalTrx)
                    category.push(moment(item.docDate).format('DD'))
                })
                // setError(null)
            }
            if (fetchedData.dailyTrxList === null && chartStatus === 'harian') {
                category = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
                data = [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null]
                // setError(null)
            }
            if (fetchedData.monthlyTrxList !== null && chartStatus === 'bulanan' && isLoading === false) {
                fetchedData.monthlyTrxList.map((item) => {
                    data.push(item.totalTrx)
                    category.push(moment(item.yearMonth).format('MMMM').substring(3, 0))
                })
                // setError(null)
            }
            if (fetchedData.monthlyTrxList === null && chartStatus === 'bulanan' && isLoading === false) {
                category = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Des']
                data = [null, null, null, null, null, null, null, null, null, null, null, null];
                // setError(null)
            }
            setChartData(data)
            setChartCategory(category)
        } if (fetchedData === undefined) {
            // setError('Data Tidak Ditemukan')
        }
    }, [chartStatus, fetchedData])

    const options = {
        chart: {
            type: 'spline',
            marginTop: 70,
            spacingLeft: 0
        },
        title: {
            text: '',
        },
        series: [{
            name: 'Jumlah Kendaraan',
            showInLegend: false,
            data: chartData,
        }],
        yAxis: {
            title: '',
            labels: {
                style: {
                    color: '#2B3499',
                    fontWeight: 'bold'
                }
            }
        },
        xAxis: {
            title: '',
            categories: chartCategory,
            labels: {
                style: {
                    color: '#2B3499',
                    fontWeight: '800'
                }
            }
        },
    };

    const handleChangeChartStatus = (e) => {
        e.preventDefault
        setChartStatus(e.target.value)
    }

    // Merchant Option Picker
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    // Kode ini akan jalan di yang pertama dan consume api
    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value.replace(/[\[\]]/g, ""))
        })
        setAllOutletCodeList(ouCodeArr);

        if (filterForm.inputDate && filterForm.outletCode !== null && ouCodeArr !== null) {
            handleGetChartJumlahKendaraan(filterForm.inputDate, filterForm.outletCode.ouCode.replace(/[\[\]]/g, ""), () => { setTriggerRecursive(Date.now()) })// Get a specific merchant
        } if (filterForm.outletCode === null && ouCodeArr !== null) {
            handleGetChartJumlahKendaraan(filterForm.inputDate, ouCodeArr, () => { setTriggerRecursive(Date.now()) }) // Get a specific merchant
        }
    }

    useEffect(() => {
        if (filterForm.inputDate && filterForm.outletCode !== null && allOutletCodeList !== null) {
            handleGetChartJumlahKendaraan(filterForm.inputDate, filterForm.outletCode.ouCode.replace(/[\[\]]/g, ""))// Get a specific merchant  
        } if (filterForm.outletCode === null && allOutletCodeList !== null) {
            handleGetChartJumlahKendaraan(filterForm.inputDate, allOutletCodeList) // Get a specific merchant
        }
    }, [filterForm])

// useEffect(() => {
//     setTimeout(() => {
//         console.log('======== RECURSIVE ========');
//         if (filterForm.inputDate && filterForm.outletCode !== null && allOutletCodeList !== null) {
//             handleGetChartJumlahKendaraan(filterForm.inputDate, filterForm.outletCode.ouCode.replace(/[\[\]]/g, ""), () => { setTriggerRecursive(Date.now()) })// Get a specific merchant  
//         } if (filterForm.outletCode === null && allOutletCodeList !== null) {
//             handleGetChartJumlahKendaraan(filterForm.inputDate, allOutletCodeList, () => { setTriggerRecursive(Date.now()) }) // Get a specific merchant
//         }
//     }, 10000)
// }, [triggerRecursive])

return (
    <Stack>
        <Stack mx={"3rem"} mt={"1.5rem"} mb={"1.25rem"} gap={4} sx={{ position: "relative" }}>
            <Typography variant="h3" fontSize={'36px'} fontWeight={'bold'} color={'#2B3499'}>{label}</Typography>
        </Stack>

        {/* Hello Card */}
        <Stack mx={"3rem"} mb={"2rem"} gap={4} sx={{ position: "relative", overflow: 'hidden' }}>
            <Card sx={{ borderRadius: "0.75rem", paddingX: "40px", paddingY: { xs: '40px', md: 0 }, backgroundImage: 'linear-gradient(52deg, rgba(46,55,155,1) 0%, rgba(77,88,183,1) 54%, rgba(141,150,230,1) 100%)', }}>
                <Stack direction={'row'} justifyContent="space-between" alignItems="center" >
                    <Stack direction={'column'} sx={{ zIndex: '1' }}>
                        <Typography variant="h3" fontSize={'40px'} fontWeight={'bold'} color={'white'} textTransform={'capitalize'}>Hello, {username} !</Typography>
                        <Typography sx={{ mt: "18px" }} fontSize={'18px'} color={'white'}>Kelola sistem parkir di tempatmu secara mudah,
                            praktis, dan akurat dengan MKP
                        </Typography>
                    </Stack>
                    <Box sx={{
                        zIndex: '1', display: {
                            xs: 'none',
                            md: 'block'
                        }
                    }}>
                        <img src="https://gitlab.com/mkp-eticketing/tix-udbm-dashboard/-/raw/ae41379a3b951d592358b942096461fb81d2d3c4/public/images/ASG_vector_dashboard.svg" alt='Vector'></img>
                    </Box>
                    <Box sx={{ position: 'absolute', right: '0', top: '0', zIndex: '0' }}>
                        <img src="https://gitlab.com/mkp-eticketing/tix-udbm-dashboard/-/raw/ae41379a3b951d592358b942096461fb81d2d3c4/public/images/ASG_vector_dashboard_bgcircle.svg" alt='Vector'></img>
                    </Box>
                    <Box sx={{ position: 'absolute', left: '0', top: '0', zIndex: '0' }}>
                        <img src="https://gitlab.com/mkp-eticketing/tix-udbm-dashboard/-/raw/ae41379a3b951d592358b942096461fb81d2d3c4/public/images/ASG_vector_dashboard_opencircle.svg" alt='Vector'></img>
                    </Box>
                </Stack>
            </Card>
        </Stack>

        <Stack justifyContent={'space-between'} gap={4} marginX={'3rem'} direction={{ xs: 'column', md: 'row' }}>
            <Card style={{ boxSizing: "border-box" }} sx={{
                width: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'space-around', borderRadius: "0.625rem", paddingX: "25px", paddingY: "20px", background: '#2B3499', height: '150px',
                "& p": {
                    color: "white",
                    fontSize: '20px'
                }
            }}>
                <SelectField
                    label={"Merchant"}
                    placeholder="All Merchant"
                    sx={{
                        width: "100%", fontSize: "18px",
                        "& input": {
                            color: "white",
                            fontSize: "18px"
                        },
                        '& .MuiInputBase-root': {
                            marginTop: '10px',
                            '&:before': {
                                borderBottom: '2px solid white', // Default underline color
                            },
                            '&:hover:not(.Mui-disabled):before': {
                                borderBottom: '2px solid orange', // Hover underline color
                            },
                            '&:after': {
                                borderBottom: '2px solid #707ad1', // Focused underline color
                            },
                        },
                        '& .MuiAutocomplete-popupIndicator': {
                            color: 'white', // Change the arrow color
                        },
                        '& .MuiAutocomplete-clearIndicator': {
                            color: 'white', // Change the color of the X button
                        },
                        '& .MuiInputBase-input': {
                            padding: '10px 26px 10px 12px', // Add padding to the input field
                        },
                    }}
                    data={merchantOption}
                    selectedValue={filterForm.outletCode}
                    setValue={(val) => {
                        setIsLoading(true)
                        // if (val !== null && allOutletCodeList !== null) {
                        //     handleGetChartJumlahKendaraan(filterForm.inputDate, val.ouCode.replace(/[\[\]]/g, ""))// Get a specific merchant
                        // } if (val === null && allOutletCodeList !== null) {
                        //     handleGetChartJumlahKendaraan(filterForm.inputDate, allOutletCodeList) // Get a specific merchant
                        // }
                        setFilterForm((prev) => {
                            return ({
                                ...prev,
                                outletCode: val
                            })
                        })
                    }}
                />
            </Card>
            <Card style={{ boxSizing: "border-box" }} sx={{
                width: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'space-around', borderRadius: "0.625rem", paddingX: "25px", paddingY: "20px", background: '#2B3499', height: '150px',
                "& p": {
                    color: "white",
                    fontSize: '20px'
                }
            }}>
                <DatePickerField
                    label={"Date"}
                    placeholder="DD MMM YYYY"
                    format={"YYYY-MM"}
                    value={filterForm.inputDate}
                    views={['month', 'year']}
                    maxDate={moment(Date.now())}
                    onChange={(newValue) => {
                        setIsLoading(true)

                        // if (filterForm.outletCode !== null && allOutletCodeList !== null) {
                        //     handleGetChartJumlahKendaraan(newValue, filterForm.outletCode.ouCode.replace(/[\[\]]/g, ""))// Get a specific merchant
                        // } if (filterForm.outletCode === null && allOutletCodeList !== null) {
                        //     handleGetChartJumlahKendaraan(newValue, allOutletCodeList) // Get a specific merchant
                        // }
                        setFilterForm((prev) => ({
                            ...prev,
                            inputDate: newValue
                        }))
                    }}
                    sx={{
                        width: "100%", fontSize: "18px",
                        "& input": {
                            color: "white",
                            fontSize: "18px"
                        },
                        '& .MuiInputBase-root': {
                            color: 'black', // Text color
                            marginTop: '10px',
                            backgroundColor: 'none', // Input background color
                            '&:before': {
                                borderBottom: '2px solid white', // Default underline
                            },
                            '&:hover:not(.Mui-disabled):before': {
                                borderBottom: '2px solid orange', // Hover underline
                            },
                            '&:after': {
                                borderBottom: '2px solid #707ad1', // Focused underline
                            },
                        },
                        '& .MuiInputAdornment-root': {
                            color: 'white', // Icon color
                        },
                        '& .MuiSvgIcon-root': {
                            color: 'white', // SVG Icon color
                        },
                        '& .MuiIconButton-root': {
                            color: 'red', // Clear button color
                        },
                    }}
                />
            </Card>
        </Stack>

        {/* Grid Cards */}
        <Stack mx={"3rem"} >
            <Grid container columnSpacing={5} rowGap={5} fontFamily={'Roboto, sans-serif'} direction={['row']} my={"2rem"} justifyContent={'space-between'}>
                <Grid item xs={12} sm={6} md={6} lg={3} >
                    <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around', borderRadius: "0.625rem", paddingX: "25px", paddingY: "20px", background: '#2B3499', height: '150px' }}>
                        <Stack>
                            <Typography color={"white"}>Jumlah Kendaraan Harian</Typography>
                        </Stack>
                        <Stack>
                            <Typography my={'10px'} color={"white"} fontWeight={'bold'} fontSize={'1.5rem'}>{jmlKendaraanHarian.toLocaleString()}</Typography>
                        </Stack>
                        <Stack direction={'row'} color={'#79C74C'} alignItems={'center'} gap={1}>
                            {statistikJmlKendaraanHarian !== undefined ? statistikJmlKendaraanHarian > 0 ? <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx="32" cy="32" r="30" fill="currentColor" /><path fill="#ffffff" d="M48 30.3L32 15L16 30.3h10.6V49h10.3V30.3z" /></svg>
                                <Typography color={'#79C74C'}> {statistikJmlKendaraanHarian} % increase this day</Typography></Stack> : <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx={32} cy={32} r={30} fill="#E7A5A5"></circle><path fill="#ffffff" d="M16 33.7L32 49l16-15.3H37.4V15H27.1v18.7z"></path></svg>
                                <Typography color={'#E7A5A5'}> {statistikJmlKendaraanHarian} % decrease this day</Typography></Stack> : null}
                        </Stack>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={3} >
                    <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around', borderRadius: "0.625rem", paddingX: "25px", paddingY: "20px", background: '#2B3499', height: '150px' }}>
                        <Stack>
                            <Typography color={"white"}>Jumlah Kendaraan Bulanan</Typography>
                        </Stack>
                        <Stack>
                            <Typography my={'10px'} color={"white"} fontWeight={'bold'} fontSize={'1.5rem'}>{jmlKendaraanBulanan.toLocaleString()}</Typography>
                        </Stack>
                        <Stack direction={'row'} color={'#79C74C'} alignItems={'center'} gap={1}>
                            {statistikJmlKendaraanBulanan !== undefined ? statistikJmlKendaraanBulanan > 0 ? <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx="32" cy="32" r="30" fill="currentColor" /><path fill="#ffffff" d="M48 30.3L32 15L16 30.3h10.6V49h10.3V30.3z" /></svg>
                                <Typography color={'#79C74C'}> {statistikJmlKendaraanBulanan}% increase this month</Typography></Stack> : <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx={32} cy={32} r={30} fill="#E7A5A5"></circle><path fill="#ffffff" d="M16 33.7L32 49l16-15.3H37.4V15H27.1v18.7z"></path></svg>
                                <Typography color={'#E7A5A5'}> {statistikJmlKendaraanBulanan}% decrease this month</Typography></Stack> : null}
                        </Stack>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={3} >
                    <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around', borderRadius: "0.625rem", paddingX: "25px", paddingY: "20px", background: '#2B3499', height: '150px' }}>
                        <Stack>
                            <Typography color={"white"}>Pendapatan Harian</Typography>
                        </Stack>
                        <Stack>
                            <Typography my={'10px'} color={"white"} fontWeight={'bold'} fontSize={'1.5rem'}>{formatCurrency(jmlPendapatanHarian)}</Typography>
                        </Stack>
                        <Stack direction={'row'} color={'#79C74C'} alignItems={'center'} gap={1}>
                            {statistikPendapatanHarian !== undefined ? statistikPendapatanHarian > 0 ? <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx="32" cy="32" r="30" fill="currentColor" /><path fill="#ffffff" d="M48 30.3L32 15L16 30.3h10.6V49h10.3V30.3z" /></svg>
                                <Typography color={'#79C74C'}> {statistikPendapatanHarian} % increase this day</Typography></Stack> : <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx={32} cy={32} r={30} fill="#E7A5A5"></circle><path fill="#ffffff" d="M16 33.7L32 49l16-15.3H37.4V15H27.1v18.7z"></path></svg>
                                <Typography color={'#E7A5A5'}> {statistikPendapatanHarian} % decrease this day</Typography></Stack> : null}
                        </Stack>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={3} >
                    <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around', borderRadius: "0.625rem", paddingX: "25px", paddingY: "20px", background: '#2B3499', height: '150px' }}>
                        <Stack>
                            <Typography color={"white"}>Pendapatan Bulanan</Typography>
                        </Stack>
                        <Stack>
                            <Typography my={'10px'} color={"white"} fontWeight={'bold'} fontSize={'1.5rem'}>{formatCurrency(jmlPendapatanBulanan)}</Typography>
                        </Stack>
                        <Stack direction={'row'} color={'#79C74C'} alignItems={'center'} gap={1}>
                            {statistikPendapatanBulanan !== undefined ? statistikPendapatanBulanan > 0 ? <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx="32" cy="32" r="30" fill="currentColor" /><path fill="#ffffff" d="M48 30.3L32 15L16 30.3h10.6V49h10.3V30.3z" /></svg>
                                <Typography color={'#79C74C'}> {statistikPendapatanBulanan} % increase this month</Typography></Stack> : <Stack direction={'row'} gap={1}><svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64"><circle cx={32} cy={32} r={30} fill="#E7A5A5"></circle><path fill="#ffffff" d="M16 33.7L32 49l16-15.3H37.4V15H27.1v18.7z"></path></svg>
                                <Typography color={'#E7A5A5'}> {statistikPendapatanBulanan} % decrease this month</Typography></Stack> : null}
                        </Stack>

                    </Card>
                </Grid>
            </Grid>
        </Stack>

        {/* Chart */}
        {isLoading === false ?
            <Stack mx={"3rem"} my={"2rem"} mb={"2rem"} gap={4} >
                <Card sx={{ borderRadius: "0.75rem", paddingX: "40px", }}>
                    <Stack></Stack>
                    <Stack>
                        <Stack direction={['column', 'row']} paddingTop={'20px'} alignContent={'center'} justifyContent={'space-between'}>
                            <Typography fontSize={'24px'} fontWeight={'bold'} align={'center'} >Jumlah Kendaraan</Typography>
                            <Stack useFlexGap flexWrap="wrap">
                                <Grid container >
                                    {/* Buat ini nanti jadi komponen terpisah ya */}
                                    {/* <Grid item xs={12} sm={4} textAlign={'center'}>
                                            
                                            <Button sx={{
                                                width: {
                                                    xs: 'auto',
                                                    sm: '100px'
                                                },
                                                borderBottom: chartStatus === 'perjam' ? 'solid #2B3499 ' : '1px solid #C7C7C7',
                                                fontWeight: chartStatus === 'perjam' && 'bold',
                                                borderRadius: '0',
                                                color: chartStatus === 'perjam' ? '#2B3499' : '#C7C7C7',
                                                ":hover": {
                                                    color: '#2B3499',
                                                    fontWeight: 'bold',
                                                    borderBottom: 'solid #2B3499',
                                                },
                                                fontSize: '15px',
                                                textTransform: 'capitalize'
                                            }} onClick={(e) => handleChangeChartStatus(e)} value='perjam'>Per Jam</Button>
                                        </Grid> */}
                                    <Grid item xs={12} sm={6} textAlign={'center'}>
                                        <Button sx={{
                                            width: {
                                                xs: 'auto',
                                                sm: '100px'
                                            },
                                            borderBottom: chartStatus === 'harian' ? 'solid #2B3499 ' : '1px solid #C7C7C7',
                                            fontWeight: chartStatus === 'harian' && 'bold',
                                            borderRadius: '0',
                                            color: chartStatus === 'harian' ? '#2B3499' : '#C7C7C7',
                                            ":hover": {
                                                color: '#2B3499',
                                                fontWeight: 'bold',
                                                borderBottom: 'solid #2B3499',
                                            },
                                            fontSize: '15px',
                                            textTransform: 'capitalize'
                                        }} onClick={(e) => handleChangeChartStatus(e)} value='harian'>Harian</Button>
                                    </Grid>
                                    <Grid item xs={12} sm={6} textAlign={'center'}>
                                        <Button sx={{
                                            width: {
                                                xs: 'auto',
                                                sm: '100px'
                                            },
                                            borderBottom: chartStatus === 'bulanan' ? 'solid #2B3499 ' : '1px solid #C7C7C7',
                                            fontWeight: chartStatus === 'bulanan' && 'bold',
                                            borderRadius: '0',
                                            color: chartStatus === 'bulanan' ? '#2B3499' : '#C7C7C7',
                                            ":hover": {
                                                color: '#2B3499',
                                                fontWeight: 'bold',
                                                borderBottom: 'solid #2B3499',
                                            },
                                            textTransform: 'capitalize',
                                            fontSize: '15px'
                                        }} onClick={(e) => handleChangeChartStatus(e)} value='bulanan'>Bulanan</Button>
                                    </Grid>
                                </Grid>
                            </Stack>
                        </Stack>
                        <Stack sx={{ height: 'auto', position: 'relative' }}>
                            <HighchartsReact
                                highcharts={Highcharts}
                                options={isLoading === false && options}
                                containerProps={{ style: { height: '550px', paddingBottom: '20px' } }}
                            />
                            {error !== null ? <Typography variant="h3" sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: 'auto' }}>{error}</Typography> : null}
                        </Stack>
                    </Stack>
                </Card>
            </Stack>
            : <Stack direction={'row'} mx={"3rem"} my={"2rem"} mb={"2rem"} gap={4} justifyContent={'center'} alignContent={'center'} sx={{ textAlign: 'center' }}>
                <CircularProgress />
            </Stack>}
    </Stack>
)
}

export default DashboardASG