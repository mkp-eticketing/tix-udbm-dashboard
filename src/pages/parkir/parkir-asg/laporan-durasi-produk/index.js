import FileUploadIcon from '@mui/icons-material/FileUpload'
import SearchIcon from '@mui/icons-material/Search'
import { Box, Card, CardContent, Stack, Typography } from "@mui/material"
import moment from "moment/moment"
import React, { useEffect, useState } from "react"
import CustomButton from '../../../../components/custom-button'
import CustomPagination from '../../../../components/custom-pagination'
import CustomTable from '../../../../components/custom-table'
import DatePickerField from "../../../../components/datepicker-field"
import FilterMessageNote from "../../../../components/filter-message-note"
import SelectField from "../../../../components/select-field"
import InputField from "../../../../components/text-field"
import TimePickerField from "../../../../components/timepicker-field"
import { getExcelExportDuration, getTableDurasiKendaraan } from "../../../../services/parkir/parkir-asg"
import { formatCurrency } from "../../../../utils/format-currency"

const LaporanDurasiProduk = ({
    label = 'Report Vehicle Duration',
    titleInfo = "To Display Specific Datas & Durations, Use the Filters Above.",
    subTitleInfo = ["Max Range of Date is 2 Weeks (14 Days)"],
    merchantData = [],
    setLoading = () => { },
    notify = () => { },
}) => {

    const [tableData, setTableData] = useState([])
    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState(0);
    const [count, setCount] = useState(-99);
    const [countLoading, setCountLoading] = useState(false);
    const [disableNext, setDisableNext] = useState(false);

    // Merchant Option
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [filterForm, setFilterForm] = useState({
        settlementFrom: moment(Date.now()).subtract(7, 'days').hours(0).minutes(0).seconds(0).milliseconds(0),
        settlementTo: moment(Date.now()).hours(23).minutes(59).seconds(59).milliseconds(99),
        outletCode: null,
        ouType: "",
        isOvt: null,
        search: ""
    })

    // Selected Filter Options
    const [selectedOuCode, setSelectedOuCode] = useState([])
    const [selectedFilterForm, setSelectedFilterForm] = useState({
        settlementFrom: null,
        settlementTo: null,
        outletCode: null,
        ouType: null,
        isOvt: null,
        search: null,
    })

    // Header 
    const header = [
        {
            title: "Document Number",
            value: "docNo",
            align: "center",
            width: "250px",
        },
        {
            title: "Business Unit Name",
            value: "ouName",
            align: "center",
            width: "250px",
        },
        {
            title: "Vehicle Number In",
            value: "vehicleNumberIn",
            align: "center",
            width: "250px",
        },
        {
            title: "Vehicle Number Out",
            value: "vehicleNumberOut",
            align: "center",
            width: "250px",
        },
        {
            title: "Product Type",
            value: "productName",
            align: "center",
            width: "250px",
        },
        {
            title: "Transaction type",
            value: "trxType",
            align: "center",
            width: "250px",
        },
        {
            title: "Member type",
            value: "memberType",
            align: "center",
            width: "250px",
        },
        {
            title: "Gate",
            value: "gate",
            align: "center",
            width: "250px",
        },
        {
            title: "Check In Date",
            value: "checkingDatetime",
            align: "center",
            width: "220px",
        },
        {
            title: "Checkout Date",
            value: "checkoutDatetime",
            align: "center",
            width: "250px",
        },
        {
            title: "Duration",
            value: "durationDatetime",
            align: "center",
            width: "250px",
        },
        {
            title: "Grand Total",
            value: "grandTotal",
            align: "center",
            width: "150px",
        },
    ]

    const handleGetData = ({
        filter,
        outletCode,
        offset,
        length,
        draw = 0,
        callback = () => { }
    }) => {
        const dateFrom = moment(filter.settlementFrom)
        const dateTo = moment(filter.settlementTo)
        if (dateTo.diff(dateFrom, 'days') <= 14) {
            const data = {
                "draw": draw,
                "checkOutDatetimeFrom": filter.settlementFrom ? `${moment(filter.settlementFrom).format("YYYY-MM-DD HH:mm")}` : null,
                "checkOutDatetimeTo": filter.settlementTo ? `${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}` : null,
                "outletCode": outletCode,
                "status": "01",
                "keyword": filter.search ? filter.search : "",
                "vehicleNumber": null,
                "paymentMethod": "",
                "ColumOrderName": `checkoutDatetime`,
                "ascDesc": 'ASC',
                "Start": offset,
                "Length": length,
                "isOvt": filter.isOvt ? filter.isOvt.value.toUpperCase() : "ALL"
            }

            setCountLoading(true)
            setDisableNext(false)
            setLoading(true)

            setSelectedFilterForm(filter)
            setSelectedOuCode(outletCode)

            getTableDurasiKendaraan(data).then((res) => {
                if (res.result.resultData !== null) {
                    let fetchedData = res.result.resultData
                    setTableData(fetchedData)
                    if (draw === 1) {
                        setCount(res.result.totalCount)
                    }
                    notify("Success Get Data List", "success");
                } else {
                    setTableData([])
                    setCount(0)
                    notify("No Data Found", "warning");
                }
                setLoading(false)
            }).then(() => {
                setCountLoading(false)
                callback()
            }).catch((e) => {
                setCountLoading(false)
                setLoading(false)
                notify(e.message, "error");
            })
        } else {
            notify("Max Range of Date is 14 Days", "warning");
        }
    }

    const handleGetExcel = ({
        outletCode,
        filter
    }) => {
        const dateFrom = moment(filter.settlementFrom)
        const dateTo = moment(filter.settlementTo)
        if (dateTo.diff(dateFrom, 'days') <= 14) {
            setLoading(true)
            const data = {
                "checkOutDatetimeFrom": `${moment(filter.settlementFrom).format("YYYY-MM-DD HH:mm")}`,
                "checkOutDatetimeTo": `${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}`,
                "keyword": filter.search,
                "outletCode": outletCode,
                "isOvt": filter.isOvt ? filter.isOvt.value.toUpperCase() : "ALL"
            }

            getExcelExportDuration(data).then((res) => {
                if (res !== null) {
                    saveAs(new Blob([res], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }), `Reporting Vehicle Duration_(${moment(filter.settlementFrom).format("YYYY-MM-DD-HH:mm")} - ${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}).xlsx`);
                    notify("Data Downloaded", "success");
                } else {
                    notify("No Data Found", "warning");
                }
            }).finally(() => {
                setLoading(false)
            }).catch((e) => {
                setLoading(false)
                notify(e.message, "warning");
            })
        } else {
            notify("Max Range of Date is 14 Days", "warning");
        }
    }

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        notify("Please Select Merchant", "warning");
        // handleGetData({
        //     offset: offset,
        //     draw: 1,
        //     outletCode: ouCodeArr,
        //     filter: filterForm,
        //     length: limit
        // })
    }

    const pageChange = async (value) => {
        var ofset = value * limit;
        setOffset(ofset);
        handleGetData({
            offset: ofset,
            draw: 0,
            outletCode: selectedOuCode,
            filter: selectedFilterForm,
            length: limit
        })
    };

    const rowsChange = async (e) => {
        setOffset(0);
        setLimit(e.props.value);
        handleGetData({
            offset: 0,
            draw: 0,
            length: e.props.value,
            outletCode: selectedOuCode,
            filter: selectedFilterForm
        })
    };

    const renderCell = (item, header, index) => {
        if (header.value === "docNo") {
            return <span>{item.docNo}</span>;
        } else if (header.value === "ouName") {
            return <span>{item.ouName}</span>;
        } else if (header.value === "vehicleNumberIn") {
            return <span>{item.vehicleNumberIn}</span>;
        } else if (header.value === "vehicleNumberOut") {
            return <span>{item.vehicleNumberOut}</span>;
        } else if (header.value === "productName") {
            return <span>{item.productName}</span>;
        } else if (header.value === "trxType") {
            return <span>{item.trxType}</span>;
        } else if (header.value === "memberType") {
            return <span>{item.memberType}</span>;
        } else if (header.value === "gate") {
            return <span>{item.gate}</span>;
        } else if (header.value === "checkingDatetime") {
            return <span>{item.checkingDatetime}</span>;
        } else if (header.value === "checkoutDatetime") {
            return <span>{item.checkoutDatetime}</span>;
        } else if (header.value === "durationDatetime") {
            return <span>{item.durationDatetime}</span>;
        } else if (header.value === "grandTotal") {
            return <span>{formatCurrency(item.grandTotal)}</span>;
        }

        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                    </Box>

                    <Stack gap={2} mt={"1.6rem"} direction={{ sm: "column" }} sx={{ borderRadius: "0.625rem", bgcolor: "#FFFFFF", padding: "20px", color: "#111111" }} justifyContent={'space-between'} mb={2} >
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'}>
                                <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    data={merchantOption}
                                    selectedValue={filterForm.outletCode}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            return ({
                                                ...prev,
                                                outletCode: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                            <Stack width={'100%'}>
                                <InputField
                                    label={"Keyword"}
                                    placeholder="Search"
                                    onChange={(e) => setFilterForm((prev) => ({ ...prev, search: e.target.value }))}
                                    value={filterForm.search}
                                />
                            </Stack>
                        </Stack>
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* From */}
                                <DatePickerField
                                    label={"Start Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(Date.now())}
                                    value={filterForm.settlementFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: moment(newValue).hour(23).minute(59).second(59),
                                            settlementFrom: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.settlementFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementFrom: newValue
                                        }))
                                    }}
                                />

                            </Stack>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* To */}
                                <DatePickerField
                                    label={"End Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(filterForm.settlementFrom).add(14, 'day') > moment() ? moment() : moment(filterForm.settlementFrom).add(14, 'day')}
                                    minDate={filterForm.settlementFrom}
                                    value={filterForm.settlementTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.settlementTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: newValue,
                                        }))
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack width={{ xs: '100%', sm: '100%', md: '50%' }} sx={{ pr: 1 }} gap={2} direction={'column'} justifyContent={'end'}>
                            <SelectField
                                label={"Transaction Type"}
                                data={[{ label: "Casual", value: 'CASUAL' }, { label: "Overnight", value: 'OVERNIGHT' }]}
                                placeholder={"All Type"}
                                selectedValue={filterForm.isOvt}
                                setValue={(val) => {
                                    setFilterForm((prev) => {
                                        return ({
                                            ...prev,
                                            isOvt: val
                                        })
                                    })
                                }}
                            />

                        </Stack>
                        <Stack style={{ boxSizing: "border-box" }} sx={{
                            display: "flex",
                            flexDirection: ["column", "row"],
                            alignItems: ["end", "center"],
                            gap: 3,
                            justifyContent: "space-between",
                        }} width={'100%'} gap={2}>
                            <FilterMessageNote
                                style={{ boxSizing: "border-box" }}
                                sx={{
                                    width: ["100%", "100%"]
                                }}
                                title={titleInfo}
                                subtitle={subTitleInfo}
                            />
                            <Stack gap={2} sx={{
                                width: ["100%", "100%"]
                            }}>
                                <Stack width={{ xs: '100%', lg: '100%' }} gap={2} direction={'row'} justifyContent={['center', 'end']}>
                                    <CustomButton
                                        onClick={(e) => {
                                            e.preventDefault()
                                            if (filterForm.outletCode === null) {
                                                notify("Please Select Merchant", "warning");
                                            } else {
                                                handleGetExcel({
                                                    filter: filterForm,
                                                    outletCode: filterForm.outletCode ? [filterForm.outletCode.value] : allOutletCodeList
                                                })
                                            }
                                        }}
                                        startIcon={<FileUploadIcon size="14px" />}
                                        name="Export"
                                        color={'success'}>
                                    </CustomButton>
                                    <CustomButton
                                        onClick={(e) => {
                                            e.preventDefault()
                                            if (filterForm.outletCode !== null) {
                                                setOffset(0)
                                                setLimit(25)
                                                handleGetData({
                                                    offset: 0,
                                                    draw: 1,
                                                    outletCode: [filterForm.outletCode.value],
                                                    filter: filterForm,
                                                    length: 25
                                                })
                                            } else {
                                                notify("Please Select Merchant", "warning");
                                            }
                                        }}
                                        startIcon={<SearchIcon size="14px" />}
                                        name="Search">
                                    </CustomButton>
                                </Stack>
                            </Stack>
                        </Stack>

                    </Stack>

                    <Box sx={{ width: "100%" }}>
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                        <CustomTable
                            headers={header}
                            items={tableData}
                            renderCell={renderCell}
                        />
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                    </Box>

                </CardContent>
            </Card>
        </Stack>

        //  <Stack mt={2}>
        //             <Card sx={{ borderRadius: "0.625rem" }}>
        //                 <TableContainer>
        //                     <Table >
        //                         <TableHead>
        //                             <TableRow>
        //                                 {headerDuration.map((item) => {
        //                                     return <TableCell onClick={() => { item.value === "durationDateTime" || item.value === "gateIn" || item.value === "gateOut" ? null : handleAscDesc(item.value) }} sx={{
        //                                         background: '#EDEDED',
        //                                         color: '#2B3499',
        //                                         textTransform: 'uppercase',
        //                                         fontWeight: 'bold',
        //                                         whiteSpace: "nowrap",
        //                                         cursor: "pointer"
        //                                     }} key={item.value}>
        //                                         <Stack direction={"row"} justifyContent={"space-between"} gap={"2px"} alignItems={"center"} >
        //                                             {item.title} {item.value === "durationDateTime" || item.value === "gateIn" || item.value === "gateOut" ? null : isAscending.value === item.value ? isAscending.order === "asc" ? <ArrowUpwardIcon sx={{ width: "16px" }} /> : <ArrowDownwardIcon sx={{ width: "16px" }} /> : <Stack direction={"row"}> <ArrowUpwardIcon sx={{ width: "16px", mr: "-4px" }} /> <ArrowDownwardIcon sx={{ width: "16px" }} /> </Stack>}
        //                                         </Stack>
        //                                     </TableCell>
        //                                 })}
        //                             </TableRow>
        //                         </TableHead>
        //                         <TableBody>
        //                             {tableData ? tableData.map((item, index) => {
        //                                 return (
        //                                     <TableRow key={index} sx={{
        //                                         ":nth-of-type(even)": {
        //                                             background: '#F1F2FB'
        //                                         },
        //                                         td: {
        //                                             color: '#2B3499'
        //                                         },
        //                                         whiteSpace: "nowrap",

        //                                     }}>
        //                                         <TableCell>{item.docNo}</TableCell>
        //                                         <TableCell>{item.ouName}</TableCell>
        //                                         <TableCell>{item.vehicleNumberIn}</TableCell>
        //                                         <TableCell>{item.vehicleNumberOut}</TableCell>
        //                                         <TableCell>{item.productName}</TableCell>
        //                                         <TableCell>{item.trxType}</TableCell>
        //                                         <TableCell>{item.gateIn}</TableCell>
        //                                         <TableCell>{item.gateOut}</TableCell>
        //                                         <TableCell>{item.checkingDatetime}</TableCell>
        //                                         <TableCell>{item.checkoutDatetime}</TableCell>
        //                                         <TableCell sx={{textTransform: "capitalize"}}>{item.durationDatetime}</TableCell>
        //                                         <TableCell>{formatCurrency(item.grandTotal)}</TableCell>
        //                                     </TableRow>
        //                                 )
        //                             })
        //                                 : null}
        //                         </TableBody>
        //                     </Table>
        //                 </TableContainer>
        //             </Card>
        //         </Stack>

    )
}

export default LaporanDurasiProduk