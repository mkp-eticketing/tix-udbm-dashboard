import FileUploadIcon from '@mui/icons-material/FileUpload';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import SearchIcon from '@mui/icons-material/Search';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Box, Button, Card, CardContent, Stack, Typography } from "@mui/material";
import moment from "moment/moment";
import React, { useEffect, useState } from "react";
import CustomButton from '../../../../components/custom-button';
import CustomPagination from '../../../../components/custom-pagination';
import CustomTable from '../../../../components/custom-table';
import DatePickerField from "../../../../components/datepicker-field";
import FilterMessageNote from "../../../../components/filter-message-note";
import SelectField from "../../../../components/select-field";
import InputField from "../../../../components/text-field";
import TimePickerField from "../../../../components/timepicker-field";
import { getDetailMonitor, getExcelExportMonitoring, getTableDurasiKendaraan } from "../../../../services/parkir/parkir-asg";
import { formatCurrency } from '../../../../utils/format-currency';
import MonitoringDialogDetail from "./components/dialog-detail";
import MonitoringDialogImage from "./components/dialog-image";

const LaporanMonitoringTransaksi = ({
    label = 'Report Transaction Monitoring',
    titleInfo = "To Display Specific Datas & Durations, Use the Filters Above.",
    subTitleInfo = ["Max Range of Date is 2 Weeks (14 Days)"],
    merchantData = [],
    setLoading = () => { },
    notify = () => { },
}) => {

    const [tableData, setTableData] = useState([])
    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState(0);
    const [count, setCount] = useState(-99);
    const [countLoading, setCountLoading] = useState(false);
    const [disableNext, setDisableNext] = useState(false);

    // Merchant Option
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [filterForm, setFilterForm] = useState({
        settlementFrom: moment(Date.now()).subtract(7, 'days').hours(0).minutes(0).seconds(0).milliseconds(0),
        settlementTo: moment(Date.now()).hours(23).minutes(59).seconds(59).milliseconds(99),
        outletCode: null,
        search: ""
    })

    // Selected Filter Options
    const [selectedOuCode, setSelectedOuCode] = useState([])
    const [selectedFilterForm, setSelectedFilterForm] = useState({
        settlementFrom: null,
        settlementTo: null,
        outletCode: null,
        search: null
    })

    // Dialog
    const [openDetail, setOpenDetail] = useState(false)
    const [openImage, setOpenImage] = useState(false)
    const [dataDialog, setDataDialog] = useState(null)
    const [imageDialog, setImageDialog] = useState(null)

    // Header 
    const header = [
        {
            title: "#",
            value: "id",
            align: "center",
            width: "40px",
        },
        {
            title: "Action",
            value: "action",
            align: "center",
            width: "40px",
        },
        {
            title: "Doc No",
            value: "docNo",
            align: "center",
            width: "250px",
        },
        {
            title: "Date",
            value: "docDate",
            align: "center",
            width: "250px",
        },
        {
            title: "Merchant Doc No",
            value: "extDocNo",
            align: "center",
            width: "250px",
        },
        {
            title: "Business Unit Code",
            value: "ouCode",
            align: "center",
            width: "250px",
        },
        {
            title: "Business Unit Name",
            value: "ouName",
            align: "center",
            width: "250px",
        },
        {
            title: "Member Code",
            value: "memberCode",
            align: "center",
            width: "250px",
        },
        {
            title: "Member Name",
            value: "memberName",
            align: "center",
            width: "250px",
        },
        {
            title: "Card UUID",
            value: "cardNumberUuid",
            align: "center",
            width: "250px",
        },
        {
            title: "Card Number",
            value: "cardPan",
            align: "center",
            width: "250px",
        },
        {
            title: "Product Code",
            value: "productCode",
            align: "center",
            width: "250px",
        },
        {
            title: "Product",
            value: "productName",
            align: "center",
            width: "250px",
        },
        {
            title: "Gate",
            value: "gate",
            align: "center",
            width: "250px",
        },
        {
            title: "Vehicle Number In",
            value: "vehicleNumberIn",
            align: "center",
            width: "250px",
        },
        {
            title: "Vehicle Number Out",
            value: "vehicleNumberOut",
            align: "center",
            width: "250px",
        },
        {
            title: "Total",
            value: "grandTotal",
            align: "center",
            width: "250px",
        },
        {
            title: "Service Fee",
            value: "serviceFee",
            align: "center",
            width: "250px",
        },
        {
            title: "MDR",
            value: "mdr",
            align: "center",
            width: "250px",
        },
        {
            title: "Total Nett",
            value: "totalNett",
            align: "center",
            width: "250px",
        },
        {
            title: "Username",
            value: "username",
            align: "center",
            width: "250px",
        },
        {
            title: "Shift Code",
            value: "shiftCode",
            align: "center",
            width: "250px",
        },
        {
            title: "Payment Doc No",
            value: "paymentRefDocNo",
            align: "center",
            width: "400px",
        },
        {
            title: "Status",
            value: "statusDesc",
            align: "center",
            width: "200px",
        },
        {
            title: "Settlement Date",
            value: "settlementDatetime",
            align: "center",
            width: "250px",
        },
        {
            title: "Deduct Date",
            value: "deductDatetime",
            align: "center",
            width: "250px",
        },
        {
            title: "Check In",
            value: "checkingDatetime",
            align: "center",
            width: "250px",
        },
        {
            title: "Check Out",
            value: "checkoutDatetime",
            align: "center",
            width: "250px",
        },
    ]

    const renderCell = (item, header, index) => {
        if (header.value === "id") {
            return <span>{offset + index + 1}</span>;
        } else if (header.value === "action") {
            return (
                <Stack justifyContent={"center"} alignItems={"center"} direction={"row"}>
                    <Button onClick={() => {
                        setOpenImage(true)
                        setImageDialog(item)
                    }}>
                        <VisibilityIcon sx={{ fontSize: "18px", }} />
                    </Button >
                    <Button onClick={() => {
                        handleGetDetailMonitor(item.id, () => { setOpenDetail(true) })
                    }}>
                        <FormatListBulletedIcon sx={{ fontSize: "18px", }} />
                    </Button >
                </Stack>
            )
        } else if (header.value === "docNo") {
            return <span>{item.docNo}</span>;
        } else if (header.value === "docDate") {
            return <span>{item.docDate}</span>;
        } else if (header.value === "extDocNo") {
            return <span>{item.extDocNo}</span>;
        } else if (header.value === "ouCode") {
            return <span>{item.ouCode}</span>;
        } else if (header.value === "ouName") {
            return <span>{item.ouName}</span>;
        } else if (header.value === "memberCode") {
            return <span>{item.memberCode}</span>;
        } else if (header.value === "memberName") {
            return <span>{item.memberName}</span>;
        } else if (header.value === "cardNumberUuid") {
            return <span>{item.cardNumberUuid}</span>;
        } else if (header.value === "cardPan") {
            return <span>{item.cardPan}</span>;
        } else if (header.value === "productCode") {
            return <span>{item.productCode}</span>;
        } else if (header.value === "productName") {
            return <span>{item.productName}</span>;
        } else if (header.value === "gate") {
            return <span>{item.gate}</span>;
        } else if (header.value === "vehicleNumberIn") {
            return <span>{item.vehicleNumberIn}</span>;
        } else if (header.value === "vehicleNumberOut") {
            return <span>{item.vehicleNumberOut}</span>;
        } else if (header.value === "grandTotal") {
            return <span>{formatCurrency(item.grandTotal)}</span>;
        } else if (header.value === "serviceFee") {
            return <span>{formatCurrency(item.serviceFee)}</span>;
        } else if (header.value === "mdr") {
            return <span>{formatCurrency(item.mdr)}</span>;
        } else if (header.value === "totalNett") {
            return <span>{formatCurrency(item.grandTotal - (item.serviceFee + item.mdr))}</span>;
        } else if (header.value === "username") {
            return <span>{item.username}</span>;
        } else if (header.value === "paymentRefDocNo") {
            return <span>{item.paymentRefDocNo}</span>;
        } else if (header.value === "statusDesc") {
            return <span>{item.statusDesc}</span>;
        } else if (header.value === "settlementDatetime") {
            return <span>{item.settlementDatetime}</span>;
        } else if (header.value === "deductDatetime") {
            return <span>{item.deductDatetime}</span>;
        } else if (header.value === "checkingDatetime") {
            return <span>{item.checkingDatetime}</span>;
        } else if (header.value === "checkoutDatetime") {
            return <span>{item.checkoutDatetime}</span>;
        }

        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }


    const handleGetData = ({
        outletCode,
        filter,
        offset,
        draw = 0,
        length,
        callback = () => { }
    }) => {
        const dateFrom = moment(filter.settlementFrom)
        const dateTo = moment(filter.settlementTo)
        if (dateTo.diff(dateFrom, 'days') <= 14) {
            const data = {
                "draw": draw,
                "checkOutDatetimeFrom": filter.settlementFrom ? `${moment(filter.settlementFrom).format("YYYY-MM-DD HH:mm")}` : null,
                "checkOutDatetimeTo": filter.settlementTo ? `${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}` : null,
                "outletCode": outletCode,
                "status": "01",
                "keyword": filter.search ? filter.search : "",
                "vehicleNumber": null,
                "paymentMethod": "",
                "ColumOrderName": `doc_date`,
                "ascDesc": `asc`,
                "Start": offset,
                "Length": length,
                "isOvt": "ALL"
            }

            setCountLoading(true)
            setDisableNext(false)
            setLoading(true)

            setSelectedFilterForm(filter)
            setSelectedOuCode(outletCode)

            getTableDurasiKendaraan(data).then((res) => {
                if (res.result.resultData !== null) {
                    notify("Success Get Data List", "success");
                    let fetchedData = res.result.resultData
                    setTableData(fetchedData)
                    if (draw === 1) {
                        setCount(res.result.totalCount)
                    }
                } else if (res.result.resultData === null) {
                    setTableData([])
                    setCount(0)
                    notify("No Data Found", "warning");
                }
            }).then(() => {
                setLoading(false)
                setCountLoading(false)
                callback()
            }).catch((e) => {
                setLoading(false)
                setCountLoading(false)
                notify(e.message, "error");
            })
        } else {
            notify("Max Range of Date is 14 Days", "warning");
        }
    }

    const handleGetExcel = ({
        outletCode,
        filter
    }) => {
        const dateFrom = moment(filter.settlementFrom)
        const dateTo = moment(filter.settlementTo)
        if (dateTo.diff(dateFrom, 'days') <= 14) {
            setLoading(true)
            const data = {
                "checkOutDatetimeFrom": `${moment(filter.settlementFrom).format("YYYY-MM-DD HH:mm")}`,
                "checkOutDatetimeTo": `${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}`,
                "keyword": filter.search ? filter.search : "",
                "outletCode": outletCode,
                "isOvt": "ALL"
            }
            getExcelExportMonitoring(data).then((res) => {
                if (res !== null) {
                    saveAs(new Blob([res], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }), `Reporting_Transaction_Monitoring_(${moment(filter.settlementFrom).format("YYYY-MM-DD-HH:mm")} - ${moment(filter.settlementTo).format("YYYY-MM-DD HH:mm")}).xlsx`);
                    notify("Data Downloaded", "success");
                } else {
                    notify("No Data Found", "warning");
                }
            }).finally(() => {
                setLoading(false)
            }).catch((e) => {
                setLoading(false)
                notify(e.message, "warning");
            })
        } else {
            notify("Max Range of Date is 14 Days", "warning");
        }
    }

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        notify("Please Select Merchant", "warning");
    }

    const handleGetDetailMonitor = (id, callback = () => { }) => {
        setLoading(true)
        setDataDialog(null)
        const data = {
            "id": id === null ? "" : id
        }
        getDetailMonitor(data).then((res) => {
            if (res !== null) {
                setDataDialog(res.result)
            } else {
                notify("No Data Found", "warning");
                setDataDialog(null)
            }
        }).then(() => {
            setLoading(false)
            callback() // wait for the data fetch is complete
        }).catch((e) => {
            setLoading(false)
            callback()
            notify(e.message, "error");
            setDataDialog(null)
        })
    }

    const pageChange = async (value) => {
        var ofset = value * limit;
        setOffset(ofset);
        handleGetData({
            outletCode: selectedOuCode,
            filter: selectedFilterForm,
            length: limit,
            offset: ofset,
            draw: 0
        });
    };

    const rowsChange = async (e) => {
        setOffset(0);
        setLimit(e.props.value);
        handleGetData({
            outletCode: selectedOuCode,
            filter: selectedFilterForm,
            length: e.props.value,
            draw: 0,
            offset: 0
        })
    };

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                    </Box>

                    {/* Render Dialog */}
                    <MonitoringDialogDetail open={openDetail} setOpen={setOpenDetail} data={dataDialog} />
                    <MonitoringDialogImage open={openImage} setOpen={setOpenImage} data={imageDialog} />

                    {/* Render Filter */}
                    <Stack gap={2} mt={"1.6rem"} direction={{ sm: "column" }} sx={{ borderRadius: "0.625rem", bgcolor: "#FFFFFF", padding: "20px", color: "#111111" }} justifyContent={'space-between'} mb={2} >
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'}>
                                <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    data={merchantOption}
                                    selectedValue={filterForm.outletCode}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            return ({
                                                ...prev,
                                                outletCode: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                            <Stack width={'100%'}>
                                <InputField
                                    label={"Keyword"}
                                    placeholder="Search"
                                    onChange={(e) => setFilterForm((prev) => ({ ...prev, search: e.target.value }))}
                                    value={filterForm.search}
                                />
                            </Stack>
                        </Stack>
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* From */}
                                <DatePickerField
                                    label={"Start Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(Date.now())}
                                    value={filterForm.settlementFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: moment(newValue).hour(23).minute(59).second(59),
                                            settlementFrom: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.settlementFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementFrom: newValue
                                        }))
                                    }}
                                />

                            </Stack>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                {/* To */}
                                <DatePickerField
                                    label={"End Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(filterForm.settlementFrom).add(14, 'days') > moment() ? moment() : moment(filterForm.settlementFrom).add(14, 'days')}
                                    minDate={filterForm.settlementFrom}
                                    value={filterForm.settlementTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.settlementTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            settlementTo: newValue,
                                        }))
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack style={{ boxSizing: "border-box" }} sx={{
                            display: "flex",
                            flexDirection: ["column", "row"],
                            alignItems: ["end", "center"],
                            gap: 3,
                            justifyContent: "space-between",
                        }} width={'100%'} gap={2}>
                            <FilterMessageNote
                                style={{ boxSizing: "border-box" }}
                                sx={{
                                    width: ["100%", "100%"]
                                }}
                                title={titleInfo}
                                subtitle={subTitleInfo}
                            />
                            <Stack width={{ xs: '100%', lg: '100%' }} gap={2} direction={'row'} justifyContent={['center','end']}>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        if (filterForm.outletCode !== null) {
                                            handleGetExcel({
                                                outletCode: filterForm.outletCode ? [filterForm.outletCode.value] : allOutletCodeList,
                                                filter: filterForm
                                            })
                                        } else {
                                            notify("Please Select Merchant", "warning");
                                        }
                                    }}
                                    startIcon={<FileUploadIcon size="14px" />}
                                    name="Export"
                                    color={'success'}>
                                </CustomButton>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        if (filterForm.outletCode !== null) {
                                            setLimit(25)
                                            setOffset(0)
                                            handleGetData({
                                                draw: 1,
                                                offset: 0,
                                                outletCode: [filterForm.outletCode.value],
                                                filter: filterForm,
                                                length: 25
                                            })
                                        } else {
                                            notify("Please Select Merchant", "warning");
                                        }
                                    }}
                                    startIcon={<SearchIcon size="14px" />}
                                    name="Search">
                                </CustomButton>
                            </Stack>
                        </Stack>
                    </Stack>

                    {/* Render Table */}
                    <Box sx={{ width: "100%" }}>
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                        <CustomTable
                            headers={header}
                            items={tableData}
                            renderCell={renderCell}
                        />
                        <CustomPagination
                            disableNext={disableNext}
                            countLoading={countLoading}
                            limit={limit}
                            offset={offset}
                            count={count}
                            pageChange={(event, v) => pageChange(v)}
                            rowsChange={async (event, e) => rowsChange(e)}
                        />
                    </Box>
                </CardContent>
            </Card>
        </Stack>
    )
}

export default LaporanMonitoringTransaksi