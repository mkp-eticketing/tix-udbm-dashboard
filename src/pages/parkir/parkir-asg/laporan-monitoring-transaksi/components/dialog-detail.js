import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, Grid, Stack, Typography } from "@mui/material"
import React, { useState } from 'react'
import { formatCurrency } from "../../../../../utils/format-currency";

const MonitoringDialogDetail = ({ open, setOpen, data = null }) => {
    // Dialog 
    const handleClose = () => {
        setOpen(false);
    };

    if (data === null) {
        return (
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                fullWidth={true}
                maxWidth={"xl"}
            >
                <DialogTitle sx={{ m: 0, p: 2, textTransform: "uppercase", fontWeight: "bold" }} id="customized-dialog-title">
                    Report Detail
                </DialogTitle>
                <DialogContent dividers sx={{ color: "#9e9e9e" }}>
                    <Typography variant="h1" fontSize={"5rem"} textAlign={"center"}>Data Not Found</Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}  variant="contained" color="error" sx={{ color:"#FEFEFE", backgroundColor:"#B71B15", width: "80px", mr: 2, }}>
                        Tutup
                    </Button>
                </DialogActions>
            </Dialog>
        )
    } else {
        let nettAmount = data.trx.grandTotal - (data.trx.mdr + data.trx.serviceFee)

        return (
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                fullWidth={true}
                maxWidth={"xl"}
                sx={{ "& .MuiDialogContent-root": { borderTop: "none" } }}
            >
                <DialogTitle sx={{ m: 0, p: 2, textTransform: "uppercase", fontWeight: "bold", color: "#494949" }} id="customized-dialog-title">
                    Report Detail
                </DialogTitle>
                <DialogContent dividers sx={{ textTransform: "uppercase", paddingY: "20px", paddingX: "50px", color: "#494949" }}>
                    <Stack direction={"column"} gap={2}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"} >Date</Typography>
                                    <Typography>{data.trx.docDate}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Gate In</Typography>
                                    <Typography>{data.trxLogItems.gateIn}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Gate Out</Typography>
                                    <Typography>{data.trxLogItems.gateOut}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Payment Type</Typography>
                                    <Typography>{data.trxExt.memberCode === "" ? "Casual" : data.trxExt.memberCode === "MANUAL" ? "Casual" : "Member"}</Typography>

                                </Stack>
                            </Grid>
                        </Grid>
                        <Divider orientation="horizontal" />  {/* Divider */}
                        <Grid container spacing={1}>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Payment Doc No</Typography>
                                    <Typography>{data.trx.paymentRefDocNo}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Merchant Doc No</Typography>
                                    <Typography>{data.trx.extDocNo}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Product</Typography>
                                    <Typography>{data.trx.productName}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Card Number</Typography>
                                    <Typography>{data.trxExt.cardPan}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Vehicle Number In</Typography>
                                    <Typography>{data.trx.vehicleNumberIn}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Vehicle Number Out</Typography>
                                    <Typography>{data.trx.vehicleNumberOut}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Member Name</Typography>
                                    <Typography>{data.trxExt.memberName}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Payment Method</Typography>
                                    <Typography>{data.trx.paymentMethod}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Date In</Typography>
                                    <Typography>{data.trxLogItems.checkingDatetime}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Date Out</Typography>
                                    <Typography>{data.trxLogItems.checkoutDatetime}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Duration</Typography>
                                    <Typography>{data.trx.durationTime}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Transaction Status</Typography>
                                    <Typography>{data.trx.statusDesc}</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                        <Divider orientation="horizontal" />  {/* Divider */}
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Fee</Typography>
                                    <Typography>{formatCurrency(data.trx.grandTotal)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>MDR</Typography>
                                    <Typography>{formatCurrency(data.trx.mdr)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Service Fee</Typography>
                                    <Typography>{formatCurrency(data.trx.serviceFee)}</Typography>
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6} lg={3}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }} fontSize={"16px"}>Nett Amount</Typography>
                                    <Typography>{nettAmount >= 0 ? formatCurrency(nettAmount) : formatCurrency(0)}</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" color="error" sx={{ color:"#FEFEFE", backgroundColor:"#B71B15", width: "80px", mr: 2, }}>
                        Tutup
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default MonitoringDialogDetail