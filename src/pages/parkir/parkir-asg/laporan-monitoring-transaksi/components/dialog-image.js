import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, Grid, Stack, Typography } from "@mui/material"
import React, { useState } from 'react'
import { formatCurrency } from "../../../../../utils/format-currency";

const MonitoringDialogImage = ({ open, setOpen, data=null }) => {
    // Dialog 

    const handleClose = () => {
        setOpen(false);
    };

    if (data === null) {
        return (
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                fullWidth={true}
                maxWidth={"xl"}
            >
                <DialogTitle sx={{ m: 0, p: 2, textTransform: "uppercase", fontWeight: "bold" }} id="customized-dialog-title">
                    Picture Detail
                </DialogTitle>
                <DialogContent dividers sx={{ color: "#9e9e9e" }}>
                    <Typography variant="h1" fontSize={"5rem"} textAlign={"center"}>Data Not Found</Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}  variant="contained" color="error" sx={{ color:"#FEFEFE", backgroundColor:"#B71B15", width: "80px", mr: 2, }}>
                        Tutup
                    </Button>
                </DialogActions>
            </Dialog>
        )
    } else {
        return (
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                fullWidth={true}
                maxWidth={"lg"}
            >
                <DialogTitle sx={{ m: 0, p: 2, color: "#494949", fontWeight: "bold", textTransform: "uppercase" }} id="customized-dialog-title">
                    Picture Detail
                </DialogTitle>
                <DialogContent dividers sx={{ color: "#494949", px: "30px",  }}>
                    <Stack direction={"column"} gap={2}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }}>CAM 1 IN</Typography>
                                    <Box
                                        component="img"
                                        onClick={() => { window.open(`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM1_${data.docNo}_IN.jpg`, '_blank').focus(); }}
                                        sx={{
                                            maxHeight: "100%",
                                            maxWidth: "100%",
                                            ":hover": {
                                                cursor: "pointer"
                                            }
                                        }}
                                        alt="CAM 1 Image IN"
                                        src={`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM1_${data.docNo}_IN.jpg`}
                                    />
                                    {/* <img src={`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM1_${data.docNo}_IN.jpg`} className="App-logo" alt="logo" /> */}

                                </Stack>

                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }}>CAM 1 OUT</Typography>
                                    <Box
                                        component="img"
                                        onClick={() => { window.open(`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM1_${data.docNo}_OUT.jpg`, '_blank').focus(); }}
                                        sx={{
                                            maxHeight: "100%",
                                            maxWidth: "100%",
                                            ":hover": {
                                                cursor: "pointer"
                                            }
                                        }}
                                        alt="CAM 1 Image OUT"
                                        src={`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM1_${data.docNo}_OUT.jpg`}
                                    />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }}>CAM 2 OUT</Typography>
                                    <Box
                                        component="img"
                                        onClick={() => { window.open(`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM2_${data.docNo}_IN.jpg`, '_blank').focus(); }}
                                        sx={{
                                            maxHeight: "100%",
                                            maxWidth: "100%",
                                            ":hover": {
                                                cursor: "pointer"
                                            }
                                        }}
                                        alt="CAM 2 Image IN"
                                        src={`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM2_${data.docNo}_IN.jpg`}
                                    />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }}>CAM 2 OUT</Typography>
                                    <Box
                                        component="img"
                                        onClick={() => { window.open(`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM2_${data.docNo}_OUT.jpg`, '_blank').focus(); }}
                                        sx={{
                                            maxHeight: "100%",
                                            maxWidth: "100%",
                                            ":hover": {
                                                cursor: "pointer"
                                            }
                                        }}
                                        alt="CAM 2 Image OUT"
                                        src={`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/CAM2_${data.docNo}_OUT.jpg`}
                                    />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }}>CAM LPR 1 IN</Typography>
                                    <Box
                                        component="img"
                                        onClick={() => { window.open(`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/lpr/${data.vehicleNumberIn}-imagePlate.jpg`, '_blank').focus(); }}
                                        sx={{
                                            maxHeight: "100%",
                                            maxWidth: "100%",
                                            ":hover": {
                                                cursor: "pointer"
                                            }
                                        }}
                                        alt="CAM LPR 1 IN"
                                        src={`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/lpr/${data.vehicleNumberIn}-imagePlate.jpg`}
                                    />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Stack direction={"column"}>
                                    <Typography sx={{ fontWeight: "bold" }}>CAM LPR 2 OUT</Typography>
                                    <Box
                                        component="img"
                                        onClick={() => { window.open(`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/lpr/${data.vehicleNumberOut}-imagePlate_OUT.jpg`, '_blank').focus(); }}
                                        sx={{
                                            maxHeight: "100%",
                                            maxWidth: "100%",
                                            ":hover": {
                                                cursor: "pointer"
                                            }
                                        }}
                                        alt="CAM LPR 2 OUT"
                                        src={`https://sandbox.mkpmobile.com/parkingimg/parkingimgmkpmobile/${data.ouCode}/lpr/${data.vehicleNumberOut}-imagePlate_OUT.jpg`}
                                    />
                                </Stack>
                            </Grid>
                        </Grid>
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" color="error" sx={{ color:"#FEFEFE", backgroundColor:"#B71B15", width: "80px", mr: 2, }}>
                        Tutup
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default MonitoringDialogImage