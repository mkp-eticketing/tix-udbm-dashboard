import FileUploadIcon from '@mui/icons-material/FileUpload'
import SearchIcon from '@mui/icons-material/Search'
import { Box, Card, CardContent, Stack, Typography } from "@mui/material"
import moment from "moment"
import React, { useEffect, useState } from "react"
import CustomButton from '../../../../components/custom-button'
import CustomTable from '../../../../components/custom-table'
import DatePickerField from "../../../../components/datepicker-field"
import FilterMessageNote from "../../../../components/filter-message-note"
import SelectField from "../../../../components/select-field"
import TimePickerField from "../../../../components/timepicker-field"
import { getExcelExportSummary, getTableJumlahKendaraanDanPendapatan } from "../../../../services/parkir/parkir-asg"
import { formatCurrency } from "../../../../utils/format-currency"

const LaporanSummaryPendapatanProduk = ({
    label = 'Report Summary Product & Income',
    titleInfo = "To Display Specific Datas & Durations, Use the Filters Above.",
    subTitleInfo = ["Max Range of Date is 1 Week (7 Days)"],
    merchantData = [],
    setLoading = () => { },
    notify = () => { },
}) => {
    const [tableData, setTableData] = useState([])

    const [headerChild, setHeaderChild] = useState({
        member: [],
        casual: []
    })

    // Merchant Option
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [filterForm, setFilterForm] = useState({
        datetimeFrom: moment(Date.now()).subtract(7, 'days').hours(0).minutes(0).seconds(0).milliseconds(0),
        datetimeTo: moment(Date.now()).hours(23).minutes(59).seconds(59).milliseconds(99),
        outletCode: null,
    })

    // Header 
    const header = [
        {
            title: "Date",
            value: "Docdate",
            align: "center",
            width: "150px",
        },
        ...headerChild.member, // Member
        ...headerChild.casual, // Casual
        {
            title: "Parking Revenue",
            value: "TotalAmount",
            align: "center",
            width: "250px",
        },
    ]

    const handleGetData = ({
        outletCode,
        filter,
    }) => {
        if (filter.datetimeTo.diff(filter.datetimeFrom, 'days') <= 7) {
            const data = {
                "datetimeFrom": filter.datetimeFrom ? `${moment(filter.datetimeFrom).format("YYYY-MM-DD HH:mm")}` : null,
                "datetimeTo": filter.datetimeTo ? `${moment(filter.datetimeTo).format("YYYY-MM-DD HH:mm")}` : null,
                "outletCode": outletCode,
                "start": 0
            }

            setLoading(true)

            getTableJumlahKendaraanDanPendapatan(data).then((res) => {
                if (res.result.Result && res.result.Result !== null) {
                    let fetchedData = res.result.Result
                    setTableData(fetchedData)
                    notify("Success Get Data List", "success");

                    // Set Header Child
                    setHeaderChild(() => {
                        const arrContainerMember = []
                        const arrContainerCasual = []

                        res.result.Result.forEach(entry => {
                            entry.DataProduct.forEach(product => {
                                if (!arrContainerMember.find(({ title }) => title === product.ProductName)) {
                                    arrContainerMember.push({
                                        title: product.ProductName,
                                        value: `MEM${product.ProductName}`,
                                        align: "center",
                                        width: "250px",
                                    })
                                    arrContainerCasual.push({
                                        title: product.ProductName,
                                        value: `CAS${product.ProductName}`,
                                        align: "center",
                                        width: "250px",
                                    });
                                }
                            });
                        });

                        return ({
                            member: arrContainerMember,
                            casual: arrContainerCasual
                        })
                    })

                    setLoading(false)
                } else {
                    setTableData([])
                    setHeaderChild({
                        member: [],
                        casual: []
                    })
                    notify("No Data Found", "warning");
                }
                setLoading(false)
            }).catch((e) => {
                setLoading(false)
                notify(e.message, "error");
            })
        } else {
            setLoading(false)
            notify("Max Range of Date is 7 Days", "warning");
        }

    }

    const handleGetExcel = ({
        outletCode,
        filter,
    }) => {
        setLoading(true)
        if (filter.datetimeTo.diff(filter.datetimeFrom, 'days') <= 7) {
            const data = {
                "datetimeFrom": `${moment(filter.datetimeFrom).format("YYYY-MM-DD HH:mm")}`,
                "datetimeTo": `${moment(filter.datetimeTo).format("YYYY-MM-DD HH:mm")}`,
                "outletCode": outletCode
            }

            getExcelExportSummary(data).then((res) => {
                if (res !== null) {
                    saveAs(new Blob([res], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }), `Summary Product Income_(${moment(filter.datetimeFrom).format("YYYY-MM-DD")} - ${moment(filter.datetimeFrom).format("YYYY-MM-DD")}).xlsx`);
                    notify("Data Downloaded", "success");
                } else {
                    notify("No Data Found", "warning");
                }
            }).finally(() => {
                setLoading(false)
            }).catch((e) => {
                setLoading(false)
                notify(e.message, "warning");
            })
        } else {
            setLoading(false)
            notify("Max Range of Date is 7 Days", "warning");
        }
    }

    // Merchant Option Logic
    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value.replace(/[\[\]]/g, ""))
        })
        setAllOutletCodeList(ouCodeArr);
        notify("Please Select Merchant", "warning");
    }

    const renderCell = (item, header, index) => {
        if (header.value === "Docdate") {
            return <span>{item.Docdate}</span>;
        } else if (header.value === "TotalAmount") {
            return <span>{formatCurrency(item.TotalAmount)}</span>
        } else if (item.DataProduct.find(({ ProductName }) => `CAS${ProductName}` === header.value)) {
            const findItem = item.DataProduct.find(({ ProductName }) => `CAS${ProductName}` === header.value)
            return <span>{findItem.TotalDataCasual}</span>
        } else if (item.DataProduct.find(({ ProductName }) => `MEM${ProductName}` === header.value)) {
            const findItem = item.DataProduct.find(({ ProductName }) => `MEM${ProductName}` === header.value)
            return <span>{findItem.TotalDataMember}</span>
        } else {
            return <span>0</span>
        }
    }

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                    </Box>

                    {/* Render Filter */}
                    <Stack gap={2} mt={"1.6rem"} direction={{ sm: "column" }} sx={{ background: '#FFFFFF', color: "#FEFEFE", borderRadius: "0.625rem" }} justifyContent={'space-between'} mb={2} >
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={{ xs: '100%', md: '50%' }}>
                                <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    data={merchantOption}
                                    selectedValue={filterForm.outletCode}
                                    setValue={(val) => {
                                        setFilterForm((prev) => {
                                            return ({
                                                ...prev,
                                                outletCode: val
                                            })
                                        })
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack direction={{ sm: "column", md: "row" }} width={'100%'} gap={2}>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                <DatePickerField
                                    label={"Start Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(Date.now())}
                                    value={filterForm.datetimeFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            datetimeTo: moment(newValue).hour(23).minute(59).second(59),
                                            datetimeFrom: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.datetimeFrom}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            datetimeFrom: newValue
                                        }))
                                    }}
                                />

                            </Stack>
                            <Stack width={'100%'} direction={'row'} gap={2}>
                                <DatePickerField
                                    label={"End Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    maxDate={moment(filterForm.datetimeFrom).add(7, 'days') > moment() ? moment() : moment(filterForm.datetimeFrom).add(7, 'days')}
                                    minDate={filterForm.datetimeFrom}
                                    value={filterForm.datetimeTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            datetimeTo: newValue
                                        }))
                                    }}
                                />
                                <TimePickerField
                                    label={""}
                                    placeholder="00:00"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"HH:mm"}
                                    value={filterForm.datetimeTo}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            datetimeTo: newValue
                                        }))
                                    }}
                                />
                            </Stack>
                        </Stack>
                        <Stack style={{ boxSizing: "border-box" }} sx={{
                            width: "100%",
                            display: "flex",
                            flexDirection: ["column", "row"],
                            alignItems: ["end", "center"],
                            gap: 3,
                            justifyContent: "space-between",
                        }} width={'100%'} gap={2}>
                            <FilterMessageNote
                                style={{ boxSizing: "border-box" }}
                                sx={{
                                    width: ["100%", "100%"]
                                }}
                                title={titleInfo}
                                subtitle={subTitleInfo}
                            />
                            <Stack width={{ xs: '100%', md: '100%' }} gap={2} direction={'row'} justifyContent={['center','end']}>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        if (filterForm.outletCode !== null) {
                                            handleGetExcel({
                                                filter: filterForm,
                                                outletCode: [filterForm.outletCode.value]
                                            })
                                        } else {
                                            notify("Please Select Merchant", "warning");
                                        }
                                    }}
                                    startIcon={<FileUploadIcon size="14px" />}
                                    name="Export"
                                    color={'success'}>
                                </CustomButton>
                                <CustomButton
                                    onClick={(e) => {
                                        e.preventDefault()
                                        if (filterForm.outletCode !== null) {
                                            handleGetData({
                                                filter: filterForm,
                                                offset: 0,
                                                outletCode: [filterForm.outletCode.value]
                                            })
                                        } else {
                                            notify("Please Select Merchant", "warning");
                                        }
                                    }}
                                    startIcon={<SearchIcon size="14px" />}
                                    name="Search">
                                </CustomButton>
                            </Stack>
                        </Stack>
                    </Stack>

                    {/* Render Table */}
                    <Box sx={{ width: "100%" }}>
                        <CustomTable
                            headers={header}
                            items={tableData}
                            renderCell={renderCell}
                            checkBox={false}
                            enableNumber={true}
                            keyName={"Docdate"}
                            groupHead={headerChild.member.length > 0 ? true : false}
                            group={[
                                {
                                    title: "",
                                    colSpan: 1,
                                    rightBorder: "1px",
                                    leftBorder: "1px",
                                },
                                {
                                    title: "MEMBER VEHICLE",
                                    colSpan: headerChild.member.length,
                                    rightBorder: "1px",
                                },
                                {
                                    title: "CASUAL VEHICLE",
                                    colSpan: headerChild.member.length,
                                    rightBorder: "1px",
                                },
                                {
                                    title: " ",
                                    colSpan: 2,
                                    rightBorder: "1px",
                                },
                            ]}
                        />
                    </Box>

                </CardContent>
            </Card>


        </Stack>
    )
}

export default LaporanSummaryPendapatanProduk