import React, { createRef, useEffect, useMemo, useRef, useState } from "react"
import { Box, Card, CardContent, FormControlLabel, Radio, RadioGroup, Stack, Typography } from "@mui/material"
import { getChartJenisKendaraan, getChartTrafficIntellegent, getChartTrafikKendaraan } from "../../../services/parkir/analytic";
import GrafikPieJenisKendaraan from "../../../widgets/grafik-pie-jenis-kendaraan";
import DatePickerField from "../../../components/datepicker-field";
import SelectField from "../../../components/select-field"
import moment from "moment";
import GrafikAreaTrafikKendaraan from "../../../widgets/grafik-area-trafik-kendaraan";
import CustomSwitch from "../../../components/custom-switch";
import GrafikColumnRevenueKendaraan from "../../../widgets/grafik-column-revenue-kendaraan";
import GrafikAreaRevenueKendaraan from "../../../widgets/grafik-area-revenue-kendaraan";

function arrayMax(arr) {
    return arr.reduce(function (p, v) {
        return (p.trxCount > v.trxCount ? p : v);
    });
}

const LaporanAnalitikParkir = ({
    labelJenisKendaraan = "Grafis Summary Jenis Kendaraan",
    labelTrafikKendaran = "Traffik Kendaraan",
    labelRevenue = "Revenue",
    merchantData = [
        {
            "id": 129,
            "tenantId": 10,
            "ouCode": "TRX3.PK.002-1",
            "ouName": "KAWASAN INDUSTRI PEDARINGAN",
            "ouParentId": 128,
            "ouTypeId": 11,
            "createUsername": "admops",
            "createAt": "20230204173551",
            "updateUsername": "admops",
            "updateAt": "20230204173551",
            "active": "Y",
            "activeAt": "20230204173551",
            "nonActiveAt": ""
        },
        {
            "id": 95,
            "tenantId": 10,
            "ouCode": "TRX67.PK.001-2",
            "ouName": "GUNUNG MAS BOGOR",
            "ouParentId": 93,
            "ouTypeId": 11,
            "createUsername": "admops",
            "createAt": "20230204162748",
            "updateUsername": "admops",
            "updateAt": "20230204162748",
            "active": "Y",
            "activeAt": "20230204162748",
            "nonActiveAt": ""
        },
    ],
    setLoading = () => { },
    notify = () => { },
}) => {
    const chartRef = createRef(null);
    const [localLoading, setLocalLoading] = useState(false);
    const [merchantOption, setMerchantOption] = useState([])
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [isServiceFee, setServiceFee] = useState(false);
    const [dataJenisKendaraan, setDataJenisKendaraan] = useState([])
    const [revenueType, setRevenueType] = useState("day");
    const [dailyOmsetTrafik, setDailyOmsetTrafik] = useState([])
    const [weeklyOmsetTrafik, setWeeklyOmsetTrafik] = useState([])
    const [monthlyOmsetTrafik, setMonthOmsetTrafik] = useState([])
    const [yearlyOmsetrafik, setYearlyOmsetTrafik] = useState([])
    const [dailyServiceTrafik, setDailyServiceTrafik] = useState([])
    const [weeklyServiceTrafik, setWeeklyServiceTrafik] = useState([])
    const [monthlyServiceTrafik, setMonthServiceTrafik] = useState([])
    const [yearlyServicerafik, setYearlyServiceTrafik] = useState([])
    const [dailyCategory, setDailyCategory] = useState([])
    const [weeklyCategory, setWeeklyCategory] = useState([])
    const [montlyCategory, setMontlyCategory] = useState([])
    const [yearlyCategory, setYearlyCategory] = useState([])
    const [categoryTrafik, setCategoryTrafik] = useState([])
    const [categoryPerDay, setCategoryPerDay] = useState([])
    const [dailyTrx, setDailyTrx] = useState([])
    const [dataTrafik, setDataTrafik] = useState([]);
    const [tanggalJenisKendaraan, setTanggalJenisKendaraan] = useState(moment(Date.now()))
    const [tanggalTrafficKendaraan, setTanggalTrafficKendaraan] = useState(moment(Date.now()))
    const [ouCodeSelected, setOuCodeSelected] = useState("");
    useEffect(() => {
        if (merchantData.length > 0 && merchantOption.length === 0) {
            let merchantArr = [];
            merchantData.map((item) => {
                merchantArr.push({
                    ...item,
                    label: item.ouName,
                    value: item.ouCode
                })
            })
            setMerchantOption(merchantArr);
        }
    }, [merchantData, merchantOption]);

    const handleGetChartTrafikKendaraan = ({
        ouCodeValue
    }) => {
        const chart = chartRef.current.chart;
        setLocalLoading(true)
        chart.showLoading();
        let data = {
            "docDate": moment(tanggalTrafficKendaraan).format("YYYY-MM-DD"),
            "outletCode": ouCodeValue,
            "ouType": ""
        }
        getChartTrafikKendaraan(data).then((res) => {
            if (res.result) {
                let categoryArr = [];
                let dataArr = []
                res.result.map((item) => {
                    categoryArr.push(item.checkinHours)
                    dataArr.push(item.count)
                })
                setCategoryTrafik(categoryArr)
                setDataTrafik(dataArr)
                chart.hideLoading()
                notify(res.message || "Success Get Data List", "success");
            } else {
                setCategoryTrafik([])
                setDataTrafik([])
                chart.hideLoading()
                notify("No Data Found", "warning");
            }
            setLocalLoading(false)
        }).catch((e) => {
            setCategoryTrafik([])
            setDataTrafik([])
            setLocalLoading(false)
            chart.hideLoading()
            notify(e.message, "error");
        })
    }

    const handleGetChartJenisKendaraan = ({
        ouCodeValue
    }) => {
        let startDate = tanggalJenisKendaraan.add('days', -1).format("YYYY-MM-DD HH:mm:ss");
        let endDate = tanggalJenisKendaraan.add('days', 1).format("YYYY-MM-DD HH:mm:ss")
        let data = {
            "startDate": startDate,
            "endDate": endDate,
            "outletCode": ouCodeValue,
            "ouType": ""
        }
        getChartJenisKendaraan(data).then((res) => {
            if (res.result) {
                let maxTrxCount = arrayMax(res.result)
                let dataArr = [];
                let total = 0
                res.result.map((item) => {
                    total += Number(item.trxCount)
                })
                let productArr = groupByName(res.result);
                if (productArr) {
                    if (Object.keys(productArr).length > 0) {
                        Object.keys(productArr).map((item) => {
                            let totalPerItem = productArr[item].reduce((sum, item) => {
                                return sum + item.trxCount;
                            }, 0);
                            let percentage = (Number(totalPerItem) / total) * 100
                            dataArr.push({
                                name: `${item}`,
                                y: Number(percentage.toFixed(1)),
                                // sliced: maxTrxCount.productCode === item.productCode,
                                // selected: maxTrxCount.productCode === item.productCode,
                                origin: totalPerItem
                            })
                        })
                    }
                }
                setDataJenisKendaraan(dataArr);
                notify(res.message || "Success Get Data List", "success");
            } else {
                setDataJenisKendaraan([])
                notify("No Data Found", "warning");
            }
            setLoading(false)
        }).catch((e) => {
            setDataJenisKendaraan([])
            setLoading(false)
            notify(e.message, "error");
        })
    }

    function groupByName(data) {
        let groupedData = {};
        data.forEach((item) => {
            const { productName } = item;
            if (!groupedData[productName]) {
                groupedData[productName] = [];
            }
            groupedData[productName].push({
                ...item
            });
        });
        return groupedData;
    }

    const handleGetChartRevenueKendaraan = ({
        ouCodeValue
    }) => {
        let yearMonth = tanggalTrafficKendaraan.format("YYYY-MM")
        let year = tanggalTrafficKendaraan.format("YYYY")
        let yearList = ""
        for (let i = 3; i > 0; i--) {
            yearList += Number(year) - i + ","
        }
        let data = {
            "yearMonth": yearMonth,
            "year": year,
            "yearList": yearList.replace(/,\s*$/, ""),
            "outletCode": ouCodeValue,
            "ouType": "",
        }
        getChartTrafficIntellegent(data).then((res) => {
            if (res.result) {
                let dailyCategoryArr = [];
                let dailyOmsetArr = [];
                let dailyServiceArr = [];
                let weeklyCategoryArr = [];
                let weeklyOmsetArr = [];
                let weeklyServiceArr = [];
                let montlyCategoryArr = [];
                let montlyOmsetArr = [];
                let montlyServiceArr = [];
                let yearlyCategoryArr = [];
                let yearlyOmsetArr = [];
                let yearlyServiceArr = [];
                let categoryPerDay = [];
                let dailyTrxArr = [];
                res.result.dailyTrxList.map((item) => {
                    dailyCategoryArr.push(moment(item.docDate).format("DD MMM YYYY"))
                    dailyOmsetArr.push(item.totalAmount)
                    dailyServiceArr.push(item.totalServiceFee)
                    categoryPerDay.push(moment(item.docDate).format("DD"))
                    dailyTrxArr.push(item.totalTrx)
                })
                res.result.weeklyTrxList.map((item) => {
                    weeklyCategoryArr.push(item.weekly)
                    weeklyOmsetArr.push(item.totalAmount)
                    weeklyServiceArr.push(item.totalServiceFee)
                })
                res.result.monthlyTrxList.map((item) => {
                    montlyCategoryArr.push(item.yearMonth)
                    montlyOmsetArr.push(item.totalAmount)
                    montlyServiceArr.push(item.totalServiceFee)
                })

                res.result.yearlyTrxList.map((item) => {
                    yearlyCategoryArr.push(item.yearMonth)
                    yearlyOmsetArr.push(item.totalAmount)
                    yearlyServiceArr.push(item.totalServiceFee)
                })
                setDailyCategory(dailyCategoryArr)
                setDailyOmsetTrafik(dailyOmsetArr)
                setDailyServiceTrafik(dailyServiceArr)
                setCategoryPerDay(categoryPerDay)
                setDailyTrx(dailyTrxArr)

                setWeeklyCategory(weeklyCategoryArr)
                setWeeklyOmsetTrafik(weeklyOmsetArr)
                setWeeklyServiceTrafik(weeklyServiceArr)

                setMontlyCategory(montlyCategoryArr)
                setMonthOmsetTrafik(montlyOmsetArr)
                setMonthServiceTrafik(montlyServiceArr)

                setYearlyCategory(yearlyCategoryArr)
                setYearlyOmsetTrafik(yearlyOmsetArr)
                setYearlyServiceTrafik(yearlyServiceArr)
                notify(res.message || "Success Get Data List", "success");
            } else {
                // setDataJenisKendaraan([])
                notify("No Data Found", "warning");
            }
            setLoading(false)
        }).catch((e) => {
            // setDataJenisKendaraan([])
            setLoading(false)
            notify(e.message, "error");
        })
    }

    useEffect(() => {
        if (merchantOption.length > 0) {
            let ouCodeArr = []
            merchantOption.map((item) => {
                ouCodeArr.push(item.value)
            })
            setAllOutletCodeList(ouCodeArr);
            handleGetChartJenisKendaraan({ ouCodeValue: ouCodeArr })
            // handleGetChartTrafikKendaraan({ ouCodeValue: ouCodeArr })
            handleGetChartRevenueKendaraan({ ouCodeValue: ouCodeArr })
        }
    }, [merchantOption]);

    useEffect(() => {
        if (allOutletCodeList.length > 0) {
            handleGetChartJenisKendaraan({ ouCodeValue: allOutletCodeList })
        }
    }, [tanggalJenisKendaraan, allOutletCodeList]);

    useEffect(() => {
        if (ouCodeSelected) {
            handleGetChartTrafikKendaraan({ ouCodeValue: ouCodeSelected ? [ouCodeSelected.value] : [] })
        }
    }, [tanggalTrafficKendaraan, ouCodeSelected]);

    useEffect(() => {
        if (!ouCodeSelected) {
            setDataTrafik([])
        }
    }, [ouCodeSelected]);

    const revenueCategory = useMemo(() => {
        let category = []
        if (dailyCategory.length !== 0 && weeklyCategory.length !== 0 && montlyCategory.length !== 0 && yearlyCategory.length !== 0 &&
            dailyOmsetTrafik.length !== 0 && weeklyOmsetTrafik.length !== 0 && monthlyOmsetTrafik.length !== 0 && yearlyOmsetrafik.length !== 0 &&
            dailyServiceTrafik.length !== 0 && weeklyServiceTrafik.length !== 0 && monthlyServiceTrafik.length !== 0 && yearlyServicerafik.length !== 0) {
            if (revenueType === "day") {
                category = dailyCategory;
            } else if (revenueType === "week") {
                category = weeklyCategory;
            } else if (revenueType === "month") {
                category = montlyCategory;
            } else if (revenueType === "year") {
                category = yearlyCategory
            }
        }
        return category;
    }, [isServiceFee, revenueType, dailyCategory, weeklyCategory, montlyCategory, yearlyCategory,
        dailyOmsetTrafik, weeklyOmsetTrafik, monthlyOmsetTrafik, yearlyOmsetrafik, dailyServiceTrafik, weeklyServiceTrafik,
        monthlyServiceTrafik, yearlyServicerafik
    ]);

    const dataRevenue = useMemo(() => {
        let revenue = []
        if (dailyCategory.length !== 0 && weeklyCategory.length !== 0 && montlyCategory.length !== 0 && yearlyCategory.length !== 0 &&
            dailyOmsetTrafik.length !== 0 && weeklyOmsetTrafik.length !== 0 && monthlyOmsetTrafik.length !== 0 && yearlyOmsetrafik.length !== 0 &&
            dailyServiceTrafik.length !== 0 && weeklyServiceTrafik.length !== 0 && monthlyServiceTrafik.length !== 0 && yearlyServicerafik.length !== 0) {
            if (!isServiceFee) {
                if (revenueType === "day") {
                    revenue = dailyOmsetTrafik;
                } else if (revenueType === "week") {
                    revenue = weeklyOmsetTrafik;
                } else if (revenueType === "month") {
                    revenue = monthlyOmsetTrafik;
                } else if (revenueType === "year") {
                    revenue = yearlyOmsetrafik
                }
            } else {
                if (revenueType === "day") {
                    revenue = dailyServiceTrafik;
                } else if (revenueType === "week") {
                    revenue = weeklyServiceTrafik;
                } else if (revenueType === "month") {
                    revenue = monthlyServiceTrafik;
                } else if (revenueType === "year") {
                    revenue = yearlyServicerafik
                }
            }
        }
        return revenue;
    }, [isServiceFee, revenueType, dailyCategory, weeklyCategory, montlyCategory, yearlyCategory,
        dailyOmsetTrafik, weeklyOmsetTrafik, monthlyOmsetTrafik, yearlyOmsetrafik, dailyServiceTrafik, weeklyServiceTrafik,
        monthlyServiceTrafik, yearlyServicerafik
    ]);
    useEffect(() => {
        if (allOutletCodeList.length > 0) {
            handleGetChartRevenueKendaraan({ ouCodeValue: allOutletCodeList })
        }
    }, [isServiceFee, allOutletCodeList]);
    return (
        <Stack direction={"column"} p={"2rem"} gap={2}>
            <Stack direction={["column", "row"]} my={"1.25rem"} gap={4} justifyContent={"space-between"}>
                <Stack sx={{
                    width: ["100%", "55%"],
                    display: "flex"
                }}>
                    <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                        <CardContent sx={{ p: "2rem" }}>
                            <Box display="flex" flexDirection="column">
                                <Stack direction={["column", "row"]} justifyContent={"space-between"} gap={2}>
                                    <Typography variant="h4" fontSize={"1.25rem"} fontWeight="600">
                                        {labelRevenue}
                                    </Typography>
                                    <Box width={["100%", "40%"]} sx={{ display: "flex", justifyContent: "flex-end", alignItems: "center", gap: 1.5 }}>
                                        <Typography fontWeight={"bold"}>Revenue</Typography>
                                        <CustomSwitch
                                            label={"Service Fee"}
                                            checked={isServiceFee}
                                            onChange={(e) => { setServiceFee(e.target.checked) }}
                                        />
                                    </Box>
                                </Stack>
                                <Stack direction={"row"} my={1}>
                                    <RadioGroup
                                        row
                                        aria-labelledby="demo-row-radio-buttons-group-label"
                                        name="row-radio-buttons-group"
                                        value={revenueType}
                                        onChange={(e) => setRevenueType(e.target.value)}
                                    >
                                        <FormControlLabel value="day" control={<Radio />} label="Daily" />
                                        <FormControlLabel value="week" control={<Radio />} label="Weekly" />
                                        <FormControlLabel value="month" control={<Radio />} label="Monthly" />
                                        <FormControlLabel
                                            value="year"
                                            control={<Radio />}
                                            label="Yearly"
                                        />
                                    </RadioGroup>
                                </Stack>
                                <Stack direction={["column"]} my={"1.25rem"} gap={4}>
                                    <GrafikColumnRevenueKendaraan
                                        category={revenueCategory}
                                        data={dataRevenue}
                                        yAxis={isServiceFee ? "Service Fee (Rp)" : "Revenue (Rp)"}
                                        xAxis={revenueType === "day" ? "Daily" :
                                            revenueType === "week" ? "Weekly" :
                                                revenueType === "month" ? "Monthly" : "Yearly"}
                                    />
                                </Stack>
                            </Box>
                        </CardContent>
                    </Card>
                </Stack>
                <Stack sx={{
                    width: ["100%", "40%", "42%"],
                    display: "flex",
                    direction: "column",
                    gap: 2
                }}>
                    <Card sx={{ minWidth: 275, borderRadius: "0.75rem", height: "350px" }}>
                        <CardContent sx={{ p: "2rem" }}>
                            <Box display="flex" flexDirection="column">
                                <Stack direction={["column", "row"]} justifyContent={"space-between"} gap={2}>
                                    <Typography variant="h4" fontSize={"1.25rem"} fontWeight="600">
                                        {labelRevenue}
                                    </Typography>
                                </Stack>
                                <Typography variant="h4" fontSize={"0.8rem"} fontWeight="600" color={"gray"}>
                                    {`Revenue ${dailyCategory[0]} - ${dailyCategory[dailyCategory.length - 1]}`}
                                </Typography>
                                <Stack direction={["column"]} my={"1.25rem"} gap={4}>
                                    <GrafikAreaRevenueKendaraan
                                        category={categoryPerDay}
                                        data={dailyOmsetTrafik}
                                        yAxis="Revenue (Rp)"
                                        type="revenue"
                                    />
                                </Stack>
                            </Box>
                        </CardContent>
                    </Card>
                    <Card sx={{ minWidth: 275, borderRadius: "0.75rem", height: "350px" }}>
                        <CardContent sx={{ p: "2rem" }}>
                            <Typography variant="h4" fontSize={"0.8rem"} fontWeight="600" color={"gray"}>
                                {`Total Transaction ${dailyCategory[0]} - ${dailyCategory[dailyCategory.length - 1]}`}
                            </Typography>
                            <Box display="flex" flexDirection="column">
                                <Stack direction={["column"]} my={"1.25rem"} gap={4}>
                                    <GrafikAreaRevenueKendaraan
                                        category={categoryPerDay}
                                        data={dailyTrx}
                                        yAxis="Total Transaction (Trx)"
                                        type="total"
                                    />
                                </Stack>
                            </Box>
                        </CardContent>
                    </Card>
                </Stack>
            </Stack>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Stack direction={["column", "row"]} justifyContent={"space-between"} gap={2}>
                            <Typography variant="h4" fontSize={"1.25rem"} fontWeight="600">
                                {labelTrafikKendaran}
                            </Typography>
                            <Box sx={{ display: "flex", flexDirection: ["column", "row"] }} width={["100%", "50%"]} gap={[0.1, 2]}>
                                <SelectField
                                    label={""}
                                    placeholder="All Merchant"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={merchantOption}
                                    selectedValue={ouCodeSelected}
                                    setValue={(val) => {
                                        setOuCodeSelected(val)
                                    }}
                                />
                                <DatePickerField
                                    label={<Box></Box>}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    value={tanggalTrafficKendaraan}
                                    onChange={(newValue) => {
                                        setTanggalTrafficKendaraan(newValue)
                                    }}
                                />
                            </Box>
                        </Stack>
                        <Stack direction={"column"} my={"1.25rem"} gap={4} position={"relative"}>
                            {
                                (dataTrafik.length === 0 && !localLoading) ? <Box sx={{ position: "absolute", top: "30%", zIndex: 1000, marginLeft: "auto", marginRight: "auto", width: "100%" }}>
                                    <Typography color={"black"} textAlign={"center"}>No Data Available</Typography>
                                </Box> : null
                            }
                            <GrafikAreaTrafikKendaraan refereal={chartRef} category={categoryTrafik} data={dataTrafik} />
                            <Typography>{ }</Typography>
                        </Stack>
                    </Box>
                </CardContent>
            </Card>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Stack direction={["column", "row"]} justifyContent={"space-between"} gap={2}>
                            <Typography variant="h4" fontSize={"1.25rem"} fontWeight="600">
                                {labelJenisKendaraan}
                            </Typography>
                            <Box width={["100%", "25%"]}>
                                <DatePickerField
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    value={tanggalJenisKendaraan}
                                    onChange={(newValue) => {
                                        setTanggalJenisKendaraan(newValue)
                                    }}
                                />
                            </Box>
                        </Stack>
                        <Stack direction={"column"} my={"1.25rem"} gap={4}>
                            <GrafikPieJenisKendaraan data={dataJenisKendaraan} />
                            <Typography>{ }</Typography>
                        </Stack>
                    </Box>
                </CardContent>
            </Card>
        </Stack>
    )
}

export default LaporanAnalitikParkir;