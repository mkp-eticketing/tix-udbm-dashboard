import React from "react";
import CardAmountSummary from "../../../../components/card-amount-summary";
import { Box } from "@mui/material";

const CardSummaryBoardingTicket = ({
    summaryDetail = {},
    isLoading = true,
    sidebarExpanded = false,
    cashlessSrc = "",
    cashSrc = ""
}) => {
    return (
        <Box
            sx={{
                display: "grid",
                gridTemplateColumns: "repeat(12, minmax(0, 1fr))",
                gap: "1.6rem"
            }}
        >
            <CardAmountSummary
                isLoading={isLoading}
                title="CASHLESS:"
                amount={summaryDetail.cashlessTotal}
                sidebarExpanded={sidebarExpanded}
                rotate={90}
                src={cashlessSrc}
            />
            <CardAmountSummary
                isLoading={isLoading}
                title="CASH:"
                amount={summaryDetail.cashTotal}
                isCurrency={true}
                sidebarExpanded={sidebarExpanded}
                src={cashSrc}
            />
        </Box>
    );
};

export default CardSummaryBoardingTicket;
