import React, { useEffect, useState } from "react"
import { Box, Card, CardContent, Stack, Typography } from "@mui/material"
import SelectField from "../../../components/select-field"
import SearchIcon from '@mui/icons-material/Search';
import CustomTable from "../../../components/custom-table";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import InputField from "../../../components/text-field";
import DatePickerField from "../../../components/datepicker-field";
import { getSummaryTransaction } from "../../../services/parkir/transaction";
import { thousandSeparator } from "../../../utils/thousand-separator";
import TimePickerField from "../../../components/timepicker-field";
import moment from "moment";
import DetailDialog from "./components/detail-dialog";
import CardSummaryBoardingTicket from "./components/card-summary-boarding-ticket";

const LaporanTransaksi = ({
    label = "Laporan Transaksi Parkir",
    titleInfo = "To Display Specific Transactions, Use the Filters Above.",
    subTitleInfo = [],
    merchantData = [],
    setLoading = () => { },
    notify = () => { },
    buttomFilter = "Search",
    sidebarExpanded = false,
    cashlessSrc = "",
    cashSrc = ""
}) => {
    const [merchantOption, setMerchantOption] = useState([])
    const [ouCodeSelected, setOuCodeSelected] = useState([]);
    const [countLoading, setCountLoading] = useState(false);
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [open, setOpen] = useState(false);
    const [detailTransaction, setDetailTransaction] = useState(null);
    const [totalSummary, setTotalSummary] = useState({
        cashlessTotal: 0,
        cashTotal: 0
    })
    const [loadingDetail, setLoadingDetail] = useState(false);
    const [filterForm, setFilterForm] = useState({
        ouCode: "",
        username: "",
        startDate: moment(Date.now()),
        startTime: moment(moment(Date.now()).format("YYYY-MM-DD") + " 00:00"),
        endDate: moment(Date.now()).add('days', 1),
        endTime: moment(moment(Date.now()).add('days', 1).format("YYYY-MM-DD") + " 23:59")
    })

    const [data, setData] = useState([]);
    const header = [
        {
            title: "PRODUCT",
            value: "productName",
            align: "left",
            width: "250px",
        },
        {
            title: "CASH",
            value: "totalAmountCash",
            align: "left",
            width: "200px",
        },
        {
            title: "CASHLESS",
            value: "totalAmountNonCash",
            align: "left",
            width: "200px",
        },
        {
            title: "GRAND TOTAL",
            value: "totalAmount",
            align: "left",
            width: "200px",
        },
        {
            title: "MEMBER",
            value: "totalMember",
            align: "left",
            width: "200px",
        },
        {
            title: "MANUAL",
            value: "totalManual",
            align: "left",
            width: "200px",
        },
        {
            title: "FREE NON MEMBER",
            value: "totalFreeNonMember",
            align: "left",
            width: "200px",
        },
        {
            title: "CASH",
            value: "totalTrx",
            align: "left",
            width: "200px",
        },
        {
            title: "CASHLESS",
            value: "totalTrxNonCash",
            align: "left",
            width: "200px",
        },
    ]

    const renderCell = (item, header, index) => {
        if (header.value === "productName") {
            return <span>{item.productName}</span>;
        } else if (header.value === "totalAmountCash") {
            return <span>Rp{thousandSeparator(item.totalAmountCash)}</span>;
        } else if (header.value === "totalAmountNonCash") {
            return <span>Rp{thousandSeparator(item.totalAmountNonCash)}</span>;
        } else if (header.value === "totalAmount") {
            return <span>Rp{thousandSeparator(item.totalAmount)}</span>;
        } else if (header.value === "totalMember") {
            return <span>{item.totalMember}</span>;
        } else if (header.value === "totalManual") {
            return <span>{item.totalManual}</span>;
        } else if (header.value === "totalFreeNonMember") {
            return <span>{item.totalFreeNonMember}</span>;
        } else if (header.value === "totalTrx") {
            return <span>{item.totalTrx}</span>;
        } else if (header.value === "totalTrxNonCash") {
            return <span>{item.totalTrxNonCash}</span>;
        }

        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    };

    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    const handleGetListTransaction = ({
        filter,
        ouCodeValue,
    }) => {
        let countResult = 0;
        if (filter.startDate && !filter.endDate) {
            return notify("End date must be filled in correctly!", "error")
        }
        if (!filter.startDate && filter.endDate) {
            return notify("Start date must be filled in correctly!", "error")
        }
        let data = {
            "datetimeFrom": filter.startDate ? filter.startDate.format("YYYY-MM-DD") + " " + filter.startTime.format("HH:mm") : "",
            "datetimeTo": filter.endDate ? filter.endDate.format("YYYY-MM-DD") + " " + filter.endTime.format("HH:mm") : "",
            "mainOuList": "6",
            "outletCode": ouCodeValue,
            "username": filter.username
        }
        setLoading(true);
        setCountLoading(true)
        setOuCodeSelected(ouCodeValue);
        setLoadingDetail(true);
        getSummaryTransaction(data).then((res) => {
            if (res.result) {
                let cashless = 0;
                let cash = 0
                setData(res.result)
                res.result.map((item) => {
                    cash = cash + item.totalAmountCash;
                    cashless = cashless + item.totalAmountNonCash;
                })
                setTotalSummary({
                    cashlessTotal: cashless,
                    cashTotal: cash
                })
                notify(res.message || "Success Get Data List", "success");
            } else {
                setData([]);
                notify("No Data Found", "warning");
            }
            setLoading(false)
            setLoadingDetail(false);
        }).catch((e) => {
            setData([]);
            setLoading(false)
            setLoadingDetail(false);
            notify(e.message, "error");
        })
    }

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetListTransaction({ filter: filterForm, ouCodeValue: ouCodeArr })
    }

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                        <Stack display="flex" direction="column" mt={"1.6rem"} gap={2}>
                            <Box display="flex" flexDirection="column">
                                <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                                    <Box sx={{
                                        display: "grid",
                                        gridTemplateColumns: ["repeat(1, 1fr)", "repeat(3, 1fr)"],
                                        gap: 2
                                    }}>
                                        <SelectField
                                            label={"Merchant"}
                                            placeholder="All Merchant"
                                            sx={{ width: "100%", fontSize: "16px" }}
                                            data={merchantOption}
                                            selectedValue={filterForm.ouCode}
                                            setValue={(val) => {
                                                setFilterForm((prev) => ({
                                                    ...prev,
                                                    ouCode: val
                                                }))
                                            }}
                                        />
                                        <InputField
                                            label={"User"}
                                            placeholder="Username"
                                            onChange={(e) => {
                                                setFilterForm((prev) => ({
                                                    ...prev,
                                                    username: e.target.value
                                                }))
                                            }}
                                            value={filterForm.username}
                                        />
                                        <Stack display="flex" direction="row" alignItems="center" gap={1}>
                                            <Box sx={{
                                                width: "60%"
                                            }}>
                                                <DatePickerField
                                                    label={"Start Date"}
                                                    placeholder="DD MMM YYYY"
                                                    sx={{ width: "100%", fontSize: "16px" }}
                                                    format={"DD MMM YYYY"}
                                                    value={filterForm.startDate}
                                                    onChange={(newValue) => {
                                                        setFilterForm((prev) => ({
                                                            ...prev,
                                                            startDate: newValue
                                                        }))
                                                    }}
                                                />
                                            </Box>
                                            <Box sx={{
                                                width: "40%"
                                            }}>
                                                <TimePickerField
                                                    label={""}
                                                    placeholder="00:00"
                                                    sx={{ width: "100%", fontSize: "16px" }}
                                                    format={"HH:mm"}
                                                    value={filterForm.startTime}
                                                    onChange={(newValue) => {
                                                        setFilterForm((prev) => ({
                                                            ...prev,
                                                            startTime: newValue
                                                        }))
                                                    }}
                                                />
                                            </Box>
                                        </Stack>
                                        <Stack display="flex" direction="row" alignItems="center" gap={1}>
                                            <Box sx={{
                                                width: "60%"
                                            }}>
                                                <DatePickerField
                                                    label={"End Date"}
                                                    placeholder="DD MMM YYYY"
                                                    sx={{ width: "100%", fontSize: "16px" }}
                                                    format={"DD MMM YYYY"}
                                                    value={filterForm.endDate}
                                                    onChange={(newValue) => {
                                                        setFilterForm((prev) => ({
                                                            ...prev,
                                                            endDate: newValue
                                                        }))
                                                    }}
                                                />
                                            </Box>
                                            <Box sx={{
                                                width: "40%"
                                            }}>
                                                <TimePickerField
                                                    label={""}
                                                    placeholder="00:00"
                                                    sx={{ width: "100%", fontSize: "16px" }}
                                                    format={"HH:mm"}
                                                    value={filterForm.endTime}
                                                    onChange={(newValue) => {
                                                        setFilterForm((prev) => ({
                                                            ...prev,
                                                            endTime: newValue
                                                        }))
                                                    }}
                                                />
                                            </Box>
                                        </Stack>
                                    </Box>
                                </Stack>
                                <Stack sx={{
                                    width: "100%",
                                    display: "flex",
                                    flexDirection: ["column", "row"],
                                    alignItems: ["end", "center"],
                                    gap: 3,
                                    justifyContent: "space-between",
                                    mt: "2rem"
                                }}>
                                    <FilterMessageNote
                                        sx={{
                                            width: ["100%", "50%"]
                                        }}
                                        title={titleInfo}
                                        subtitle={subTitleInfo}
                                    />
                                    <div style={{
                                        display: "flex",
                                        gap: 3
                                    }}>
                                        <CustomButton
                                            onClick={() => {
                                                handleGetListTransaction({ filter: filterForm, ouCodeValue: filterForm.ouCode ? [filterForm.ouCode.value] : allOutletCodeList })
                                            }}
                                            startIcon={<SearchIcon size="14px" />}
                                            name={buttomFilter}
                                        >
                                            Filter
                                        </CustomButton>
                                    </div>
                                </Stack>
                                <Stack mt={6} gap={"1rem"} mb={"1.6rem"}>
                                    <Typography fontSize={"1.25rem"} color="rgb(30, 41, 59)" fontWeight={"bold"}>Transaction Summary</Typography>
                                    <CardSummaryBoardingTicket
                                        summaryDetail={totalSummary}
                                        isLoading={loadingDetail}
                                        sidebarExpanded={sidebarExpanded}
                                        cashlessSrc={cashlessSrc}
                                        cashSrc={cashSrc}
                                    />
                                </Stack>
                                <Box sx={{ width: "100%" }}>
                                    <Typography fontSize={"1.25rem"} color="rgb(30, 41, 59)" fontWeight={"bold"}>Transaction Detail</Typography>
                                    <CustomTable
                                        headers={header}
                                        items={data}
                                        renderCell={renderCell}
                                        enableNumber={true}
                                        groupHead={true}
                                        group={[
                                            {
                                                title: "",
                                                colSpan: 1,
                                                rightBorder: "1px"
                                            },
                                            {
                                                title: "TRANSACTION AMOUNT",
                                                colSpan: 3,
                                                rightBorder: "1px"
                                            },
                                            {
                                                title: "TOTAL TRANSACTION",
                                                colSpan: 5,
                                                rightBorder: "1px"
                                            }
                                        ]}
                                    />
                                </Box>
                            </Box>
                        </Stack>
                    </Box>
                </CardContent>
            </Card>
            {
                detailTransaction && <DetailDialog
                    setOpen={setOpen}
                    open={open}
                    detail={detailTransaction}
                />
            }
        </Stack>
    )
}

export default LaporanTransaksi