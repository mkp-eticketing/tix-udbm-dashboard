import React, { useState } from "react";
import ApprovalPage from "./approval-page";
import ApprovalDetail from "./approval-detail";

const Approval = () => {
  const [removeCode, setRemoveCode] = useState("");

  if (removeCode) {
    return <ApprovalDetail removeCode={removeCode} setRemoveCode={setRemoveCode}/>;
  }

  return (
    <ApprovalPage
      merchantData={[
        { ouName: "TEST", ouCode: "184.PS.001" },
        { ouName: "TEST2", ouCode: "184.PS.002" },
        { ouName: "TEST3", ouCode: "184.PS.003" },
        { ouName: "TEST4", ouCode: "188.PS.006" },
      ]}

      setRemoveCode={setRemoveCode}
    />
  );
};
export default Approval;
