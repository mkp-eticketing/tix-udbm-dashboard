import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography, Grid } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import SelectField from "../../../components/select-field";
import SearchIcon from "@mui/icons-material/Search";
import moment from "moment";
import {
  getApprovalList,
  getApprovalMetadata,
  editApproval,
} from "../../../services/pasar/approval";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import InputField from "../../../components/text-field";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import DatePickerField from "../../../components/datepicker-field";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import HourglassFullIcon from "@mui/icons-material/HourglassFull";
import Avatar from "@mui/material/Avatar";

const ApprovalPage = ({
  setRemoveCode,
  username,
  label = "Approval Page",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  // dropdown options
  const [merchantOption, setMerchantOption] = useState([]); //dropdown for merchant filter
  const approvalOption = [
    {
      label: "PENDING",
      value: "PENDING",
    },
    {
      label: "APPROVE",
      value: "APPROVE",
    },
    {
      label: "DECLINE",
      value: "DECLINE",
    },
  ]; //dropdown for approval filter

  //filter state
  const [ouCode, setOuCode] = useState(""); //controlled form
  const [date, setDate] = useState(null); //controlled form
  const [approval, setApproval] = useState(""); //controlled form

  // table state
  const [data, setData] = useState([]); //table content
  const [search, setSearch] = useState(""); //search state

  // modal state
  const [open, setOpen] = useState(false); //modal state
  const [detail, setDetail] = useState({
    approvalCode: "",
    merchant: "",
    reqDate: "",
    createdBy: "",
    description: "",
    status: "",
  });
  const [finalApproval, setFinalApproval] = useState(null);
  const [selectedApprovalCode, setSelectedApprovalCode] = useState("");

  // pagination state
  const [pagination, setPagination] = useState({
    limit: 25,
    offset: 0,
    count: -99,
    countLoading: false,
    disableNext: false,
  });

  let searchMetaData = 0;
  const [originalMetadata, setOriginalMetadata] = useState(0);

  // table header
  const header = [
    {
      title: "Approval",
      value: "approval",
      align: "center",
      width: "100px",
    },
    {
      title: "Approval Remove Code",
      value: "approvalRemoveCode",
      align: "left",
      width: "200px",
    },
    {
      title: "Removed Date",
      value: "removedDate",
      align: "left",
      width: "200px",
    },
    {
      title: "Corporate Name",
      value: "corporateName",
      align: "left",
      width: "250px",
    },
    {
      title: "Description",
      value: "description",
      align: "left",
      width: "200px",
    },
    {
      title: "Status",
      value: "approvalStatus",
      align: "left",
      width: "200px",
    },
  ];

  // style the data for each column
  const renderCell = (item, header) => {
    switch (header.value) {
      case "approval":
        return (
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Box
              onClick={() => {
                setOpen(true);
                setDetail({
                  approvalCode: item.approvalRemoveCode,
                  merchant: item.corporateName,
                  reqDate: moment(item.removedDate).format("Do MMMM YYYY"),
                  createdBy: item.createdBy,
                  description: item.description,
                  status: item.approvalStatus,
                });
                setSelectedApprovalCode(item.approvalRemoveCode);
                setFinalApproval(item.approvalStatus);
              }}
              sx={{
                cursor: "pointer",
                width: "fit-content",
              }}
            >
              <EditIcon sx={{ color: "primary.main" }} />
            </Box>
          </Box>
        );
      case "approvalRemoveCode":
        return (
          <Box
            onClick={() => {
              setRemoveCode(item.approvalRemoveCode);
            }}
            sx={{
              cursor: "pointer",
              color: "primary.main",
              textDecoration: "underline",
            }}
          >
            {item.approvalRemoveCode}
          </Box>
        );
      case "removedDate":
        return <span>{moment(item.removedDate).format("Do MMMM YYYY")}</span>;
      case "corporateName":
        return <span>{item.corporateName}</span>;
      case "description":
        return <span>{item.description}</span>;
      case "approvalStatus":
        return (
          <span className="flex gap-2 items-center">
            <Avatar
              sx={{
                width: 28,
                height: 28,
                backgroundColor:
                  item.approvalStatus === "APPROVE"
                    ? "success.main"
                    : item.approvalStatus === "PENDING"
                    ? "warning.light"
                    : "error.main",
                color: "white",
              }}
            >
              {item.approvalStatus === "APPROVE" ? (
                <CheckCircleIcon fontSize="small" />
              ) : item.approvalStatus === "PENDING" ? (
                <HourglassFullIcon fontSize="small" />
              ) : (
                <CancelIcon fontSize="small" />
              )}
            </Avatar>
            {item.approvalStatus}
          </span>
        );
      default:
        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }
  };

  // fetch table data
  const handleGetApprovalList = ({ date, status, ouCodeValue }) => {
    let countResult = 0;
    const data = {
      periode: date,
      approvalStatus: status,
      outletCode: [ouCodeValue],
      limit: 0,
      offset: 0,
    };
    setLoading(true);
    setPagination((prev) => ({ ...prev, countLoading: true }));

    getApprovalMetadata({
      outletCode: [ouCodeValue],
      periode: date,
      approvalStatus: status,
    })
      .then((res) => {
        countResult = res.result;
        setPagination((prev) => ({
          ...prev,
          count: countResult,
          disableNext: false,
        }));
        setOriginalMetadata(countResult);
      })
      .catch((e) => {
        setPagination((prev) => ({ ...prev, count: -99 }));
      })
      .finally(() => {
        setPagination((prev) => ({ ...prev, countLoading: false }));
      });

    getApprovalList(data)
      .then((res) => {
        if (res.result) {
          setData(res.result);
          notify(res.message || "Success Get Data List", "success");
        } else {
          setPagination((prev) => ({ ...prev, disableNext: true }));
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setPagination((prev) => ({ ...prev, disableNext: true }));
        setLoading(false);
        notify(e.message, "error");
      });
  };

  // edit approval remove
  const handleEditRemoveApproval = ({ approvalRemoveCode, approvalStatus }) => {
    const data = {
      userUsername: username,
      approvalRemoveCode: approvalRemoveCode,
      approvalStatus: approvalStatus,
    };

    editApproval(data)
      .then((res) => {
        console.log(res);
        handleGetApprovalList({
          date: date ? date.format("YYYYMM") : "",
          status: approval ? approval.value : "",
          ouCodeValue: ouCode ? ouCode.value : [],
        });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const pageChange = async (value) => {
    var pageOffset = value * pagination.limit;
    setPagination((prev) => ({ ...prev, offset: pageOffset }));
  };

  const rowsChange = async (e) => {
    setPagination((prev) => ({ ...prev, limit: e.props.value, offset: 0 }));
  };

  // handle dividing the data
  const handlePagination = (data) => {
    const endIndex = Math.min(
      pagination.offset + pagination.limit,
      data.length
    );
    const paginatedData = data.slice(pagination.offset, endIndex);

    return paginatedData;
  };

  // handle searching the data based on account name
  const handleSearch = () => {
    const filteredData = data.filter((item) =>
      item.corporateName.toLowerCase().includes(search.trim().toLowerCase())
    );
    searchMetaData = filteredData.length;
    return handlePagination(filteredData);
  };

  // moving data from merchantData to merchantOption and setting the dropdown
  useEffect(() => {
    let merchantArr = []; //placeholder for erchantOption value
    merchantData.forEach((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);

  useEffect(() => {
    setPagination((prev) => ({
      ...prev,
      count: search === "" ? originalMetadata : searchMetaData,
    }));
  }, [search]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            {/* Title */}
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>

            {/* filter and filter button */}
            <Stack
              display="flex"
              direction="column"
              mt={"2rem"}
              mb={"0rem"}
              gap={2}
            >
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: [
                    "repeat(1, 1fr)",
                    "repeat(1, 1fr)",
                    "1fr 1fr 1fr auto",
                  ],
                  gap: 2,
                }}
              >
                {/* input */}
                <SelectField
                  label={"Merchant Name"}
                  placeholder="All Merchant Name"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={merchantOption}
                  selectedValue={ouCode}
                  setValue={setOuCode}
                />
                <DatePickerField
                  label={"Year Month"}
                  placeholder="All Year Month"
                  sx={{ width: "100%", fontSize: "16px" }}
                  value={date}
                  onChange={(prev) => setDate(prev)}
                  views={["month", "year"]}
                  disabled={ouCode ? false : true}
                />
                <SelectField
                  label={"Invoice Type"}
                  placeholder="All Invoice Type"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={approvalOption}
                  selectedValue={approval}
                  setValue={setApproval}
                  disabled={date ? false : true}
                />
                {/* filter button */}
                <CustomButton
                  onClick={() => {
                    setData([]);
                    setPagination((prev) => ({
                      ...prev,
                      limit: 25,
                      offset: 0,
                    }));
                    handleGetApprovalList({
                      date: date ? date.format("YYYYMM") : "",
                      status: approval ? approval.value : "",
                      ouCodeValue: ouCode ? ouCode.value : [],
                    });
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttonFilter}
                >
                  Filter
                </CustomButton>
              </Box>
            </Stack>

            {/* filter message note */}
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
            </Stack>

            {/* confirmation modal */}
            <Dialog
              open={open}
              onClose={() => setOpen(false)}
              scroll="paper"
              aria-labelledby="delete-title"
              aria-describedby="delete-description"
            >
              <DialogTitle id="scroll-dialog-title">
                <Typography
                  color={"rgb(71 85 105)"}
                  fontSize="1.3rem"
                  fontWeight={"600"}
                  height={25}
                >
                  Request Details
                </Typography>
              </DialogTitle>
              <DialogContent dividers={true}>
                <DialogContentText id="scroll-dialog-confirmation">
                  <Box sx={{ marginBlock: "2rem" }}>
                    <Grid container spacing={3}>
                      <Grid item xs={6} sm={3}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          Approval Code
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={1}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          :
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={7}>
                        <Typography>{detail.approvalCode}</Typography>
                      </Grid>
                      <Grid item xs={6} sm={3}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          Merchant Name
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={1}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          :
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={7}>
                        <Typography>{detail.merchant}</Typography>
                      </Grid>
                      <Grid item xs={6} sm={3}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          Request Date
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={1}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          :
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={7}>
                        <Typography>{detail.reqDate}</Typography>
                      </Grid>
                      <Grid item xs={6} sm={3}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          Proposer
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={1}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          :
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={7}>
                        <Typography>{detail.createdBy}</Typography>
                      </Grid>
                      <Grid item xs={6} sm={3}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          Description
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={1}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          :
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={7}>
                        <Typography>{detail.description}</Typography>
                      </Grid>
                      <Grid item xs={6} sm={3}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          Approval
                        </Typography>
                      </Grid>
                      <Grid item xs={6} sm={1}>
                        <Typography
                          color={"rgb(71 85 105)"}
                          fontSize="0.95rem"
                          fontWeight={"600"}
                        >
                          :
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={7}>
                        <FormControl
                          sx={{ width: ["80%", "50%"] }}
                          size="small"
                        >
                          <Select
                            value={finalApproval}
                            displayEmpty
                            inputProps={{ "aria-label": "Without label" }}
                            onChange={(event) => {
                              setFinalApproval(event.target.value);
                            }}
                            disabled={["APPROVE", "DECLINE"].includes(
                              detail.status
                            )}
                          >
                            <MenuItem value={"PENDING"}>Pending</MenuItem>
                            <MenuItem value={"APPROVE"}>Approve</MenuItem>
                            <MenuItem value={"DECLINE"}>Decline</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                    </Grid>
                  </Box>
                </DialogContentText>
              </DialogContent>
              <DialogActions
                sx={{ display: "flex", flexDirection: "column", gap: 2 }}
              >
                <FilterMessageNote
                  sx={{
                    width: "100%",
                    alignSelf: "flex-start",
                  }}
                  title="Setiap aksi penghapusan yang dilakukan menjadi tanggung jawab pengguna masing-masing."
                  subtitle={subTitleInfo}
                />
                <Box sx={{ alignSelf: "flex-end" }}>
                  <Button
                    onClick={() => {
                      setOpen(false);
                    }}
                  >
                    Cancel
                  </Button>
                  <Button
                    onClick={() => {
                      handleEditRemoveApproval({
                        approvalRemoveCode: selectedApprovalCode,
                        approvalStatus: finalApproval,
                      });
                      setOpen(false);
                    }}
                  >
                    Confirm
                  </Button>
                </Box>
              </DialogActions>
            </Dialog>

            {/* Data result*/}
            <Box sx={{ width: "100%" }}>
              <CustomPagination
                disableNext={pagination.disableNext}
                countLoading={pagination.countLoading}
                limit={pagination.limit}
                offset={pagination.offset}
                count={pagination.count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
              {/* search bar */}
              <Box
                sx={{
                  display: "flex",
                  gap: 1,
                  width: ["60%", "40%", "20%"],
                  marginLeft: "auto",
                  alignItems: "flex-end",
                }}
              >
                <Box sx={{ marginBottom: "9px" }}>
                  <SearchIcon size="14px" />
                </Box>

                <InputField
                  value={search}
                  onChange={(e) => {
                    setSearch(e.target.value);
                  }}
                  placeholder="search"
                />
              </Box>
              <CustomTable
                headers={header}
                items={search ? handleSearch() : handlePagination(data)}
                renderCell={renderCell}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default ApprovalPage;
