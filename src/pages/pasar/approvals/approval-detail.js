import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography, Grid } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import moment from "moment";
import {
  getApprovalDetail,
  getApprovalDetailMetadata,
  getSummaryApproval,
} from "../../../services/pasar/approval";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import InputField from "../../../components/text-field";
import CardAmountSummary from "../../../components/pasar-components/custom-summary";
import PaymentsIcon from "@mui/icons-material/Payments";
import ViewListIcon from "@mui/icons-material/ViewList";

const ApprovalDetail = ({
  removeCode,
  setRemoveCode,
  label = "Approval Detail",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  setLoading = () => {},
  notify = () => {},
}) => {
  // table state
  const [data, setData] = useState([]); //table content
  const [search, setSearch] = useState(""); //search state

  // summary state
  const [summary, setSummary] = useState({
    billingAmount: 0,
    billingQuantity: 0,
  });

  // pagination state
  const [pagination, setPagination] = useState({
    limit: 25,
    offset: 0,
    count: -99,
    countLoading: false,
    disableNext: false,
  });

  let searchMetaData = 0;
  const [originalMetadata, setOriginalMetadata] = useState(0);

  // table header
  const header = [
    {
      title: "Billing Date",
      value: "billingDate",
      align: "left",
      width: "200px",
    },
    {
      title: "Account Name",
      value: "accountName",
      align: "left",
      width: "200px",
    },
    {
      title: "Store Code",
      value: "storeCode",
      align: "left",
      width: "200px",
    },
    {
      title: "Billing Name",
      value: "billingName",
      align: "left",
      width: "200px",
    },
    {
      title: "Billing Amount",
      value: "billingAmount",
      align: "left",
      width: "200px",
    },
    {
      title: "Corporate Name",
      value: "corporateName",
      align: "left",
      width: "200px",
    },
    {
      title: "Billing Code",
      value: "billingCode",
      align: "left",
      width: "200px",
    },
  ];

  // style the data for each column
  const renderCell = (item, header) => {
    switch (header.value) {
      case "billingDate":
        return <span>{moment(item.billingDate).format("Do MMMM YYYY")}</span>;
      case "accountName":
        return <span>{item.accountName}</span>;
      case "storeCode":
        return <span>{item.storeCode}</span>;
      case "billingName":
        return <span>{item.billingName}</span>;
      case "billingAmount":
        return <span>{item.billingAmount}</span>;
      case "corporateName":
        return <span>{item.corporateName}</span>;
      case "billingCode":
        return <span>{item.billingCode}</span>;
      default:
        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }
  };

  // fetch table data
  const handleGetApprovalDetailList = () => {
    let countResult = 0;
    const data = {
      approvalRemoveCode: removeCode,
      limit: 0,
      offset: 0,
    };
    setLoading(true);
    setPagination((prev) => ({ ...prev, countLoading: true }));

    getApprovalDetailMetadata({
      approvalRemoveCode: removeCode,
    })
      .then((res) => {
        countResult = res.result;
        setPagination((prev) => ({
          ...prev,
          count: countResult,
          disableNext: false,
        }));
        setOriginalMetadata(countResult);
      })
      .catch((e) => {
        setPagination((prev) => ({ ...prev, count: -99 }));
      })
      .finally(() => {
        setPagination((prev) => ({ ...prev, countLoading: false }));
      });

    getSummaryApproval({
      approvalRemoveCode: removeCode,
    })
      .then((res) => {
        setSummary({
          billingAmount: res.result.transactionAmount,
          billingQuantity: res.result.transactionCount,
        });
      })
      .catch((e) => {
        notify(e.message, "error");
        setSummary({
          billingAmount: 0,
          billingQuantity: 0,
        });
      });

    getApprovalDetail(data)
      .then((res) => {
        if (res.result) {
          setData(res.result);
          notify(res.message || "Success Get Data List", "success");
        } else {
          setPagination((prev) => ({ ...prev, disableNext: true }));
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setPagination((prev) => ({ ...prev, disableNext: true }));
        setLoading(false);
        notify(e.message, "error");
      });
  };

  const pageChange = async (value) => {
    var pageOffset = value * pagination.limit;
    setPagination((prev) => ({ ...prev, offset: pageOffset }));
  };

  const rowsChange = async (e) => {
    setPagination((prev) => ({ ...prev, limit: e.props.value, offset: 0 }));
  };

  // handle dividing the data
  const handlePagination = (data) => {
    const endIndex = Math.min(
      pagination.offset + pagination.limit,
      data.length
    );
    const paginatedData = data.slice(pagination.offset, endIndex);

    return paginatedData;
  };

  // handle searching the data based on account name
  const handleSearch = () => {
    const filteredData = data.filter((item) =>
      item.corporateName.toLowerCase().includes(search.trim().toLowerCase())
    );

    searchMetaData = filteredData.length;
    return handlePagination(filteredData);
  };

  useEffect(() => {
    handleGetApprovalDetailList();
  }, []);

  useEffect(() => {
    setPagination((prev) => ({
      ...prev,
      count: search === "" ? originalMetadata : searchMetaData,
    }));
  }, [search]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            {/* Title */}
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>

            {/* filter message note and confirmation button */}
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              <CustomButton
                onClick={() => {
                  setRemoveCode("");
                }}
                name="Back"
              >
                Back
              </CustomButton>
            </Stack>

            {/* summary card */}
            <Stack
              display="flex"
              direction="column"
              mt={"2rem"}
              mb={"0rem"}
              gap={2}
            >
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: ["repeat(1, 1fr)"],
                  gap: 2,
                }}
              >
                <Typography variant="h5" fontWeight="600">
                  Summary
                </Typography>
                <Box
                  sx={{
                    display: "grid",
                    gridTemplateColumns: [
                      "repeat(1, 1fr)",
                      "repeat(2, 1fr)",
                      "repeat(4, 1fr)",
                    ],
                    gap: 2,
                  }}
                >
                  <CardAmountSummary
                    title="billing amount"
                    src={
                      <PaymentsIcon
                        sx={{ fontSize: "30px", color: "#3875CA" }}
                      />
                    }
                    amount={summary.billingAmount}
                  />
                  <CardAmountSummary
                    title="billing quantity"
                    src={
                      <ViewListIcon
                        sx={{ fontSize: "30px", color: "#3875CA" }}
                      />
                    }
                    amount={summary.billingQuantity}
                    isCurrency={false}
                  />
                </Box>
              </Box>
            </Stack>

            {/* Data result*/}
            <Box sx={{ width: "100%" }}>
              <CustomPagination
                disableNext={pagination.disableNext}
                countLoading={pagination.countLoading}
                limit={pagination.limit}
                offset={pagination.offset}
                count={pagination.count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
              {/* search bar */}
              <Box
                sx={{
                  display: "flex",
                  gap: 1,
                  width: ["60%", "40%", "20%"],
                  marginLeft: "auto",
                  alignItems: "flex-end",
                }}
              >
                <Box sx={{ marginBottom: "9px" }}>
                  <SearchIcon size="14px" />
                </Box>

                <InputField
                  value={search}
                  onChange={(e) => {
                    setSearch(e.target.value);
                  }}
                  placeholder="search"
                />
              </Box>
              <CustomTable
                headers={header}
                items={search ? handleSearch() : handlePagination(data)}
                renderCell={renderCell}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default ApprovalDetail;
