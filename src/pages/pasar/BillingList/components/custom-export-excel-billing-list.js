import React from "react";
import { getBillingList } from "../../../../services/pasar/billingListSummary";
import { saveAs } from "file-saver";
import { formatCurrency } from "../../../../utils/format-currency";
import ExcelJS from "exceljs";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";

const ExportExcel = ({
  disabled,
  setDisabled,
  notify,
  body,
  setLoading,
  fileName,
}) => {
  const fetchData = async () => {
    setDisabled(true);
    setLoading(true);
    try {
      const res = await getBillingList(body);
      if (res.result) {
        const formattedData = res.result.map((item) => ({
          ...item,
          billingAmount: formatCurrency(item.billingAmount),
        }));
        exportHandler(formattedData);
      } else {
        notify("No Data Found", "error");
      }
    } catch (e) {
      console.error("Error :", e);
      notify("Fetch Error", "error");
    } finally {
      setDisabled(false);
      setLoading(false);
    }
  };

  const exportHandler = (data) => {
    console.log(body);
    const workbook = new ExcelJS.Workbook();
    const ws = workbook.addWorksheet("Laporan Data Billing");

    // add columns to the worksheet
    ws.columns = [
      { header: "NAMA TAGIHAN", key: "billingName", width: 50 },
      { header: "TANGGAL TAGIHAN", key: "billingDate", width: 35 },
      { header: "NOMINAL TAGIHAN", key: "billingAmount", width: 35 },
      { header: "STATUS TAGIHAN", key: "billingStatus", width: 35 },
      { header: "NAMA AKUN", key: "accountName", width: 35 },
      { header: "NOMOR KIOS", key: "storeCode", width: 35 },
      { header: "REMARK", key: "billingRemark", width: 35 },
      { header: "NAMA MERCHANT", key: "corporateName", width: 35 },
    ];

    // add data to the worksheet rows
    ws.addRows(data);

    // save the file
    workbook.xlsx.writeBuffer().then((buffer) => {
      saveAs(new Blob([buffer]), `${fileName}.xlsx`);
      notify("Success Export Data", "success");
    });
  };

  return (
    <CustomButton
      onClick={fetchData}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={disabled}
      color="success"
    >
      Download
    </CustomButton>
  );
};

export default ExportExcel;
