import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import { getBillingTypeList } from "../../../services/pasar/billingListSummary";
import moment from "moment";
import SelectField from "../../../components/select-field";
import FilterMessageNote from "../../../components/filter-message-note";
import DatePickerField from "../../../components/datepicker-field";
import ExportExcel from "./components/custom-export-excel-billing-list";

const BillingList = ({
  label = "Billing List",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  // dropdown options
  const [merchantOption, setMerchantOption] = useState([]); //dropdown for ouCode filter
  const [billingTypeOption, setBillingTypeOption] = useState([]); //dropdown for Billing Type filter
  const billingStatusOption = [
    {
      label: "SUCCESS",
      value: "SUCCESS",
    },
    {
      label: "CANCELLED",
      value: "CANCELLED",
    },
    {
      label: "PENDING",
      value: "PENDING",
    },
  ];

  //controlled form filter state
  const [startDate, setStartDate] = useState(moment());
  const [endDate, setEndDate] = useState(moment());
  const [ouCode, setOuCode] = useState("");
  const [billingType, setBillingType] = useState("");
  const [billingStatus, setBillingStatus] = useState("");

  // export to excel button state
  const [disabled, setDisabled] = useState(true);

  // function to fetch billing type list
  const getBillingType = async (body) => {
    try {
      setLoading(true);
      const res = await getBillingTypeList(body);
      if (res.result) {
        setBillingTypeOption(
          res.result.map((item) => ({
            label: item.billingName,
            value: item.billingType,
          }))
        );
        notify("Success Fetch Billing Type", "success");
      } else {
        notify("No Billing Type Found", "warning");
        setBillingTypeOption([{ label: "-", value: "" }]);
      }
    } catch (e) {
      console.error("Error : ", e);
      notify("Failed Fetch Billing Type", "error");
      setBillingTypeOption([]);
    } finally {
      setLoading(false);
    }
  };

  // moving data from merchantData to merchantOption and setting the dropdown
  useEffect(() => {
    let merchantArr = []; //placeholder for erchantOption value
    merchantData.forEach((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);
  // fetch different billing type list every oucode change
  useEffect(() => {
    if (ouCode) {
      getBillingType({
        outletCode: ouCode.value,
      });
    }
  }, [ouCode]);
  // enable export button if all filter are filled
  useEffect(() => {
    if (ouCode && billingType && startDate && endDate && billingStatus) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [ouCode, billingType, startDate, endDate, billingStatus]);
  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Stack>
            {/* Title */}
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>

            {/* filter and filter button */}
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: [
                  "repeat(1, 1fr)",
                  "repeat(2, 1fr)",
                  "repeat(4, 1fr)",
                ],
                gap: 3,
                marginTop: "2rem",
              }}
            >
              {/* input */}
              <SelectField
                label={"Merchant Name"}
                placeholder="Choose a Merchant"
                sx={{ width: "100%", fontSize: "16px" }}
                data={merchantOption}
                selectedValue={ouCode}
                setValue={setOuCode}
              />
              <SelectField
                label={"Billing Type"}
                placeholder="Choose a Billing Type"
                sx={{ width: "100%", fontSize: "16px" }}
                data={billingTypeOption}
                selectedValue={billingType}
                setValue={setBillingType}
                disabled={ouCode ? false : true}
              />
              <DatePickerField
                label={"Date From"}
                placeholder="Pick A Start Date"
                maxDate={moment()}
                sx={{ width: "100%", fontSize: "16px" }}
                value={startDate}
                onChange={(newInput) => setStartDate(newInput)}
              />
              <DatePickerField
                label={"Date To"}
                placeholder="Pick An End Date"
                minDate={startDate}
                maxDate={moment()}
                sx={{ width: "100%", fontSize: "16px" }}
                value={endDate}
                onChange={(newInput) => setEndDate(newInput)}
              />
              <SelectField
                label={"Billing Status"}
                placeholder="Choose a Billing Status"
                sx={{ width: "100%", fontSize: "16px" }}
                data={billingStatusOption}
                selectedValue={billingStatus}
                setValue={setBillingStatus}
                disabled={billingType ? false : true}
              />
            </Box>

            {/* filter message note */}
            <Stack
              sx={{
                width: "100%",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              <ExportExcel
                disabled={disabled}
                setDisabled={setDisabled}
                setLoading={setLoading}
                notify={notify}
                body={{
                  outletCode: ouCode ? ouCode.value : "",
                  billingType: billingType ? billingType.value : "",
                  billingStatus: billingStatus ? billingStatus.value : "",
                  dateFrom: startDate ? startDate.format("YYYY-MM-DD") : "",
                  dateTo: endDate ? endDate.format("YYYY-MM-DD") : "",
                }}
                fileName={`${ouCode && ouCode.label} ${
                  billingType && billingType.label
                } ${billingStatus && billingStatus.label} (${
                  startDate && startDate.format("YYYY-MM-DD")
                }_${endDate && endDate.format("YYYY-MM-DD")})`}
              />
            </Stack>
          </Stack>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default BillingList;
