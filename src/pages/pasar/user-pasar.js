import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import SelectField from "../../components/select-field";
import SearchIcon from "@mui/icons-material/Search";
import moment from "moment";
import {
  getUserAccountList,
  getUserAccountMetadata,
} from "../../services/pasar/userPasar";
import CustomTable from "../../components/custom-table";
import CustomPagination from "../../components/custom-pagination";
import FilterMessageNote from "../../components/filter-message-note";
import CustomButton from "../../components/custom-button";
import ExportToExcel from "../../components/export-to-excel";

const UserPasar = ({
  label = "User Pasar",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  const [merchantOption, setMerchantOption] = useState([]); //dropdown
  const [ouCode, setOuCode] = useState(""); //controlled form
  const [limit, setLimit] = useState(25);
  const [offset, setOffset] = useState(0);
  const [ouCodeSelected, setOuCodeSelected] = useState([]); //selectd ouCode (only one ouCode)
  const [count, setCount] = useState(-99); //?
  const [countLoading, setCountLoading] = useState(false);
  const [disableNext, setDisableNext] = useState(false);
  const [data, setData] = useState([]); //content
  const [allMerchant, setAllMerchant] = useState([]); //all merchant ouCode

  // table header
  const header = [
    {
      title: "Customer Name",
      value: "accountName",
      align: "left",
      width: "250px",
    },
    {
      title: "Account ID Type",
      value: "accountIdentityType",
      align: "left",
      width: "200px",
    },
    {
      title: "Account ID",
      value: "accountIdentity",
      align: "left",
      width: "200px",
    },
    {
      title: "Phone Number",
      value: "accountPhoneNumber",
      align: "left",
      width: "200px",
    },
    {
      title: "Address",
      value: "accountAddress",
      align: "left",
      width: "200px",
    },
    {
      title: "Email",
      value: "accountEmail",
      align: "left",
      width: "200px",
    },
    {
      title: "Merchant Name",
      value: "corporateName",
      align: "left",
      width: "200px",
    },
    {
      title: "Registered Date",
      value: "createdAt",
      align: "left",
      width: "200px",
    },
  ];

  // style the data for each column
  const renderCell = (item, header) => {
    switch (header.value) {
      case "createdAt":
        return (
          <span>{moment(item.createdAt).format("Do MMMM YYYY, h:mm:ss")}</span>
        );
      default:
        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }
  };

  // moving data from merchantData to merchantArray and setting the dropdown
  useEffect(() => {
    let merchantArr = [];
    merchantData.forEach((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);

  // if the dropdown is not empty then we will extract the ouCode only to the ouCodeArr,
  //allMerchant === ouCodeArr
  // we also fetch all of the data using the extracted ouCode
  useEffect(() => {
    if (merchantOption.length > 0) {
      let ouCodeArr = []; //ouCode buat awal component mount
      merchantOption.forEach((item) => {
        ouCodeArr.push(item.value);
      });
      setAllMerchant(ouCodeArr);
      handleGetListUserAccount({
        limitDt: limit,
        offsetDt: 0,
        ouCodeValue: ouCodeArr,
      });
    }
  }, [merchantOption]);

  // this function fetch the user data
  const handleGetListUserAccount = ({ limitDt, offsetDt, ouCodeValue }) => {
    let countResult = 0;
    let data = {
      outletCode: ouCodeValue,
      limit: limitDt,
      offset: offsetDt,
    };
    setLoading(true);
    setCountLoading(true);
    setOuCodeSelected(ouCodeValue);

    getUserAccountMetadata({
      outletCode: ouCodeValue,
    })
      .then((res) => {
        countResult = res.result;
        setDisableNext(false);
        setCountLoading(false);
        setCount(countResult);
      })
      .catch((e) => {
        setCount(-99);
        setCountLoading(false);
        setCount(countResult);
      });

    getUserAccountList(data)
      .then((res) => {
        if (res.result) {
          setData(res.result);
          notify(res.message || "Success Get Data List", "success");
        } else {
          setDisableNext(true);
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setDisableNext(true);
        setLoading(false);
        notify(e.message, "error");
      });
  };

  const pageChange = async (value) => {
    var pageOffset = value * limit;
    setOffset(pageOffset);
    handleGetListUserAccount({
      limitDt: limit,
      offsetDt: pageOffset,
      ouCodeValue: ouCodeSelected,
    });

    console.log(ouCodeSelected);
    console.log(allMerchant);
  };

  const rowsChange = async (e) => {
    setOffset(0);
    setLimit(e.props.value);
    handleGetListUserAccount({
      limitDt: e.props.value,
      offsetDt: 0,
      ouCodeValue: ouCodeSelected,
    });
  };

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            {/* Title */}
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>

            <Stack
              display="flex"
              direction="column"
              mt={"2rem"}
              mb={"0rem"}
              gap={2}
            >
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)"],
                  gap: 2,
                }}
              >
                {/* input */}
                <SelectField
                  label={"Merchant"}
                  placeholder="All Merchant"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={merchantOption}
                  selectedValue={ouCode}
                  setValue={setOuCode}
                />
              </Box>
            </Stack>

            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              <div
                style={{
                  display: "flex",
                  gap: 15,
                }}
              >
                {/* search button */}
                <ExportToExcel
                  filename="AccountList.xlsx"
                  ouCodeSelected={ouCodeSelected}
                  color={"success"}
                />
                <CustomButton
                  onClick={() => {
                    setLimit(25);
                    setOffset(0);
                    handleGetListUserAccount({
                      limitDt: 25,
                      offsetDt: 0,
                      ouCodeValue: ouCode ? [ouCode.value] : allMerchant,
                    });
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttonFilter}
                >
                  Filter
                </CustomButton>
              </div>
            </Stack>

            {/* search result */}
            <Box sx={{ width: "100%" }}>
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
              <CustomTable
                headers={header}
                items={data}
                renderCell={renderCell}
              />
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default UserPasar;
