import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import {
  getAccountDetailList,
  getAccountDetailMetadata,
  getCardData,
} from "../../../services/pasar/retribusi";
import ViewListIcon from "@mui/icons-material/ViewList";
import SearchIcon from "@mui/icons-material/Search";
import moment from "moment";
import SelectField from "../../../components/select-field";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import CardAmountSummary from "../../../components/pasar-components/custom-summary";
import ExportExcel from "./components/custom-export-excel-active-accounts";

const ActiveAccounts = ({
  label = "Active Accounts",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttomFilter = "Search",
}) => {
  const [merchantOption, setMerchantOption] = useState([]);
  const [ouCode, setOuCode] = useState("");
  const [limit, setLimit] = useState(25);
  const [offset, setOffset] = useState(0);
  const [ouCodeSelected, setOuCodeSelected] = useState([]);
  const [count, setCount] = useState(-99);
  const [countLoading, setCountLoading] = useState(false);
  const [disableNext, setDisableNext] = useState(true);
  const [data, setData] = useState([]);
  const [disabled, setDisabled] = useState(true);
  const [summary, setSummary] = useState({
    accountDetailQty: 0,
    lastActiveQty: 0,
    currentActiveQty: 0,
  });
  const lastMonth = moment().subtract(1, "months").format("MMMM");
  const thisMonth = moment().format("MMMM");
  const header = [
    {
      title: "MERCHANT NAME",
      value: "corporateName",
      align: "left",
      width: "200px",
    },
    {
      title: "STORE CODE",
      value: "storeCode",
      align: "left",
      width: "300px",
    },
    {
      title: "CUSTOMER NAME",
      value: "accountName",
      align: "left",
      width: "250px",
    },
    {
      title: "ACCOUNT TYPE ID",
      value: "accountIdentityType",
      align: "left",
      width: "200px",
    },
    {
      title: "ACCOUNT ID",
      value: "accountIdentity",
      align: "left",
      width: "200px",
    },
    {
      title: "BUSINESS TYPE",
      value: "businessType",
      align: "left",
      width: "200px",
    },
    {
      title: "ADDRESS",
      value: "accountAddress",
      align: "left",
      width: "200px",
    },
    {
      title: "EMAIL",
      value: "accountEmail",
      align: "left",
      width: "200px",
    },
    {
      title: "STORE AREA",
      value: "storeArea",
      align: "left",
      width: "200px",
    },
    {
      title: "STORE ID",
      value: "accountDetailQR",
      align: "left",
      width: "200px",
    },
  ];

  const summaryComponents = [
    {
      title: "total merchant",
      amount: summary.accountDetailQty,
      isCurrency: false,
    },
    {
      title: `${lastMonth} active merchant`,
      amount: summary.lastActiveQty,
      isCurrency: false,
    },
    {
      title: `${thisMonth} active merchant`,
      amount: summary.currentActiveQty,
      isCurrency: false,
    },
  ];

  const renderCell = (item, header) => {
    return <span>{item[header.value] ? item[header.value] : "-"}</span>;
  };

  const handleGetListAccountDetail = ({ limitDt, offsetDt, ouCodeValue }) => {
    let countResult = 0;
    let data = {
      outletCode: ouCodeValue,
      limit: limitDt,
      offset: offsetDt,
    };
    setDisabled(true);
    setLoading(true);
    setCountLoading(true);
    setOuCodeSelected(ouCodeValue);
    getAccountDetailMetadata({
      outletCode: ouCodeValue,
    })
      .then((res) => {
        countResult = res.result;
        setDisableNext(false);
        setCountLoading(false);
        setCount(countResult);
      })
      .catch((e) => {
        setCount(-99);
        setCountLoading(false);
        setCount(countResult);
      });
    getAccountDetailList(data)
      .then((res) => {
        if (res.result) {
          setData(res.result);
          notify(res.message || "Success Get Data List", "success");
          setDisabled(false);
        } else {
          setDisableNext(true);
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setDisableNext(true);
        setLoading(false);
        notify(e.message, "error");
      });
  };
  const fetchSummary = async (body) => {
    try {
      setLoading(true);
      const res = await getCardData(body);
      if (res.result) {
        setSummary(res.result);
      } else {
        setSummary({
          accountDetailQty: 0,
          lastActiveQty: 0,
          currentActiveQty: 0,
        });
      }
    } catch (e) {
      console.error("Error : ", e);
      setSummary({
        accountDetailQty: 0,
        lastActiveQty: 0,
        currentActiveQty: 0,
      });
      notify("Failed Fetch Summary Cards", "error");
    } finally {
      setLoading(false);
    }
  };

  const pageChange = async (value) => {
    var ofset = value * limit;
    setOffset(ofset);
    handleGetListAccountDetail({
      limitDt: limit,
      offsetDt: ofset,
      ouCodeValue: ouCodeSelected,
    });
  };
  const rowsChange = async (e) => {
    setOffset(0);
    setLimit(e.props.value);
    handleGetListAccountDetail({
      limitDt: e.props.value,
      offsetDt: 0,
      ouCodeValue: ouCodeSelected,
    });
  };

  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        ...item,
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: [
                  "repeat(1, 1fr)",
                  "repeat(2, 1fr)",
                  "repeat(4, 1fr)",
                ],
                gap: 2,
                marginTop: "2rem",
              }}
            >
              <SelectField
                required={true}
                isError={!ouCode}
                label={"Merchant"}
                placeholder="All Merchant"
                sx={{ width: "100%", fontSize: "16px" }}
                data={merchantOption}
                selectedValue={ouCode}
                setValue={setOuCode}
              />
            </Box>
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 8,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              {/* export excel and filter button */}
              <Stack direction={"row"} spacing={2}>
                <ExportExcel
                  disabled={disabled}
                  setDisabled={setDisabled}
                  notify={notify}
                  body={{
                    outletCode: ouCodeSelected,
                  }}
                  setLoading={setLoading}
                />
                <CustomButton
                  disabled={!ouCode}
                  onClick={() => {
                    setLimit(25);
                    setOffset(0);
                    fetchSummary({ outletCode: [ouCode.value] });
                    handleGetListAccountDetail({
                      limitDt: 25,
                      offsetDt: 0,
                      ouCodeValue: [ouCode.value],
                    });
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttomFilter}
                >
                  Filter
                </CustomButton>
              </Stack>
            </Stack>
            {/* summary cards */}
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: ["repeat(1, 1fr)"],
                gap: 2,
                marginTop: "2rem",
                marginBottom: "0rem",
              }}
            >
              <Typography variant="h5" fontWeight="600">
                Summary
              </Typography>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: [
                    "repeat(1, 1fr)",
                    "repeat(2, 1fr)",
                    "repeat(4, 1fr)",
                  ],
                  gap: 2,
                }}
              >
                {summaryComponents.map((item, index) => (
                  <CardAmountSummary
                    key={index}
                    title={item.title}
                    src={
                      <ViewListIcon
                        sx={{ fontSize: "30px", color: "#3875CA" }}
                      />
                    }
                    amount={item.amount}
                    isCurrency={item.isCurrency}
                  />
                ))}
              </Box>
            </Box>
            {/* table content */}
            <Box sx={{ width: "100%", mt: 10 }}>
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={async (event, e) => rowsChange(e)}
              />
              <CustomTable
                headers={header}
                items={data}
                renderCell={renderCell}
              />
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={async (event, e) => rowsChange(e)}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default ActiveAccounts;
