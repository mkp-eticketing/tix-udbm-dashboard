import React from "react";
import ExcelJS from "exceljs";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { getAccountDetailList } from "../../../../services/pasar/retribusi";
import { saveAs } from "file-saver";

const ExportExcel = ({ disabled, setDisabled, notify, body, setLoading }) => {
  const fetchData = async () => {
    setDisabled(true);
    setLoading(true);
    try {
      const res = await getAccountDetailList(body);
      if (res.result) {
        exportHandler(res.result);
      } else {
        notify("No Data Found", "error");
      }
    } catch (e) {
      console.error("Error :", e);
      notify("Fetch Error", "error");
    } finally {
      setDisabled(false);
      setLoading(false);
    }
  };

  const exportHandler = (data) => {
    console.log(body);
    const workbook = new ExcelJS.Workbook();
    const ws = workbook.addWorksheet("Laporan Transaction");

    // add columns to the worksheet
    ws.columns = [
      { header: "MERCHANT NAME", key: "corporateName", width: 20 },
      { header: "STORE CODE", key: "storeCode", width: 30 },
      { header: "CUSTOMER NAME", key: "accountName", width: 30 },
      { header: "ACCOUNT TYPE ID", key: "accountIdentityType", width: 20 },
      { header: "ACCOUNT ID", key: "accountIdentity", width: 30 },
      { header: "BUSINESS TYPE", key: "businessType", width: 20 },
      { header: "ADDRESS", key: "accountAddress", width: 30 },
      { header: "EMAIL", key: "accountEmail", width: 30 },
      { header: "AREA", key: "storeArea", width: 15 },
      { header: "STORE ID", key: "accountDetailQR", width: 15 },
    ];
    // add data to the worksheet rows
    ws.addRows(data);
    // save the file
    workbook.xlsx.writeBuffer().then((buffer) => {
      saveAs(
        new Blob([buffer]),
        `Data Active_Accounts_${data[0].corporateName}.xlsx`
      );
      notify("Success Export Data", "success");
    });
  };

  return (
    <CustomButton
      onClick={fetchData}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={disabled}
      color="success"
    >
      Download
    </CustomButton>
  );
};

export default ExportExcel;
