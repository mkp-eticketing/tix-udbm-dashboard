import React from "react";
import { getGradingList } from "../../../../services/pasar/grading";
import ExcelJS from "exceljs";
import { saveAs } from "file-saver";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";

const ExportExcel = ({
  disabled,
  setDisabled,
  setLoading,
  date,
  notify,
  ouCode,
  grade,
  label,
}) => {
  const fetchData = async () => {
    setDisabled(true);
    setLoading(true);
    const body = {
      periode: date,
      outletCode: ouCode,
      grade: grade,
    };
    try {
      const res = await getGradingList(body);
      if (res.result) {
        exportHandler(res.result);
      } else {
        notify("No Data Found", "error");
      }
    } catch (e) {
      console.error("Error :", e);
      notify("Fetch Error", "error");
    } finally {
      setDisabled(false);
      setLoading(false);
    }
  };

  const exportHandler = (data) => {
    const workbook = new ExcelJS.Workbook();
    const ws = workbook.addWorksheet("Laporan Grading");

    // add columns to the worksheet
    ws.columns = [
      { header: "MERCHANT NAME", key: "corporateName", width: 35 },
      { header: "STORE CODE", key: "storeCode", width: 35 },
      { header: "CUSTOMER NAME", key: "accountName", width: 35 },
      { header: "GRADE", key: "grade", width: 10 },
    ];

    // add data to the worksheet rows
    ws.addRows(data);

    // save the file
    workbook.xlsx.writeBuffer().then((buffer) => {
      saveAs(
        new Blob([buffer]),
        `Laporan Grading-${label}-${date}-${grade ? grade : "All Grade"}.xlsx`
      );
      notify("Success Export Data", "success");
    });
  };

  return (
    <CustomButton
      onClick={fetchData}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={disabled}
      color="success"
    >
      Download
    </CustomButton>
  );
};

export default ExportExcel;
