import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import {
  getGradingList,
  getGradingMetadata,
} from "../../../services/pasar/grading";
import moment from "moment";
import SearchIcon from "@mui/icons-material/Search";
import SelectField from "../../../components/select-field";
import DatePickerField from "../../../components/datepicker-field";
import CustomPagination from "../../../components/custom-pagination";
import CustomTable from "../../../components/custom-table";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import ExportExcel from "./components/custom-export-excel-grading";

const LaporanGrading = ({
  label = "Laporan Grading",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttomFilter = "Search",
}) => {
  const [merchantOption, setMerchantOption] = useState([]);
  const [ouCode, setOuCode] = useState("");
  const [grade, setGrade] = useState("");
  const [limit, setLimit] = useState(25);
  const [offset, setOffset] = useState(0);
  const [ouCodeSelected, setOuCodeSelected] = useState([]);
  const [gradeSelected, setGradeSelected] = useState("");
  const [periodeSelected, setPeriodeSelected] = useState("");
  const [count, setCount] = useState(-99);
  const [countLoading, setCountLoading] = useState(false);
  const [disableNext, setDisableNext] = useState(false);
  const [data, setData] = useState([]);
  const [periode, setPeriode] = useState(moment());
  const [allMerchant, setAllMerchant] = useState([]);
  const [disabled, setDisabled] = useState(false);
  const gradeOption = [
    {
      label: "A",
      value: "A",
    },
    {
      label: "B",
      value: "B",
    },
    {
      label: "C",
      value: "C",
    },
    {
      label: "D",
      value: "D",
    },
    {
      label: "E",
      value: "E",
    },
  ];
  const header = [
    {
      title: "MERCHANT NAME",
      value: "corporateName",
      align: "left",
      width: "200px",
    },
    {
      title: "STORE CODE",
      value: "storeCode",
      align: "left",
      width: "250px",
    },
    {
      title: "CUSTOMER NAME",
      value: "accountName",
      align: "left",
      width: "200px",
    },
    {
      title: "GRADE",
      value: "grade",
      align: "left",
      width: "200px",
    },
  ];
  const renderCell = (item, header) => {
    if (header.value === "corporateName") {
      return <span>{item.corporateName}</span>;
    } else if (header.value === "storeCode") {
      return <span>{item.storeCode}</span>;
    } else if (header.value === "accountName") {
      return <span>{item.accountName}</span>;
    } else if (header.value === "grade") {
      return <span>{item.grade}</span>;
    }

    return <span>{item[header.value] ? item[header.value] : "-"}</span>;
  };

  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        ...item,
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);

  const handleGetListGrading = ({ limitDt, offsetDt, ouCodeValue, grade }) => {
    let countResult = 0;
    let periodeDt = periode
      ? periode.format("YYYYMM")
      : moment(Date.now()).format("YYYYMM");
    let data = {
      periode: periodeDt,
      outletCode: ouCodeValue,
      limit: limitDt,
      offset: offsetDt,
      grade: grade,
    };
    setLoading(true);
    setDisabled(true);
    setCountLoading(true);
    setOuCodeSelected(ouCodeValue);
    setGradeSelected(grade);
    setPeriodeSelected(periodeDt);
    getGradingMetadata({
      periode: periodeDt,
      outletCode: ouCodeValue,
      grade: grade,
    })
      .then((res) => {
        countResult = res.result;
        setDisableNext(false);
        setCountLoading(false);
        setCount(countResult);
      })
      .catch((e) => {
        setCount(-99);
        setCountLoading(false);
        setCount(countResult);
      });
    getGradingList(data)
      .then((res) => {
        if (res.result) {
          notify(res.message || "Success Get Data List", "success");
          setData(res.result);
          setDisabled(false);
        } else {
          setDisableNext(true);
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setDisableNext(true);
        setLoading(false);
        notify(e.message, "error");
      });
  };

  const pageChange = async (value) => {
    var ofset = value * limit;
    setOffset(ofset);
    handleGetListGrading({
      limitDt: limit,
      offsetDt: ofset,
      ouCodeValue: ouCodeSelected,
      grade: gradeSelected,
    });
  };
  const rowsChange = async (e) => {
    setOffset(0);
    setLimit(e.props.value);
    handleGetListGrading({
      limitDt: e.props.value,
      offsetDt: 0,
      ouCodeValue: ouCodeSelected,
      grade: gradeSelected,
    });
  };

  useEffect(() => {
    if (merchantOption.length > 0) {
      let ouCodeArr = [];
      merchantOption.map((item) => {
        ouCodeArr.push(item.value);
      });
      setAllMerchant(ouCodeArr);
      handleGetListGrading({
        limitDt: limit,
        offsetDt: 0,
        ouCodeValue: ouCodeArr,
        grade: grade,
      });
    }
  }, [merchantOption]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>
            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: ["repeat(1, 1fr)", "repeat(4, 1fr)"],
                  gap: 2,
                }}
              >
                <SelectField
                  label={"Merchant"}
                  placeholder="All Merchant"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={merchantOption}
                  selectedValue={ouCode}
                  setValue={setOuCode}
                />
                <DatePickerField
                  label={"Year Month"}
                  placeholder="MMM YYYY"
                  sx={{ width: "100%", fontSize: "16px" }}
                  value={periode}
                  format={"MMM YYYY"}
                  onChange={(newValue) => setPeriode(newValue)}
                  views={["month", "year"]}
                />
                <SelectField
                  label={"Grade"}
                  placeholder="All Grade"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={gradeOption}
                  selectedValue={grade}
                  setValue={setGrade}
                />
              </Box>
            </Stack>
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 8,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              <div
                style={{
                  display: "flex",
                  gap: 10,
                }}
              >
                <ExportExcel
                  disabled={disabled}
                  setDisabled={setDisabled}
                  setLoading={setLoading}
                  notify={notify}
                  date={periodeSelected}
                  grade={gradeSelected}
                  ouCode={ouCodeSelected}
                  label={ouCode ? ouCode.label : "All Merchant"}
                />
                <CustomButton
                  onClick={() => {
                    setLimit(25);
                    setOffset(0);
                    handleGetListGrading({
                      limitDt: 25,
                      offsetDt: 0,
                      ouCodeValue: ouCode ? [ouCode.value] : allMerchant,
                      grade: grade ? grade.value : "",
                    });
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttomFilter}
                >
                  Filter
                </CustomButton>
              </div>
            </Stack>
            <Box sx={{ width: "100%", mt: 10 }}>
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={async (event, e) => rowsChange(e)}
              />
              <CustomTable
                headers={header}
                items={data}
                renderCell={renderCell}
              />
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={async (event, e) => rowsChange(e)}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default LaporanGrading;
