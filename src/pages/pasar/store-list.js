import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import SelectField from "../../components/select-field";
import SearchIcon from "@mui/icons-material/Search";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import Avatar from "@mui/material/Avatar";
import {
  getStoreDetailList,
  getStoreDetailMetadata,
} from "../../services/pasar/store";
import CustomTable from "../../components/custom-table";
import CustomPagination from "../../components/custom-pagination";
import FilterMessageNote from "../../components/filter-message-note";
import CustomButton from "../../components/custom-button";
import StoreExportButton from "../../components/store-export-button";
import moment from "moment";

const StoreList = ({
  label = "Store List",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttomFilter = "Search",
}) => {
  const [merchantOption, setMerchantOption] = useState([]);
  const [ouCode, setOuCode] = useState("");
  const [limit, setLimit] = useState(25);
  const [offset, setOffset] = useState(0);
  const [ouCodeSelected, setOuCodeSelected] = useState([]);
  const [count, setCount] = useState(-99);
  const [countLoading, setCountLoading] = useState(false);
  const [disableNext, setDisableNext] = useState(false);
  const [data, setData] = useState([]);
  const [allMerchant, setAllMerchant] = useState([]);
  const header = [
    {
      title: "Store Code",
      value: "storeCode",
      align: "left",
      width: "200px",
    },
    {
      title: "Store Length",
      value: "storeLength",
      align: "left",
      width: "170px",
    },
    {
      title: "Store Width",
      value: "storeWidth",
      align: "left",
      width: "150px",
    },
    {
      title: "Store Area",
      value: "storeArea",
      align: "left",
      width: "150px",
    },
    {
      title: "status",
      value: "storeStatus",
      align: "left",
      width: "150px",
    },
    {
      title: "Serial Number",
      value: "storeSerialNumber",
      align: "left",
      width: "170px",
    },
    {
      title: "District Name",
      value: "districtName",
      align: "left",
      width: "170px",
    },
    {
      title: "Merchant Name",
      value: "corporateName",
      align: "left",
      width: "200px",
    },
    {
      title: "Registered Date",
      value: "createdAt",
      align: "left",
      width: "200px",
    },
  ];

  const renderCell = (item, header) => {
    if (header.value === "storeCode") {
      return <span>{item.storeCode}</span>;
    } else if (header.value === "storeLength") {
      return <span>{item.storeLength}</span>;
    } else if (header.value === "storeWidth") {
      return <span>{item.storeWidth}</span>;
    } else if (header.value === "storeArea") {
      return <span>{item.storeArea}</span>;
    } else if (header.value === "storeStatus") {
      return (
        <span className="flex gap-2 items-center">
          <Avatar
            sx={{
              width: 28,
              height: 28,
              backgroundColor:
                item.storeStatus === "ACTIVE" ? "success.main" : "error.main",
              color: "white",
            }}
          >
            {item.storeStatus === "ACTIVE" ? (
              <CheckCircleIcon fontSize="small" />
            ) : (
              <CancelIcon fontSize="small" />
            )}
          </Avatar>

          {item.storeStatus}
        </span>
      );
    } else if (header.value === "storeSerialNumber") {
      return <span>{item.storeSerialNumber}</span>;
    } else if (header.value === "districtName") {
      return <span>{item.districtName}</span>;
    } else if (header.value === "corporateName") {
      return <span>{item.corporateName}</span>;
    } else if (header.value === "createdAt") {
      return (
        <span>{moment(item.createdAt).format("Do MMMM YYYY, h:mm:ss")}</span>
      );
    }

    return <span>{item[header.value] ? item[header.value] : "-"}</span>;
  };

  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);

  const handleGetListAccountDetail = ({ limitDt, offsetDt, ouCodeValue }) => {
    let countResult = 0;
    let data = {
      outletCode: ouCodeValue,
      limit: limitDt,
      offset: offsetDt,
    };
    // let data2 = {
    //   outletCode: ouCodeValue,
    // };

    setLoading(true);
    setCountLoading(true);
    setOuCodeSelected(ouCodeValue);
    getStoreDetailMetadata({
      outletCode: ouCodeValue,
    })
      .then((res) => {
        countResult = res.result;
        setDisableNext(false);
        setCountLoading(false);
        setCount(countResult);
      })
      .catch((e) => {
        setCount(-99);
        setCountLoading(false);
        setCount(countResult);
      });

    getStoreDetailList(data)
      .then((res) => {
        if (res.result) {
          setData(res.result);
          notify(res.message || "Success Get Data List", "success");
        } else {
          setDisableNext(true);
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setDisableNext(true);
        setLoading(false);
        notify(e.message, "error");
      });
  };

  const pageChange = async (value) => {
    var ofset = value * limit;
    setOffset(ofset);
    handleGetListAccountDetail({
      limitDt: limit,
      offsetDt: ofset,
      ouCodeValue: ouCodeSelected,
    });
  };

  const rowsChange = async (e) => {
    setOffset(0);
    setLimit(e.props.value);
    handleGetListAccountDetail({
      limitDt: e.props.value,
      offsetDt: 0,
      ouCodeValue: ouCodeSelected,
    });
  };

  useEffect(() => {
    if (merchantOption.length > 0) {
      let ouCodeArr = [];
      merchantOption.map((item) => {
        ouCodeArr.push(item.value);
      });
      setAllMerchant(ouCodeArr);
      handleGetListAccountDetail({
        limitDt: limit,
        offsetDt: 0,
        ouCodeValue: ouCodeArr,
      });
    }
  }, [merchantOption]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>
            <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)"],
                  gap: 2,
                }}
              >
                <SelectField
                  label={"Merchant"}
                  placeholder="All Merchant"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={merchantOption}
                  selectedValue={ouCode}
                  setValue={setOuCode}
                />
              </Box>
            </Stack>
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: "2rem",
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              <div
                style={{
                  display: "flex",
                  gap: 3,
                }}
              >
                <StoreExportButton
                  ouCodeSelected={ouCodeSelected}
                  color="success"
                />
                <CustomButton
                  onClick={() => {
                    setLimit(25);
                    setOffset(0);
                    handleGetListAccountDetail({
                      limitDt: 25,
                      offsetDt: 0,
                      ouCodeValue: ouCode ? [ouCode.value] : allMerchant,
                    });
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttomFilter}
                >
                  Filter
                </CustomButton>
              </div>
            </Stack>
            <Box sx={{ width: "100%" }}>
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={async (event, e) => rowsChange(e)}
              />
              <CustomTable
                headers={header}
                items={data}
                renderCell={renderCell}
              />
              <CustomPagination
                disableNext={disableNext}
                countLoading={countLoading}
                limit={limit}
                offset={offset}
                count={count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={async (event, e) => rowsChange(e)}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default StoreList;
