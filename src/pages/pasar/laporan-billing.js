import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
// import moment from "moment";
import {
  getBillingRemoveList,
  getBillingRemoveMetadata,
  getInvoiceList,
  getReasonPermit,
  addBulkSession,
  addApprovalRemove,
} from "../../services/pasar/billing";
import SelectField from "../../components/select-field";
import SearchIcon from "@mui/icons-material/Search";
import CustomBillingTable from "../../components/custom-billing-components/custom-billing-table";
import CustomPagination from "../../components/custom-pagination";
import FilterMessageNote from "../../components/filter-message-note";
import CustomButton from "../../components/custom-button";
import InputField from "../../components/text-field";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import DatePickerField from "../../components/datepicker-field";

const LaporanBilling = ({
  username,
  label = "Billing Pasar",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  // dropdown options
  const [merchantOption, setMerchantOption] = useState([]); //dropdown for merchant filter
  const [invoiceOption, setInvoiceOption] = useState([]); //dropdown for invoice filter
  const [reasonPermitList, setReasonPermitList] = useState([]);

  //filter state
  const [ouCode, setOuCode] = useState(""); //controlled form
  const [date, setDate] = useState(null); //controlled form
  const [invoice, setInvoice] = useState(""); //controlled form

  // table state
  const [data, setData] = useState([]); //table content
  const [search, setSearch] = useState(""); //search state

  // modal state
  const [open, setOpen] = useState(false); //modal state
  const [reason, setReason] = useState(""); //controlled form
  const [confirmed, setConfirmed] = useState(false);
  const [summary, setSummary] = useState({
    billingAmountTotal: 0,
    selectedData: 0,
  });

  // selected Billing ID tracker (only save the billing ID)
  const [billingID, setBillingID] = useState([]);
  // checked items state (save the selected item object)
  const [checkedItems, setCheckedItems] = useState([]);

  // pagination state
  const [pagination, setPagination] = useState({
    limit: 25,
    offset: 0,
    count: -99,
    countLoading: false,
    disableNext: false,
  });

  let searchMetaData = 0;
  const [originalMetadata, setOriginalMetadata] = useState(0);

  // table header
  const header = [
    {
      title: "Billing Date",
      value: "billingDate",
      align: "left",
      width: "200px",
    },
    {
      title: "Store Code",
      value: "storeCode",
      align: "left",
      width: "200px",
    },
    {
      title: "Account Name",
      value: "accountName",
      align: "left",
      width: "250px",
    },
    {
      title: "Billing Category",
      value: "billingRepeat",
      align: "left",
      width: "200px",
    },
    {
      title: "Billing Amount",
      value: "billingAmount",
      align: "left",
      width: "200px",
    },
    {
      title: "Merchant Name",
      value: "corporateName",
      align: "left",
      width: "200px",
    },
  ];

  // style the data for each column
  const renderCell = (item, header) => {
    switch (header.value) {
      case "billingDate":
        return <span>{item.billingDate}</span>;
      case "storeCode":
        return <span>{item.storeCode}</span>;
      case "accountName":
        return <span>{item.accountName}</span>;
      case "billingRepeat":
        return <span>{item.billingRepeat}</span>;
      case "billingAmount":
        return <span>{item.billingAmount}</span>;
      case "corporateName ":
        return <span>{item.corporateName}</span>;
      default:
        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }
  };

  // handle tracking checked billing ID, displaying summary, and displaying confirmed button
  const multiFunctionHandler = (checkedItems) => {
    setConfirmed(checkedItems.length !== 0 && true);
    setSummary(() => {
      let billingAmountTotal = 0;
      for (let checkedItem of checkedItems) {
        billingAmountTotal += checkedItem.billingAmount;
      }

      return {
        billingAmountTotal,
        selectedData: checkedItems.length,
      };
    });
    setBillingID(checkedItems.map((data) => data.billingID));
  };

  // fetch table data
  const handleGetBillingRemoveList = ({ date, invoice, ouCodeValue }) => {
    let countResult = 0;
    const data = {
      periode: date,
      billingProfileCode: invoice,
      outletCode: [ouCodeValue],
      limit: 0,
      offset: 0,
    };
    setLoading(true);
    setPagination((prev) => ({ ...prev, countLoading: true }));

    getBillingRemoveMetadata({
      outletCode: [ouCodeValue],
      periode: date,
      billingProfileCode: invoice,
    })
      .then((res) => {
        countResult = res.result;
        setPagination((prev) => ({
          ...prev,
          count: countResult,
          disableNext: false,
        }));

        setOriginalMetadata(countResult);
      })
      .catch((e) => {
        setPagination((prev) => ({ ...prev, count: -99 }));
      })
      .finally(() => {
        setPagination((prev) => ({ ...prev, countLoading: false }));
      });

    getBillingRemoveList(data)
      .then((res) => {
        if (res.result) {
          setData(res.result);

          // extract the checked data immediately to the checkedItems state
          const initialCheckedItems = res.result.filter(
            (data) => data.checkStatus === true
          );
          setCheckedItems(initialCheckedItems);
          multiFunctionHandler(initialCheckedItems);
          notify(res.message || "Success Get Data List", "success");
        } else {
          setPagination((prev) => ({ ...prev, disableNext: true }));
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setPagination((prev) => ({ ...prev, disableNext: true }));
        setLoading(false);
        notify(e.message, "error");
      });
  };

  // fetch invoice list based on oucode
  const handleGetInvoiceList = (ouCode) => {
    const data = {
      outletCode: [ouCode.value],
      limit: 0,
      offset: 0,
    };

    getInvoiceList(data)
      .then((res) => {
        if (res.result) {
          setInvoiceOption(
            res.result.map((data) => ({
              label: data.billingProfileName,
              value: data.billingProfileCode,
            }))
          );
          notify(res.message || "Success Get Invoice List", "success");
        } else {
          setInvoiceOption([]);
          notify("No Invoice Data Found", "warning");
        }
      })
      .catch((e) => {
        console.log(e);
        notify(e.message, "error");
      });
  };

  // fetch reason permit list
  const handleGetReasonPermit = (ouCode) => {
    const data = {
      outletCode: ouCode.value,
    };

    getReasonPermit(data)
      .then((res) => {
        if (res.result) {
          setReasonPermitList(
            res.result.map((data) => ({
              label: data.description,
              value: data.description,
            }))
          );
          notify(res.message || "Success Get Reason Permit List", "success");
        } else {
          setReasonPermitList([]);
          notify("No Reason Permit Data Found", "warning");
        }
      })
      .catch((e) => {
        console.log(e);
        notify(e.message, "error");
      });
  };

  // send the selected billing ID to the server
  const handleAddBulkSession = ({ ouCodeValue, date, invoice, id }) => {
    let data = {
      outletCode: ouCodeValue,
      periode: date,
      billingProfileCode: invoice,
      billingID: id,
    };

    addBulkSession(data)
      .then((res) => {
        console.log(res);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleAddApprovalRemove = ({
    reason,
    ouCodeValue,
    date,
    invoice,
    id,
  }) => {
    const data = {
      userUsername: username,
      description: reason,
      outletCode: ouCodeValue,
      periode: date,
      billingProfileCode: invoice,
      billingID: id,
    };

    addApprovalRemove(data)
      .then((res) => {
        console.log(res);
        notify(res.message || "Success Send Remove Request", "success");
      })
      .catch((e) => {
        console.log(e);
        notify("Failed to Send Remove Request", "warning");
      });
  };

  const pageChange = async (value) => {
    var pageOffset = value * pagination.limit;
    setPagination((prev) => ({ ...prev, offset: pageOffset }));
  };

  const rowsChange = async (e) => {
    setPagination((prev) => ({ ...prev, limit: e.props.value, offset: 0 }));
  };

  // handle dividing the data
  const handlePagination = (data) => {
    const endIndex = Math.min(
      pagination.offset + pagination.limit,
      data.length
    );
    const paginatedData = data.slice(pagination.offset, endIndex);

    return paginatedData;
  };

  // handle searching the data based on account name
  const handleSearch = () => {
    const filteredData = data.filter((item) =>
      item.accountName.toLowerCase().includes(search.trim().toLowerCase())
    );

    searchMetaData = filteredData.length;
    return handlePagination(filteredData);
  };

  // moving data from merchantData to merchantOption and setting the dropdown
  useEffect(() => {
    let merchantArr = []; //placeholder for erchantOption value
    merchantData.forEach((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);

  // fetch invoice list every oucode change
  useEffect(() => {
    // reset invoice and date every merchant changes
    setInvoice("");
    setDate(null);
    if (ouCode) {
      handleGetInvoiceList(ouCode);
    } else {
      setInvoiceOption([]);
    }
  }, [ouCode]);

  useEffect(() => {
    setPagination((prev) => ({
      ...prev,
      count: search === "" ? originalMetadata : searchMetaData,
    }));
  }, [search]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            {/* Title */}
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>

            {/* filter and filter button */}
            <Stack
              display="flex"
              direction="column"
              mt={"2rem"}
              mb={"0rem"}
              gap={2}
            >
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: [
                    "repeat(1, 1fr)",
                    "repeat(1, 1fr)",
                    "1fr 1fr 1fr auto",
                  ],
                  gap: 2,
                }}
              >
                {/* input */}
                <SelectField
                  label={"Merchant Name"}
                  placeholder="All Merchant Name"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={merchantOption}
                  selectedValue={ouCode}
                  setValue={setOuCode}
                />
                <DatePickerField
                  label={"Year Month"}
                  placeholder="All Year Month"
                  sx={{ width: "100%", fontSize: "16px" }}
                  value={date}
                  onChange={(prev) => setDate(prev)}
                  views={["month", "year"]}
                  disabled={ouCode ? false : true}
                />
                <SelectField
                  label={"Invoice Type"}
                  placeholder="All Invoice Type"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={invoiceOption}
                  selectedValue={invoice}
                  setValue={setInvoice}
                  disabled={date ? false : true}
                />
                {/* filter button */}
                <CustomButton
                  onClick={() => {
                    setData([]);
                    setConfirmed(false);
                    setPagination((prev) => ({
                      ...prev,
                      limit: 25,
                      offset: 0,
                    }));
                    handleGetBillingRemoveList({
                      date: date ? date.format("YYYYMM") : "",
                      invoice: invoice ? invoice.value : "",
                      ouCodeValue: ouCode ? ouCode.value : [],
                    });
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttonFilter}
                >
                  Filter
                </CustomButton>
              </Box>
            </Stack>

            {/* filter message note and confirmation button */}
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              {confirmed && (
                <CustomButton
                  onClick={() => {
                    // fetch reason permit on confirm button clicked
                    handleGetReasonPermit(ouCode);
                    // send billing ID to the server on confirm button clicked
                    handleAddBulkSession({
                      ouCodeValue: ouCode ? ouCode.value : "",
                      date: date ? date.format("YYYYMM") : "",
                      invoice: invoice ? invoice.value : "",
                      id: billingID,
                    });
                    setOpen(true);
                  }}
                  name="Confirm"
                >
                  Confirm
                </CustomButton>
              )}
            </Stack>

            {/* confirmation modal */}
            <Dialog
              open={open}
              onClose={() => setOpen(false)}
              scroll="paper"
              aria-labelledby="delete-title"
              aria-describedby="delete-description"
            >
              <DialogTitle id="scroll-dialog-title">
                <Typography
                  color={"rgb(71 85 105)"}
                  fontSize="1.3rem"
                  fontWeight={"600"}
                  height={25}
                >
                  Delete Confirmation
                </Typography>
              </DialogTitle>
              <DialogContent dividers={true}>
                <DialogContentText id="scroll-dialog-confirmation">
                  <SelectField
                    label={"Alasan"}
                    placeholder="Masukkan Alasan"
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={reasonPermitList}
                    selectedValue={reason}
                    setValue={setReason}
                  />

                  <Box sx={{ marginBlock: "2rem" }}>
                    <Typography
                      color={"rgb(71 85 105)"}
                      fontSize="0.875rem"
                      fontWeight={"600"}
                      height={25}
                    >
                      Detail/Summary
                    </Typography>
                    <Typography>
                      {`Billing Amount Total : ${summary.billingAmountTotal}`}
                    </Typography>
                    <Typography>
                      {`Total Selected Items : ${summary.selectedData}`}
                    </Typography>
                  </Box>
                </DialogContentText>
              </DialogContent>
              <DialogActions
                sx={{ display: "flex", flexDirection: "column", gap: 2 }}
              >
                <FilterMessageNote
                  sx={{
                    width: "100%",
                    alignSelf: "flex-start",
                  }}
                  title="Setiap aksi penghapusan yang dilakukan menjadi tanggung jawab pengguna masing-masing."
                  subtitle={subTitleInfo}
                />
                <Box sx={{ alignSelf: "flex-end" }}>
                  <Button onClick={() => setOpen(false)}>Cancel</Button>
                  <Button
                    onClick={() => {
                      handleAddApprovalRemove({
                        reason: reason ? reason.value : "",
                        ouCodeValue: ouCode ? ouCode.value : "",
                        date: date ? date.format("YYYYMM") : "",
                        invoice: invoice ? invoice.value : "",
                        id: billingID,
                      });
                      setOpen(false);
                      setReason("");
                    }}
                  >
                    Delete
                  </Button>
                </Box>
              </DialogActions>
            </Dialog>

            {/* Data result*/}
            <Box sx={{ width: "100%" }}>
              <CustomPagination
                disableNext={pagination.disableNext}
                countLoading={pagination.countLoading}
                limit={pagination.limit}
                offset={pagination.offset}
                count={pagination.count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
              {/* search bar */}
              <Box
                sx={{
                  display: "flex",
                  gap: 1,
                  width: ["60%", "40%", "20%"],
                  marginLeft: "auto",
                  alignItems: "flex-end",
                }}
              >
                <Box sx={{ marginBottom: "9px" }}>
                  <SearchIcon size="14px" />
                </Box>

                <InputField
                  value={search}
                  onChange={(e) => {
                    setSearch(e.target.value);
                  }}
                  placeholder="search"
                />
              </Box>
              <CustomBillingTable
                headers={header}
                checkedItems={checkedItems}
                setCheckedItems={setCheckedItems}
                items={search ? handleSearch() : handlePagination(data)}
                renderCell={renderCell}
                checkBox={true}
                onItemCheck={(checkedItems) => {
                  multiFunctionHandler(checkedItems);

                  if (checkedItems.length === 0) {
                    handleAddBulkSession({
                      ouCodeValue: ouCode ? ouCode.value : "",
                      date: date ? date.format("YYYYMM") : "",
                      invoice: invoice ? invoice.value : "",
                      id: [],
                    });
                  }
                }}
                onAllCheck={(checkedItems) => {
                  multiFunctionHandler(checkedItems);

                  if (checkedItems.length === 0) {
                    handleAddBulkSession({
                      ouCodeValue: ouCode ? ouCode.value : "",
                      date: date ? date.format("YYYYMM") : "",
                      invoice: invoice ? invoice.value : "",
                      id: [],
                    });
                  }
                }}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default LaporanBilling;
