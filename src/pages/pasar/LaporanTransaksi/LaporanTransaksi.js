import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import {
  getTransactionList,
  getTransactionMetadata,
  getDistrictList,
  getUserList,
  getSummaryTransaction,
} from "../../../services/pasar/transaction-report";
import { formatCurrency } from "../../../utils/format-currency";
import axios from "axios";
import moment from "moment";
import SearchIcon from "@mui/icons-material/Search";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import HourglassFullIcon from "@mui/icons-material/HourglassFull";
import PaymentsIcon from "@mui/icons-material/Payments";
import ViewListIcon from "@mui/icons-material/ViewList";
import PersonIcon from "@mui/icons-material/Person";
import Avatar from "@mui/material/Avatar";
import SelectField from "../../../components/select-field";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import DatePickerField from "../../../components/datepicker-field";
import InputField from "../../../components/text-field";
import CardAmountSummary from "../../../components/pasar-components/custom-summary";
import ExportExcel from "./components/custom-export-excel-transaction";

const LaporanTransaksi = ({
  label = "Laporan Transaksi",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
  avatarPayment = () => {},
}) => {
  // dropdown options
  const [merchantOption, setMerchantOption] = useState([]); //dropdown for merchant filter
  const [usernameOption, setUsernameOption] = useState([]); //dropdown for username filter
  const [districtOption, setDistrictOption] = useState([]); //dropdown for district filter
  const [paymentMethodOption, setPaymentMethodOption] = useState([]); //dropdown for payment method filter
  const searchTypeOption = [
    {
      label: "Payment Ref",
      value: "Payment Ref",
    },
    {
      label: "Device ID",
      value: "Device ID",
    },
    {
      label: "Payment Amount",
      value: "Payment Amount",
    },
    {
      label: "Reference No",
      value: "Reference No",
    },
  ];
  //dropdown for transaction status filter
  const transactionStatusOption = [
    {
      label: "SUCCESS",
      value: "SUCCESS",
    },
    {
      label: "CANCELLED",
      value: "CANCELLED",
    },
    {
      label: "PENDING",
      value: "PENDING",
    },
  ];

  // controlled form filter state
  const [startDate, setStartDate] = useState(moment());
  const [endDate, setEndDate] = useState(moment());
  const [ouCode, setOuCode] = useState("");
  const [username, setUsername] = useState("");
  const [district, setDistrict] = useState("");
  const [paymentMethod, setPaymentMethod] = useState("");
  const [transactionStatus, setTransactionStatus] = useState("");
  const [searchType, setSearchType] = useState("");
  const [search, setSearch] = useState("");

  // states for previous selected value
  const [selected, setSelected] = useState({
    startDate: "",
    endDate: "",
    ouCode: "",
    district: "",
    username: "",
    method: "",
    status: "",
  });

  // table state
  const [data, setData] = useState([]);

  // summary state
  const [summary, setSummary] = useState({
    totalTransaction: 0,
    totalAmount: 0,
    totalMDR: 0,
    totalServiceFee: 0,
  });

  // pagination state
  const [pagination, setPagination] = useState({
    limit: 25,
    offset: 0,
    count: -99,
    countLoading: false,
    disableNext: true,
  });

  // export to excel button state
  const [disabled, setDisabled] = useState(true);

  // table header
  const header = [
    {
      title: "Nomor Transaski",
      value: "paymentRef",
      align: "left",
      width: "250px",
    },
    {
      title: "Status Pembayaran",
      value: "paymentStatus",
      align: "left",
      width: "250px",
    },
    {
      title: "Metode Pembayaran",
      value: "paymentMethod",
      align: "left",
      width: "250px",
    },
    {
      title: "Nominal Transaksi",
      value: "paymentTotalAmount",
      align: "left",
      width: "250px",
    },
    {
      title: "Nama Akun",
      value: "accountName",
      align: "left",
      width: "200px",
    },
    {
      title: "Nomor TU",
      value: "storeCode",
      align: "left",
      width: "200px",
    },
    {
      title: "Jenis Akun",
      value: "accountDetailType",
      align: "left",
      width: "200px",
    },
    {
      title: "Nomor Apps2pay",
      value: "paymentResponseRef",
      align: "left",
      width: "250px",
    },
    {
      title: "Service Fee",
      value: "paymentServiceFee",
      align: "left",
      width: "200px",
    },
    {
      title: "MDR",
      value: "paymentMdr",
      align: "left",
      width: "200px",
    },
    {
      title: "MDR Fee",
      value: "paymentMdrFee",
      align: "left",
      width: "200px",
    },
    {
      title: "MID",
      value: "paymentMID",
      align: "left",
      width: "200px",
    },
    {
      title: "TID",
      value: "paymentTID",
      align: "left",
      width: "200px",
    },
    {
      title: "Nama Merchant",
      value: "corporateName",
      align: "left",
      width: "250px",
    },
    {
      title: "Nama Petugas",
      value: "userName",
      align: "left",
      width: "200px",
    },
    {
      title: "Saldo Awal",
      value: "cardCurrentBalance",
      align: "left",
      width: "200px",
    },
    {
      title: "Saldo Akhir",
      value: "cardLastBalance",
      align: "left",
      width: "200px",
    },
    {
      title: "Card Type",
      value: "cardType",
      align: "left",
      width: "200px",
    },
    {
      title: "Card Pan",
      value: "cardPan",
      align: "left",
      width: "200px",
    },
    {
      title: "Nama Distrik",
      value: "districtName",
      align: "left",
      width: "250px",
    },
    {
      title: "SN",
      value: "deviceSerialNumber",
      align: "left",
      width: "200px",
    },
    {
      title: "Tanggal Transaksi",
      value: "createdAt",
      align: "left",
      width: "250px",
    },
  ];
  // style the data for each column
  const renderCell = (item, header) => {
    switch (header.value) {
      case "paymentStatus":
        return (
          <span className="flex gap-2 items-center">
            <Avatar
              sx={{
                width: 28,
                height: 28,
                backgroundColor:
                  item.paymentStatus === "SUCCESS"
                    ? "success.main"
                    : item.paymentStatus === "PENDING"
                    ? "warning.light"
                    : "error.main",
                color: "white",
              }}
            >
              {item.paymentStatus === "SUCCESS" ? (
                <CheckCircleIcon fontSize="small" />
              ) : item.paymentStatus === "PENDING" ? (
                <HourglassFullIcon fontSize="small" />
              ) : (
                <CancelIcon fontSize="small" />
              )}
            </Avatar>
            {item.paymentStatus}
          </span>
        );
      case "paymentMethod":
        let modifiedPayment;
        if (item.paymentMethod === "TUNAI") {
          modifiedPayment = "CASH";
        } else {
          modifiedPayment = item.paymentMethod;
        }
        return (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              gap: 1,
            }}
          >
            {avatarPayment(modifiedPayment)}
          </Box>
        );
      case "paymentTotalAmount":
        return <span>{formatCurrency(item.paymentTotalAmount)}</span>;
      case "paymentServiceFee":
        return <span>{formatCurrency(item.paymentServiceFee)}</span>;
      case "paymentMdr":
        return <span>{formatCurrency(item.paymentMdr)}</span>;
      case "paymentMdrFee":
        return <span>{formatCurrency(item.paymentMdrFee)}</span>;
      case "cardCurrentBalance":
        return <span>{formatCurrency(item.cardCurrentBalance)}</span>;
      case "cardLastBalance":
        return <span>{formatCurrency(item.cardLastBalance)}</span>;
      case "createdAt":
        return <span>{moment(item.createdAt).format("Do MMMM YYYY")}</span>;
      default:
        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }
  };

  // fetch district filter data
  const handleGetDistrict = async (body) => {
    try {
      setLoading(true);
      const res = await getDistrictList(body);
      if (res.result) {
        const data = res.result;
        setDistrictOption(
          data.map((item) => ({
            label: item.districtName,
            value: item.districtUniqueCode,
          }))
        );
      } else {
        setDistrictOption([{ label: "-", value: "" }]);
        notify("No Districts Found", "warning");
      }
    } catch (e) {
      console.error("Error : ", e);
      notify("Failed Fetch Districts", "error");
      setDistrictOption([]);
    } finally {
      setLoading(false);
    }
  };
  // fetch user filter data
  const handleGetUser = async (body) => {
    try {
      setLoading(true);
      const res = await getUserList(body);
      if (res.result) {
        const data = res.result;
        setUsernameOption(
          data.map((item) => ({
            label: item.usersName,
            value: item.usersUsername,
          }))
        );
      } else {
        setUsernameOption([{ label: "-", value: "" }]);
        notify("No Users Found", "warning");
      }
    } catch (e) {
      console.error("Error : ", e);
      setUsernameOption([]);
      notify("Failed Fetch Users", "error");
    } finally {
      setLoading(false);
    }
  };
  // fetch payment method filter data
  const handleGetPaymentMethod = async () => {
    try {
      setLoading(true);
      const res = await axios.get(
        "https://sandbox.mkpmobile.com/api/eretribusiudbm/public/filter/payment-method"
      );
      if (res.data.result) {
        const data = res.data.result;
        setPaymentMethodOption(
          data.map((item) => ({
            label: item.paymentMethodName,
            value: item.paymentMethodName,
          }))
        );
        notify("Success Fetch Payment Methods", "success");
      } else {
        setPaymentMethodOption([{ label: "-", value: "" }]);
      }
    } catch (e) {
      console.error("Error :", e);
      setPaymentMethodOption([]);
      notify("Failed Fetch Payment Methods", "error");
    } finally {
      setLoading(false);
    }
  };

  // fetch table data
  const handleGetTransaction = async ({
    ouCodeValue,
    startDate,
    endDate,
    status,
    method,
    username,
    uniqueCode,
    limitDt,
    offsetDt,
  }) => {
    const body = {
      outletCode: ouCodeValue,
      dateFrom: startDate,
      dateTo: endDate,
      paymentStatus: status,
      paymentMethod: method,
      usersUsername: username,
      districtUniqueCode: uniqueCode,
      limit: limitDt,
      offset: offsetDt,
    };
    setLoading(true);
    setPagination((prev) => ({ ...prev, countLoading: true }));
    // put previous selected value in a placeholder
    setSelected({
      startDate: startDate,
      endDate: endDate,
      ouCode: ouCodeValue,
      district: uniqueCode,
      username: username,
      method: method,
      status: status,
    });

    try {
      // fetch transaction metadata
      const res = await getTransactionMetadata({
        outletCode: ouCodeValue,
        dateFrom: startDate,
        dateTo: endDate,
        paymentStatus: status,
        paymentMethod: method,
        usersUsername: username,
        districtUniqueCode: uniqueCode,
      });
      // fetch transaction list
      const res2 = await getTransactionList(body);
      if (res2.result) {
        setData(res2.result);
        setDisabled(false);
        setPagination((prev) => ({
          ...prev,
          count: res.result,
          disableNext: false,
        }));
        notify(res2.message || "Success Get Data List", "success");
      } else {
        setPagination((prev) => ({ ...prev, disableNext: true }));
        setData([]);
        notify("No Data Found", "warning");
      }
    } catch (e) {
      setData([]);
      setPagination((prev) => ({ ...prev, count: -99, disableNext: true }));
      notify(e.message, "error");
    } finally {
      setPagination((prev) => ({
        ...prev,
        countLoading: false,
      }));
      setLoading(false);
    }
  };
  // fetch summary data
  const handleGetSummary = async (body) => {
    try {
      setLoading(true);
      const res = await getSummaryTransaction(body);
      if (res.result) {
        setSummary({
          totalTransaction: res.result.totalTransaction,
          totalAmount: res.result.totalAmount,
          totalMDR: res.result.totalMDR,
          totalServiceFee: res.result.totalServiceFee,
        });
      } else {
        setSummary({
          totalTransaction: 0,
          totalAmount: 0,
          totalMDR: 0,
          totalServiceFee: 0,
        });
      }
    } catch (e) {
      console.error("Error : ", e);
      notify("Failed Fetch Summary Cards", "error");
      setSummary({
        totalTransaction: 0,
        totalAmount: 0,
        totalMDR: 0,
        totalServiceFee: 0,
      });
    } finally {
      setLoading(false);
    }
  };
  // fetch search filter
  const handleGetSearchResult = async (body) => {
    setLoading(true);
    try {
      const res = await getTransactionList(body);
      if (res.result) {
        setData(res.result);
        notify(res.message || "Success Search Data List", "success");
      }
    } catch (e) {
      setData([]);
      notify(e.message, "error");
    } finally {
      setLoading(false);
    }
  };

  // handle filter data
  const handleFilterData = () => {
    setData([]);
    setPagination((prev) => ({
      ...prev,
      limit: 25,
      offset: 0,
    }));
    handleGetSummary({
      outletCode: [ouCode.value], //user must pick 1 ouCode
      dateFrom: startDate.format("YYYY-MM-DD"), //user must pick 1 startDate
      dateTo: endDate.format("YYYY-MM-DD"), //user must pick 1 endDate
      paymentStatus: transactionStatus ? transactionStatus.value : "", // optional
      paymentMethod: paymentMethod ? paymentMethod.value : "", // optional
      usersUsername: username ? username.value : "", // optional
      districtUniqueCode: district ? district.value : "", // optional
    });
    handleGetTransaction({
      ouCodeValue: [ouCode.value], //user must pick 1 ouCode
      startDate: startDate.format("YYYY-MM-DD"), //user must pick 1 startDate
      endDate: endDate.format("YYYY-MM-DD"), //user must pick 1 endDate
      status: transactionStatus ? transactionStatus.value : "", // optional
      method: paymentMethod ? paymentMethod.value : "", // optional
      username: username ? username.value : "", // optional
      uniqueCode: district ? district.value : "", // optional
      limitDt: 25,
      offsetDt: 0,
    });
  };

  const pageChange = async (value) => {
    const pageOffset = value * pagination.limit;
    setPagination((prev) => ({ ...prev, offset: pageOffset }));
    setSearch("");
    setSearchType("");
    handleGetTransaction({
      ouCodeValue: selected.ouCode,
      startDate: selected.startDate,
      endDate: selected.endDate,
      status: selected.status,
      method: selected.method,
      username: selected.username,
      uniqueCode: selected.district,
      limitDt: pagination.limit,
      offsetDt: pageOffset,
    });
  };
  const rowsChange = async (e) => {
    setPagination((prev) => ({ ...prev, limit: e.props.value, offset: 0 }));
    setSearch("");
    setSearchType("");
    handleGetTransaction({
      ouCodeValue: selected.ouCode,
      startDate: selected.startDate,
      endDate: selected.endDate,
      status: selected.status,
      method: selected.method,
      username: selected.username,
      uniqueCode: selected.district,
      limitDt: e.props.value,
      offsetDt: 0,
    });
  };

  // moving data from merchantData to merchantOption and setting the dropdown
  useEffect(() => {
    let merchantArr = []; //placeholder for erchantOption value
    merchantData.forEach((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);
  // fetch different district and username every ouCode changes
  useEffect(() => {
    if (ouCode) {
      handleGetDistrict({
        outletCode: [ouCode.value],
        search: "",
        limit: 0,
        offset: 0,
      });
      handleGetUser({
        outletCode: [ouCode.value],
        search: "",
        limit: 0,
        offset: 0,
      });
    }
  }, [ouCode]);
  // fetch payment method data only on first mount
  useEffect(() => {
    handleGetPaymentMethod();
  }, []);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            {/* Title */}
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>

            {/* filter fields*/}
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: [
                  "repeat(1, 1fr)",
                  "repeat(2, 1fr)",
                  "repeat(4, 1fr)",
                ],
                gap: 3,
                marginBottom: "0rem",
                marginTop: "2rem",
              }}
            >
              {/* input */}
              <DatePickerField
                label={"Date From"}
                placeholder="Pick A Start Date"
                maxDate={moment()}
                sx={{ width: "100%", fontSize: "16px" }}
                value={startDate}
                onChange={(newInput) => setStartDate(newInput)}
              />
              <DatePickerField
                label={"Date To"}
                placeholder="Pick An End Date"
                minDate={startDate}
                maxDate={moment()}
                sx={{ width: "100%", fontSize: "16px" }}
                value={endDate}
                onChange={(newInput) => setEndDate(newInput)}
              />
              <SelectField
                label={"Payment Method"}
                placeholder="All Payment Method"
                sx={{ width: "100%", fontSize: "16px" }}
                data={paymentMethodOption}
                selectedValue={paymentMethod}
                setValue={setPaymentMethod}
              />
              <SelectField
                label={"Transaction Status"}
                placeholder="All Transaction Status"
                sx={{ width: "100%", fontSize: "16px" }}
                data={transactionStatusOption}
                selectedValue={transactionStatus}
                setValue={setTransactionStatus}
              />
              <SelectField
                label={"Merchant Name"}
                placeholder="Choose A Merchant"
                sx={{ width: "100%", fontSize: "16px" }}
                data={merchantOption}
                selectedValue={ouCode}
                setValue={setOuCode}
              />
              <SelectField
                label={"Username"}
                placeholder="Choose a Username"
                sx={{ width: "100%", fontSize: "16px" }}
                data={usernameOption}
                selectedValue={username}
                setValue={setUsername}
                disabled={ouCode ? false : true}
              />
              <SelectField
                label={"District"}
                placeholder="Choose a District"
                sx={{ width: "100%", fontSize: "16px" }}
                data={districtOption}
                selectedValue={district}
                setValue={setDistrict}
                disabled={ouCode ? false : true}
              />
            </Box>

            {/* filter message note and filter button*/}
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              <Stack direction={"row"} spacing={2}>
                <ExportExcel
                  disabled={disabled}
                  setDisabled={setDisabled}
                  setLoading={setLoading}
                  notify={notify}
                  body={{
                    outletCode: selected.ouCode,
                    dateFrom: selected.startDate,
                    dateTo: selected.endDate,
                    paymentStatus: selected.status,
                    paymentMethod: selected.method,
                    usersUsername: selected.username,
                    districtUniqueCode: selected.district,
                  }}
                />
                {/* filter button */}
                <CustomButton
                  disabled={!startDate || !endDate || !ouCode}
                  onClick={() => {
                    handleFilterData();
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttonFilter}
                >
                  Filter
                </CustomButton>
              </Stack>
            </Stack>

            {/* summary card */}
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: ["repeat(1, 1fr)"],
                gap: 2,
                marginBottom: "0rem",
                marginTop: "2rem",
              }}
            >
              <Typography variant="h5" fontWeight="600">
                Summary
              </Typography>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: [
                    "repeat(1, 1fr)",
                    "repeat(2, 1fr)",
                    "repeat(4, 1fr)",
                  ],
                  gap: 2,
                }}
              >
                <CardAmountSummary
                  title="Total Transaction"
                  src={
                    <ViewListIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.totalTransaction}
                  isCurrency={false}
                />
                <CardAmountSummary
                  title="Total Amount"
                  src={
                    <PaymentsIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.totalAmount}
                />
                <CardAmountSummary
                  title="Total MDR"
                  src={
                    <PaymentsIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.totalMDR}
                />
                <CardAmountSummary
                  title="Total Service Fee"
                  src={
                    <PaymentsIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.totalServiceFee}
                />
              </Box>
            </Box>

            {/* Data result*/}
            <Box sx={{ width: "100%", mt: 4 }}>
              <CustomPagination
                disableNext={pagination.disableNext}
                countLoading={pagination.countLoading}
                limit={pagination.limit}
                offset={pagination.offset}
                count={pagination.count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
              {/* search bar and search type field*/}
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: "repeat(12, 1fr)",
                  gap: 3,
                }}
              >
                <Box
                  sx={{
                    gridColumn: { xs: "span 6", md: "span 3 / span 3" },
                    gridColumnStart: { md: 7 },
                  }}
                >
                  <SelectField
                    label={"Search Type"}
                    placeholder="Pick A Type"
                    sx={{ fontSize: "16px" }}
                    data={searchTypeOption}
                    selectedValue={searchType}
                    setValue={setSearchType}
                    disabled={!startDate || !endDate || !ouCode}
                  />
                </Box>
                <Box
                  sx={{
                    gridColumn: { xs: "span 6", md: "span 3 / span 3" },
                    gridColumnStart: { md: 10 },
                  }}
                >
                  <InputField
                    value={search}
                    onChange={(e) => {
                      setSearch(e.target.value);
                    }}
                    onKeyPress={(e) =>
                      e.key === "Enter" &&
                      handleGetSearchResult({
                        outletCode: [ouCode.value],
                        dateFrom: startDate.format("YYYY-MM-DD"),
                        dateTo: endDate.format("YYYY-MM-DD"),
                        paymentStatus: transactionStatus
                          ? transactionStatus.value
                          : "",
                        paymentMethod: paymentMethod ? paymentMethod.value : "",
                        usersUsername: username ? username.value : "",
                        districtUniqueCode: district ? district.value : "",
                        search: search,
                        searchType: searchType ? searchType.value : "",
                        limit: pagination.limit,
                        offset: 0,
                      })
                    }
                    disabled={!startDate || !endDate || !ouCode}
                    placeholder="search"
                    startAdornment={
                      <SearchIcon size="14px" sx={{ marginRight: 1 }} />
                    }
                  />
                </Box>
              </Box>

              <CustomTable
                headers={header}
                items={data}
                renderCell={renderCell}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default LaporanTransaksi;
