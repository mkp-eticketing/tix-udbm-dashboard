import React from "react";
import ExcelJS from "exceljs";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { getTransactionList } from "../../../../services/pasar/transaction-report";
import { saveAs } from "file-saver";
import { formatCurrency } from "../../../../utils/format-currency";

const ExportExcel = ({ disabled, setDisabled, notify, body, setLoading }) => {
  const fetchData = async () => {
    setDisabled(true);
    setLoading(true);
    try {
      const res = await getTransactionList(body);
      if (res.result) {
        const formattedData = res.result.map((item) => ({
          ...item,
          paymentTotalAmount: formatCurrency(item.paymentTotalAmount),
          paymentServiceFee: formatCurrency(item.paymentServiceFee),
          paymentMdr: formatCurrency(item.paymentMdr),
          paymentMdrFee: formatCurrency(item.paymentMdrFee),
          cardCurrentBalance: formatCurrency(item.cardCurrentBalance),
          cardLastBalance: formatCurrency(item.cardLastBalance),
        }));
        exportHandler(formattedData);
      } else {
        notify("No Data Found", "error");
      }
    } catch (e) {
      console.error("Error :", e);
      notify("Fetch Error", "error");
    } finally {
      setDisabled(false);
      setLoading(false);
    }
  };

  const exportHandler = (data) => {
    const workbook = new ExcelJS.Workbook();
    const ws = workbook.addWorksheet("Laporan Transaction");

    // add columns to the worksheet
    ws.columns = [
      { header: "NOMOR TRANSAKSI", key: "paymentRef", width: 50 },
      { header: "STATUS PEMBAYARAN", key: "paymentStatus", width: 35 },
      { header: "METODE PEMBAYARAN", key: "paymentMethod", width: 35 },
      { header: "NOMINAL TRANSAKSI", key: "paymentTotalAmount", width: 35 },
      { header: "NAMA AKUN", key: "accountName", width: 35 },
      { header: "NOMOR TU", key: "storeCode", width: 35 },
      { header: "JENIS AKUN", key: "accountDetailType", width: 35 },
      { header: "NOMOR APPS2PAY", key: "paymentResponseRef", width: 35 },
      { header: "Service Fee", key: "paymentServiceFee", width: 35 },
      { header: "MDR", key: "paymentMdr", width: 35 },
      { header: "MDR FEE", key: "paymentMdrFee", width: 35 },
      { header: "MID", key: "paymentMID", width: 35 },
      { header: "TID", key: "paymentTID", width: 35 },
      { header: "NAMA MERCHANT", key: "corporateName", width: 35 },
      { header: "NAMA PETUGAS", key: "userName", width: 35 },
      { header: "SALDO AWAL", key: "cardCurrentBalance", width: 35 },
      { header: "SALDO AKHIR", key: "cardLastBalance", width: 35 },
      { header: "CARD TYPE", key: "cardType", width: 35 },
      { header: "CARD PAN", key: "cardPan", width: 35 },
      { header: "NAMA DISTRIK", key: "districtName", width: 35 },
      { header: "SN", key: "deviceSerialNumber", width: 35 },
      { header: "TANGGAL TRANSAKSI", key: "createdAt", width: 35 },
    ];

    // add data to the worksheet rows
    ws.addRows(data);

    // save the file
    workbook.xlsx.writeBuffer().then((buffer) => {
      saveAs(
        new Blob([buffer]),
        `Data Transaksi_${data[0].corporateName}_${body.dateFrom}_${body.dateTo}.xlsx`
      );
      notify("Success Export Data", "success");
    });
  };

  return (
    <CustomButton
      onClick={fetchData}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={disabled}
      color="success"
    >
      Download
    </CustomButton>
  );
};

export default ExportExcel;
