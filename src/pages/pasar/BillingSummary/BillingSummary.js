import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import { formatCurrency } from "../../../utils/format-currency";
import {
  getBillingTypeList,
  getBillingSummaryList,
  getBillingSummaryMetadata,
  getBillingSummaryCard,
} from "../../../services/pasar/billingListSummary";
import moment from "moment";
import SearchIcon from "@mui/icons-material/Search";
import PaymentsIcon from "@mui/icons-material/Payments";
import ViewListIcon from "@mui/icons-material/ViewList";
import SelectField from "../../../components/select-field";
import CustomTable from "../../../components/custom-table";
import CustomPaginationBillingSummary from "./components/custom-pagination-billing-summary";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import DatePickerField from "../../../components/datepicker-field";
import CardAmountSummary from "../../../components/pasar-components/custom-summary";
import ExportExcel from "./components/custom-export-excel-billing-summary";

const BillingSummary = ({
  label = "Billing Summary",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  // dropdown options
  const [merchantOption, setMerchantOption] = useState([]); //dropdown for merchant filter
  const [billingTypeOption, setBillingTypeOption] = useState([]); //dropdown for billing type filter

  // controlled form filter state
  const [startDate, setStartDate] = useState(moment());
  const [endDate, setEndDate] = useState(moment());
  const [ouCode, setOuCode] = useState("");
  const [billingType, setBillingType] = useState("");

  // export to excel button state
  const [disabled, setDisabled] = useState(true);

  // states for previous selected value
  const [selected, setSelected] = useState({
    startDate: "",
    endDate: "",
    ouCode: "",
    billingType: "",
  });

  // table content state
  const [data, setData] = useState([]);

  // summary card state
  const [summary, setSummary] = useState({
    totalSuccess: 0,
    amountSuccess: 0,
    totalPending: 0,
    amountPending: 0,
  });

  // pagination state
  const [pagination, setPagination] = useState({
    limit: 25,
    offset: 0,
    count: -99,
    countLoading: false,
    disableNext: true,
    disableBack: true,
  });

  // table header
  const header = [
    {
      title: "Nama Tagihan",
      value: "billingName",
      align: "left",
      width: "250px",
    },
    {
      title: "Nama Akun",
      value: "accountName",
      align: "left",
      width: "250px",
    },
    {
      title: "Nomor Akun",
      value: "storeCode",
      align: "left",
      width: "250px",
    },
    {
      title: "Nama Distrik",
      value: "districtName",
      align: "left",
      width: "280px",
    },
    {
      title: "Nama Merchant",
      value: "corporateName",
      align: "left",
      width: "200px",
    },
    {
      title: "Jumlah Tagihan Lunas",
      value: "totalSuccess",
      align: "left",
      width: "250px",
    },
    {
      title: "Nominal Tagihan Lunas",
      value: "amountSuccess",
      align: "left",
      width: "250px",
    },
    {
      title: "Jumlah Tagihan Pending",
      value: "totalPending",
      align: "left",
      width: "250px",
    },
    {
      title: "Nominal Tagihan Pending",
      value: "amountPending",
      align: "left",
      width: "250px",
    },
  ];
  // style the data for each column
  const renderCell = (item, header) => {
    switch (header.value) {
      case "amountSuccess":
        return <span>{formatCurrency(item.amountSuccess)}</span>;
      default:
        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }
  };

  // function to fetch billing type list
  const getBillingType = async (body) => {
    try {
      setLoading(true);
      const res = await getBillingTypeList(body);
      if (res.result) {
        setBillingTypeOption(
          res.result.map((item) => ({
            label: item.billingName,
            value: item.billingType,
          }))
        );
        notify("Success Fetch Billing Type", "success");
      } else {
        notify("No Billing Type Found", "warning");
        setBillingTypeOption([{ label: "-", value: "" }]);
      }
    } catch (e) {
      console.error("Error : ", e);
      notify("Failed Fetch Billing Type", "error");
      setBillingTypeOption([]);
    } finally {
      setLoading(false);
    }
  };
  // fetch table data
  const handleGetBillingSummary = async ({
    ouCodeValue,
    billingType,
    startDate,
    endDate,
    limitDt,
    offsetDt,
  }) => {
    const body = {
      outletCode: ouCodeValue,
      billingType: billingType,
      dateFrom: startDate,
      dateTo: endDate,
      limit: limitDt,
      offset: offsetDt,
    };
    setLoading(true);
    setPagination((prev) => ({ ...prev, countLoading: true }));
    // put previous selected value in a placeholder
    setSelected({
      startDate: startDate,
      endDate: endDate,
      ouCode: ouCodeValue,
      billingType: billingType,
    });

    try {
      // fetch billing summary metadata
      const res = await getBillingSummaryMetadata({
        outletCode: ouCodeValue,
        billingType: billingType,
        dateFrom: startDate,
        dateTo: endDate,
      });
      // fetch billing summary list
      const res2 = await getBillingSummaryList(body);
      if (res2.result) {
        setDisabled(false);
        setData(res2.result);
        notify(res2.message || "Success Get Data List", "success");
        setPagination((prev) => ({
          ...prev,
          count: res.result,
          disableNext: false,
          disableBack: false,
        }));
      } else {
        setPagination((prev) => ({
          ...prev,
          disableNext: true,
          disableBack: true,
        }));
        setData([]);
        notify("No Data Found", "warning");
      }
    } catch (e) {
      setData([]);
      setPagination((prev) => ({
        ...prev,
        count: -99,
        disableNext: true,
        disableBack: true,
      }));
      notify(e.message, "error");
    } finally {
      setPagination((prev) => ({
        ...prev,
        countLoading: false,
      }));
      setLoading(false);
    }
  };
  // fetch summary card data
  const handleGetSummaryCard = async (body) => {
    try {
      setLoading(true);
      const res = await getBillingSummaryCard(body);
      if (res.result) {
        setSummary({
          totalSuccess: res.result.totalSuccess,
          amountSuccess: res.result.amountSuccess,
          totalPending: res.result.totalPending,
          amountPending: res.result.amountPending,
        });
      } else {
        setSummary({
          totalSuccess: 0,
          amountSuccess: 0,
          totalPending: 0,
          amountPending: 0,
        });
      }
    } catch (e) {
      console.error("Error : ", e);
      notify("Failed Fetch Summary Cards", "error");
      setSummary({
        totalSuccess: 0,
        amountSuccess: 0,
        totalPending: 0,
        amountPending: 0,
      });
    } finally {
      setLoading(false);
    }
  };

  // handle filter data
  const handleFilterData = () => {
    setData([]);
    setPagination((prev) => ({
      ...prev,
      limit: 25,
      offset: 0,
    }));
    handleGetSummaryCard({
      outletCode: ouCode.value, //user must pick 1 ouCode
      billingType: billingType.value,
      dateFrom: startDate.format("YYYY-MM-DD"), //user must pick 1 startDate
      dateTo: endDate.format("YYYY-MM-DD"), //user must pick 1 endDate
    });
    handleGetBillingSummary({
      ouCodeValue: ouCode.value, //user must pick 1 ouCode
      billingType: billingType.value,
      startDate: startDate.format("YYYY-MM-DD"), //user must pick 1 startDate
      endDate: endDate.format("YYYY-MM-DD"), //user must pick 1 endDate
      limitDt: 25,
      offsetDt: 0,
    });
  };

  const pageChange = async (value) => {
    const pageOffset = value * pagination.limit;
    setPagination((prev) => ({
      ...prev,
      offset: pageOffset,
      disableNext: true,
      disableBack: true,
    }));
    handleGetBillingSummary({
      ouCodeValue: selected.ouCode,
      billingType: selected.billingType,
      startDate: selected.startDate,
      endDate: selected.endDate,
      limitDt: pagination.limit,
      offsetDt: pageOffset,
    });
  };
  const rowsChange = async (e) => {
    setPagination((prev) => ({
      ...prev,
      limit: e.props.value,
      offset: 0,
      disableNext: true,
      disableBack: true,
    }));
    handleGetBillingSummary({
      ouCodeValue: selected.ouCode,
      billingType: selected.billingType,
      startDate: selected.startDate,
      endDate: selected.endDate,
      limitDt: e.props.value,
      offsetDt: 0,
    });
  };

  // moving data from merchantData to merchantOption and setting the dropdown
  useEffect(() => {
    let merchantArr = []; //placeholder for erchantOption value
    merchantData.forEach((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);
  // fetch different district and username every ouCode changes
  useEffect(() => {
    if (ouCode) {
      getBillingType({
        outletCode: ouCode.value,
      });
    }
  }, [ouCode]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            {/* Title */}
            <Typography variant="h4" fontWeight="600">
              {label}
            </Typography>

            {/* filter fields */}
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: [
                  "repeat(1, 1fr)",
                  "repeat(2, 1fr)",
                  "repeat(4, 1fr)",
                ],
                gap: 3,
                marginBottom: "0rem",
                marginTop: "2rem",
              }}
            >
              {/* input */}
              <SelectField
                label={"Merchant Name"}
                placeholder="Choose A Merchant"
                sx={{ width: "100%", fontSize: "16px" }}
                data={merchantOption}
                selectedValue={ouCode}
                setValue={setOuCode}
              />
              <SelectField
                label={"Billing Type"}
                placeholder="Pick A Billing Type"
                sx={{ width: "100%", fontSize: "16px" }}
                data={billingTypeOption}
                selectedValue={billingType}
                setValue={setBillingType}
                disabled={ouCode ? false : true}
              />
              <DatePickerField
                label={"Date From"}
                placeholder="Pick A Start Date"
                maxDate={moment()}
                sx={{ width: "100%", fontSize: "16px" }}
                value={startDate}
                onChange={(newInput) => setStartDate(newInput)}
              />
              <DatePickerField
                label={"Date To"}
                placeholder="Pick An End Date"
                minDate={startDate}
                maxDate={moment()}
                sx={{ width: "100%", fontSize: "16px" }}
                value={endDate}
                onChange={(newInput) => setEndDate(newInput)}
              />
            </Box>

            {/* filter message note and filter button*/}
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              {/* filter button */}
              <Stack direction={"row"} spacing={2}>
                <ExportExcel
                  disabled={disabled}
                  setDisabled={setDisabled}
                  setLoading={setLoading}
                  notify={notify}
                  body={{
                    outletCode: selected.ouCode,
                    billingType: selected.billingType,
                    dateFrom: selected.startDate,
                    dateTo: selected.endDate,
                  }}
                  fileName={`${ouCode && ouCode.label} ${
                    billingType && billingType.label
                  } (${startDate && startDate.format("YYYY-MM-DD")}_${
                    endDate && endDate.format("YYYY-MM-DD")
                  })`}
                />
                <CustomButton
                  disabled={!startDate || !endDate || !ouCode || !billingType}
                  onClick={() => {
                    handleFilterData();
                  }}
                  startIcon={<SearchIcon size="14px" />}
                  name={buttonFilter}
                >
                  Filter
                </CustomButton>
              </Stack>
            </Stack>

            {/* summary card */}
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: ["repeat(1, 1fr)"],
                gap: 2,
                marginBottom: "0rem",
                marginTop: "2rem",
              }}
            >
              <Typography variant="h5" fontWeight="600">
                Summary
              </Typography>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: [
                    "repeat(1, 1fr)",
                    "repeat(2, 1fr)",
                    "repeat(4, 1fr)",
                  ],
                  gap: 2,
                }}
              >
                <CardAmountSummary
                  title="Total Success"
                  src={
                    <ViewListIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.totalSuccess}
                  isCurrency={false}
                />
                <CardAmountSummary
                  title="Amount Success"
                  src={
                    <PaymentsIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.amountSuccess}
                />
                <CardAmountSummary
                  title="Total Pending"
                  src={
                    <ViewListIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.totalPending}
                  isCurrency={false}
                />
                <CardAmountSummary
                  title="Amount Pending"
                  src={
                    <PaymentsIcon sx={{ fontSize: "30px", color: "#3875CA" }} />
                  }
                  amount={summary.amountPending}
                />
              </Box>
            </Box>

            {/* Data result*/}
            <Box sx={{ width: "100%", mt: 4 }}>
              <CustomPaginationBillingSummary
                disableNext={pagination.disableNext}
                disableBack={pagination.disableBack}
                countLoading={pagination.countLoading}
                limit={pagination.limit}
                offset={pagination.offset}
                count={pagination.count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
              <CustomTable
                headers={header}
                items={data}
                renderCell={renderCell}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default BillingSummary;
