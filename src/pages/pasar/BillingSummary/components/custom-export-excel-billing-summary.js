import React from "react";
import { saveAs } from "file-saver";
import { getBillingSummaryList } from "../../../../services/pasar/billingListSummary";
import { formatCurrency } from "../../../../utils/format-currency";
import ExcelJS from "exceljs";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";

const ExportExcel = ({
  disabled,
  setDisabled,
  notify,
  body,
  setLoading,
  fileName,
}) => {
  const fetchData = async () => {
    setDisabled(true);
    setLoading(true);
    try {
      const res = await getBillingSummaryList(body);
      if (res.result) {
        const formattedData = res.result.map((item) => ({
          ...item,
          amountSuccess: formatCurrency(item.amountSuccess),
          amountPending: formatCurrency(item.amountPending),
        }));
        exportHandler(formattedData);
      } else {
        notify("No Data Found", "error");
      }
    } catch (e) {
      console.error("Error :", e);
      notify("Fetch Error", "error");
    } finally {
      setDisabled(false);
      setLoading(false);
    }
  };

  const exportHandler = (data) => {
    console.log(body);
    const workbook = new ExcelJS.Workbook();
    const ws = workbook.addWorksheet("Laporan Billing Summary");

    // add columns to the worksheet
    ws.columns = [
      { header: "KODE WAJIB RETRIBUSI", key: "accountDetailCode", width: 35 },
      { header: "NAMA AKUN", key: "accountName", width: 35 },
      { header: "NOMOR AKUN", key: "storeCode", width: 35 },
      { header: "NAMA DISTRIK", key: "districtName", width: 50 },
      { header: "NAMA MERCHANT", key: "corporateName", width: 25 },
      { header: "JUMLAH TAGIHAN LUNAS", key: "totalSuccess", width: 25 },
      { header: "NOMINAL TAGIHAN LUNAS", key: "amountSuccess", width: 35 },
      { header: "JUMLAH TAGIHAN PENDING", key: "totalPending", width: 25 },
      { header: "NOMINAL TAGIHAN PENDING", key: "amountPending", width: 35 },
    ];

    // add data to the worksheet rows
    ws.addRows(data);

    // save the file
    workbook.xlsx.writeBuffer().then((buffer) => {
      saveAs(new Blob([buffer]), `${fileName}.xlsx`);
      notify("Success Export Data", "success");
    });
  };

  return (
    <CustomButton
      onClick={fetchData}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={disabled}
      color="success"
    >
      Download
    </CustomButton>
  );
};

export default ExportExcel;
