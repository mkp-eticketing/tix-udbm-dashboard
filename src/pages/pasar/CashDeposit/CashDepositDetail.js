import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import SelectField from "../../../components/select-field";
import SearchIcon from "@mui/icons-material/Search";
import moment from "moment";
import {
  getCashDepositDetail,
  getCashDepositDetailMetadata,
  getSummaryCashDepositDetail,
} from "../../../services/pasar/cash-deposit";
import { formatCurrency } from "../../../utils/format-currency";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import PaymentsIcon from "@mui/icons-material/Payments";
import ViewListIcon from "@mui/icons-material/ViewList";
import CardAmountSummary from "../../../components/pasar-components/custom-summary";

const CashDepositDetail = ({
  depositCode,
  setDepositCode,
  label = "Cash Deposit Detail Page",
  titleInfo = "To Display Specific Transactions, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  // dropdown options
  const [merchantOption, setMerchantOption] = useState([]); //dropdown for merchant filter

  //filter state
  const [ouCode, setOuCode] = useState(""); //controlled form
  const [selectedOuCode, setSelectedOucode] = useState("");

  // table state
  const [data, setData] = useState([]); //table content
  const [allMerchant, setAllMerchant] = useState([]); //all merchant's ouCode

  // summary state
  const [summary, setSummary] = useState({
    totalCashTrx: "",
    amountCashTrx: "",
  });

  // pagination state
  const [pagination, setPagination] = useState({
    limit: 25,
    offset: 0,
    count: -99,
    countLoading: false,
    disableNext: false,
  });

  // table header
  const header = [
    {
      title: "Deposit Code",
      value: "depositCode",
      align: "left",
      width: "200px",
    },
    {
      title: "Deposit Date",
      value: "createdDate",
      align: "left",
      width: "200px",
    },
    {
      title: "Billing ID",
      value: "billingID",
      align: "left",
      width: "200px",
    },
    {
      title: "npnwrd",
      value: "npnwrd",
      align: "left",
      width: "200px",
    },
    {
      title: "Daily Amount",
      value: "dailyAmount",
      align: "left",
      width: "200px",
    },
    {
      title: "Outstanding Deposit",
      value: "outstandingDeposit",
      align: "left",
      width: "220px",
    },
    {
      title: "Total",
      value: "totalDeposit",
      align: "left",
      width: "200px",
    },
    {
      title: "Account Code",
      value: "accountDetailCode",
      align: "left",
      width: "250px",
    },
    {
      title: "Name",
      value: "accountName",
      align: "left",
      width: "200px",
    },
    {
      title: "Store Code",
      value: "storeCode",
      align: "left",
      width: "200px",
    },
  ];

  // style the data for each column
  const renderCell = (item, header) => {
    switch (header.value) {
      case "createdDate":
        return <span>{moment(item.createdDate).format("Do MMMM YYYY")}</span>;
      case "dailyAmount":
        return <span>{formatCurrency(item.dailyAmount)}</span>;
      case "outstandingDeposit":
        return <span>{formatCurrency(item.outstandingDeposit)}</span>;
      case "totalDeposit":
        return <span>{formatCurrency(item.totalDeposit)}</span>;
      default:
        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    }
  };

  // fetch table data
  const handleGetCashDepositDetail = ({ ouCodeValue, limitDt, offsetDt }) => {
    let countResult = 0;
    const data = {
      outletCode: ouCodeValue,
      depositCode,
      limit: limitDt,
      offset: offsetDt,
    };
    setLoading(true);
    setPagination((prev) => ({ ...prev, countLoading: true }));
    setSelectedOucode(ouCodeValue);

    getCashDepositDetailMetadata({
      outletCode: ouCodeValue,
      depositCode,
    })
      .then((res) => {
        countResult = res.result;
        setPagination((prev) => ({
          ...prev,
          count: countResult,
          disableNext: false,
        }));
      })
      .catch((e) => {
        setPagination((prev) => ({ ...prev, count: -99 }));
      })
      .finally(() => {
        setPagination((prev) => ({ ...prev, countLoading: false }));
      });

    getCashDepositDetail(data)
      .then((res) => {
        if (res.result) {
          setData(res.result);
          notify(res.message || "Success Get Data List", "success");
        } else {
          setPagination((prev) => ({ ...prev, disableNext: true }));
          setData([]);
          notify("No Data Found", "warning");
        }
        setLoading(false);
      })
      .catch((e) => {
        setData([]);
        setPagination((prev) => ({ ...prev, disableNext: true }));
        setLoading(false);
        notify(e.message, "error");
      });
  };

  // fetch summary data
  const handleGetSummary = async (body) => {
    try {
      const res = await getSummaryCashDepositDetail(body);
      setSummary({
        totalCashTrx: res.result.totalCashTrx,
        amountCashTrx: res.result.amountCashTrx,
      });
    } catch (e) {
      console.error("Error : ", e);
    }
  };

  const pageChange = async (value) => {
    var pageOffset = value * pagination.limit;
    setPagination((prev) => ({ ...prev, offset: pageOffset }));
    handleGetCashDepositDetail({
      limitDt: pagination.limit,
      offsetDt: pageOffset,
      ouCodeValue: selectedOuCode,
    });
  };

  const rowsChange = async (e) => {
    setPagination((prev) => ({ ...prev, limit: e.props.value, offset: 0 }));
    handleGetCashDepositDetail({
      limitDt: e.props.value,
      offsetDt: 0,
      ouCodeValue: selectedOuCode,
    });
  };

  // moving data from merchantData to merchantOption and setting the dropdown
  useEffect(() => {
    let merchantArr = []; //placeholder for erchantOption value
    merchantData.forEach((item) => {
      merchantArr.push({
        label: item.ouName,
        value: item.ouCode,
      });
    });
    setMerchantOption(merchantArr);
  }, [merchantData]);

  // if the dropdown is not empty then we will extract the ouCode only to the ouCodeArr,
  //allMerchant === ouCodeArr
  // we also fetch all of the data using the extracted ouCode
  useEffect(() => {
    if (merchantOption.length > 0) {
      let ouCodeArr = []; //ouCode buat awal component mount
      merchantOption.forEach((item) => {
        ouCodeArr.push(item.value);
      });
      setAllMerchant(ouCodeArr);
      handleGetCashDepositDetail({
        limitDt: pagination.limit,
        offsetDt: 0,
        ouCodeValue: ouCodeArr,
      });
      handleGetSummary({
        outletCode: ouCodeArr,
        depositCode,
      });
    }

    console.log(depositCode);
  }, [merchantOption]);

  return (
    <Stack direction={"column"} p={"2rem"}>
      <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
        <CardContent sx={{ p: "2rem" }}>
          <Box display="flex" flexDirection="column">
            <Box display={"flex"} justifyContent={"space-between"}>
              {/* Title */}
              <Typography variant="h4" fontWeight="600">
                {label}
              </Typography>
              <CustomButton
                onClick={() => {
                  setDepositCode("");
                }}
                name="Back"
              >
                Back
              </CustomButton>
            </Box>

            {/* filter and filter button */}
            <Stack
              display="flex"
              direction="column"
              mt={"2rem"}
              mb={"0rem"}
              gap={2}
            >
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: [
                    "repeat(1, 1fr)",
                    "repeat(1, 1fr)",
                    "1fr 1fr 1fr auto",
                  ],
                  gap: 2,
                }}
              >
                {/* input */}
                <SelectField
                  label={"Merchant Name"}
                  placeholder="All Merchant Name"
                  sx={{ width: "100%", fontSize: "16px" }}
                  data={merchantOption}
                  selectedValue={ouCode}
                  setValue={setOuCode}
                />
              </Box>
            </Stack>

            {/* filter message note */}
            <Stack
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: ["column", "row"],
                alignItems: ["end", "center"],
                gap: 3,
                justifyContent: "space-between",
                mt: 4,
              }}
            >
              <FilterMessageNote
                sx={{
                  width: ["100%", "50%"],
                }}
                title={titleInfo}
                subtitle={subTitleInfo}
              />
              {/* filter button */}
              <CustomButton
                onClick={() => {
                  setData([]);
                  setPagination((prev) => ({
                    ...prev,
                    limit: 25,
                    offset: 0,
                  }));
                  handleGetCashDepositDetail({
                    ouCodeValue: ouCode ? [ouCode.value] : allMerchant,
                    limitDt: pagination.limit,
                    offsetDt: 0,
                  });
                  handleGetSummary({
                    outletCode: ouCode ? [ouCode.value] : allMerchant,
                    depositCode,
                  });
                }}
                startIcon={<SearchIcon size="14px" />}
                name={buttonFilter}
              >
                Filter
              </CustomButton>
            </Stack>

            {/* summary card */}
            <Stack
              display="flex"
              direction="column"
              mt={"2rem"}
              mb={"0rem"}
              gap={2}
            >
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: ["repeat(1, 1fr)"],
                  gap: 2,
                }}
              >
                <Typography variant="h5" fontWeight="600">
                  Summary
                </Typography>
                <Box
                  sx={{
                    display: "grid",
                    gridTemplateColumns: [
                      "repeat(1, 1fr)",
                      "repeat(2, 1fr)",
                      "repeat(4, 1fr)",
                    ],
                    gap: 2,
                  }}
                >
                  <CardAmountSummary
                    title="Total Cash Trx"
                    src={
                      <ViewListIcon
                        sx={{ fontSize: "30px", color: "#3875CA" }}
                      />
                    }
                    amount={summary.totalCashTrx}
                    isCurrency={false}
                  />
                  <CardAmountSummary
                    title="amount cash trx"
                    src={
                      <PaymentsIcon
                        sx={{ fontSize: "30px", color: "#3875CA" }}
                      />
                    }
                    amount={summary.amountCashTrx}
                  />
                </Box>
              </Box>
            </Stack>

            {/* Data result*/}
            <Box sx={{ width: "100%", mt: 4 }}>
              <CustomPagination
                disableNext={pagination.disableNext}
                countLoading={pagination.countLoading}
                limit={pagination.limit}
                offset={pagination.offset}
                count={pagination.count}
                pageChange={(event, v) => pageChange(v)}
                rowsChange={(event, e) => rowsChange(e)}
              />
              <CustomTable
                headers={header}
                items={data}
                renderCell={renderCell}
              />
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Stack>
  );
};

export default CashDepositDetail;
