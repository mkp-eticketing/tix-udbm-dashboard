import React, { useState } from "react";
import CashDeposit from "./CashDeposit";
import CashDepositDetail from "./CashDepositDetail";

const CashDepositBTJG = () => {
  const [depositCode, setDepositCode] = useState("");
  if (depositCode) {
    return (
      <CashDepositDetail
        merchantData={[
          { ouName: "TEST", ouCode: "184.PS.001" },
          { ouName: "TEST2", ouCode: "184.PS.002" },
          { ouName: "TEST3", ouCode: "184.PS.003" },
          { ouName: "TEST4", ouCode: "188.PS.004" },
        ]}
        depositCode={depositCode}
        setDepositCode={setDepositCode}
      />
    );
  }
  return (
    <CashDeposit
      merchantData={[
        { ouName: "TEST", ouCode: "184.PS.001" },
        { ouName: "TEST2", ouCode: "184.PS.002" },
        { ouName: "TEST3", ouCode: "184.PS.003" },
        { ouName: "TEST4", ouCode: "188.PS.004" },
      ]}
      setDepositCode={setDepositCode}
    />
  );
};

export default CashDepositBTJG;
