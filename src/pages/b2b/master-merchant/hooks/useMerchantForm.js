import { useFormik } from "formik";
import * as yup from "yup";
import { addMerchant, updateMerchant } from "../../../../services/b2b/merchant";
import moment from "moment";
import { useState } from "react";

const initialValues = {
    "id": "",
    "merchantCode": "",
    "businessType": "",
    "name": "",
    "description": "",
    "email": "",
    "phone": "",
    "website": "",
    "address": "",
    "profilePicture": null
}
const UseMerchantForm = ({
    notify = () => { },
    callback = () => { },
    username = "admin",
    isUpdate = false
}) => {
    const [preview, setPreview] = useState(null)
    const [loading, setLoading] = useState(false)
    const formik = useFormik({
        initialValues,
        onSubmit: (values) => {
            setLoading(true)
            const body = parseInitValueToBody(values)
            if (isUpdate) {
                updateMerchant(body).then((_res) => {
                    notify("Success update merchant", "success");
                    callback();
                }).catch((error) => {
                    notify(JSON.stringify(error), "error");
                }).finally(() => {
                    setLoading(false)
                })
            } else {
                addMerchant(body).then((_res) => {
                    notify("Success add merchant", "success");
                    callback();
                }).catch((error) => {
                    notify(JSON.stringify(error), "error");
                }).finally(() => {
                    setLoading(false)
                })
            }
        },
        validationSchema: yup.object({
            businessType: yup.string().required("Business Type is required"),
            name: yup.string().required("Name is required"),
            description: yup.string().required("Description is required"),
            email: yup.string().required("Email is required"),
            phone: yup.string().required("Phone is required"),
            website: yup.string().required("Website is required"),
            address: yup.string().required("Address is required"),
            profilePicture: yup.mixed().required("Profile Picture is required"),
        }),
        enableReinitialize: true,
    });

    const parseInitValueToBody = (values) => {
        let formData = new FormData();
        formData.append('merchantCode', values.merchantCode);
        formData.append('businessType', values.businessType);
        formData.append('name', values.name);
        formData.append('description', values.description);
        formData.append('email', values.email);
        formData.append('phone', values.phone);
        formData.append('website', values.website);
        formData.append('address', values.address);
        formData.append('profilePicture', values.profilePicture);
        if (values.id) {
            formData.append('id', values.id);
        }
        return formData
    }

    const handleUploadImage = (e) => {
        if (!e.target.files[0]) {
            ref.current.value = null;
            return;
        }
        let imagePreview = URL.createObjectURL(e.target.files[0]);
        let data = e.target.files[0];
        formik.setFieldValue("profilePicture", data)
        setPreview(imagePreview)
    };

    return {
        ...formik,
        handleUploadImage,
        preview,
        loading
    }
}

export default UseMerchantForm;