import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import CustomButton from "../../../components/custom-button";
import CustomPagination from "../../../components/custom-pagination";
import CustomTable from "../../../components/custom-table";
import FilterMessageNote from "../../../components/filter-message-note";
import InputField from "../../../components/text-field";
import { deleteMerchantByID, getMerchantList, getMerchantListRecord } from "../../../services/b2b/merchant";
// import MemberForm from "./forms/member-form";
import { Delete, Edit } from "@mui/icons-material";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import StatusLabel from "../../../components/status-label";
import { merchant_data } from "../../../data/merchant";
import { dateFormatWithTime } from "../../../utils/dateformat";
import AddMerchantB2B from "./forms/add-merchant";
import ConfirmModal from "../../../components/confirm-modal";

const MasterProductB2B = ({
    label = "Master Merchant",
    titleInfo = "To Display Specific Transactions, Use the Filters Above.",
    subTitleInfo = [],
    merchantData = merchant_data,
    setLoading = () => { },
    notify = () => { },
    buttomFilter = "Search",
    buttonAdd = "Add Merchant",
    username = "admin"
}) => {
    const [merchantOption, setMerchantOption] = useState([])
    const [ouCode, setOuCode] = useState("")
    const [openDelete, setOpenDelete] = useState(false)
    const [limit, setLimit] = useState(25);
    const [offset, setOffset] = useState(0);
    const [ouCodeSelected, setOuCodeSelected] = useState([]);
    const [count, setCount] = useState(-99);
    const [countLoading, setCountLoading] = useState(false);
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [disableNext, setDisableNext] = useState(false);
    const [openForm, setOpenForm] = useState(false);
    const [search, setSearch] = useState("")
    const [data, setData] = useState([]);
    const [merchantId, setMerchantId] = useState(null)
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(25)
    const header = [
        {
            title: "#",
            value: "id",
            align: "left",
            width: "50px",
        },
        {
            title: "MERCHANT CODE",
            value: "merchantCode",
            align: "center",
            width: "200px",
        },
        {
            title: "OUTLET CODE",
            value: "outletCode",
            align: "left",
            width: "200px",
        },
        {
            title: "BUSINESS TYPE",
            value: "businessType",
            align: "left",
            width: "250px",
        },
        {
            title: "NAME",
            value: "name",
            align: "left",
            width: "200px",
        },
        {
            title: "DESCRIPTION",
            value: "description",
            align: "left",
            width: "200px",
        },
        {
            title: "EMAIL",
            value: "email",
            align: "left",
            width: "200px",
        },
        {
            title: "PHONE",
            value: "phone",
            align: "left",
            width: "200px",
        },
        {
            title: "WEBSITE",
            value: "website",
            align: "left",
            width: "200px",
        },
        {
            title: "ADDRESS",
            value: "address",
            align: "left",
            width: "250px",
        },
        {
            title: "PROFILE PICTURE",
            value: "profilePicture",
            align: "left",
            width: "200px",
        },
        {
            title: "STATUS",
            value: "status",
            align: "left",
            width: "200px",
        },
        {
            title: "JOIN DATE",
            value: "joinDate",
            align: "left",
            width: "200px",
        },
        {
            title: "RATING",
            value: "rating",
            align: "left",
            width: "200px",
        },
        {
            title: "TICKET SOLD",
            value: "ticketSold",
            align: "left",
            width: "200px",
        },
        {
            title: "VERIFIED",
            value: "isVerified",
            align: "left",
            width: "200px",
        },
        {
            title: "CREATED AT",
            value: "createdAt",
            align: "left",
            width: "200px",
        },
        {
            title: "ACTION",
            value: "action",
            align: "left",
            width: "200px",
        },
    ]

    const renderCell = (item, header, index) => {
        if (header.value === "id") {
            return <span>{(index + 1) + ((page - 1) * pageSize)}</span>;
        } else if (header.value === "merchantCode") {
            return <span>{item.merchantCode}</span>;
        } else if (header.value === "outletCode") {
            return <span>{item.outletCode || "-"}</span>;
        } else if (header.value === "businessType") {
            return <span>{item.businessType}</span>;
        } else if (header.value === "name") {
            return <span>{item.name}</span>;
        } else if (header.value === "description") {
            return <span
                style={{
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    WebkitLineClamp: 3,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                }}
                dangerouslySetInnerHTML={{ __html: item.description }}
            />;
        } else if (header.value === "email") {
            return <span>{item.email}</span>;
        } else if (header.value === "phone") {
            return <span>{item.phone}</span>;
        } else if (header.value === "website") {
            return <span>{item.website}</span>;
        } else if (header.value === "address") {
            return <span>{item.address}</span>;
        } else if (header.value === "profilePicture") {
            return <span>{item.profilePicture}</span>;
        } else if (header.value === "status") {
            if (item.status === "Active") {
                return (
                    <StatusLabel
                        variant="active"
                        icon={<CheckCircleIcon sx={{ color: "white" }} />}
                        label="ACTIVE"
                    />
                )
            } else {
                return (
                    <StatusLabel
                        variant="inactive"
                        icon={<HighlightOffIcon sx={{ color: "white" }} />}
                        label="INACTIVE"
                    />
                )
            }
        } else if (header.value === "joinDate") {
            return <span>{dateFormatWithTime(item.joinDate)}</span>;
        } else if (header.value === "rating") {
            return <span>{item.rating}</span>;
        } else if (header.value === "ticketSold") {
            return <span>{item.ticketSold}</span>;
        } else if (header.value === "isVerified") {
            if (item.isVerified === true) {
                return (
                    <StatusLabel
                        variant="active"
                        icon={<CheckCircleIcon sx={{ color: "white" }} />}
                        label="VERIFIED"
                    />
                )
            } else {
                return (
                    <StatusLabel
                        variant="inactive"
                        icon={<HighlightOffIcon sx={{ color: "white" }} />}
                        label="NOT VERIFIED"
                    />
                )
            }
        } else if (header.value === "createdAt") {
            return <span>{dateFormatWithTime(item.createdAt)}</span>;
        } else if (header.value === "action") {
            return (
                <Stack direction={"row"} alignItems={"center"} gap={2}>
                    <Box
                        onClick={() => {
                            setMerchantId(item.id)
                            setOpenForm(true)
                        }}
                        sx={{
                            p: 1,
                            bgcolor: "#d0ebf5",
                            borderRadius: "12px",
                            cursor: "pointer"
                        }}
                    >
                        <Edit sx={{ color: "blue" }} />
                    </Box>
                    <Box
                        onClick={() => {
                            setMerchantId(item.id)
                            setOpenDelete(true)
                        }}
                        sx={{
                            p: 1,
                            bgcolor: "#f7d0cd",
                            borderRadius: "12px",
                            cursor: "pointer"
                        }}
                    >
                        <Delete sx={{ color: "red" }} />
                    </Box>
                </Stack>
            )
        }

        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    };

    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    const handleGetListMerchant = ({
        page,
        pageSize,
        search
    }) => {
        let countResult = 0;
        let data = {
            "search": search,
            "pagination": {
                "page": page,
                "pageSize": pageSize
            }
        }
        setLoading(true);
        getMerchantListRecord({}).then((res) => {
            countResult = res.result;
            setDisableNext(false);
            setCountLoading(false);
            setCount(countResult)
        }).catch((e) => {
            setCount(-99);
            setCountLoading(false);
            setCount(countResult)
        })
        getMerchantList(data).then((res) => {
            if (res.result) {
                setData(res.result)
                notify(res.message || "Success Get Data List", "success");
            } else {
                setDisableNext(true);
                setData([]);
                notify("No Data Found", "warning");
            }
            setLoading(false)
        }).catch((e) => {
            setData([]);
            setDisableNext(true);
            setLoading(false)
            notify(e.message, "error");
        })
    }

    const handleDeleteMerchant = () => {
        setLoading(true);
        deleteMerchantByID({
            id: merchantId
        }).then((res) => {
            if (res.result) {
                notify(res.message || "Success Delete Data List", "success");
            } else {
                notify("No Data Found", "warning");
            }
        }).catch((e) => {
            notify(e.message, "error");
        }).finally(() => {
            setLoading(false)
            setOpenDelete(false)
            setMerchantId(null)
            handleGetListMerchant({ page: page, pageSize: pageSize, search });
        })
    }

    const pageChange = async (value) => {
        setPage(value + 1);
        handleGetListMerchant({ page: value + 1, pageSize: pageSize, search: search });
    };

    const rowsChange = async (e) => {
        setPageSize(e.props.value);
        handleGetListMerchant({ page: 1, pageSize: e.props.value, search: search });
    };

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetListMerchant({ page: page, pageSize: pageSize, search });
    }

    if (openForm) {
        return (
            <Stack direction={"column"} p={"2rem"}>
                <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                    <CardContent sx={{ p: "2rem" }}>
                        <Box display="flex" flexDirection="column">
                            <Typography variant="h4" fontWeight="600">
                                {
                                    merchantId ? "Update Merchant" : "Add New Merchant"
                                }
                            </Typography>
                            <AddMerchantB2B
                                onOpen={setOpenForm}
                                merchantId={merchantId}
                                setMerchantId={setMerchantId}
                                merchantData={merchantOption}
                                notify={notify}
                                refreshData={() => {
                                    refreshData();
                                }}
                            />
                        </Box>
                    </CardContent>
                </Card>
            </Stack>
        )
    }

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                        <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                            <Box sx={{
                                display: "grid",
                                gridTemplateColumns: ["repeat(1, 1fr)", "repeat(4, 1fr)"],
                                gap: 2
                            }}>
                                {/* <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={merchantOption}
                                    selectedValue={ouCode}
                                    setValue={setOuCode}
                                /> */}
                                <InputField
                                    label={"Merchant"}
                                    placeholder="Type merchant name"
                                    onChange={(e) => setSearch(e.target.value)}
                                    value={search}
                                />
                            </Box>
                        </Stack>
                        <Stack sx={{
                            width: "100%",
                            display: "flex",
                            flexDirection: ["column", "row"],
                            alignItems: ["end", "center"],
                            gap: 3,
                            justifyContent: "space-between",
                            mt: 8
                        }}>
                            <FilterMessageNote
                                sx={{
                                    width: ["100%", "50%"]
                                }}
                                title={titleInfo}
                                subtitle={subTitleInfo}
                            />
                            <div style={{
                                display: "flex",
                                gap: 3
                            }}>
                                <CustomButton
                                    onClick={() => {
                                        setPageSize(25)
                                        setPage(1)
                                        handleGetListMerchant({ page: 1, pageSize: pageSize, search });
                                    }}
                                    startIcon={<SearchIcon size="14px" />}
                                    name={buttomFilter}
                                >
                                    Filter
                                </CustomButton>
                            </div>
                        </Stack>
                        <Box sx={{ width: "100%", mt: 10 }}>
                            <CustomButton
                                onClick={() => setOpenForm(true)}
                                startIcon={<AddIcon size="14px" />}
                                name={buttonAdd}
                            />
                            <CustomPagination
                                disableNext={disableNext}
                                countLoading={countLoading}
                                limit={pageSize}
                                offset={(page - 1) * pageSize}
                                count={count}
                                pageChange={(event, v) => pageChange(v)}
                                rowsChange={async (event, e) => rowsChange(e)}
                            />
                            <CustomTable
                                headers={header}
                                items={data}
                                renderCell={renderCell}
                                enableNumber={true}
                            />
                            <CustomPagination
                                disableNext={disableNext}
                                countLoading={countLoading}
                                limit={pageSize}
                                offset={(page - 1) * pageSize}
                                count={count}
                                pageChange={(event, v) => pageChange(v)}
                                rowsChange={async (event, e) => rowsChange(e)}
                            />
                        </Box>
                    </Box>
                </CardContent>
            </Card>
            <ConfirmModal
                title="Delete Confirmation"
                description="Are you sure to delete?"
                open={openDelete}
                handleClose={() => setOpenDelete(false)}
                handleConfirm={() => handleDeleteMerchant()}
            />
        </Stack>
    )
}

export default MasterProductB2B