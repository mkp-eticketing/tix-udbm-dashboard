import { Person } from "@mui/icons-material";
import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import { Box, Stack, Typography } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import CustomButton from "../../../../components/custom-button";
import EditorField from "../../../../components/editor-field";
import InputField from "../../../../components/text-field";
import { getMerchantByID } from "../../../../services/b2b/merchant";
import UseMerchantForm from "../hooks/useMerchantForm";

const AddMerchantB2B = ({
    onOpen = () => { },
    merchantId,
    setMerchantId = () => { },
    notify,
    refreshData = () => { }
}) => {
    const ref = useRef()
    const formik = UseMerchantForm({
        notify: notify,
        callback: () => {
            onOpen(false)
            setMerchantId(null);
            refreshData()
        },
        isUpdate: merchantId ? true : false,
    })

    useEffect(() => {
        if (merchantId) {
            getDetailById(merchantId)
        }
    }, [merchantId]);

    const getDetailById = (id) => {
        getMerchantByID({ id: id }).then((res) => {
            let data = res.result;
            formik.setValues({
                id: data.id,
                address: data.address,
                businessType: data.businessType,
                description: data.description,
                email: data.email,
                merchantCode: data.merchantCode,
                name: data.name,
                phone: data.phone,
                profilePicture: data.profilePicture,
                website: data.website
            })
        }).catch((e) => {
            notify(e.message, "error");
        })
    }

    return (
        <Box
            component="form"
            onSubmit={formik.handleSubmit}
            sx={{
                display: "flex",
                flexDirection: "column",
                mt: "2rem",
                gap: 2
            }}>
            <Box sx={{
                display: "grid",
                gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)"],
                gap: 2
            }}>
                <InputField
                    label={"Business Type"}
                    required={true}
                    placeholder="Type business type"
                    value={formik.values.businessType}
                    onChange={(e) => formik.setFieldValue("businessType", e.target.value)}
                    isError={formik.touched.businessType && formik.errors.businessType}
                    errorMessage={formik.touched.businessType && formik.errors.businessType}
                />
                <InputField
                    label={"Merchant Code"}
                    placeholder="Type merchant code"
                    value={formik.values.merchantCode}
                    onChange={(e) => formik.setFieldValue("merchantCode", e.target.value)}
                    isError={formik.touched.merchantCode && formik.errors.merchantCode}
                    errorMessage={formik.touched.merchantCode && formik.errors.merchantCode}
                />
                <InputField
                    label={"Name"}
                    required={true}
                    placeholder="Type merchant name"
                    value={formik.values.name}
                    onChange={(e) => formik.setFieldValue("name", e.target.value)}
                    isError={formik.touched.name && formik.errors.name}
                    errorMessage={formik.touched.name && formik.errors.name}
                />
                <InputField
                    label={"Email"}
                    required={true}
                    type="email"
                    placeholder="Type merchant email"
                    value={formik.values.email}
                    onChange={(e) => formik.setFieldValue("email", e.target.value)}
                    isError={formik.touched.email && formik.errors.email}
                    errorMessage={formik.touched.email && formik.errors.email}
                />
                <InputField
                    label={"Website"}
                    required={true}
                    placeholder="Type merchant website"
                    value={formik.values.website}
                    onChange={(e) => formik.setFieldValue("website", e.target.value)}
                    isError={formik.touched.website && formik.errors.website}
                    errorMessage={formik.touched.website && formik.errors.website}
                />
                <InputField
                    label={"Phone"}
                    required={true}
                    placeholder="Type merchant phone"
                    value={formik.values.phone}
                    onChange={(e) => formik.setFieldValue("phone", e.target.value)}
                    isError={formik.touched.phone && formik.errors.phone}
                    errorMessage={formik.touched.phone && formik.errors.phone}
                />
                <div className="hidden">
                    <input
                        type="file"
                        accept="image/png, image/gif, image/jpeg"
                        ref={ref}
                        name="upload-image"
                        onChange={(e) =>
                            formik.handleUploadImage(e)
                        }
                    />
                </div>
                <Box sx={{ display: "flex", flexDirection: "column", gap: 1, width: "100%" }}>
                    <Typography color={"rgb(71 85 105)"} fontSize="0.875rem" fontWeight={"600"} height={25}>Upload Image Profile <span style={{ color: 'red' }}>*</span></Typography>
                    <Box
                        onClick={() => ref.current.click()}
                        sx={{
                            width: "200px",
                            height: "200px",
                            borderStyle: "solid",
                            borderWidth: "1px",
                            borderColor: "gray",
                            display: "flex",
                            flexDirection: "column",
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: "12px",
                            cursor: "pointer"
                        }}>
                        {
                            formik.preview ?
                                <img
                                    src={formik.preview}
                                    alt="avatar"
                                    style={{
                                        width: "100%",
                                        height: "100%",
                                        objectFit: "cover",
                                        borderRadius: "12px"
                                    }}
                                /> : formik.values.profilePicture ?
                                    <img
                                        src={formik.values.profilePicture}
                                        alt="avatar"
                                        style={{
                                            width: "100%",
                                            height: "100%",
                                            objectFit: "cover",
                                            borderRadius: "12px"
                                        }}
                                    /> :
                                    <Person sx={{ fontSize: "100px" }} />
                        }
                    </Box>
                    <Typography fontSize={12} color="red">{(formik.touched.profilePicture && formik.errors.profilePicture) && (formik.touched.profilePicture && formik.errors.profilePicture)}</Typography>
                </Box>
                {/* <InputField
                    label={"Upload Profile"}
                    required={true}
                    type="file"
                    accept="image/png, image/gif, image/jpeg"
                    placeholder="Type merchant phone"
                    // value={formik.values.profilePicture}
                    onChange={(e) => formik.handleUploadImage(e)}
                    isError={formik.touched.profilePicture && formik.errors.profilePicture}
                    errorMessage={formik.touched.profilePicture && formik.errors.profilePicture}
                /> */}
            </Box>
            <InputField
                label={"Address"}
                required={true}
                multiline={true}
                rows={5}
                placeholder="Type merchant address"
                value={formik.values.address}
                onChange={(e) => formik.setFieldValue("address", e.target.value)}
                isError={formik.touched.address && formik.errors.address}
                errorMessage={formik.touched.address && formik.errors.address}
            />
            <EditorField
                label={"Description"}
                required={true}
                isError={formik.touched.description && formik.errors.description}
                errorMessage={formik.touched.description && formik.errors.description}
                content={formik.values.description}
                setContent={(val) => formik.setFieldValue("description", val)}
            />
            <Stack direction="row" justifyContent="flex-end" gap={2} mt={4}>
                <CustomButton
                    onClick={() => {
                        setMerchantId(null);
                        onOpen(false)
                    }}
                    startIcon={<KeyboardBackspaceIcon size="14px" />}
                    name={"Back"}
                    sx={{
                        backgroundColor: "grey",
                        ":hover": {
                            backgroundColor: "#333333"
                        }
                    }}
                >
                    Kembali
                </CustomButton>
                <CustomButton
                    onClick={() => { }}
                    startIcon={<SaveIcon size="14px" />}
                    name={"Save"}
                    type="submit"
                    loading={formik.loading}
                >
                    Simpan
                </CustomButton>
            </Stack>
        </Box>
    )
}

export default AddMerchantB2B;