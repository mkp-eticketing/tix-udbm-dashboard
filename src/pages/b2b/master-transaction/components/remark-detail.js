import { RemoveRedEyeOutlined } from "@mui/icons-material";
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { Box, Stack } from "@mui/material";
import React, { useState } from "react";

const RemarkDetail = ({
    remark
}) => {
    const [openDetail, setOpenDetail] = useState(false);
    return (
        <Stack sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: 2
        }}>
            <span style={{
                width: "300px",
                display: openDetail ? "block" : "-webkit-box",
                WebkitBoxOrient: 'vertical',
                WebkitLineClamp: 2,
                overflow: 'hidden',
                textOverflow: 'ellipsis',
            }}>{remark}</span>
            <Box onClick={() => setOpenDetail(!openDetail)} sx={{ cursor: "pointer" }}>
                {
                    openDetail ? <RemoveRedEyeOutlined /> : <VisibilityOffIcon />
                }
            </Box>
        </Stack>
    )
}

export default RemarkDetail;