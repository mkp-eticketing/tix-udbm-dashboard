import React from "react";
import CardAmountSummary from "../../../../components/card-amount-summary";
import { Box } from "@mui/material";

const CardSummaryBoardingTicket = ({
    summaryDetail = {},
    isLoading = true,
    sidebarExpanded = false,
    revenueSrc = "",
    transactionTotalSrc = "",
    marginSrc = "",
    serviceSrc = ""
}) => {
    return (
        <Box
            sx={{
                display: "grid",
                gridTemplateColumns: "repeat(12, minmax(0, 1fr))",
                gap: "1.6rem"
            }}
        >
            <CardAmountSummary
                isLoading={isLoading}
                title="REVENUE:"
                amount={summaryDetail.revenueTotal}
                sidebarExpanded={sidebarExpanded}
                rotate={90}
                src={revenueSrc}
            />
            <CardAmountSummary
                isLoading={isLoading}
                title="TOTAL TRANSACTION:"
                amount={summaryDetail.transactionTotal}
                isCurrency={false}
                sidebarExpanded={sidebarExpanded}
                src={transactionTotalSrc}
            />
            <CardAmountSummary
                isLoading={isLoading}
                title="TOTAL MARGIN:"
                amount={summaryDetail.marginTotal}
                isCurrency={true}
                sidebarExpanded={sidebarExpanded}
                src={marginSrc}
            />
            <CardAmountSummary
                isLoading={isLoading}
                title="TOTAL SERVICE FEE:"
                amount={summaryDetail.serviceTotal}
                isCurrency={true}
                sidebarExpanded={sidebarExpanded}
                src={serviceSrc}
            />
        </Box>
    );
};

export default CardSummaryBoardingTicket;
