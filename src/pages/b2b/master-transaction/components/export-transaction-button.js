import React from "react";
import ExcelJS from "exceljs";
import { saveAs } from "file-saver";
import CustomButton from "../../../../components/custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { getTransactionReport } from "../../../../services/b2b/transaction";

const ExportTransactionButton = ({
    disabled,
    setDisabled,
    form,
    notify,
    allMerchant
}) => {
    const fetchData = async () => {
        setDisabled(true);
        let diffDay = form.dateEnd.diff(form.dateStart, 'days')
        if (diffDay < 0) {
            return notify("Date format does not match", "warning");
        }
        if (diffDay > 30) {
            return notify("Max Range of Date is 1 Month (30 Days)", "warning");
        }
        const body = {
            "keyword": form.search,
            "dateStart": form.dateStart.format("YYYY-MM-DD"), //filter by trx date
            "dateEnd": form.dateEnd.format("YYYY-MM-DD"), //filter by trx date
            "isUsingTrxDate": form.isTransactionDate,
            "paymentMethod": form.paymentMethod ? form.paymentMethod.value : "",
            "outletCode": form.ouCode ? [form.ouCode.value] : allMerchant,
        }
        try {
            const res = await getTransactionReport(body);
            exportHandler(res.result);
        } catch (e) {
            console.error("Error :", e);
        } finally {
            setDisabled(false);
        }
    };

    const exportHandler = (data) => {
        const workbook = new ExcelJS.Workbook();
        const ws = workbook.addWorksheet("Laporan Transaksi");

        // add columns to the worksheet
        ws.columns = [
            {
                header: "UUID",
                key: "batchUUID",
                width: 40,
            },
            {
                header: "TRANSACTION NUMBER",
                key: "trxNo",
                width: 30,
            },
            {
                header: "TICKET NUMBER",
                key: "ticketNo",
                width: 25,
            },
            {
                header: "MERCHANT CODE",
                key: "merchantCode",
                width: 20,
            },
            {
                header: "PRODUCT OPTION CODE",
                key: "productOptionCode",
                width: 30,
            },
            {
                header: "CUSTOMER NAME",
                key: "customerName",
                width: 20,
            },
            {
                header: "CUSTOMER EMAIL",
                key: "customerEmail",
                width: 20,
            },
            {
                header: "CUSTOMER PHONE",
                key: "customerPhone",
                width: 20,
            },
            {
                header: "PAYMENT CATEGORY",
                key: "paymentCategory",
                width: 20,
            },
            {
                header: "VISITOR ID",
                key: "visitorID",
                width: "250px",
            },
            {
                header: "VISITOR ID TYPE",
                key: "visitorIDType",
                width: 20,
            },
            {
                header: "VISITOR NAME",
                key: "visitorName",
                width: 20,
            },
            {
                header: "VISITOR NATIONALITY",
                key: "visitorNationality",
                width: 20,
            },
            {
                header: "VISITOR PHONE",
                key: "visitorPhone",
                width: 20,
            },
            {
                header: "VISITOR ADDRESS",
                key: "visitorAddress",
                width: 20,
            },
            {
                header: "VISITOR DOB",
                key: "visitorDOB",
                width: 20,
            },
            {
                header: "PUBLIC PRICE",
                key: "publicPrice",
                width: 20,
            },
            {
                header: "SERVICE FEE",
                key: "serviceFee",
                width: 20,
            },
            {
                header: "MARGIN AMOUNT",
                key: "marginAmount",
                width: 20,
            },
            {
                header: "SUB TOTAL",
                key: "subTotal",
                width: 20,
            },
            {
                header: "REMARK",
                key: "remark",
                width: 40,
            },
            {
                header: "UDB STATUS",
                key: "UDBStatus",
                width: 20,
            },
            {
                header: "UDB STATUS MESSAGE",
                key: "UDBStatusMessage",
                width: "300px",
            },
            {
                header: "TRANSACTION DATE",
                key: "trxDate",
                width: 20,
            },
            {
                header: "BOOKING DATE",
                key: "bookingDate",
                width: 20,
            },
            {
                header: "USERNAME",
                key: "username",
                width: 20,
            },
            {
                header: "MODE PAYMENT",
                key: "modePayment",
                width: 20,
            },
            {
                header: "BANK PAYMENT",
                key: "bankPayment",
                width: 20,
            },
            {
                header: "VALIDATION DATE",
                key: "validationDate",
                width: 20,
            },
            {
                header: "REC TYPE",
                key: "recType",
                width: 20,
            },
            {
                header: "SOURCE CODE",
                key: "sourceCode",
                width: 20,
            },
            {
                header: "PARTNER CODE",
                key: "partnerCode",
                width: 20,
            },
            {
                header: "CREATED AT",
                key: "createdAt",
                width: 20,
            },
        ]
        // add data to the worksheet rows
        ws.addRows(data);

        // save the file
        workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheet.sheet" }), `Laporan Transaksi-${form.dateStart.format("YYYY-MM-DD")}-${form.dateEnd.format("YYYY-MM-DD")}.xlsx`);
            notify("Success Export Data", "success");
        });
    };

    return (
        <CustomButton
            onClick={fetchData}
            startIcon={<FileDownloadIcon size="20px" />}
            name="Export&nbsp;"
            loading={disabled}
            color="success"
        >
            Download
        </CustomButton>
    );
};

export default ExportTransactionButton;
