import SearchIcon from '@mui/icons-material/Search';
import { Box, Card, CardContent, Input, InputAdornment, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import CustomButton from "../../../components/custom-button";
import CustomPagination from "../../../components/custom-pagination";
import CustomTable from "../../../components/custom-table";
import FilterMessageNote from "../../../components/filter-message-note";
import { getTransactionList, getTransactionListRecord, getTransactionSummary } from "../../../services/b2b/transaction";
import { getPaymentList } from "../../../services/pariwisata/transaction";
// import MemberForm from "./forms/member-form";
import { merchant_data } from "../../../data/merchant";
import { dateFormatWithTime } from "../../../utils/dateformat";
// import AddMerchantB2B from "./forms/add-merchant";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import Search from '@mui/icons-material/Search';
import moment from 'moment';
import DatePickerField from '../../../components/datepicker-field';
import SelectField from '../../../components/select-field';
import StatusLabel from '../../../components/status-label';
import SwitchField from '../../../components/switch-field';
import { thousandSeparator } from '../../../utils/thousand-separator';
import CardSummaryBoardingTicket from './components/card-summary-boarding-ticket';
import RemarkDetail from './components/remark-detail';
import ExportTransactionButton from './components/export-transaction-button';

const MasterTransactionB2B = ({
    label = "Master Transaction B2B",
    titleInfo = "To Display Specific Transactions, Use the Filters Above.",
    subTitleInfo = [
        "Max Range of Date is 1 Month (30 Days)",
        "Select the Merchant to Use the Terminal Filter",
        "Use the Required Filter to Search the Detail Report",
        "Revenue is total income per transaction date period",
        "Total Transaction is number of transactions per transaction date period",
        "Total Margin is total margin per transaction date period",
        "Total Service Fee is total service fee per transaction date period"
    ],
    merchantData = merchant_data,
    setLoading = () => { },
    notify = () => { },
    buttomFilter = "Search",
    sidebarExpanded = false,
    revenueSrc = "",
    transactionTotalSrc = "",
    marginSrc = "",
    serviceSrc = ""
}) => {
    const [merchantOption, setMerchantOption] = useState([])
    const [count, setCount] = useState(-99);
    const [countLoading, setCountLoading] = useState(false);
    const [allOutletCodeList, setAllOutletCodeList] = useState([])
    const [disableNext, setDisableNext] = useState(false);
    const [data, setData] = useState([]);
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(25)
    const [loadingDetail, setLoadingDetail] = useState(false);
    const [paymentOption, setPaymentOption] = useState([])
    const [totalSummary, setTotalSummary] = useState({
        revenueTotal: 0,
        transactionTotal: 0,
        marginTotal: 0,
        serviceTotal: 0
    })
    const [filterForm, setFilterForm] = useState({
        ouCode: null,
        search: "",
        paymentMethod: "",
        dateStart: moment(moment(Date.now()).subtract('days', 30).format("YYYY-MM-DD")),
        dateEnd: moment(Date.now()),
        isTransactionDate: false
    })
    const header = [
        {
            title: "NO",
            value: "id",
            align: "left",
            width: "50px",
        },
        {
            title: "UUID",
            value: "batchUUID",
            align: "center",
            width: "300px",
        },
        {
            title: "TRANSACTION NUMBER",
            value: "trxNo",
            align: "left",
            width: "300px",
        },
        {
            title: "TICKET NUMBER",
            value: "ticketNo",
            align: "left",
            width: "250px",
        },
        {
            title: "MERCHANT CODE",
            value: "merchantCode",
            align: "left",
            width: "200px",
        },
        {
            title: "PRODUCT OPTION CODE",
            value: "productOptionCode",
            align: "left",
            width: "300px",
        },
        {
            title: "CUSTOMER NAME",
            value: "customerName",
            align: "left",
            width: "200px",
        },
        {
            title: "CUSTOMER EMAIL",
            value: "customerEmail",
            align: "left",
            width: "200px",
        },
        {
            title: "CUSTOMER PHONE",
            value: "customerPhone",
            align: "left",
            width: "200px",
        },
        {
            title: "PAYMENT CATEGORY",
            value: "paymentCategory",
            align: "left",
            width: "200px",
        },
        {
            title: "VISITOR ID",
            value: "visitorID",
            align: "left",
            width: "250px",
        },
        {
            title: "VISITOR ID TYPE",
            value: "visitorIDType",
            align: "left",
            width: "200px",
        },
        {
            title: "VISITOR NAME",
            value: "visitorName",
            align: "left",
            width: "200px",
        },
        {
            title: "VISITOR NATIONALITY",
            value: "visitorNationality",
            align: "left",
            width: "200px",
        },
        {
            title: "VISITOR PHONE",
            value: "visitorPhone",
            align: "left",
            width: "200px",
        },
        {
            title: "VISITOR ADDRESS",
            value: "visitorAddress",
            align: "left",
            width: "200px",
        },
        {
            title: "VISITOR DOB",
            value: "visitorDOB",
            align: "left",
            width: "200px",
        },
        {
            title: "PUBLIC PRICE",
            value: "publicPrice",
            align: "left",
            width: "200px",
        },
        {
            title: "SERVICE FEE",
            value: "serviceFee",
            align: "left",
            width: "200px",
        },
        {
            title: "MARGIN AMOUNT",
            value: "marginAmount",
            align: "left",
            width: "200px",
        },
        {
            title: "SUB TOTAL",
            value: "subTotal",
            align: "left",
            width: "200px",
        },
        {
            title: "REMARK",
            value: "remark",
            align: "left",
            width: "400px",
        },
        {
            title: "UDB STATUS",
            value: "UDBStatus",
            align: "left",
            width: "200px",
        },
        {
            title: "UDB STATUS MESSAGE",
            value: "UDBStatusMessage",
            align: "left",
            width: "300px",
        },
        {
            title: "TRANSACTION DATE",
            value: "trxDate",
            align: "left",
            width: "200px",
        },
        {
            title: "BOOKING DATE",
            value: "bookingDate",
            align: "left",
            width: "200px",
        },
        {
            title: "USERNAME",
            value: "username",
            align: "left",
            width: "200px",
        },
        {
            title: "MODE PAYMENT",
            value: "modePayment",
            align: "left",
            width: "200px",
        },
        {
            title: "BANK PAYMENT",
            value: "bankPayment",
            align: "left",
            width: "200px",
        },
        {
            title: "VALIDATION DATE",
            value: "validationDate",
            align: "left",
            width: "200px",
        },
        {
            title: "REC TYPE",
            value: "recType",
            align: "left",
            width: "200px",
        },
        {
            title: "SOURCE CODE",
            value: "sourceCode",
            align: "left",
            width: "200px",
        },
        {
            title: "PARTNER CODE",
            value: "partnerCode",
            align: "left",
            width: "200px",
        },
        {
            title: "CREATED AT",
            value: "createdAt",
            align: "left",
            width: "200px",
        },
    ]

    const renderCell = (item, header, index) => {
        if (header.value === "id") {
            return <span>{(index + 1) + ((page - 1) * pageSize)}</span>;
        } else if (header.value === "batchUUID") {
            return <span style={{ whiteSpace: "nowrap" }}>{item.batchUUID || "-"}</span>;
        } else if (header.value === "trxNo") {
            return <span>{item.trxNo || "-"}</span>;
        } else if (header.value === "ticketNo") {
            return <span>{item.ticketNo || "-"}</span>;
        } else if (header.value === "merchantCode") {
            return <span>{item.merchantCode || "-"}</span>;
        } else if (header.value === "productOptionCode") {
            return <span>{item.productOptionCode || "-"}</span>;
        } else if (header.value === "customerName") {
            return <span>{item.customerName || "-"}</span>;
        } else if (header.value === "customerEmail") {
            return <span>{item.customerEmail || "-"}</span>;
        } else if (header.value === "customerPhone") {
            return <span>{item.customerPhone || "-"}</span>;
        } else if (header.value === "paymentCategory") {
            return <span>{item.paymentCategory || "-"}</span>;
        } else if (header.value === "visitorID") {
            return <span>{item.visitorID || "-"}</span>;
        } else if (header.value === "visitorIDType") {
            return <span>{item.visitorIDType || "-"}</span>;
        } else if (header.value === "visitorName") {
            return <span>{item.visitorName || "-"}</span>;
        } else if (header.value === "visitorNationality") {
            return <span>{item.visitorNationality || "-"}</span>;
        } else if (header.value === "visitorPhone") {
            return <span>{item.visitorPhone || "-"}</span>;
        } else if (header.value === "visitorAddress") {
            return <span>{item.visitorAddress || "-"}</span>;
        } else if (header.value === "visitorDOB") {
            return <span>{item.visitorDOB || "-"}</span>;
        } else if (header.value === "publicPrice") {
            return <span>Rp{thousandSeparator(item.publicPrice)}</span>;
        } else if (header.value === "serviceFee") {
            return <span>Rp{thousandSeparator(item.serviceFee)}</span>;
        } else if (header.value === "marginAmount") {
            return <span>Rp{thousandSeparator(item.marginAmount)}</span>;
        } else if (header.value === "subTotal") {
            return <span>Rp{thousandSeparator(item.subTotal)}</span>;
        } else if (header.value === "remark") {
            return <RemarkDetail remark={item.remark} />;
        } else if (header.value === "UDBStatus") {
            if (item.UDBStatus === "Success") {
                return (
                    <StatusLabel
                        variant="active"
                        icon={<CheckCircleIcon sx={{ color: "white" }} />}
                        label={item.UDBStatus}
                    />
                )
            } else {
                return (
                    <StatusLabel
                        variant="inactive"
                        icon={<HighlightOffIcon sx={{ color: "white" }} />}
                        label={item.UDBStatus}
                    />
                )
            }
        } else if (header.value === "UDBStatusMessage") {
            return <span>{item.UDBStatusMessage || "-"}</span>;
        } else if (header.value === "trxDate") {
            return <span>{dateFormatWithTime(item.trxDate)}</span>;
        } else if (header.value === "bookingDate") {
            return <span>{dateFormatWithTime(item.bookingDate)}</span>;
        } else if (header.value === "username") {
            return <span style={{ whiteSpace: "nowrap" }}>{item.username}</span>;
        } else if (header.value === "modePayment") {
            return <span>{item.modePayment || "-"}</span>;
        } else if (header.value === "bankPayment") {
            return <span>{item.bankPayment || "-"}</span>;
        } else if (header.value === "validationDate") {
            return <span>{dateFormatWithTime(item.validationDate)}</span>;
        } else if (header.value === "recType") {
            return <span>{item.recType || "-"}</span>;
        } else if (header.value === "sourceCode") {
            return <span>{item.sourceCode || "-"}</span>;
        } else if (header.value === "partnerCode") {
            return <span>{item.partnerCode || "-"}</span>;
        } else if (header.value === "createdAt") {
            return <span>{dateFormatWithTime(item.createdAt)}</span>;
        }

        return <span>{item[header.value] ? item[header.value] : "-"}</span>;
    };

    useEffect(() => {
        let merchantArr = [];
        merchantData.map((item) => {
            merchantArr.push({
                ...item,
                label: item.ouName,
                value: item.ouCode
            })
        })
        setMerchantOption(merchantArr);
    }, [merchantData]);

    const handleGetListTransaction = ({
        page,
        pageSize,
        form,
        allMerchant = allOutletCodeList
    }) => {
        let countResult = 0;
        let diffDay = form.dateEnd.diff(form.dateStart, 'days')
        if (diffDay < 0) {
            return notify("Date format does not match", "warning");
        }
        if (diffDay > 30) {
            return notify("Max Range of Date is 1 Month (30 Days)", "warning");
        }
        let data = {
            "keyword": form.search,
            "dateStart": form.dateStart.format("YYYY-MM-DD"), //filter by trx date
            "dateEnd": form.dateEnd.format("YYYY-MM-DD"), //filter by trx date
            "isUsingTrxDate": form.isTransactionDate,
            "paymentMethod": form.paymentMethod ? form.paymentMethod.value : "",
            "outletCode": form.ouCode ? [form.ouCode.value] : allMerchant,
            "pagination": {
                "page": page,
                "pageSize": pageSize
            }
        }
        setLoading(true);
        setLoadingDetail(true)
        getTransactionListRecord({}).then((res) => {
            countResult = res.result;
            setDisableNext(false);
            setCountLoading(false);
            setCount(countResult)
        }).catch((e) => {
            setCount(-99);
            setCountLoading(false);
            setCount(countResult)
        })
        getTransactionSummary({
            "dateStart": data.dateStart,
            "dateEnd": data.dateEnd,
            "isUsingTrxDate": form.isTransactionDate,
            "paymentMethod": form.paymentMethod ? form.paymentMethod.value : "",
            "outletCode": form.ouCode ? [form.ouCode.value] : allMerchant
        }).then((res) => {
            if (res.result) {
                setTotalSummary({
                    marginTotal: res.result.totalMargin,
                    revenueTotal: res.result.totalAmount,
                    serviceTotal: res.result.totalServiceFee,
                    transactionTotal: res.result.totalTickets
                })
            } else {
                setTotalSummary({
                    revenueTotal: 0,
                    transactionTotal: 0,
                    marginTotal: 0,
                    serviceTotal: 0
                })
            }
        }).catch((e) => {
            setTotalSummary({
                revenueTotal: 0,
                transactionTotal: 0,
                marginTotal: 0,
                serviceTotal: 0
            })
            setDisableNext(true);
        }).finally(() => {
            setLoadingDetail(false)
        })
        getTransactionList(data).then((res) => {
            if (res.result) {
                setData(res.result)
                notify(res.message || "Success Get Data List", "success");
            } else {
                setDisableNext(true);
                setData([]);
                notify("No Data Found", "warning");
            }
            setLoading(false)
        }).catch((e) => {
            setData([]);
            setDisableNext(true);
            setLoading(false)
            notify(e.message, "error");
        })
    }

    const handleGetPaymentData = async () => {
        getPaymentList()
            .then((res) => {
                if (res.success) {
                    let paymentArr = [];
                    res.result.map((item) => {
                        paymentArr.push({
                            label: item.paymentCategory,
                            value: item.paymentCategory,
                        });
                    });
                    setPaymentOption(paymentArr);
                    // console.log(res.message || "Success Get payment Data");
                    notify(res.message || "Success Get Payment Method Data", "success");
                }
            })
            .catch((error) => {
                setPaymentOption([]);
                if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                } else {
                    console.log(error.message || error);
                    notify(error.message || error, "error");
                }
            });
    };

    const pageChange = async (value) => {
        setPage(value + 1);
        handleGetListTransaction({ page: value + 1, pageSize: pageSize, form: filterForm });
    };

    const rowsChange = async (e) => {
        setPageSize(e.props.value);
        handleGetListTransaction({ page: 1, pageSize: e.props.value, form: filterForm });
    };

    useEffect(() => {
        if (merchantOption.length > 0) {
            refreshData();
        }
    }, [merchantOption]);

    const refreshData = () => {
        let ouCodeArr = []
        merchantOption.map((item) => {
            ouCodeArr.push(item.value)
        })
        setAllOutletCodeList(ouCodeArr);
        handleGetListTransaction({ page: page, pageSize: pageSize, form: filterForm, allMerchant: ouCodeArr });
        handleGetPaymentData()
    }

    return (
        <Stack direction={"column"} p={"2rem"}>
            <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
                <CardContent sx={{ p: "2rem" }}>
                    <Box display="flex" flexDirection="column">
                        <Typography variant="h4" fontWeight="600">
                            {label}
                        </Typography>
                        <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                            <Box sx={{
                                display: "grid",
                                gridTemplateColumns: ["repeat(1, 1fr)", "repeat(4, 1fr)"],
                                gap: 2
                            }}>
                                <SelectField
                                    label={"Merchant"}
                                    placeholder="All Merchant"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={merchantOption}
                                    selectedValue={filterForm.ouCode}
                                    setValue={(val) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            ouCode: val
                                        }))
                                    }}
                                />
                                <SelectField
                                    label={"Payment Method"}
                                    placeholder="All Merchant"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    data={paymentOption}
                                    selectedValue={filterForm.paymentMethod}
                                    setValue={(val) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            paymentMethod: val
                                        }))
                                    }}
                                />
                                <DatePickerField
                                    label={"Start Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    value={filterForm.dateStart}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            dateStart: newValue
                                        }))
                                    }}
                                />
                                <DatePickerField
                                    label={"End Date"}
                                    placeholder="DD MMM YYYY"
                                    sx={{ width: "100%", fontSize: "16px" }}
                                    format={"DD MMM YYYY"}
                                    value={filterForm.dateEnd}
                                    onChange={(newValue) => {
                                        setFilterForm((prev) => ({
                                            ...prev,
                                            dateEnd: newValue
                                        }))
                                    }}
                                />
                            </Box>
                            <Box sx={{
                                display: "flex",
                                flexDirection: "row",
                                gap: 2
                            }}>
                                <Box sx={{ display: "flex", flexDirection: "row", justifyContent: "flex-end" }}>
                                    <SwitchField
                                        placeholder='Input Here'
                                        label={'By Transaction Date ?'}
                                        value={filterForm.isTransactionDate}
                                        selectedValue={filterForm.isTransactionDate}
                                        setValue={(val) => setFilterForm({ ...filterForm, isTransactionDate: val })}
                                    />
                                </Box>
                            </Box>
                        </Stack>
                        <Stack sx={{
                            width: "100%",
                            display: "flex",
                            flexDirection: ["column", "row"],
                            alignItems: ["end", "flex-start"],
                            gap: 3,
                            justifyContent: "space-between",
                            mt: "2rem"
                        }}>
                            <FilterMessageNote
                                sx={{
                                    width: ["100%", "50%"]
                                }}
                                title={titleInfo}
                                subtitle={subTitleInfo}
                            />
                            <div style={{
                                display: "flex",
                                gap: 3
                            }}>
                                {/* <ExportExcel
                                    disabled={disabled}
                                    setDisabled={setDisabled}
                                    notify={notify}
                                    date={periodeSelected}
                                    grade={gradeSelected}
                                    ouCode={ouCodeSelected}
                                    label={ouCode ? ouCode.label : "All Merchant"}
                                /> */}
                                <ExportTransactionButton
                                    disabled={data.length > 0 ? false : true}
                                    form={filterForm}
                                    notify={notify}
                                    setDisabled={() => { }}
                                    allMerchant={allOutletCodeList}
                                />
                                <CustomButton
                                    onClick={() => {
                                        setPageSize(25)
                                        setPage(1)
                                        handleGetListTransaction({
                                            page: 1,
                                            pageSize: pageSize,
                                            form: {
                                                ...filterForm
                                            }
                                        });
                                    }}
                                    startIcon={<SearchIcon size="14px" />}
                                    name={buttomFilter}
                                >
                                    Filter
                                </CustomButton>
                            </div>
                        </Stack>
                        <Stack mt={6} gap={"1rem"} mb={"1.6rem"}>
                            <Typography fontSize={"1.25rem"} color="rgb(30, 41, 59)" fontWeight={"bold"}>Transaction Summary</Typography>
                            <CardSummaryBoardingTicket
                                summaryDetail={totalSummary}
                                isLoading={loadingDetail}
                                sidebarExpanded={sidebarExpanded}
                                revenueSrc={revenueSrc}
                                transactionTotalSrc={transactionTotalSrc}
                                marginSrc={marginSrc}
                                serviceSrc={serviceSrc}
                            />
                        </Stack>
                        <Box sx={{ width: "100%", mt: 10 }}>
                            <Stack sx={{ display: "flex", flexDirection: "row", justifyContent: "flex-end", width: "100%" }}>
                                <Box sx={{ width: ["100%", "25%"] }}>
                                    <Input
                                        placeholder="Search by keyword"
                                        onChange={(e) => {
                                            setFilterForm((prev) => ({
                                                ...prev,
                                                search: e.target.value
                                            }))
                                            setPageSize(25)
                                            setPage(1)
                                            handleGetListTransaction({
                                                page: 1,
                                                pageSize: pageSize,
                                                form: {
                                                    ...filterForm,
                                                    search: e.target.value
                                                }
                                            });
                                        }}
                                        sx={{
                                            borderStyle: "solid",
                                            borderWidth: "1px",
                                            borderColor: "gray",
                                            borderRadius: "12px",
                                            width: "100%",
                                            py: "4px",
                                            px: "10px"
                                        }}
                                        disableUnderline
                                        value={filterForm.search}
                                        startAdornment={(
                                            <InputAdornment position="start">
                                                <Search />
                                            </InputAdornment>
                                        )}
                                    />
                                </Box>
                            </Stack>
                            <CustomPagination
                                disableNext={disableNext}
                                countLoading={countLoading}
                                limit={pageSize}
                                offset={(page - 1) * pageSize}
                                count={count}
                                pageChange={(event, v) => pageChange(v)}
                                rowsChange={async (event, e) => rowsChange(e)}
                            />
                            <CustomTable
                                headers={header}
                                items={data}
                                renderCell={renderCell}
                                enableNumber={true}
                            />
                            <CustomPagination
                                disableNext={disableNext}
                                countLoading={countLoading}
                                limit={pageSize}
                                offset={(page - 1) * pageSize}
                                count={count}
                                pageChange={(event, v) => pageChange(v)}
                                rowsChange={async (event, e) => rowsChange(e)}
                            />
                        </Box>
                    </Box>
                </CardContent>
            </Card>
        </Stack>
    )
}

export default MasterTransactionB2B