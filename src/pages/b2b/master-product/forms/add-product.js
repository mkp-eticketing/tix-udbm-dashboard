import { Box, Stack, Typography } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import SelectField from "../../../../components/select-field";
import InputField from "../../../../components/text-field";
import DatePickerField from "../../../../components/datepicker-field";
import CustomButton from "../../../../components/custom-button";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import UseProductForm from "../hooks/useProductForm";
import EditorField from "../../../../components/editor-field";
import { getMerchantList } from "../../../../services/b2b/merchant";

const AddProductB2B = ({
    onOpen = () => { },
    detailMember,
    setDetailMember = () => { },
    notify,
    refreshData = () => { }
}) => {
    const [merchantData, setMerchantData] = useState(null);
    const formik = UseProductForm({
        notify: notify,
        callback: () => {
            onOpen(false)
            setDetailMember(null);
            refreshData()
        },
        isUpdate: detailMember ? true : false,
    })
    useEffect(() => {
        handleMerchantList("")
    }, []);

    const handleMerchantList = (search) => {
        let data = {
            "search": search,
            "pagination": {
                "page": 1,
                "pageSize": 15
            }
        }
        let resData = []
        setMerchantData(resData);
        getMerchantList(data).then(async (res) => {
            if (res.result) {
                resData = res.result.map((item) => {
                    return ({
                        ...item,
                        label: item.name,
                        value: item.id
                    })
                })
                setMerchantData(resData);
            } else {
                setMerchantData(resData);
            }
        }).catch((e) => {
            setMerchantData(resData);
        })
    }

    return (
        <Box
            component="form"
            onSubmit={formik.handleSubmit}
            sx={{
                display: "flex",
                flexDirection: "column",
                mt: "2rem",
                gap: 2
            }}>
            <Box sx={{
                display: "grid",
                gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)"],
                gap: 2
            }}>
                <SelectField
                    label={"Merchant"}
                    placeholder="All Merchant"
                    required={true}
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={merchantData ? merchantData : []}
                    selectedValue={formik.values.ouId}
                    setValue={(val) => formik.setFieldValue("ouId", val)}
                    isError={formik.touched.ouId && formik.errors.ouId}
                    errorMessage={formik.touched.ouId && formik.errors.ouId}
                    onInputChange={(e) => {
                        if (e.target.value.length >= 3) {
                            handleMerchantList(e.target.value)
                        } else {
                            setMerchantData([])
                        }
                    }}
                />
                <InputField
                    label={"Product Code"}
                    required={true}
                    placeholder="PSR-PKR-0001, ..."
                    value={formik.values.productCode}
                    onChange={(e) => formik.setFieldValue("productCode", e.target.value)}
                    isError={formik.touched.productCode && formik.errors.productCode}
                    errorMessage={formik.touched.productCode && formik.errors.productCode}
                />
                <SelectField
                    label={"Product Category"}
                    placeholder="Choose Product Category"
                    required={true}
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={["Pariwisata"]}
                    selectedValue={formik.values.productCategory}
                    setValue={(val) => formik.setFieldValue("productCategory", val)}
                    isError={formik.touched.productCategory && formik.errors.productCategory}
                    errorMessage={formik.touched.productCategory && formik.errors.productCategory}
                />
                <SelectField
                    label={"Product Sub Category"}
                    placeholder="Choose Product Sub Category"
                    required={true}
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={["Pariwisata"]}
                    selectedValue={formik.values.productSubCategory}
                    setValue={(val) => formik.setFieldValue("productSubCategory", val)}
                    isError={formik.touched.productSubCategory && formik.errors.productSubCategory}
                    errorMessage={formik.touched.productSubCategory && formik.errors.productSubCategory}
                />
            </Box>
            <Typography fontSize={24} fontWeight="600" my={3}>Product Detail</Typography>
            <Box sx={{
                display: "grid",
                gridTemplateColumns: "repeat(1, 1fr)",
                gap: 2
            }}>
                <InputField
                    label={"Name"}
                    required={true}
                    placeholder="Type name product"
                    value={formik.values.name}
                    onChange={(e) => formik.setFieldValue("name", e.target.value)}
                    isError={formik.touched.name && formik.errors.name}
                    errorMessage={formik.touched.name && formik.errors.name}
                />
                <EditorField
                    label={"Description"}
                    required={true}
                    isError={formik.touched.description && formik.errors.description}
                    errorMessage={formik.touched.description && formik.errors.description}
                    content={formik.values.description}
                    setContent={(val) => formik.setFieldValue("description", val)}
                />
            </Box>
            <Stack direction={["column", "row"]} gap={2} justifyContent={"space-between"}>
                <InputField
                    label={"Contact"}
                    required={true}
                    placeholder="Type contact"
                    value={formik.values.contact}
                    onChange={(e) => formik.setFieldValue("contact", e.target.value)}
                    isError={formik.touched.contact && formik.errors.contact}
                    errorMessage={formik.touched.contact && formik.errors.contact}
                />
                <InputField
                    label={"Keyword"}
                    required={true}
                    placeholder="Type keyword"
                    value={formik.values.keyword}
                    onChange={(e) => formik.setFieldValue("keyword", e.target.value)}
                    isError={formik.touched.keyword && formik.errors.keyword}
                    errorMessage={formik.touched.keyword && formik.errors.keyword}
                />
            </Stack>
            <Stack direction={["column", "row"]} gap={2} justifyContent={"space-between"}>
                <InputField
                    label={"Additional Note"}
                    required={true}
                    placeholder="Type additional note"
                    value={formik.values.contact}
                    onChange={(e) => formik.setFieldValue("contact", e.target.value)}
                    isError={formik.touched.contact && formik.errors.contact}
                    errorMessage={formik.touched.contact && formik.errors.contact}
                />
                <InputField
                    label={"Max Capacity"}
                    required={true}
                    placeholder="Type max capacity"
                    value={formik.values.keyword}
                    onChange={(e) => formik.setFieldValue("keyword", e.target.value)}
                    isError={formik.touched.keyword && formik.errors.keyword}
                    errorMessage={formik.touched.keyword && formik.errors.keyword}
                />
            </Stack>
            <Typography fontSize={24} fontWeight="600" my={3}>Product Location</Typography>
            <InputField
                label={"Location"}
                required={true}
                placeholder="Type location product"
                value={formik.values.name}
                onChange={(e) => formik.setFieldValue("name", e.target.value)}
                isError={formik.touched.name && formik.errors.name}
                errorMessage={formik.touched.name && formik.errors.name}
            />
            <Box sx={{
                display: "grid",
                gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)"],
                gap: 2
            }}>
                <SelectField
                    label={"Country"}
                    placeholder="Choose Product Country"
                    required={true}
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={["Indonesia"]}
                    selectedValue={formik.values.productSubCategory}
                    setValue={(val) => formik.setFieldValue("productSubCategory", val)}
                    isError={formik.touched.productSubCategory && formik.errors.productSubCategory}
                    errorMessage={formik.touched.productSubCategory && formik.errors.productSubCategory}
                />
                <SelectField
                    label={"Product Province"}
                    placeholder="Choose Product Province"
                    required={true}
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={["Jawa Tengah"]}
                    selectedValue={formik.values.productSubCategory}
                    setValue={(val) => formik.setFieldValue("productSubCategory", val)}
                    isError={formik.touched.productSubCategory && formik.errors.productSubCategory}
                    errorMessage={formik.touched.productSubCategory && formik.errors.productSubCategory}
                />
                <SelectField
                    label={"City"}
                    placeholder="Choose Product City"
                    required={true}
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={["Semarang"]}
                    selectedValue={formik.values.productSubCategory}
                    setValue={(val) => formik.setFieldValue("productSubCategory", val)}
                    isError={formik.touched.productSubCategory && formik.errors.productSubCategory}
                    errorMessage={formik.touched.productSubCategory && formik.errors.productSubCategory}
                />
            </Box>
            <InputField
                label={"Map"}
                required={true}
                placeholder="<iframe src='https://www.google.com/maps/abc' />"
                value={formik.values.name}
                onChange={(e) => formik.setFieldValue("name", e.target.value)}
                isError={formik.touched.name && formik.errors.name}
                errorMessage={formik.touched.name && formik.errors.name}
                multiline
                rows={3}
            />
            <Box sx={{ width: "100%", height: "300px" }}>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.0961345587734!2d110.42374390965377!3d-6.99795939297405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708b00d33d9c43%3A0xc691f742d1ce2c7e!2sMKP%20Mobile%20Headquarters!5e0!3m2!1sid!2sid!4v1717558144949!5m2!1sid!2sid" width="100%" height="100%" style={{ border: 0 }} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </Box>
            <Typography fontSize={24} fontWeight="600" my={3}>Product Duration</Typography>
            <Stack direction={["column", "row"]} gap={2} justifyContent={"space-between"}>
                <DatePickerField
                    label={"Start Date"}
                    placeholder="DD MMM YYYY"
                    sx={{ width: "100%", fontSize: "16px" }}
                    format={"DD MMM YYYY"}
                // value={formik.values.startDate}
                // onChange={(newValue) => formik.setFieldValue("startDate", newValue)}
                // isError={formik.touched.startDate && formik.errors.startDate}
                // errorMessage={formik.touched.startDate && formik.errors.startDate}
                />
                <DatePickerField
                    label={"End Date"}
                    placeholder="DD MMM YYYY"
                    sx={{ width: "100%", fontSize: "16px" }}
                    format={"DD MMM YYYY"}
                // value={formik.values.endDate}
                // onChange={(newValue) => formik.setFieldValue("endDate", newValue)}
                // isError={formik.touched.endDate && formik.errors.endDate}
                // errorMessage={formik.touched.endDate && formik.errors.endDate}
                />
            </Stack>
            <Stack direction="row" justifyContent="flex-end" gap={2} mt={4}>
                <CustomButton
                    onClick={() => {
                        setDetailMember(null);
                        onOpen(false)
                    }}
                    startIcon={<KeyboardBackspaceIcon size="14px" />}
                    name={"Back"}
                    sx={{
                        backgroundColor: "grey",
                        ":hover": {
                            backgroundColor: "#333333"
                        }
                    }}
                >
                    Kembali
                </CustomButton>
                <CustomButton
                    onClick={() => { }}
                    startIcon={<SaveIcon size="14px" />}
                    name={"Save"}
                    type="submit"
                >
                    Simpan
                </CustomButton>
            </Stack>
        </Box>
    )
}

export default AddProductB2B;