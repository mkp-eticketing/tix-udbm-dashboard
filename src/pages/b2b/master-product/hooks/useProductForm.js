import { useFormik } from "formik";
import * as yup from "yup";
import { addMember, updateMember } from "../../../../services/parkir/member";
import moment from "moment";

const initialValues = {
    "id": "",
    "productCode": "",
    "productCategory": "",
    "productSubCategory": "",
    "name": "",
    "description": "",
    "contact": "",
    "keyword": ""
}
const UseProductForm = ({
    notify = () => { },
    callback = () => { },
    username = "admin",
    isUpdate = false
}) => {
    const formik = useFormik({
        initialValues,
        onSubmit: (values) => {
            const body = parseInitValueToBody(values)
            if (isUpdate) {
                updateMember({
                    id: values.id,
                    partnerCode: values.partnerCode,
                    firstName: values.firstName,
                    lastName: values.lastName,
                    phoneNumber: values.phoneNumber,
                    email: values.email,
                    username: values.username
                }).then((_res) => {
                    notify("Success update data", "success");
                    callback();
                }).catch((error) => {
                    notify(JSON.stringify(error), "error");
                });
            } else {
                addMember({ ...body, username: username }).then((_res) => {
                    notify("Success add data", "success");
                    callback();
                }).catch((error) => {
                    notify(JSON.stringify(error), "error");
                });
            }
        },
        validationSchema: yup.object({
            productCode: yup.string().required("Product Code is required"),
            productCategory: yup.string().required("Product Category is required"),
            productSubCategory: yup.string().required("Product Sub Category is required"),
            name: yup.string().required("Name is required"),
            description: yup.string().required("Description is required"),
            contact: yup.string().required("Contact is required"),
            keyword: yup.string().required("Keyword is required"),
        }),
        enableReinitialize: true,
    });

    const parseInitValueToBody = (values) => {
        const body = {
            "partnerCode": values.partnerCode,
            "firstName": values.firstName,
            "lastName": values.lastName,
            "roleType": values.roleType || "",
            "typePartner": values.typePartner ? values.typePartner.value : "",
            "phoneNumber": values.phoneNumber,
            "registeredType": values.registeredType.value,
            "email": values.email,
            "startDate": values.startDate ? values.startDate.format("YYYY-MM-DD") : moment(Date.now()).format("YYYY-MM-DD"),
            "endDate": values.endDate ? values.endDate.format("YYYY-MM-DD") : "2030-12-31",
            "outletCode": values.ouId.value,
            "cardNumber": values.cardNumber,
            "vehicleNumber": values.vehicleNumber,
            "productId": values.productId ? Number(values.productId.value) : -99,
            "remark": values.remark,
            "username": values.username
        }
        return body
    }

    return {
        ...formik
    }
}

export default UseProductForm;