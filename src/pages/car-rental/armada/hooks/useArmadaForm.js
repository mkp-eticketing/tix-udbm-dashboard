import { useFormik } from "formik";
import * as yup from "yup";
import React from "react";
import { editCarProvider, addCarProvider } from "../../../../services/car-rental/armada";
import moment from "moment";

const initialValues = {
    merchant: '',
    carTypeProvider: '',
    carNumber: "",
    taxYear: "",
}

const UseArmadaForm = ({
    notify = () => { },
    callback = () => { },
    isUpdate = false,
    id
}) => {

    const handleAddData = async (values) => {
        await addCarProvider({
            carTypeProviderId: values.carTypeProvider.value,
            carNumber: values.carNumber,
            taxYear: Number(values.taxYear),
        })
        .then((res) => {
            if(res.success){
                notify("Success Add Data", "success");
                callback();
            }
        })
        .catch((error) => {
            if (error.response) {
              // console.log(error.response.data.message);
              notify(error.response.data.message, "error");
            } else {
              // console.log(error.message || error);
              notify(error.message || error, "error");
            }
        });
    }

    const formik = useFormik({
        initialValues,
        onSubmit: (values) => {
            if (isUpdate) {
                editCarProvider(id, {
                    carNumber: values.carNumber,
                    taxYear: values.taxYear,
                }).then((res) => {
                    if(res.success){
                        notify("Success Update Data", "success");
                        callback();
                    }
                }).catch((error) => {
                    if (error.response) {
                        // console.log(error.response.data.message);
                        notify(error.response.data.message, "error");
                    } else {
                        // console.log(error.message || error);
                        notify(error.message || error, "error");
                    }
                });
            } else {
                handleAddData(values)
            }
        },
        validationSchema: isUpdate ? 
        yup.object({
            carNumber: yup.string().required('Registration Plate is Required'),
            taxYear: yup.number().required('Tax Year is Required'),
        }) : 
        yup.object({
            merchant: yup.object().required('Merchant is Required'),
            carTypeProvider: yup.object().required('Car Type is Required'),
            carNumber: yup.string().required('Registration Plate is Required'),
            taxYear: yup.number().required('Tax Year is Required'),
        }),
        enableReinitialize: true,
    });

    return {
        ...formik
    }
}

export default UseArmadaForm;