import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography, Avatar } from "@mui/material";
import SelectField from "../../../components/select-field";
import SelectField2 from "../../../components/select-field-2";
import SearchIcon from "@mui/icons-material/Search";
import { getCarProvider, getCarTypeProvider } from "../../../services/car-rental/armada";
import CustomTable from "../../../components/custom-table";
import TextField from "../../../components/text-field";
import SwitchField from "../../../components/switch-field";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CustomDialogDelete from './components/custom-dialog-delete'
import ArmadaForm from './forms/armada-form'

const Armada = ({
  label = "Vehicle",
  titleInfo = "To Display Specific Data, Use the Filters Above.",
  subTitleInfo = ["Select the Merchant to Use the Car Type Filter"],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  const [limit, setLimit] = useState(25);
  const [offset, setOffset] = useState(0);
  const [disableNext, setDisableNext] = useState(false);
  const [data, setData] = useState([]);
  const [totalData, setTotalData] = useState(0);

  const [merchant, setMerchant] = useState(null);
  const [brand, setBrand] = useState(null);
  const [type, setType] = useState(null);

  const [merchantOption, setMerchantOption] = useState([]);
  const [brandOption, setBrandOption] = useState([]);
  const [typeOption, setTypeOption] = useState([]);

  //untuk menyimpan data filter yang sudah di search
  const [ouCodeSelected, setOuCodeSelected] = useState("");
  const [brandSelected, setBrandSelected] = useState("");
  const [typeSelected, setTypeSelected] = useState("");

  const [errormerchant, setErrormerchant] = useState(false);
  const [openForm, setOpenForm] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [dataDetail, setDataDetail] = useState(null);

  const [carTypeInput, setCarTypeInput] = useState('');
  
  const header = [
    {
      title: "No",
      value: "no",
      align: "center",
      width: "10px",
    },
    {
      title: "Action",
      value: "action",
      align: "center",
      width: "100px",
    },
    {
      title: "Car Brand",
      value: "carBrand",
      align: "center",
      width: "150px",
    },
    {
      title: "Car Type",
      value: "carType",
      align: "center",
      width: "150px",
    },
    {
      title: "Registration Plates",
      value: "carNumber",
      align: "center",
      width: "150px",
    },
    {
      title: "Tax Year",
      value: "taxYear",
      align: "center",
      width: "150px",
    },
  ];

  const renderCell = (item, header, index) => {
    if (header.value === "no") {
      return <span>{index >= 0 ? index + offset + 1 : ""}</span>;
    } else if (header.value === "carBrand") {
      return <span>{item.carTypeProvider.carType.brand.brandName ? item.carTypeProvider.carType.brand.brandName : '-'}</span>;
    } else if (header.value === "carType") {
      return <span>{item.carTypeProvider.carType.carName ? item.carTypeProvider.carType.carName : "-"}</span>;
    } else if (header.value === "carNumber") {
      return <span>{item.carNumber ? item.carNumber : "-"}</span>;
    } else if (header.value === "taxYear") {
      return <span>{item.taxYear ? item.taxYear : "-"}</span>;
    } else if (header.value === "action") {
      return (
        <Box
          sx={{
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: header.align === 'left' ? 'start' : (header.align === 'right' ? 'end': 'center'),
            gap: 2,
          }}
        >
          <Box 
            onClick={() => {
              setDataDetail(item.id)
              setOpenForm(true)
            }} 
            sx={{cursor: 'pointer'}}
          >
            <EditIcon sx={{ color: '#3875CA'}}/>
          </Box>

          <Box 
            onClick={() => {
              setDataDetail(item.id)
              setOpenDeleteModal(true)
            }} 
            sx={{cursor: 'pointer'}}
          >
            <RemoveCircleOutlineIcon sx={{ color: 'red'}}/>
          </Box>
        
        </Box>
      );
    } 
  };

  // untuk rename option select field merchant
  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        label: item.corporateName,
        value: item.ouCode,
      });
    });
    // console.log('merchantArr',merchantArr.length)
    setMerchantOption(merchantArr);
  }, [merchantData]);

  useEffect(() => {
    if (merchant && merchant.value !== ouCodeSelected) {
      // setBrandOption([]);
      // setBrand(null);
      setCarTypeInput('')
      setTypeOption([]);
      setType(null);
      handleGetCarTypeData(merchant.value);
    } else if (merchant === null){
      // setBrandOption([]);
      // setBrand(null);
      setCarTypeInput('')
      setTypeOption([]);
      setType(null);
    }
  }, [merchant]);

  const handleGetCarTypeData = async (ouCode) => {
    if (ouCode){
      await getCarTypeProvider({ouCode})
      .then((res) => {
        if (res.success && res.result.total > 0) {
          const filteredData = res.result.items.map((data) => {
            return ({
                label : `${data.carType.brand.brandName} - ${data.carType.carName}`,
                value : {carTypeCode : data.carType.carTypeCode, brandCode: data.carType.brand.brandCode}
            })
          })
          setTypeOption(filteredData)
          notify(res.message || "Success Get Data", "success");
        }else{
          notify(res.message || "Data Not Found", "error");
        }
      })
      .catch((error) => {
        if (error.response) {
          // console.log(error.response.data.message);
          notify(error.response.data.message, "error");
        } else {
          // console.log(error.message || error);
          notify(error.message || error, "error");
        }
      })
    }
  }

  //get laporan tiket
  const handleCarProvider = async (query, data) => {
    if (data.ouCode) {
      setLoading(true);
      setOuCodeSelected(data.ouCode);
      setBrandSelected(data.brandCode);
      setTypeSelected(data.carTypeCode);

      await getCarProvider(query, data)
      .then((res) => {
        if (res.success && res.result.total > 0) {
          // console.log("Success", res.result.items);
          setData(res.result.items);
          setTotalData(res.result.total);
          notify(res.message || "Success Get Data", "success");
        }else{
          notify(res.message || "Data Not Found", "error");
        }
      })
      .catch((error) => {
        setData([]);
        if (error.response) {
          // console.log(error.response.data.message);
          notify(error.response.data.message, "error");
        } else {
          // console.log(error.message || error);
          notify(error.message || error, "error");
        }
      });
      setLoading(false);
    } else {
      if (!data.ouCode) {
        setErrormerchant(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  };

  const pageChange = async (value) => {
    // console.log(value)
    const offset = value * limit;
    setOffset(offset);
    handleCarProvider({ size: limit, page: value }, { ouCode: ouCodeSelected, brandCode : brandSelected, carTypeCode : typeSelected });
  };

  const rowsChange = async (e) => {
    setOffset(0);
    setLimit(e.props.value);
    handleCarProvider({ size: e.props.value, page: 0 }, { ouCode: ouCodeSelected, brandCode : brandSelected, carTypeCode : typeSelected });
  };

  const refreshData = () => {
    handleCarProvider({ size: limit, page: offset }, { ouCode: ouCodeSelected, brandCode : brandSelected, carTypeCode : typeSelected });
  };

  return (
    <React.Fragment>
      <Stack direction={"column"} p={"2rem"}>
        <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
          <CardContent sx={{ p: "2rem" }}>
            <Box display="flex" flexDirection="column">
              <Typography variant="h4" fontWeight="600">
                {label}
              </Typography>
              <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                <Box
                  sx={{
                    display: "grid",
                    gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)","repeat(4, 1fr)"],
                    gap: 2,
                  }}
                >
                  <SelectField
                    label={"Merchant"}
                    placeholder="Select Merchant"
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={merchantOption}
                    selectedValue={merchant}
                    setValue={(newValue) => {
                      setMerchant(newValue);
                      setErrormerchant(false);
                    }}
                    required={true}
                    isError={errormerchant}
                    errorMessage={"Merchant is required"}
                  />

                  {/* <SelectField 
                    label={"Car Brand"} 
                    placeholder={merchant ? "All Brand" : "Please Select Merchant"} 
                    sx={{ width: "100%", fontSize: "16px" }} 
                    data={brandOption} 
                    selectedValue={brand} 
                    setValue={setBrand} 
                  /> */}

                  <SelectField2
                    label={"Car Type"} 
                    placeholder={typeOption.length > 0 ? "All Type" : "Please Select Car Brand"} 
                    sx={{ width: "100%", fontSize: "16px" }} 
                    data={typeOption} 
                    selectedValue={type} 
                    setValue={setType} 
                    inputValue={carTypeInput}
                    setInputValue={setCarTypeInput}
                  />
                </Box>
              </Stack>
              <Stack
                sx={{
                  width: "100%",
                  display: "flex",
                  flexDirection: ["column", "column" ,"row"],
                  alignItems: ["end", "end", "center"],
                  gap: 3,
                  justifyContent: "space-between",
                  mt: "2rem",
                }}
              >
                <FilterMessageNote
                  sx={{
                    width: ["100%","100%", "50%"],
                  }}
                  title={titleInfo}
                  subtitle={subTitleInfo}
                />
                <div
                  style={{
                    display: "flex",
                    gap: 10,
                  }}
                >
                  <CustomButton
                    onClick={() => {
                      setOpenForm(true)
                    }}
                    startIcon={<AddIcon size="14px" />}
                    name={'Add Data'}
                  />
                  <CustomButton
                    onClick={() => {
                      setLimit(25);
                      setOffset(0);
                      handleCarProvider({ size: 25, page: 0 }, { 
                        ouCode: merchant ? merchant.value : "" ,
                        brandCode: type ? type.value.brandCode : "" ,
                        carTypeCode: type ? type.value.carTypeCode : ""
                      });
                    }}
                    startIcon={<SearchIcon size="14px" />}
                    name={buttonFilter}
                  >
                    Filter
                  </CustomButton>
                  
                </div>
              </Stack>
              <Box sx={{ width: "100%" }} mt={"2rem"}>
                <CustomPagination disableNext={disableNext} limit={limit} countLoading={false} offset={offset} count={totalData} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
                <CustomTable headers={header} items={data} renderCell={renderCell} />
                <CustomPagination disableNext={disableNext} limit={limit} countLoading={false} offset={offset} count={totalData} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Stack>
      
      <CustomDialogDelete
        openModal={openDeleteModal}
        setOpenModal={setOpenDeleteModal}
        data={dataDetail}
        title={"Delete Data"}
        notify={notify}
        refreshData={refreshData}
      />

      <ArmadaForm
        onOpen={setOpenForm}
        openForm={openForm}
        dataId={dataDetail}
        setDataId={setDataDetail} 
        notify={notify}
        merchantOption={merchantOption}
        refreshData={() => {refreshData()}}
      />
    </React.Fragment>
  );
};

export default Armada;
