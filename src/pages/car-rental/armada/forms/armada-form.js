import { Box, Stack,  Dialog, DialogContent, DialogTitle, DialogActions } from "@mui/material";
import React, { useEffect, useState } from "react";
import TextField from "../../../../components/text-field";
import SwitchField from "../../../../components/switch-field"
import CustomButton from "../../../../components/custom-button";
import SelectField from "../../../../components/select-field";
import SelectField2 from "../../../../components/select-field-2";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import UseArmadaForm from "../hooks/useArmadaForm";
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { getCarProviderById, getCarTypeProvider } from "../../../../services/car-rental/armada";

const ArmadaForm = ({
    openForm = false,
    onOpen = () => { },
    merchantOption = [],
    notify = () => { },
    refreshData = () => { },
    dataId = null,
    setDataId = () => { },
}) => {

    const [carTypeOption, setCarTypeOption] = useState([]);
    const [carTypeInput, setCarTypeInput] = useState('');

    const formik = UseArmadaForm({
        notify: notify,
        callback: () => {
          handleClose()
          refreshData()
        },
        isUpdate: dataId ? true : false,
        id : dataId
    });

    const handleGetDataById = async (id) => {
      await getCarProviderById(id)
        .then((res) => {
          if (res.success && res.result) {
            notify('Success Get Data', "success");
            formik.setValues({
              carNumber: res.result.carNumber,
              taxYear: res.result.taxYear,
            })
          }
          else{
            notify(res.message, "error");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
    }

    const handleGetCarTypeData = async (ouCode) => {
      await getCarTypeProvider({ouCode})
        .then((res) => {
          if (res.success && res.result.items.length > 0) {
            const filteredData = res.result.items.map((data) => {
              return ({
                label: `${data.carType.brand.brandName} - ${data.carType.carName}`,
                value: data.id
              })
            })
            setCarTypeOption(filteredData)
            notify('Success Get Data', "success");
          }
          else{
            notify(res.message, "error");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
    }
  
    useEffect(() => {
        if (dataId) {
          handleGetDataById(dataId)
        }
    }, [dataId]);

    useEffect(() => {
      if(formik.values.merchant){
        setCarTypeInput('')
        setCarTypeOption([])
        handleGetCarTypeData(formik.values.merchant.value)
      }else{
        setCarTypeInput('')
        setCarTypeOption([])
      }
    }, [formik.values.merchant]);

    const theme = useTheme()

    const handleClose = () => {
      setCarTypeInput('')
      setCarTypeOption([])  
      setDataId(null)
      formik.resetForm()
      onOpen(false)
    }

    const handleNumber = (data) => {
      const values = data.replace(/\D/g, '')
      if (values === '' || /^[1-9]\d*$/.test(values)) {
        formik.setFieldValue("taxYear", values);
      }
    };

    return (
      <Dialog
        open={openForm}
        onClose={() => handleClose()}
        fullWidth={true}
        maxWidth={'sm'}
        fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}
      >
        <DialogTitle
          sx={{
            backgroundColor: '#3875CA'
          }}
        >
          {dataId ? 'Edit Car Data' : 'Add Car Data'}
        </DialogTitle>
        <DialogContent>
          <Box
            sx={{
                display: "flex",
                flexDirection: "column",
                mt: "2rem",
                gap: 2
            }}>

            {!dataId && <SelectField
              required
              label={"Merchant"}
              placeholder="Choose Merchant"
              data={merchantOption}
              selectedValue={formik.values.merchant}
              setValue={(newValues) => {
                formik.setFieldValue("merchant", newValues)
              }}
              isError={formik.touched.merchant && formik.errors.merchant}
              errorMessage={formik.touched.merchant && formik.errors.merchant}
            />}

            {!dataId && <SelectField2
              required
              label={"Car Type"}
              placeholder={formik.values.merchant ? "Choose Car Type" : "Please Select Merchant"}
              data={carTypeOption}
              selectedValue={formik.values.carTypeProvider}
              setValue={(newValues) => {
                formik.setFieldValue("carTypeProvider", newValues)
              }}
              isError={formik.touched.carTypeProvider && formik.errors.carTypeProvider}
              errorMessage={formik.touched.carTypeProvider && formik.errors.carTypeProvider}
              inputValue={carTypeInput}
              setInputValue={setCarTypeInput}
            />}
            
            <TextField
              required
              placeholder='Input Here'
              label={'Registration Plates'}
              value={formik.values.carNumber}
              onChange={(e) => formik.setFieldValue("carNumber", e.target.value)}
              isError={formik.touched.carNumber && formik.errors.carNumber}
              errorMessage={formik.touched.carNumber && formik.errors.carNumber}
            />
            <TextField
              required
              placeholder='Input Here'
              label={'Tax Year'}
              value={formik.values.taxYear}
              onChange={(e) => handleNumber(e.target.value)}
              isError={formik.touched.taxYear && formik.errors.taxYear}
              errorMessage={formik.touched.taxYear && formik.errors.taxYear}
              InputProps={{
                inputProps: {
                  inputMode: 'numeric',
                  pattern: '[0-9]*',
                },
              }}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Stack direction="row" justifyContent="flex-end" gap={2} mt={4}>
            <CustomButton
              onClick={() => handleClose()}
              startIcon={<KeyboardBackspaceIcon size="14px" />}
              name={"Back"}
              sx={{
                  backgroundColor: "grey",
                  ":hover": {
                    backgroundColor: "#333333"
                  }
              }}
            />
            <CustomButton
              onClick={() => formik.handleSubmit()}
              startIcon={<SaveIcon size="14px" />}
              name={"Save"}
              type="submit"
            />
          </Stack>
        </DialogActions>
      </Dialog>
    )
}

export default ArmadaForm;