import { Box, Stack, Dialog, DialogContent, DialogTitle, DialogActions} from "@mui/material";
import SelectField from "../../../../components/select-field";
import SelectField2 from "../../../../components/select-field-2";
import CustomButton from "../../../../components/custom-button";
import DatePicker from "../../../../components/datepicker-field";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import React, { useState, useEffect } from 'react';
import moment from "moment";
import { getDriversByMerchant } from "../../../../services/car-rental/supir";
import { addDriverSchedule } from "../../../../services/car-rental/penugasan";


const DialogMonthly = ({
  openModal,
  setOpenModal,
  fullWidth = true,
  maxWidth = 'sm',
  backgroundColorTitle = '#3875CA', 
  merchantOption= [],
  notify = () => {} ,
  setLoading = () => {},
  refreshData = () => {},
  ...others
}) => {
  const theme = useTheme()
  const [merchant, setMerchant] = useState(null);
  const [driver, setDriver] = useState(null);
  const [tanggal, setTanggal] = useState(moment());
  const [status, setStatus] = useState(null);

  const [driverOption, setDriverOption] = useState([]);
  const [driverInput, setDriverInput] = useState('');

  const [errormerchant, setErrorMerchant] = useState(false);
  const [errorDriver, setErrorDriver] = useState(false);
  const [errorTanggal, setErrorTanggal] = useState(false);
  const [errorStatus, setErrorStatus] = useState(false);

  const statusOption = [
    {
      label : 'Available',
      value : 1
    },
    {
      label : 'Not Available',
      value : 2
    },
  ]

  const handleClose = () => {
    setOpenModal(false)
    setMerchant(null)
    setDriver(null)
    setTanggal(null)
    setDriverOption([])
    setDriverInput('')
    setErrorMerchant(false)
    setErrorDriver(false)
    setErrorTanggal(false)
    setErrorStatus(false)
  }

  useEffect(() => {
    if (merchant) {
      setDriverOption([]);
      setDriver(null);
      setDriverInput('')
      handleGetDriverData(merchant.value);
    } else if (merchant === null) {
      setDriverInput('')
      setDriverOption([]);
      setDriver(null);
    }
  }, [merchant]);

  const handleGetDriverData = async (ouCode) => {
    if(ouCode){
      await getDriversByMerchant({ ouCode })
        .then((res) => {
          if (res.success && res.result.total > 0) {
            const filteredData = res.result.items.map((data) => {
              return({
                label : data.driverName,
                value: data.id
              })
            })
            setDriverOption(filteredData)
            notify(res.message || "Success Get Data", "success");
          }else{
            notify(res.message || "Data Not Found", "error");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
    }
  }

  const handleSetSchedule = async () => {
    if (merchant && driver && tanggal && status) {
      setLoading(true);

      const startDate = tanggal.clone().startOf("month");
      const endDate = tanggal.clone().endOf("month");

      let day = startDate.clone();
      let driverSchedule = []

      while (day <= endDate) {
        const formattedDate = day.clone().format('YYYY-MM-DD')
        driverSchedule.push(
          {
            date : formattedDate,
            availableStatus : status.value
          }
        )
        day.add(1, "day")
      }

      console.log(driverSchedule)

      await addDriverSchedule({driverId: driver.value, driverSchedule : driverSchedule})
        .then((res) => {
          if (res.success) {
            handleClose()
            refreshData()
            notify(res.message || "Success Set Monthly Schedule", "success");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      setLoading(false);
    } else {
      if (!merchant) {
        setErrorMerchant(true);
      }
      if (!driver) {
        setErrorDriver(true);
      }
      if (!tanggal) {
        setErrorTanggal(true);
      }
      if (!status) {
        setErrorStatus(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  }

  return (
    <Dialog
      open={openModal}
      onClose={() => handleClose()}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}
      {...others}
    >
      <DialogTitle
        sx={{
          backgroundColor: backgroundColorTitle
        }}
      >
        Set Monthly Schedule
      </DialogTitle>

      <DialogContent>
        <Box
          sx={{
              display: "flex",
              flexDirection: "column",
              mt: "2rem",
              mb: '2rem',
              gap: 2
          }}>

            <SelectField
              label={"Merchant"}
              placeholder="Select Merchant"
              sx={{ width: "100%", fontSize: "16px" }}
              data={merchantOption}
              selectedValue={merchant}
              setValue={(newValue) => {
                setMerchant(newValue);
                setErrorMerchant(false);
              }}
              required={true}
              isError={errormerchant}
              errorMessage={"Merchant is required"}
            />

            <SelectField2
              label={"Driver"} 
              placeholder={driverOption.length > 0 ? "Select Driver" : "Please Select Merchant"} 
              sx={{ width: "100%", fontSize: "16px" }} 
              data={driverOption} 
              selectedValue={driver} 
              setValue={(newValue) => {
                setDriver(newValue);
                setErrorDriver(false);
              }} 
              inputValue={driverInput}
              setInputValue={(newValue) => {
                setDriverInput(newValue)
              }}
              required
              isError={errorDriver}
              errorMessage={"Driver is required"}
            />

            <DatePicker
              label={'Date'}
              placeholder="MMM YYYY"
              sx={{ width: "100%", fontSize: "16px"}}
              value={tanggal}
              onChange={(newValue) => {
                setTanggal(newValue);
              }}
              required
              format={"MMM YYYY"}
              views={['year', 'month']}
              isError={errorTanggal}
              errorMessage={"Date is required"}
            />

            <SelectField 
              label={"Status Schedule"} 
              placeholder={"Select Status"} 
              sx={{ width: "100%", fontSize: "16px" }} 
              data={statusOption} 
              selectedValue={status} 
              setValue={(newValue) => {
                setStatus(newValue);
                setErrorStatus(false);
              }} 
              required
              isError={errorStatus}
              errorMessage={"Status is required"}
            />

        </Box>
      </DialogContent>

      <DialogActions>
        <Stack direction="row" justifyContent="flex-end" gap={2}>
          <CustomButton
            onClick={() => handleClose()}
            startIcon={<KeyboardBackspaceIcon size="14px" />}
            name={"Back"}
            sx={{
              backgroundColor: "grey",
              ":hover": {
                backgroundColor: "#333333"
              }
            }}
          />
          <CustomButton
            onClick={() => handleSetSchedule()}
            startIcon={<SaveIcon size="14px" />}
            name={"Save"}
          />
        </Stack>
      </DialogActions>
    </Dialog>
  );
}

export default DialogMonthly;