import { Box, Stack, Dialog, DialogContent, DialogTitle, DialogActions} from "@mui/material";
import { formatCurrency } from '../../../../utils/format-currency';
import SelectField from "../../../../components/select-field";
import CustomButton from "../../../../components/custom-button";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import React, { useState, useEffect } from 'react';
import { addDriverSchedule, editDriverSchedule, deleteDriverSchedule } from "../../../../services/car-rental/penugasan";
import DeleteIcon from '@mui/icons-material/Delete';
import moment from "moment";

const DialogPenugasan = ({
  openModal,
  setOpenModal,
  dataModal,
  setDataModal,
  fullWidth = true,
  maxWidth = 'sm',
  backgroundColorTitle = '#3875CA',  
  notify = () => {} ,
  setLoading = () => {},
  driver = {},
  refreshData = () => {},
  ...others}
) => {
  const theme = useTheme()
  const [status, setStatus] = useState(null);
  const [errorStatus, setErrorStatus] = useState(false);

  const statusOption = [
    {
      label : 'Available',
      value : 1
    },
    {
      label : 'Not Available',
      value : 2
    }
  ]

  const handleClose = () => {
    setOpenModal(false)
    setDataModal(null)
    setStatus(null)
    setErrorStatus(false)
  }

  useEffect(() => {
    if(dataModal){
      const filter = statusOption.filter(data => data.value === dataModal.availableStatus)
      filter.length > 0 && setStatus(filter[0])
    }
  }, [dataModal]);

  const handleSetSchedule = async () => {
    if (status && driver) {
      setLoading(true);
      if(dataModal.id){
        await editDriverSchedule(dataModal.id, {"availableStatus" : status.value})
        .then((res) => {
          if (res.success) {
            handleClose()
            refreshData()
            notify(res.message || "Success Set Schedule", "success");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      }
      else{
        const driverSchedule = [{
          "date" : dataModal.date,
          "availableStatus" : status.value
        }]
        await addDriverSchedule({driverId: driver.id, driverSchedule : driverSchedule})
        .then((res) => {
          if (res.success) {
            refreshData()
            handleClose()
            notify(res.message || "Success Set Schedule", "success");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      }
      setLoading(false);
    } else {
      if (!status) {
        setErrorStatus(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  }

  const handleDeleteSchedule = async () => {
    await deleteDriverSchedule(dataModal.id)
    .then((res) => {
      if (res.success) {
        refreshData()
        handleClose()
        notify(res.message || "Success Delete Schedule", "success");
      }
    })
    .catch((error) => {
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    });
  }
  
  return (
    <Dialog
      open={openModal}
      onClose={() => handleClose()}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}
      {...others}
    >
      <DialogTitle
        sx={{
          backgroundColor: backgroundColorTitle
        }}
      >
        Set Schedule - {dataModal && moment(dataModal.date).format('DD MMMM YYYY')}
      </DialogTitle>

      <DialogContent>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            mt: "2rem",
            gap: 2
          }}
        >
          <SelectField 
            label={"Status Schedule"} 
            placeholder={"Select Status"} 
            sx={{ width: "100%", fontSize: "16px" }} 
            data={statusOption} 
            selectedValue={status} 
            setValue={(newValue) => {
              setStatus(newValue);
              setErrorStatus(false);
            }} 
            required
            isError={errorStatus}
            errorMessage={"Status is required"}
          />
        </Box>
      </DialogContent>

      <DialogActions>
        <Stack direction="row" justifyContent="flex-end" gap={2} mt={8}>
          <CustomButton
            onClick={() => handleClose()}
            startIcon={<KeyboardBackspaceIcon size="14px" />}
            name={"Back"}
            sx={{
              backgroundColor: "grey",
              ":hover": {
                backgroundColor: "#333333"
              }
            }}
          />
          {/* {(dataModal && dataModal.id) && 
          <CustomButton
            onClick={() => handleDeleteSchedule()}
            startIcon={<DeleteIcon size="14px" />}
            name={"Delete"}
            sx={{
              backgroundColor: "error.main",
              ":hover": {
                backgroundColor: "error.dark"
              }
            }}
          />} */}
          <CustomButton
            onClick={() => handleSetSchedule()}
            startIcon={<SaveIcon size="14px" />}
            name={"Save"}
          />
        </Stack>
      </DialogActions>
    </Dialog>
  );
}

export default DialogPenugasan;