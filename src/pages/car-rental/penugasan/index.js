import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography} from "@mui/material";
import SelectField from "../../../components/select-field";
import SelectField2 from "../../../components/select-field-2";
import SearchIcon from "@mui/icons-material/Search";
import DatePicker from "../../../components/datepicker-field";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import SaveIcon from '@mui/icons-material/Save';
import SaveAsIcon from '@mui/icons-material/SaveAs';
import EditIcon from "@mui/icons-material/Edit";
import CustomCalendar from './components/custom-calendar-penugasan'
import DialogPenugasan from './components/dialog-penugasan'
import DialogMonthly from './components/dialog-monthly-schedule'
import moment from "moment";
import { getDriversByMerchant } from "../../../services/car-rental/supir";
import { getDriverSchedule } from "../../../services/car-rental/penugasan";

const Penugasan = ({
  label = "Driver Schedule",
  titleInfo = "To Display Specific Data, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {

  const [merchant, setMerchant] = useState(null);
  const [driver, setDriver] = useState(null);
  const [tanggal, setTanggal] = useState(moment().startOf('month'));

  const [merchantOption, setMerchantOption] = useState([]);
  const [driverOption, setDriverOption] = useState([]);

  const [driverInput, setDriverInput] = useState('');

  //untuk menyimpan data filter yang sudah di search
  const [ouCodeSelected, setOuCodeSelected] = useState("");
  const [driverSelected, setDriverSelected] = useState("");
  const [tanggalSelected, setTanggalSelected] = useState("");

  const [errormerchant, setErrorMerchant] = useState(false);
  const [errorDriver, setErrorDriver] = useState(false);
  const [errorTanggal, setErrorTanggal] = useState(false);

  const [openDialogDate, setOpenDialogDate] = useState(false);
  const [openDialogMonth, setOpenDialogMonth] = useState(false);
  const [dataModal, setDataModal] = useState(null);
  const [driverSchedule, setDriverSchedule] = useState({});

  // untuk rename option select field merchant
  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        label: item.corporateName,
        value: item.ouCode,
      });
    });
    // console.log('merchantArr',merchantArr.length)
    setMerchantOption(merchantArr);
  }, [merchantData]);

  useEffect(() => {
    if (merchant && merchant.value !== ouCodeSelected) {
      setDriverOption([]);
      setDriver(null);
      setDriverInput('')
      handleGetDriverData(merchant.value);
    } else if (merchant === null) {
      setDriverInput('')
      setDriverOption([]);
      setDriver(null);
    }
  }, [merchant]);
  
  const handleGetDriverData = async (ouCode) => {
    if(ouCode){
      await getDriversByMerchant({ ouCode })
        .then((res) => {
          if (res.success && res.result.total > 0) {
            const filteredData = res.result.items.map((data) => {
              return({
                label : data.driverName,
                value: data.id
              })
            })
            setDriverOption(filteredData)
            notify(res.message || "Success Get Data", "success");
          }else{
            notify(res.message || "Data Not Found", "error");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
    }
  }

  const handleGetSchedule = async ({ouCode, driver, tanggal}) => {
    if (ouCode && driver && tanggal) {
      setLoading(true);
      setOuCodeSelected(ouCode);
      setDriverSelected(driver);
      setTanggalSelected(tanggal);

      const formatDate = tanggal.clone().format('YYYY-MM')

      await getDriverSchedule({driver: driver.value, tanggal : formatDate})
        .then((res) => {
          if (res.success && res.result.driver.id) {
            setDriverSchedule(res.result)
            notify(res.message || "Success Get Data List", "success");
            if(res.result.schedule.length === 0){
              notify("Driver don't Have Schedule yet", "warning")
            }
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      setLoading(false);
    } else {
      if (!ouCode) {
        setErrorMerchant(true);
      }
      if (!driver) {
        setErrorDriver(true);
      }
      if (!tanggal) {
        setErrorTanggal(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  };

  const refreshData = () => {
    handleGetSchedule({ouCode: ouCodeSelected, driver: driverSelected, tanggal : tanggalSelected})
  }

  return (
    <React.Fragment>
      <Stack direction={"column"} p={"2rem"}>
        <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
          <CardContent sx={{ p: "2rem" }}>
            <Box display="flex" flexDirection="column">
              <Typography variant="h4" fontWeight="600">
                {label}
              </Typography>
              <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                <Box
                  sx={{
                    display: "grid",
                    gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)","repeat(3, 1fr)"],
                    gap: 2,
                  }}
                >
                  <SelectField
                    label={"Merchant"}
                    placeholder="Select Merchant"
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={merchantOption}
                    selectedValue={merchant}
                    setValue={(newValue) => {
                      setMerchant(newValue);
                      setErrorMerchant(false);
                    }}
                    required={true}
                    isError={errormerchant}
                    errorMessage={"Merchant is required"}
                  />

                  <SelectField2
                    label={"Driver"} 
                    placeholder={driverOption.length > 0 ? "Select Driver" : "Please Select Merchant"} 
                    sx={{ width: "100%", fontSize: "16px" }} 
                    data={driverOption} 
                    selectedValue={driver} 
                    setValue={(newValue) => {
                      setDriver(newValue);
                      setErrorDriver(false);
                    }} 
                    inputValue={driverInput}
                    setInputValue={(newValue) => {
                      setDriverInput(newValue)
                    }}
                    required
                    isError={errorDriver}
                    errorMessage={"Driver is required"}
                  />

                  <DatePicker
                    label={'Date'}
                    placeholder="MMM YYYY"
                    sx={{ width: "100%", fontSize: "16px"}}
                    value={tanggal}
                    onChange={(newValue) => {
                      setTanggal(newValue);
                    }}
                    required
                    format={"MMM YYYY"}
                    views={['year', 'month']}
                    isError={errorTanggal}
                    errorMessage={"Date is required"}
                  />
                </Box>
              </Stack>
              <Stack
                sx={{
                  width: "100%",
                  display: "flex",
                  flexDirection: ["column", "column" ,"row"],
                  alignItems: ["end", "end", "center"],
                  gap: 3,
                  justifyContent: "space-between",
                  mt: "2rem",
                }}
              >
                <FilterMessageNote
                  sx={{
                    width: ["100%","100%", "50%"],
                  }}
                  title={titleInfo}
                  subtitle={subTitleInfo}
                />
                <div
                  style={{
                    display: "flex",
                    gap: 10,
                  }}
                >
                  <CustomButton
                    onClick={() => {
                      setOpenDialogMonth(true)
                    }}
                    startIcon={<EditIcon size="14px" />}
                    name={'Set Monthly'}
                  />
                  <CustomButton
                    onClick={() => {
                      handleGetSchedule({
                        ouCode: merchant ? merchant.value : "",
                        tanggal: tanggal,
                        driver: driver
                      });
                    }}
                    startIcon={<SearchIcon size="14px" />}
                    name={buttonFilter}
                  />
                </div>
              </Stack>
              <Stack
                sx={{
                  width: "100%",
                  display: "flex",
                  flexDirection: ["column", "row"],
                  alignItems: ["start", "center"],
                  gap: 3,
                  justifyContent: "space-between",
                  mt: "2rem",
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: ['row','column'],
                    gap: 1,
                    alignItems: ['start'],
                  }}
                >
                  {(driverSchedule.driver && tanggalSelected) && 
                    <Typography color={"rgb(71 85 105)"} fontSize="1.2rem" fontWeight={"600"}>
                      {driverSchedule.driver.driverName} - {tanggalSelected.clone().format('MMMM YYYY')} 
                    </Typography>
                  }
                </Box>
                {/* <Box
                  sx={{
                    width: [1/1, 'fit-content'],
                    display: "flex",
                    flexDirection: ['column', 'row'],
                    gap: 2,
                  }}
                >
                  <CustomButton
                    width={[1/1,'fit-content']}
                    onClick={() => {
                    }}
                    startIcon={<SaveAsIcon size="14px" />}
                    name={'Save Draft'}
                  />
                  <CustomButton
                    width={[1/1,'fit-content']}
                    onClick={() => {
                    }}
                    startIcon={<SaveIcon size="14px" />}
                    name={'Save & Apply'}
                  />
                </Box> */}
              </Stack>
              <Box sx={{ width: "100%" }} mt={"2rem"}>
                <CustomCalendar filterDate={tanggalSelected} setOpenModal={setOpenDialogDate} setDataModal={setDataModal} dataModal={dataModal} schedule={driverSchedule.driverSchedule && driverSchedule.driverSchedule} />
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Stack>

      <DialogPenugasan openModal={openDialogDate} setOpenModal={setOpenDialogDate} dataModal={dataModal} setDataModal={setDataModal} notify={notify} setLoading={setLoading} driver={driverSchedule.driver && driverSchedule.driver} refreshData={refreshData} />
      <DialogMonthly openModal={openDialogMonth} setOpenModal={setOpenDialogMonth} merchantOption={merchantOption} notify={notify} setLoading={setLoading} refreshData={refreshData}/>
    </React.Fragment>
  );
};

export default Penugasan;
