import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography, Avatar } from "@mui/material";
import SelectField from "../../../components/select-field";
import SelectField2 from "../../../components/select-field-2";
import SearchIcon from "@mui/icons-material/Search";
import AddIcon from "@mui/icons-material/Add";
import { getProvinceCorporate, getPlaceCorporate } from "../../../services/car-rental/kota-operasional";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import DialogAdd from './components/dialog-add-lokasi'
import EditIcon from '@mui/icons-material/Edit';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import CustomDialogDelete from './components/custom-dialog-delete'

const KotaOperasional = ({
  label = "Operational City",
  titleInfo = "To Display Specific Data, Use the Filters Above.",
  subTitleInfo = ["Select the Merchant to Use the Terminal Filter"],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search",
}) => {
  const [limit, setLimit] = useState(25);
  const [offset, setOffset] = useState(0);
  const [disableNext, setDisableNext] = useState(false);
  const [data, setData] = useState([]);
  const [totalData, setTotalData] = useState(0);

  const [provinceInput, setProvinceInput] = useState('');

  const [merchant, setMerchant] = useState(null);
  const [province, setProvince] = useState(null);

  const [merchantOption, setMerchantOption] = useState([]);
  const [provinceOption, setProvinceOption] = useState([]);

  //untuk menyimpan data filter yang sudah di search
  const [ouCodeSelected, setOuCodeSelected] = useState("");
  const [provinceSelected, setProvinceSelected] = useState("");

  const [dialogAdd, setDialogAdd] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [dataDetail, setDataDetail] = useState('');
  const [errormerchant, setErrormerchant] = useState(false);

  const header = [
    {
      title: "No",
      value: "no",
      align: "left",
      width: "10px",
    },
    {
      title: "Action",
      value: "action",
      align: "left",
      width: "100px",
    },
    {
      title: "Province",
      value: "provinceName",
      align: "left",
      width: "100px",
    },
    {
      title: "City",
      value: "cityName",
      align: "left",
      width: "100px",
    },
  ];

  const renderCell = (item, header, index) => {
    if (header.value === "no") {
      return <span>{index >= 0 ? index + offset + 1 : ""}</span>;
    } else if (header.value === "provinceName") {
      return <span>{item.provinceName ? item.provinceName : '-'}</span>;
    } else if (header.value === "cityName") {
      return <span>{item.cityName ? item.cityName : "-"}</span>;
    } else if (header.value === "aksesPenjemputan") {
      return (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center'
          }}
        >
          <Avatar
            sx={{
              width: 28,
              height: 28,
              backgroundColor:
                item.aksesPenjemputan ? "success.main" : "error.main",
              color: "white",
            }}
          >
            {item.aksesPenjemputan ? (
              <CheckCircleIcon fontSize="small" />
            ) : (
              <CancelIcon fontSize="small" />
            )}
          </Avatar>
        </Box>
      );
    } else if (header.value === "aksesPengantaran") {
      return (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center'
          }}
        >
          <Avatar
            sx={{
              width: 28,
              height: 28,
              backgroundColor:
                item.aksesPengantaran ? "success.main" : "error.main",
              color: "white",
            }}
          >
            {item.aksesPengantaran ? (
              <CheckCircleIcon fontSize="small" />
            ) : (
              <CancelIcon fontSize="small" />
            )}
          </Avatar>
        </Box>
      );
    } else if (header.value === "action") {
      return (
        <Box 
            onClick={() => {
              setDataDetail(item.id)
              setOpenDeleteModal(true)
            }} 
            sx={{cursor: 'pointer'}}
          >
            <RemoveCircleOutlineIcon sx={{ color: 'red'}}/>
          </Box>
      );
    } 
  };

  // untuk rename option select field merchant
  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        label: item.corporateName,
        value: item.ouCode,
      });
    });
    // console.log('merchantArr',merchantArr.length)
    setMerchantOption(merchantArr);
  }, [merchantData]);

  useEffect(() => {
    if (merchant && merchant.value !== ouCodeSelected) {
      setProvince(null)
      setProvinceOption([]);
      setProvinceInput('')
      handleGetProvince(merchant.value);
    } else if (merchant === null) {
      setProvinceInput('')
      setProvinceOption([]);
      setProvince(null);
    }
  }, [merchant]);

  const handleGetProvince = async (ouCode) => {
    await getProvinceCorporate({ouCode})
    .then((res) => {
      if (res.success && res.result.total > 0) {
        const result = res.result.items;
        let provinceSet = new Set();

        result.forEach((data) => {
          provinceSet.add(data.provinceName);
        });

        let province = Array.from(provinceSet);
        setProvinceOption(province)
        notify(res.message || "Success Get Data Province", "success");
      }else{
        notify(res.message || "Data Province Not Found", "error");
      }
    })
    .catch((error) => {
      setData([]);
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    });
    setLoading(false);
  }

  //get laporan tiket
  const handleGetPlaces = async (query, data) => {
    if (data.ouCode) {
      setLoading(true);
      setOuCodeSelected(data.ouCode);

      await getPlaceCorporate(query, data)
      .then((res) => {
        if (res.success && res.result.total > 0) {
          setData(res.result.items);
          setTotalData(res.result.total);
          notify(res.message || "Success Get Data", "success");
        }else{
          notify(res.message || "Data Not Found", "error");
        }
      })
      .catch((error) => {
        setData([]);
        if (error.response) {
          // console.log(error.response.data.message);
          notify(error.response.data.message, "error");
        } else {
          // console.log(error.message || error);
          notify(error.message || error, "error");
        }
      });
      setLoading(false);
    } else {
      if (!data.ouCode) {
        setErrormerchant(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  };

  const pageChange = async (value) => {
    // console.log(value)
    const offset = value * limit;
    setOffset(offset);
    handleGetPlaces({ size: limit, page: value }, { ouCode: ouCodeSelected, provinceName : provinceSelected});
  };
  
  const rowsChange = async (e) => {
    setOffset(0);
    setLimit(e.props.value);
    handleGetPlaces({ size: e.props.value, page: 0 }, { ouCode: ouCodeSelected, provinceName : provinceSelected});
  };
  
  const refreshData = () => {
    handleGetPlaces({ size: limit, page: offset }, { ouCode: ouCodeSelected, provinceName : provinceSelected});
  }

  return (
    <React.Fragment>
      <Stack direction={"column"} p={"2rem"}>
        <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
          <CardContent sx={{ p: "2rem" }}>
            <Box display="flex" flexDirection="column">
              <Typography variant="h4" fontWeight="600">
                {label}
              </Typography>
              <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                <Box
                  sx={{
                    display: "grid",
                    gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)","repeat(4, 1fr)"],
                    gap: 2,
                  }}
                >
                  <SelectField
                    label={"Merchant"}
                    placeholder="Choose Merchant"
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={merchantOption}
                    selectedValue={merchant}
                    setValue={(newValue) => {
                      setMerchant(newValue);
                      setErrormerchant(false);
                    }}
                    required={true}
                    isError={errormerchant}
                    errorMessage={"Merchant is required"}
                  />

                  <SelectField2
                    label={"Province"} 
                    placeholder={provinceOption.length > 0 ? "All Province" : "Please Select Merchant"} 
                    sx={{ width: "100%", fontSize: "16px" }} 
                    data={provinceOption} 
                    selectedValue={province} 
                    setValue={(newValue) => {
                      setProvince(newValue);
                    }} 
                    inputValue={provinceInput}
                    setInputValue={(newValue) => {
                      setProvinceInput(newValue);
                    }}
                  />

                </Box>
              </Stack>
              <Stack
                sx={{
                  width: "100%",
                  display: "flex",
                  flexDirection: ["column", "column" ,"row"],
                  alignItems: ["end", "end", "center"],
                  gap: 3,
                  justifyContent: "space-between",
                  mt: "2rem",
                }}
              >
                <FilterMessageNote
                  sx={{
                    width: ["100%","100%", "50%"],
                  }}
                  title={titleInfo}
                  subtitle={subTitleInfo}
                />
                <div
                  style={{
                    display: "flex",
                    gap: 10,
                  }}
                >
                  <CustomButton
                    onClick={() => setDialogAdd(true)}
                    startIcon={<AddIcon size="14px" />}
                    name={'Add Location'}
                  />
                  <CustomButton
                    onClick={() => {
                      setLimit(25);
                      setOffset(0);
                      handleGetPlaces({ size: 25, page: 0 }, { 
                        ouCode: merchant ? merchant.value : "" ,
                        provinceName: province ? province : "" ,
                      });
                    }}
                    startIcon={<SearchIcon size="14px" />}
                    name={buttonFilter}
                  />
                </div>
              </Stack>
              <Box sx={{ width: "100%" }} mt={"2rem"}>
                <CustomPagination disableNext={disableNext} limit={limit} countLoading={false} offset={offset} count={totalData} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
                <CustomTable headers={header} items={data} renderCell={renderCell} />
                <CustomPagination disableNext={disableNext} limit={limit} countLoading={false} offset={offset} count={totalData} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Stack>

      <CustomDialogDelete
        openModal={openDeleteModal}
        setOpenModal={setOpenDeleteModal}
        data={dataDetail}
        title={"Delete Data"}
        notify={notify}
        refreshData={refreshData}
      />

      <DialogAdd openModal={dialogAdd} setOpenModal={setDialogAdd} merchantOption={merchantOption} notify={notify} setLoading={setLoading} refreshData={refreshData} />
    </React.Fragment>
  );
};

export default KotaOperasional;
