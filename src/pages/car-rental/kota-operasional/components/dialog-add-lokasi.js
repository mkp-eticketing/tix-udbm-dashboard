import { Box, Stack, Dialog, DialogContent, DialogTitle, DialogActions} from "@mui/material";
import SelectField from "../../../../components/select-field";
import SelectField2 from "../../../../components/select-field-2";
import CustomButton from "../../../../components/custom-button";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import React, { useState, useEffect } from 'react';
import { addPlaceCorporate, getCountry, getCity } from '../../../../services/car-rental/kota-operasional'
import { getCorporate } from '../../../../services/car-rental/corporate'

const DialogAddLocation = ({
  openModal,
  setOpenModal,
  fullWidth = true,
  maxWidth = 'sm',
  backgroundColorTitle = '#3875CA', 
  merchantOption= [],
  notify = () => {} ,
  setLoading = () => {},
  refreshData = () => {},
  ...others
}) => {
  const theme = useTheme()
  const [merchant, setMerchant] = useState(null);
  const [province, setProvince] = useState(null);
  const [city, setCity] = useState(null);

  const [provinceOption, setProvinceOption] = useState([]);
  const [cityOption, setCityOption] = useState([]);
  const [cityInput, setCityInput] = useState('');

  const [errormerchant, setErrorMerchant] = useState(false);
  const [errorProvince, setErrorProvince] = useState(false);
  const [errorCity, setErrorCity] = useState(false);

  const handleClose = () => {
    setMerchant(null)
    setProvince(null)
    setCity(null)
    setProvinceOption([])
    setCityOption([])
    setCityInput('')
    setErrorMerchant(false)
    setErrorProvince(false)
    setErrorCity(false)
    setOpenModal(false)
  }

  useEffect(() => {
    if (province) {
      setCityOption([]);
      setCity(null);
      setCityInput('')
      handleGetCityData(province.value);
    } else if (province === null) {
      setCityOption([]);
      setCity(null);
      setCityInput('')
    }
  }, [province]);

  useEffect(() => {
    handleGetProvinceData()
  }, []);

  const handleGetProvinceData = async () => {
    await getCountry()
    .then((res) => {
      if (res.success && res.result.length > 0) {
        const filteredCountry = res.result.filter(data => data.countryName === 'INDONESIA')
        const filteredProvince = filteredCountry[0].provinces.map((data) => {
          return({
            label : data.provinceName,
            value: data.provinceName
          })
        })
        setProvinceOption(filteredProvince)
        notify(res.message || "Success Get Province Data", "success");
      }else{
        notify(res.message || "Province Data Not Found", "error");
      }
    })
    .catch((error) => {
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    });
  }

  const handleGetCityData = async (provinceName) => {
    await getCity({provinceName})
    .then((res) => {
      if (res.success && res.result.length > 0) {
        const filteredCity = res.result.map((data) => {
          return({
            label : data.cityName,
            value: data.cityName
          })
        })
        setCityOption(filteredCity)
        notify(res.message || "Success Get City Data", "success");
      }else{
        notify(res.message || "City Data Not Found", "error");
      }
    })
    .catch((error) => {
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    });
  }
  

  const handleAddLocation = async () => {
    if (merchant && province && city) {
      setLoading(true);
      let corporateId
      await getCorporate({ouCode: merchant.value})
      .then((res) => {
        if (res.success && res.result.items.length === 1) {
          corporateId = res.result.items[0].id
        }
      })
      .catch((error) => {
        if (error.response) {
          // console.log(error.response.data.message);
          notify(error.response.data.message, "error");
        } else {
          // console.log(error.message || error);
          notify(error.message || error, "error");
        }
      });

      await addPlaceCorporate({corporateId, provinceName: province.value, cityName: city.value})
        .then((res) => {
          if (res.success) {
            handleClose()
            refreshData()
            notify(res.message || "Success Set Monthly Schedule", "success");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      setLoading(false);
    } else {
      if (!ouCode) {
        setErrorMerchant(true);
      }
      if (!province) {
        setErrorProvince(true);
      }
      if (!city) {
        setErrorCity(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  }

  return (
    <Dialog
      open={openModal}
      onClose={() => handleClose()}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}
      {...others}
    >
      <DialogTitle
        sx={{
          backgroundColor: backgroundColorTitle
        }}
      >
        Add Location
      </DialogTitle>

      <DialogContent>
        <Box
          sx={{
              display: "flex",
              flexDirection: "column",
              mt: "2rem",
              mb: '2rem',
              gap: 2
          }}>

            <SelectField
              label={"Merchant"}
              placeholder="Select Merchant"
              sx={{ width: "100%", fontSize: "16px" }}
              data={merchantOption}
              selectedValue={merchant}
              setValue={(newValue) => {
                setMerchant(newValue);
                setErrorMerchant(false);
              }}
              required={true}
              isError={errormerchant}
              errorMessage={"Merchant is required"}
            />

            <SelectField
              label={"Province"} 
              placeholder={"Select Province"} 
              sx={{ width: "100%", fontSize: "16px" }} 
              data={provinceOption} 
              selectedValue={province} 
              setValue={(newValue) => {
                setProvince(newValue);
                setErrorProvince(false);
              }} 
              required
              isError={errorProvince}
              errorMessage={"Province is required"}
            />

            <SelectField2
              label={"City"} 
              placeholder={cityOption.length > 0 ? "Select City" : "Please Select Province"} 
              sx={{ width: "100%", fontSize: "16px" }} 
              data={cityOption} 
              selectedValue={city} 
              setValue={(newValue) => {
                setCity(newValue);
                setErrorCity(false);
              }} 
              required
              isError={errorCity}
              errorMessage={"City is required"}
              inputValue={cityInput}
              setInputValue={setCityInput}
            />
        </Box>
      </DialogContent>

      <DialogActions>
        <Stack direction="row" justifyContent="flex-end" gap={2}>
          <CustomButton
            onClick={() => handleClose()}
            startIcon={<KeyboardBackspaceIcon size="14px" />}
            name={"Back"}
            sx={{
              backgroundColor: "grey",
              ":hover": {
                backgroundColor: "#333333"
              }
            }}
          />
          <CustomButton
            onClick={() => handleAddLocation()}
            startIcon={<SaveIcon size="14px" />}
            name={"Save"}
          />
        </Stack>
      </DialogActions>
    </Dialog>
  );
}

export default DialogAddLocation;