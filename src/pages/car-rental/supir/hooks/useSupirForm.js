import { useFormik } from "formik";
import * as yup from "yup";
import React from "react";
import { editDrivers, addDrivers } from "../../../../services/car-rental/supir"
import { getCorporate } from "../../../../services/car-rental/corporate"
import moment from "moment";

const initialValues = {
    merchant: '',
    driverName: '',
    driverCode: '',
    email: '',
    phoneNumber: ''
}

const UseSupirForm = ({
    notify = () => { },
    callback = () => { },
    isUpdate = false,
    id
}) => {

    const handleAddData = async (ouCode, values) => {
        let corporateId
        await getCorporate({ouCode})
        .then((res) => {
          if (res.success) {
            corporateId = res.result.items[0].id
          }
          else{
            notify(res.message, "error");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });

        await addDrivers({
            corporateId: corporateId,
            driverName: values.driverName,
            driverCode: values.driverCode,
            email: values.email,
            phoneNumber: values.phoneNumber,
        })
        .then((res) => {
            if(res.success){
                notify("Success Add Data", "success");
                callback();
            }
        })
        .catch((error) => {
            if (error.response) {
              // console.log(error.response.data.message);
              notify(error.response.data.message, "error");
            } else {
              // console.log(error.message || error);
              notify(error.message || error, "error");
            }
        });
    }

    const formik = useFormik({
        initialValues,
        onSubmit: (values) => {
            if (isUpdate) {
                editDrivers(id, {
                  driverName: values.driverName,
                  driverCode: values.driverCode,
                  email: values.email,
                  phoneNumber: values.phoneNumber,
                }).then((res) => {
                  if(res.success){
                      notify("Success Update Data", "success");
                      callback();
                  }
                }).catch((error) => {
                  if (error.response) {
                    // console.log(error.response.data.message);
                    notify(error.response.data.message, "error");
                  } else {
                    // console.log(error.message || error);
                    notify(error.message || error, "error");
                  }
                });
            } else {
                // console.log(values.merchant.value)
                handleAddData(values.merchant.value, values)
            }
        },
        validationSchema: isUpdate ? 
        yup.object({
            driverName: yup.string().required('Name is required'),
            driverCode: yup.string().required('Code is required'),
            email: yup.string().required('Email is required'),
            phoneNumber: yup.number().required('Phone Number is required'),
        }) : 
        yup.object({
            merchant: yup.object().required('Merchant is required'),
            driverName: yup.string().required('Name is required'),
            driverCode: yup.string().required('Code is required'),
            email: yup.string().required('Email is required'),
            phoneNumber: yup.number().required('Phone Number is required'),
        }), 
        enableReinitialize: true,
    });

    return {
        ...formik
    }
}

export default UseSupirForm;