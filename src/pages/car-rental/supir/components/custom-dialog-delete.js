import React from "react"
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Box} from '@mui/material'
import CustomButton from "../../../../components/custom-button";
import DeleteIcon from '@mui/icons-material/Delete';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import { deleteDrivers } from "../../../../services/car-rental/supir"

export default function FormDialog({
  openModal,
  setOpenModal,
  title,
  contentText = "Are you sure you want to delete this data ?",
  fullWidth = true,
  maxWidth = 'sm',
  data,
  notify,
  refreshData = () => {},
  ...others
}) {

  const handleClose = () => {
    setOpenModal(false)
  }

  const handleDelete = async () => {
    await deleteDrivers(data)
    .then((res) => {
      if (res.success) {
        notify('Success Delete Data', "success");
        refreshData()
        handleClose()
      }
      else{
        notify(res.message, "error");
      }
    })
    .catch((error) => {
      handleClose()
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    });
  }

  return (
    <Dialog
      open={openModal}
      onClose={() => handleClose()}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      {...others}
    >
      <DialogTitle
        sx={{
          backgroundColor: '#d32f2f'
        }}
      >
        {title}
      </DialogTitle>
      <DialogContent>
        <DialogContentText
          sx={{
            marginTop: '16px',
            marginBottom: '16px'
          }}
        >{contentText}</DialogContentText>
      </DialogContent>
      <DialogActions>
       <CustomButton
          onClick={() => handleClose()}
          startIcon={<KeyboardBackspaceIcon size="14px" />}
          name={"Cancel"}
          sx={{
              backgroundColor: "grey",
              ":hover": {
                  backgroundColor: "#333333"
              }
          }}
        />
        <CustomButton
          onClick={() => {handleDelete()}}
          startIcon={<DeleteIcon size="14px" />}
          name={"Delete"}
          sx={{
            backgroundColor: "error.main",
            ":hover": {
              backgroundColor: 'error.dark'
          }
        }}
        />
      </DialogActions>
    </Dialog>
  );
}
