import { Box, Stack,  Dialog, DialogContent, DialogTitle, DialogActions } from "@mui/material";
import React, { useEffect, useState } from "react";
import TextField from "../../../../components/text-field";
import SwitchField from "../../../../components/switch-field"
import CustomButton from "../../../../components/custom-button";
import SelectField from "../../../../components/select-field";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import UseSupirForm from "../hooks/useSupirForm";
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { getDriversById} from "../../../../services/car-rental/supir"

const SupirForm = ({
    openForm = false,
    onOpen = () => { },
    merchantOption = [],
    notify = () => { },
    refreshData = () => { },
    dataId = null,
    setDataId = () => { },
}) => {
  const theme = useTheme()

  const formik = UseSupirForm({
      notify: notify,
      callback: () => {
        handleClose()
        refreshData()
      },
      isUpdate: dataId ? true : false,
      id: dataId
  });

  const handleGetDataById = async (id) => {
    await getDriversById(id)
      .then((res) => {
        if (res.success && res.result) {
          notify('Success Get Data', "success");
          formik.setValues({
            driverName: res.result.driverName,
            driverCode: res.result.driverCode,
            email: res.result.email,
            phoneNumber: res.result.phoneNumber,
            corporate: res.result.corporate
          })
        }
        else{
          notify(res.message, "error");
        }
      })
      .catch((error) => {
        if (error.response) {
          // console.log(error.response.data.message);
          notify(error.response.data.message, "error");
        } else {
          // console.log(error.message || error);
          notify(error.message || error, "error");
        }
      });
  }

  useEffect(() => {
      if (dataId) {
        handleGetDataById(dataId)
      }
  }, [dataId]);

  const handleClose = () => {
    onOpen(false)
    formik.resetForm()
    setDataId(null)
  }

  const handleNumber = (data) => {
    const values = data.replace(/\D/g, '')
    if (values === '' || /^[1-9]\d*$/.test(values)) {
      formik.setFieldValue("phoneNumber", values)
    }
  };

  return (
    <Dialog
      open={openForm}
      onClose={() => {
        handleClose()
      }}
      fullWidth={true}
      maxWidth={'sm'}
      fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}
    >
      <DialogTitle
        sx={{
          backgroundColor: '#3875CA'
        }}
      >
        {dataId ? 'Edit Driver Data' : 'Add Driver Data'}
      </DialogTitle>
      <DialogContent>
        <Box
          sx={{
              display: "flex",
              flexDirection: "column",
              mt: "2rem",
              gap: 2
          }}>

          {!dataId && <SelectField
            required
            label={"Merchant"}
            placeholder="Choose Merchant"
            data={merchantOption}
            selectedValue={formik.values.merchant}
            setValue={(newValues) => {
              formik.setFieldValue("merchant", newValues)
            }}
            isError={formik.touched.merchant && formik.errors.merchant}
            errorMessage={formik.touched.merchant && formik.errors.merchant}
          />}
          <TextField
            required
            placeholder='Input Here'
            label={'Name'}
            value={formik.values.driverName}
            onChange={(e) => formik.setFieldValue("driverName", e.target.value)}
            isError={formik.touched.driverName && formik.errors.driverName}
            errorMessage={formik.touched.driverName && formik.errors.driverName}
          />
          <TextField
            required
            placeholder='Input Here'
            label={'Driver Code'}
            value={formik.values.driverCode}
            onChange={(e) => formik.setFieldValue("driverCode", e.target.value)}
            isError={formik.touched.driverCode && formik.errors.driverCode}
            errorMessage={formik.touched.driverCode && formik.errors.driverCode}
          />
          {/* <TextField
            required
            placeholder='Input Here'
            label={'KTP'}
            value={formik.values.ktp}
            onChange={(e) => formik.setFieldValue("ktp", e.target.value)}
            isError={formik.touched.ktp && formik.errors.ktp}
            errorMessage={formik.touched.ktp && formik.errors.ktp}
          /> */}
          
          <TextField
            required
            placeholder='Input Here'
            label={'Email'}
            value={formik.values.email}
            onChange={(e) => formik.setFieldValue("email", e.target.value)}
            isError={formik.touched.email && formik.errors.email}
            errorMessage={formik.touched.email && formik.errors.email}
          />
          
          <TextField
            required
            placeholder='Input Here'
            label={'Phone Number'}
            value={formik.values.phoneNumber}
            onChange={(e) => handleNumber(e.target.value)}
            isError={formik.touched.phoneNumber && formik.errors.phoneNumber}
            errorMessage={formik.touched.phoneNumber && formik.errors.phoneNumber}
            InputProps={{
              inputProps: {
                inputMode: 'numeric',
                pattern: '[0-9]*',
              },
            }}
          />
          {/* <SwitchField
            required
            placeholder='Input Here'
            label={'Status'}
            selectedValue={formik.values.status}
            setValue={(newValues) => formik.setFieldValue("status", newValues)}
          /> */}
        </Box>
      </DialogContent>
      <DialogActions>
        <Stack direction="row" justifyContent="flex-end" gap={2} mt={4}>
          <CustomButton
            onClick={() => {
              handleClose()
            }}
            startIcon={<KeyboardBackspaceIcon size="14px" />}
            name={"Back"}
            sx={{
                backgroundColor: "grey",
                ":hover": {
                    backgroundColor: "#333333"
                }
            }}
          />
          <CustomButton
            startIcon={<SaveIcon size="14px" />}
            name={"Save"}
            onClick={() => {
              formik.handleSubmit()
            }}
          />
        </Stack>  
      </DialogActions>
    </Dialog>
  )
}

export default SupirForm;