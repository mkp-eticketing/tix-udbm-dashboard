import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography, Avatar } from "@mui/material";
import SelectField from "../../../components/select-field";
import SearchIcon from "@mui/icons-material/Search";
import { getDrivers } from "../../../services/car-rental/supir";
import CustomTable from "../../../components/custom-table";
import CustomPagination from "../../../components/custom-pagination";
import SwitchField from "../../../components/switch-field";
import TextField from "../../../components/text-field";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import CustomDialogDelete from "./components/custom-dialog-delete";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import SupirForm from "./forms/supir-form";

const Supir = ({ label = "Driver", titleInfo = "To Display Specific Data, Use the Filters Above.", subTitleInfo = [], merchantData = [], setLoading = () => {}, notify = () => {}, buttonFilter = "Search" }) => {
  const [limit, setLimit] = useState(25);
  const [offset, setOffset] = useState(0);
  const [disableNext, setDisableNext] = useState(false);
  const [data, setData] = useState([]);
  const [totalData, setTotalData] = useState(0);

  const [merchant, setMerchant] = useState(null);
  const [merchantOption, setMerchantOption] = useState([]);
  const [ouCodeSelected, setOuCodeSelected] = useState("");

  const [errormerchant, setErrormerchant] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openForm, setOpenForm] = useState(false);
  const [dataDetail, setDataDetail] = useState(null);

  const header = [
    {
      title: "No",
      value: "no",
      align: "left",
      width: "10px",
    },
    {
      title: "Action",
      value: "action",
      align: "left",
      width: "100px",
    },
    {
      title: "Name",
      value: "driverName",
      align: "left",
      width: "200px",
    },
    {
      title: "Driver Code",
      value: "driverCode",
      align: "left",
      width: "150px",
    },
    {
      title: "Email",
      value: "email",
      align: "left",
      width: "200px",
    },
    {
      title: "Phone Number",
      value: "phoneNumber",
      align: "left",
      width: "200px",
    },
  ];

  const renderCell = (item, header, index) => {
    if (header.value === "no") {
      return <span>{index >= 0 ? index + offset + 1 : ""}</span>;
    } else if (header.value === "driverName") {
      return <span>{item.driverName ? item.driverName : "-"}</span>;
    } else if (header.value === "driverCode") {
      return <span>{item.driverCode ? item.driverCode : "-"}</span>;
    } else if (header.value === "email") {
      return <span>{item.email ? item.email : "-"}</span>;
    } else if (header.value === "phoneNumber") {
      return <span>{item.phoneNumber ? item.phoneNumber : "-"}</span>;
    } else if (header.value === "action") {
      return (
        <Box
          sx={{
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: header.align === "left" ? "start" : header.align === "right" ? "end" : "center",
            gap: 2,
          }}
        >
          <Box
            onClick={() => {
              setOpenForm(true);
              setDataDetail(item.id);
            }}
            sx={{ cursor: "pointer" }}
          >
            <EditIcon sx={{ color: "#3875CA" }} />
          </Box>

          <Box
            onClick={() => {
              setOpenDeleteModal(true);
              setDataDetail(item.id);
            }}
            sx={{ cursor: "pointer" }}
          >
            <RemoveCircleOutlineIcon sx={{ color: "red" }} />
          </Box>
        </Box>
      );
    }
  };

  // untuk rename option select field merchant
  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        label: item.corporateName,
        value: item.ouCode,
      });
    });
    // console.log('merchantArr',merchantArr.length)
    setMerchantOption(merchantArr);
  }, [merchantData]);

  const handleDataDrivers = async (query, { ouCode }) => {
    if (ouCode) {
      setData([])
      setLoading(true);
      setOuCodeSelected(ouCode);
      // console.log(query);

      await getDrivers(query, { ouCode })
        .then((res) => {
          if (res.success && res.result.total > 0) {
            // console.log("Success", res.result.items);
            setData(res.result.items);
            setTotalData(res.result.total);
            notify(res.message || "Success Get Data", "success");
          }else{
            notify(res.message || "Data Not Found", "error");
          }
        })
        .catch((error) => {
          setData([]);
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      setLoading(false);
    } else {
      if (!ouCode) {
        setErrormerchant(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  };

  const pageChange = async (value) => {
    console.log(value)
    const offset = value * limit;
    setOffset(offset);
    handleDataDrivers({ size: limit, page: value }, { ouCode: ouCodeSelected });
  };

  const rowsChange = async (e) => {
    setOffset(0);
    setLimit(e.props.value);
    handleDataDrivers({ size: e.props.value, page: 0 }, { ouCode: ouCodeSelected });
  };

  const refreshData = () => {
    handleDataDrivers({ size: limit, page: offset }, { ouCode: ouCodeSelected });
  };

  return (
    <React.Fragment>
      <Stack direction={"column"} p={"2rem"}>
        <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
          <CardContent sx={{ p: "2rem" }}>
            <Box display="flex" flexDirection="column">
              <Typography variant="h4" fontWeight="600">
                {label}
              </Typography>
              <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                <Box
                  sx={{
                    display: "grid",
                    gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(4, 1fr)"],
                    gap: 2,
                  }}
                >
                  <SelectField
                    label={"Merchant"}
                    placeholder="Choose Merchant"
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={merchantOption}
                    selectedValue={merchant}
                    setValue={(newValue) => {
                      setMerchant(newValue);
                      setErrormerchant(false);
                    }}
                    required={true}
                    isError={errormerchant}
                    errorMessage={"Merchant is required"}
                  />
                </Box>
              </Stack>
              <Stack
                sx={{
                  width: "100%",
                  display: "flex",
                  flexDirection: ["column", "column" ,"row"],
                  alignItems: ["end", "end", "center"],
                  gap: 3,
                  justifyContent: "space-between",
                  mt: "2rem",
                }}
              >
                <FilterMessageNote
                  sx={{
                    width: ["100%","100%", "50%"],
                  }}
                  title={titleInfo}
                  subtitle={subTitleInfo}
                />
                <div
                  style={{
                    display: "flex",
                    gap: 10,
                  }}
                >
                  <CustomButton
                    onClick={() => {
                      setOpenForm(true);
                    }}
                    startIcon={<AddIcon size="14px" />}
                    name={"Add Data"}
                  />
                  <CustomButton
                    onClick={() => {
                      setLimit(25);
                      setOffset(0);
                      handleDataDrivers({ size: 25, page: 0 }, { ouCode: merchant ? merchant.value : "" });
                    }}
                    startIcon={<SearchIcon size="14px" />}
                    name={buttonFilter}
                  >
                    Filter
                  </CustomButton>
                </div>
              </Stack>
              <Box sx={{ width: "100%" }} mt={"2rem"}>
                <CustomPagination disableNext={disableNext} limit={limit} countLoading={false} offset={offset} count={totalData} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
                <CustomTable headers={header} items={data} renderCell={renderCell} />
                <CustomPagination disableNext={disableNext} limit={limit} countLoading={false} offset={offset} count={totalData} pageChange={(event, v) => pageChange(v)} rowsChange={async (event, e) => rowsChange(e)} />
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Stack>

      <CustomDialogDelete
        openModal={openDeleteModal}
        setOpenModal={setOpenDeleteModal}
        data={dataDetail}
        title={"Delete Data"}
        notify={notify}
        refreshData={refreshData}
      />

      <SupirForm onOpen={setOpenForm} openForm={openForm} dataId={dataDetail} setDataId={setDataDetail} notify={notify} merchantOption={merchantOption} refreshData={() => refreshData()} />
    </React.Fragment>
  );
};

export default Supir;
