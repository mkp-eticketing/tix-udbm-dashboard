import React, { useEffect, useState } from "react";
import { Box, Card, CardContent, Stack, Typography } from "@mui/material";
import SelectField from "../../../components/select-field";
import SelectField2 from "../../../components/select-field-2";
import SearchIcon from "@mui/icons-material/Search";
import DatePicker from "../../../components/datepicker-field";
import FilterMessageNote from "../../../components/filter-message-note";
import CustomButton from "../../../components/custom-button";
import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";
import SaveAsIcon from "@mui/icons-material/SaveAs";
import CustomCalendar from "./components/custom-calendar-penjadwalan-tarif";
import moment from "moment";
import DialogTarif from "./components/dialog-tarif";
import DialogMonthly from "./components/dialog-monthly-schedule";
import { getCarTypeProvider } from "../../../services/car-rental/armada";
import { getVehiclePrice } from "../../../services/car-rental/penjadwalan-tarif";

const PenjadwalanTarif = ({ 
  label = "Vehicle Price",
  titleInfo = "To Display Specific Data, Use the Filters Above.",
  subTitleInfo = [],
  merchantData = [],
  setLoading = () => {},
  notify = () => {},
  buttonFilter = "Search" 
}) => {
  const [merchant, setMerchant] = useState(null);
  const [vehicle, setVehicle] = useState(null);
  const [tanggal, setTanggal] = useState(moment().startOf("month"));

  const [merchantOption, setMerchantOption] = useState([]);
  const [vehicleOption, setVehicleOption] = useState([]);

  const [vehicleInput, setVehicleInput] = useState('');

  //untuk menyimpan data filter yang sudah di search
  const [ouCodeSelected, setOuCodeSelected] = useState("");
  const [vehicleSelected, setVehicleSelected] = useState("");
  const [tanggalSelected, setTanggalSelected] = useState("");

  const [errorMerchant, setErrorMerchant] = useState(false);
  const [errorVehicle, setErrorVehicle] = useState(false);
  const [errorTanggal, setErrorTanggal] = useState(false);

  const [openDialogDate, setOpenDialogDate] = useState(false);
  const [openDialogMonth, setOpenDialogMonth] = useState(false);
  const [dataModal, setDataModal] = useState({});

  const [vehiclePrice, setVehiclePrice] = useState({});

  // untuk rename option select field merchant
  useEffect(() => {
    let merchantArr = [];
    merchantData.map((item) => {
      merchantArr.push({
        label: item.corporateName,
        value: item.ouCode,
      });
    });
    // console.log('merchantArr',merchantArr.length)
    setMerchantOption(merchantArr);
  }, [merchantData]);

  useEffect(() => {
    if (merchant && merchant.value !== ouCodeSelected) {
      setVehicle(null)
      setVehicleOption([]);
      setVehicleInput('')
      handleGetVehicle(merchant.value);
    } else if (merchant === null) {
      setVehicleInput('')
      setVehicleOption([]);
      setVehicle(null);
    }
  }, [merchant]);

  const handleGetVehicle = async (ouCode) => {
    await getCarTypeProvider({ouCode})
    .then((res) => {
      if (res.success && res.result.total > 0) {
        const filteredData = res.result.items.map((data) => {
          return({
            label : `${data.carType.brand.brandName} - ${data.carType.carName}`,
            value: data.id
          })
        })
        setVehicleOption(filteredData)
        notify(res.message || "Success Get Data", "success");
      }else{
        notify(res.message || "Data Not Found", "error");
      }
    })
    .catch((error) => {
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    });
  }

  const handleGetPrice = async ({ouCode, vehicle, tanggal}) => {
    if (ouCode && vehicle && tanggal) {
      setLoading(true);
      setOuCodeSelected(ouCode);
      setVehicleSelected(vehicle);
      setTanggalSelected(tanggal);

      const formatDate = tanggal.clone().format('YYYY-MM')

      await getVehiclePrice({vehicle: vehicle.value, tanggal : formatDate})
        .then((res) => {
          if (res.success && res.result.carTypeProvider.id) {
            setVehiclePrice(res.result)
            notify(res.message || "Success Get Data List", "success");
            if(res.result.schedule.length === 0){
              notify("Vehicle don't Have Schedule yet", "warning")
            }
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      setLoading(false);
    } else {
      if (!ouCode) {
        setErrorMerchant(true);
      }
      if (!vehicle) {
        setErrorVehicle(true);
      }
      if (!tanggal) {
        setErrorTanggal(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  };

  const refreshData = () => {
    handleGetPrice({ouCode: ouCodeSelected, vehicle: vehicleSelected, tanggal : tanggalSelected})
  }

  return (
    <React.Fragment>
      <Stack direction={"column"} p={"2rem"}>
        <Card sx={{ minWidth: 275, borderRadius: "0.75rem" }}>
          <CardContent sx={{ p: "2rem" }}>
            <Box display="flex" flexDirection="column">
              <Typography variant="h4" fontWeight="600">
                {label}
              </Typography>
              <Stack display="flex" direction="column" mt={"2rem"} gap={2}>
                <Box
                  sx={{
                    display: "grid",
                    gridTemplateColumns: ["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(3, 1fr)"],
                    gap: 2,
                  }}
                >
                  <SelectField
                    label={"Merchant"}
                    placeholder="Select Merchant"
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={merchantOption}
                    selectedValue={merchant}
                    setValue={(newValue) => {
                      setMerchant(newValue);
                      setErrorMerchant(false);
                    }}
                    required={true}
                    isError={errorMerchant}
                    errorMessage={"Merchant is required"}
                  />

                  <SelectField2
                    label={"Vehicle"}
                    placeholder={vehicleOption.length > 0 ? "Select Vehicle" : "Please Select Merchant"}
                    sx={{ width: "100%", fontSize: "16px" }}
                    data={vehicleOption}
                    selectedValue={vehicle}
                    setValue={(newValue) => {
                      setVehicle(newValue);
                      setErrorVehicle(false);
                    }}
                    inputValue={vehicleInput}
                    setInputValue={(newValue) => {
                      setVehicleInput(newValue);
                    }}
                    required
                    isError={errorVehicle}
                    errorMessage={"Vehicle is required"}
                  />

                  <DatePicker
                    label={"Date"}
                    placeholder="MMM YYYY"
                    sx={{ width: "100%", fontSize: "16px" }}
                    value={tanggal}
                    onChange={(newValue) => {
                      setTanggal(newValue);
                    }}
                    required
                    format={"MMM YYYY"}
                    views={["year", "month"]}
                    isError={errorTanggal}
                    errorMessage={"Date is required"}
                  />
                </Box>
              </Stack>
              <Stack
                sx={{
                  width: "100%",
                  display: "flex",
                  flexDirection: ["column", "column" ,"row"],
                  alignItems: ["end", "end", "center"],
                  gap: 3,
                  justifyContent: "space-between",
                  mt: "2rem",
                }}
              >
                <FilterMessageNote
                  sx={{
                    width: ["100%","100%", "50%"],
                  }}
                  title={titleInfo}
                  subtitle={subTitleInfo}
                />
                <div
                  style={{
                    display: "flex",
                    gap: 10,
                  }}
                >
                  <CustomButton
                    onClick={() => {
                      setOpenDialogMonth(true)
                    }}
                    startIcon={<EditIcon size="14px" />}
                    name={'Set Monthly'}
                  />
                  <CustomButton
                    onClick={() => {
                      handleGetPrice({
                        ouCode: merchant ? merchant.value : '',
                        vehicle,
                        tanggal
                      })
                    }}
                    startIcon={<SearchIcon size="14px" />}
                    name={buttonFilter}
                  >
                    Filter
                  </CustomButton>
                </div>
              </Stack>
              <Stack
                sx={{
                  width: "100%",
                  display: "flex",
                  flexDirection: ["column", "row"],
                  alignItems: ["start", "center"],
                  gap: 3,
                  justifyContent: "space-between",
                  mt: "2rem",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: ["row", "column"],
                    gap: 1,
                    alignItems: ["start"],
                  }}
                >
                  {(vehiclePrice.carTypeProvider && tanggalSelected) &&
                    <Typography color={"rgb(71 85 105)"} fontSize="1.2rem" fontWeight={"600"}>
                      {vehiclePrice.carTypeProvider.carType.brand.brandName} {vehiclePrice.carTypeProvider.carType.carName} - {moment(tanggalSelected).format("MMM YYYY")}
                    </Typography>
                  }
                </Box>
                {/* <Box
                  sx={{
                    width: [1 / 1, "fit-content"],
                    display: "flex",
                    flexDirection: ["column", "row"],
                    gap: 2,
                  }}
                >
                  <CustomButton width={[1 / 1, "fit-content"]} onClick={() => {}} startIcon={<SaveAsIcon size="14px" />} name={"Save Draft"} />
                  <CustomButton width={[1 / 1, "fit-content"]} onClick={() => {}} startIcon={<SaveIcon size="14px" />} name={"Save & Apply"} />
                </Box> */}
              </Stack>
              <Box sx={{ width: "100%" }} mt={"2rem"}>
                <CustomCalendar filterDate={tanggalSelected} setOpenModal={setOpenDialogDate} setDataModal={setDataModal} dataModal={dataModal} pricing={vehiclePrice.carTypePricing && vehiclePrice.carTypePricing} />
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Stack>

      <DialogTarif openModal={openDialogDate} setOpenModal={setOpenDialogDate} dataModal={dataModal} setDataModal={setDataModal} notify={notify} setLoading={setLoading} carTypeProvider={vehiclePrice.carTypeProvider && vehiclePrice.carTypeProvider} refreshData={refreshData} />
      <DialogMonthly openModal={openDialogMonth} setOpenModal={setOpenDialogMonth} merchantOption={merchantOption} notify={notify} setLoading={setLoading} refreshData={refreshData}/>

    </React.Fragment>
  );
};

export default PenjadwalanTarif;
