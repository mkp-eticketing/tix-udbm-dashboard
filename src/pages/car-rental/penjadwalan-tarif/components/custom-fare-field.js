import React, { useState, useEffect } from 'react';
import { Autocomplete, Box, TextField, Typography, InputAdornment } from "@mui/material";
import { thousandSeparator } from "../../../../utils/thousand-separator";
const FareField = ({
    label,
    placeholder,
    data,
    setValue,
    required = false,
    selectedValue,
    errorMessage = "Merchant wajib diisi",
    isError = false,
    displayValue = '',
    setDisplayValue = () => {},
    ...other
}) => {
    const handleChange = (event) => {
      const values = event.target.value.replace(/\D/g, '')
      if (values === '' || /^[1-9]\d*$/.test(values)) {
        setValue(values);
        setDisplayValue(thousandSeparator(values));
      }
    };

    return (
        <Box sx={{ display: "flex", flexDirection: "column", gap: 1, width: "100%" }}>
            <Typography color={"rgb(71 85 105)"} fontSize="0.875rem" fontWeight={"600"} height={25}>{label} <span style={{ color: 'red' }}>{`${required ? "*" : ""}`}</span></Typography>
            <TextField
                variant="standard"
                value={displayValue}
                onChange={handleChange}
                placeholder={placeholder}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      Rp.
                    </InputAdornment>
                  ),
                  inputProps: {
                    inputMode: 'numeric',
                    pattern: '[0-9]*',
                  },
                }}
                {...other}
            />
            <Typography fontSize={12} color="red">{isError && errorMessage}</Typography>
        </Box>
    )
}
export default FareField;