import React, { useState, useEffect } from "react";
import { Box, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import moment from "moment";
import { formatCurrency } from "../../../../utils/format-currency";

const Calendar = ({ filterDate, setOpenModal, dataModal, setDataModal, pricing}) => {
  const [daysData, setDaysData] = useState(null);
  const [cellData, setCellData] = useState(null);

  useEffect(() => {
    setDaysData(null);
    setCellData(null);
    renderDays(moment());
    (filterDate && pricing) && renderCells(filterDate, dataModal);
  }, [filterDate, dataModal, pricing]);

  const renderDays = (filterDate) => {
    const dateFormat = "dddd";
    const days = [];
    let startDate = filterDate.clone().startOf("isoWeek");
    for (let i = 0; i < 7; i++) {
      const formattedDate = startDate.clone().add(i, "days").format(dateFormat).toUpperCase();
      days.push(
        <TableCell
          sx={{
            textAlign: "center",
            fontWeight: "600",
            minWidth: "150px",
            padding: ".75em 0",
            borderRight: "1px solid #ccc",
            borderBottom: "1px solid #ccc",
            color: formattedDate === "MINGGU" || formattedDate === "SABTU" ? "rgb(211, 47, 47)" : "black",
            "&:last-child": {
              borderRight: "none",
            },
          }}
          key={i}
        >
          {formattedDate}
        </TableCell>
      );
    }
    setDaysData(days);
  };

  const renderCells = (filterDate, dataModal) => {
    const monthStart = filterDate.clone().startOf("month");
    const monthEnd = filterDate.clone().endOf("month");
    const startDate = monthStart.clone().startOf("isoWeek");
    const endDate = monthEnd.clone().endOf("isoWeek");

    const rows = [];
    let days = [];
    let day = startDate.clone();
    let formattedDate = "";

    while (day <= endDate) {
      
      for (let i = 0; i < 7; i++) {
        const formatedClone = day.clone().format('YYYY-MM-DD')
        const filteredPricing = pricing.filter(data => data.date === formatedClone)
        formattedDate = day.format("D");

        const price = (filteredPricing[0] && filteredPricing[0].price) || 0
        let displayStatus
        let pointerEvents
        let backgroundColor
        
        if (day.isSame(monthStart, "month")) {
          if(price > 0){
            backgroundColor = "#fff" 
          }
          else{
            backgroundColor = "#eee"
          }
          pointerEvents = true;
        } else {
          backgroundColor = "#a6a6a6"
          pointerEvents = "none";
        }

        days.push(
          <TableCell
            key={i}
            sx={{
              position: "relative",
              height: "5em",
              padding: 0,
              overflow: "hidden",
              borderRight: "1px solid #ccc",
              borderBottom: "1px solid #ccc",
              cursor: "pointer",
              backgroundColor: backgroundColor,
              pointerEvents: pointerEvents,
                // ":hover": {
                //   background: "#3875CA",
                //   transition: "0.1s ease-out",
                // },
              "&:last-child": {
                borderRight: "none",
              },
            }}
            onClick={() => {
              filteredPricing[0] ? setDataModal(filteredPricing[0]) : setDataModal({date: formatedClone});
              setOpenModal(true);
            }}
          >
            {day.isSame(monthStart, "month") && (
              <Box>
                <Typography
                  sx={{
                    position: "absolute",
                    fontSize: "",
                    lineHeight: 1,
                    top: ".75em",
                    right: ".75em",
                    fontWeight: 700,
                  }}
                >
                  {formattedDate}
                </Typography>
                <Typography
                  sx={{
                    position: "absolute",
                    lineHeight: 1,
                    top: "50%",
                    right: ".75em",
                    fontWeight: 500,
                    fontSize: "1rem"
                  }}
                >
                  {price > 0 ? formatCurrency(price) : 'Not Available'}
                </Typography>
              </Box>
            )}
          </TableCell>
        )

        day.add(1, "day")
      }

      rows.push(
        <TableRow sx={{}} key={day}>
          {days}
        </TableRow>
      );

      days = [];
    }

    setCellData(rows);
  };

  return (
    <TableContainer>
      <Table
        sx={{
          minWidth: "650px",
          background: "#fff",
        }}
      >
        <TableHead>
          <TableRow>{daysData}</TableRow>
        </TableHead>

        <TableBody>{cellData}</TableBody>
      </Table>
    </TableContainer>
  );
};

export default Calendar;
