import { Box, Stack, Dialog, DialogContent, DialogTitle, DialogActions} from "@mui/material";
import CustomPrice from './custom-fare-field'
import SelectField from "../../../../components/select-field";
import SelectField2 from "../../../../components/select-field-2";
import CustomButton from "../../../../components/custom-button";
import DatePicker from "../../../../components/datepicker-field";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import React, { useState, useEffect } from 'react';
import moment from "moment";
import { getCarTypeProvider } from "../../../../services/car-rental/armada";
import { addVehiclePrice } from "../../../../services/car-rental/penjadwalan-tarif";

const DialogMonthly = ({
  openModal,
  setOpenModal,
  fullWidth = true,
  maxWidth = 'sm',
  backgroundColorTitle = '#3875CA', 
  merchantOption= [],
  notify = () => {} ,
  setLoading = () => {},
  refreshData = () => {},
  ...others
}) => {
  const theme = useTheme()
  const [merchant, setMerchant] = useState(null);
  const [vehicle, setVehicle] = useState(null);
  const [tanggal, setTanggal] = useState(moment());
  const [price, setPrice] = useState(0);
  const [displayPrice, setDisplayPrice] = useState(''); 

  const [vehicleInput, setVehicleInput] = useState('');

  const [vehicleOption, setVehicleOption] = useState([]);

  const [errormerchant, setErrorMerchant] = useState(false);
  const [errorVehicle, setErrorVehicle] = useState(false);
  const [errorTanggal, setErrorTanggal] = useState(false);
  const [errorPrice, setErrorPrice] = useState(false);

  const handleClose = () => {
    setOpenModal(false)
    setMerchant(null)
    setVehicle(null)
    setTanggal(null)
    setVehicleOption([])
    setVehicleInput('')
    setPrice(0)
    setDisplayPrice('')
    setErrorMerchant(false)
    setErrorVehicle(false)
    setErrorTanggal(false)
    setErrorPrice(false)
  }

  useEffect(() => {
    if (merchant) {
      setVehicle(null)
      setVehicleOption([]);
      setVehicleInput('')
      handleGetVehicle(merchant.value);
    } else {
      setVehicleInput('')
      setVehicleOption([]);
      setVehicle(null);
    }
  }, [merchant]);

  const handleGetVehicle = async (ouCode) => {
    await getCarTypeProvider({ouCode})
    .then((res) => {
      if (res.success && res.result.total > 0) {
        const filteredData = res.result.items.map((data) => {
          return({
            label : `${data.carType.brand.brandName} - ${data.carType.carName}`,
            value: data.id
          })
        })
        setVehicleOption(filteredData)
        notify(res.message || "Success Get Data", "success");
      }else{
        notify(res.message || "Data Not Found", "error");
      }
    })
    .catch((error) => {
      if (error.response) {
        // console.log(error.response.data.message);
        notify(error.response.data.message, "error");
      } else {
        // console.log(error.message || error);
        notify(error.message || error, "error");
      }
    });
  }

  const handleSetSchedule = async () => {
    if (merchant && vehicle && tanggal && price) {
      setLoading(true);

      const startDate = tanggal.clone().startOf("month");
      const endDate = tanggal.clone().endOf("month");

      let day = startDate.clone();
      let vehicleSchedule = []

      while (day <= endDate) {
        const formattedDate = day.clone().format('YYYY-MM-DD')
        vehicleSchedule.push(
          {
            date : formattedDate,
            price : Number(price)
          }
        )
        day.add(1, "day")
      }

      // console.log(vehicleSchedule)

      await addVehiclePrice ({carTypeProviderId: vehicle.value, carTypePricing : vehicleSchedule})
        .then((res) => {
          if (res.success) {
            handleClose()
            refreshData()
            notify(res.message || "Success Set Monthly Schedule", "success");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
      setLoading(false);
    } else {
      if (!merchant) {
        setErrorMerchant(true);
      }
      if (!vehicle) {
        setErrorVehicle(true);
      }
      if (!tanggal) {
        setErrorTanggal(true);
      }
      if (!price) {
        setErrorPrice(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  }

  return (
    <Dialog
      open={openModal}
      onClose={() => handleClose()}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}
      {...others}
    >
      <DialogTitle
        sx={{
          backgroundColor: backgroundColorTitle
        }}
      >
        Set Monthly Price
      </DialogTitle>

      <DialogContent>
        <Box
          sx={{
              display: "flex",
              flexDirection: "column",
              mt: "2rem",
              mb: '2rem',
              gap: 2
          }}>

            <SelectField
              label={"Merchant"}
              placeholder="Select Merchant"
              sx={{ width: "100%", fontSize: "16px" }}
              data={merchantOption}
              selectedValue={merchant}
              setValue={(newValue) => {
                setMerchant(newValue);
                setErrorMerchant(false);
              }}
              required={true}
              isError={errormerchant}
              errorMessage={"Merchant is required"}
            />

            <SelectField2
              label={"Vehicle"} 
              placeholder={merchant ? 'Select Vehicle' : "Please Select Merchant"} 
              sx={{ width: "100%", fontSize: "16px" }} 
              data={vehicleOption} 
              selectedValue={vehicle} 
              setValue={(newValue) => {
                setVehicle(newValue);
                setErrorVehicle(false);
              }} 
              inputValue={vehicleInput}
              setInputValue={(newValue) => {
                setVehicleInput(newValue);
              }}
              required
              isError={errorVehicle}
              errorMessage={"Vehicle is required"}
            />

            <DatePicker
              label={'Date'}
              placeholder="MMM YYYY"
              sx={{ width: "100%", fontSize: "16px"}}
              value={tanggal}
              onChange={(newValue) => {
                setTanggal(newValue);
              }}
              required
              format={"MMM YYYY"}
              views={['year', 'month']}
              isError={errorTanggal}
              errorMessage={"Date is required"}
            />

            <CustomPrice
              label={'Price'}
              selectedValue={price}
              setValue={setPrice}
              required
              errorMessage={'Price is Required'}
              isError={errorPrice}
              displayValue={displayPrice}
              setDisplayValue={setDisplayPrice}
            />

        </Box>
      </DialogContent>

      <DialogActions>
        <Stack direction="row" justifyContent="flex-end" gap={2}>
          <CustomButton
            onClick={() => handleClose()}
            startIcon={<KeyboardBackspaceIcon size="14px" />}
            name={"Back"}
            sx={{
              backgroundColor: "grey",
              ":hover": {
                backgroundColor: "#333333"
              }
            }}
          />
          <CustomButton
            onClick={() => handleSetSchedule()}
            startIcon={<SaveIcon size="14px" />}
            name={"Save"}
          />
        </Stack>
      </DialogActions>
    </Dialog>
  );
}

export default DialogMonthly;