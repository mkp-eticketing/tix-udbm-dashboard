import { Box, Stack, Dialog, DialogContent, DialogTitle, DialogActions} from "@mui/material";
import CustomPrice from './custom-fare-field'
import CustomButton from "../../../../components/custom-button";
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import SaveIcon from '@mui/icons-material/Save';
import DeleteIcon from '@mui/icons-material/Delete';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import React, { useState, useEffect } from 'react';
import { addVehiclePrice, editVehiclePrice, deleteVehiclePrice } from "../../../../services/car-rental/penjadwalan-tarif";
import { thousandSeparator } from "../../../../utils/thousand-separator";
import moment from "moment";
import EventBusyIcon from '@mui/icons-material/EventBusy';

const DialogTarif = ({
  openModal,
  setOpenModal,
  dataModal,
  setDataModal,
  fullWidth = true,
  maxWidth = 'sm',
  backgroundColorTitle = '#3875CA',  
  notify = () => {} ,
  setLoading = () => {},
  carTypeProvider = {},
  refreshData = () => {},
  ...others}
) => {
  const theme = useTheme()
  const [price, setPrice] = useState(0);
  const [errorPrice, setErrorPrice] = useState(false);
  const [displayPrice, setDisplayPrice] = useState(''); 

  const handleClose = () => {
    setOpenModal(false)
    setDataModal(null)
    setPrice(null)
    setDisplayPrice('')
    setErrorPrice(false)
  }

  useEffect(() => {
    if(dataModal){
      setPrice(dataModal.price)
      setDisplayPrice(thousandSeparator(dataModal.price))
    }
  }, [dataModal]);

  const handleSetPrice = async () => {
    if (price && carTypeProvider) {
      setLoading(true);
      if(dataModal.id){
        await editVehiclePrice(dataModal.id, {"price" : Number(price)})
          .then((res) => {
            if (res.success) {
              handleClose()
              refreshData()
              notify(res.message || "Success Set Price", "success");
            }
          })
          .catch((error) => {
            if (error.response) {
              // console.log(error.response.data.message);
              notify(error.response.data.message, "error");
            } else {
              // console.log(error.message || error);
              notify(error.message || error, "error");
            }
          });
      }
      else{
        const vehiclePrice = [{
          "date" : dataModal.date,
          "price" : Number(price)
        }]
        await addVehiclePrice({carTypeProviderId: carTypeProvider.id, carTypePricing : vehiclePrice})
          .then((res) => {
            if (res.success) {
              refreshData()
              handleClose()
              notify(res.message || "Success Set Price", "success");
            }
          })
          .catch((error) => {
            if (error.response) {
              // console.log(error.response.data.message);
              notify(error.response.data.message, "error");
            } else {
              // console.log(error.message || error);
              notify(error.message || error, "error");
            }
          });
      }
      setLoading(false);
    } else {
      if (!price) {
        setErrorPrice(true);
      }
      notify("Fill The Required Filter", "warning");
    }
  }

  const handleDeleteprice = async () => {
    if(dataModal.id){
      await deleteVehiclePrice(dataModal.id)
        .then((res) => {
          if (res.success) {
            refreshData()
            handleClose()
            notify(res.message || "Success Delete Price", "success");
          }
        })
        .catch((error) => {
          if (error.response) {
            // console.log(error.response.data.message);
            notify(error.response.data.message, "error");
          } else {
            // console.log(error.message || error);
            notify(error.message || error, "error");
          }
        });
    }
  }

  return (
    <Dialog
      open={openModal}
      onClose={() => handleClose()}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}
      {...others}
    >
      <DialogTitle
        sx={{
          backgroundColor: backgroundColorTitle
        }}
      >
        Set Pricing - {dataModal && moment(dataModal.date).format('DD MMMM YYYY')}
      </DialogTitle>

      <DialogContent>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            mt: "2rem",
            gap: 2
          }}
        >
            <CustomPrice
              label={'Price'}
              selectedValue={price}
              setValue={setPrice}
              required
              errorMessage={'Price is Required'}
              isError={errorPrice}
              displayValue={displayPrice}
              setDisplayValue={setDisplayPrice}
            />
        </Box>
      </DialogContent>

      <DialogActions>
        <Stack direction="row" justifyContent="flex-end" gap={2} mt={8}>
          <CustomButton
            onClick={() => handleClose()}
            startIcon={<KeyboardBackspaceIcon size="14px" />}
            name={"Back"}
            sx={{
              backgroundColor: "grey",
              ":hover": {
                backgroundColor: "#333333"
              }
            }}
          />
          {(dataModal && dataModal.id) &&
            <CustomButton
              onClick={() => handleDeleteprice()}
              startIcon={<EventBusyIcon size="14px" />}
              name={"Not Available"}
              sx={{
                backgroundColor: "error.main",
                ":hover": {
                  backgroundColor: "error.dark"
                }
              }}
            />
          }
          <CustomButton
            onClick={() => handleSetPrice()}
            startIcon={<SaveIcon size="14px" />}
            name={"Save"}
          />
        </Stack>
      </DialogActions>
    </Dialog>
  );
}

export default DialogTarif;