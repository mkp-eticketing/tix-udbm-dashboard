import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official';
import React from 'react';
import { formatCurrency } from '../utils/format-currency';
require('highcharts/indicators/indicators')(Highcharts)
require('highcharts/indicators/pivot-points')(Highcharts)
require('highcharts/indicators/macd')(Highcharts)
require('highcharts/modules/exporting')(Highcharts)
require('highcharts/modules/map')(Highcharts)

const GrafikColumnRevenueKendaraan = ({ category, data, yAxis = "Omset", xAxis = "Harian" }) => {
    Highcharts.setOptions({
        lang: {
            thousandsSep: '.'
        }
    })
    const options = {
        chart: {
            type: 'column'
        },
        title: {
            text: '',
            align: 'left'
        },
        subtitle: {
            text: '',
            align: 'left'
        },
        xAxis: {
            categories: category,
            crosshair: true,
            accessibility: {
                description: 'Countries'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxis
            }
        },
        tooltip: {
            valuePrefix: "Rp",
            valueSuffix: ',00',
            formatter: function () {
                return `<p style="font-size:10px;">${this.x}</p></b><br/>${this.series.name}:<b> ${formatCurrency(this.y)} </b>`;
            }
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                color: "#5859D4"
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            {
                name: xAxis,
                data: data
            }
        ],
        legend: {
            enabled: false
        }
    };
    return (
        <HighchartsReact
            highcharts={Highcharts}
            constructorType={'chart'}
            options={options}
            containerProps={{ style: { height: "520px" } }}
        />
    )
}

export default GrafikColumnRevenueKendaraan;