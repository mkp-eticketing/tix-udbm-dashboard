import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official';
import React from 'react';
require('highcharts/indicators/indicators')(Highcharts)
require('highcharts/indicators/pivot-points')(Highcharts)
require('highcharts/indicators/macd')(Highcharts)
require('highcharts/modules/exporting')(Highcharts)
require('highcharts/modules/map')(Highcharts)

const GrafikAreaRevenueKendaraan = ({ category, data, yAxis = "Omset", seriesName = "Tanggal", type = "revenue" }) => {
    Highcharts.setOptions({
        lang: {
            thousandsSep: '.'
        }
    })
    const options = {
        chart: {
            type: 'areaspline'
        },
        accessibility: {
            description: 'Image description: An area chart compares the nuclear stockpiles of the USA and the USSR/Russia between 1945 and 2017. The number of nuclear weapons is plotted on the Y-axis and the years on the X-axis. The chart is interactive, and the year-on-year stockpile levels can be traced for each country. The US has a stockpile of 6 nuclear weapons at the dawn of the nuclear age in 1945. This number has gradually increased to 369 by 1950 when the USSR enters the arms race with 6 weapons. At this point, the US starts to rapidly build its stockpile culminating in 32,040 warheads by 1966 compared to the USSR’s 7,089. From this peak in 1966, the US stockpile gradually decreases as the USSR’s stockpile expands. By 1978 the USSR has closed the nuclear gap at 25,393. The USSR stockpile continues to grow until it reaches a peak of 45,000 in 1986 compared to the US arsenal of 24,401. From 1986, the nuclear stockpiles of both countries start to fall. By 2000, the numbers have fallen to 10,577 and 21,000 for the US and Russia, respectively. The decreases continue until 2017 at which point the US holds 4,018 weapons compared to Russia’s 4,500.'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: category,
            label: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                text: yAxis
            },
            stackLabels: {
                enabled: true,
                format: type === "revenue" ? 'Rp' + '{point.y}' + ',00' : '{point.y}'
            },
        },
        tooltip: {
            headerFormat: "",
            pointFormat: type === "revenue" ? 'Rp' + '{point.y}' + ',00' : '{point.y}'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false,
                },
                color: "#5859D4",
            },
            area: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: seriesName,
            data: data,
            marker: {
                enable: false
            },
            color: {
                linearGradient: {
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, '#EBEBFA'],
                    [1, '#5859D4']
                ]
            }
        }],
        legend: {
            enabled: false
        }
    };
    return (
        <HighchartsReact
            highcharts={Highcharts}
            constructorType={'chart'}
            options={options}
            containerProps={{ style: { height: "260px" } }}
        />
    )
}

export default GrafikAreaRevenueKendaraan;